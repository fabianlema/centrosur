




<Module>
 <ModulePrefs title="Agent Chat Interface"
              author="Cisco"
              author_email="help@cisco.com"
              scrolling="true"
              height="700">
   <Require feature="dynamic-height"/>
   <Require feature="settitle"/>
   <Require feature="minimessage"/>
   <Require feature="setprefs"/>
   <Locale messages='/messages/en_ALL.xml'/>
<Locale lang='en' country='ALL' messages='/messages/en_ALL.xml'/>
  
 </ModulePrefs>
 <Content type="html">
   <![CDATA[
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<title>__MSG_chat_heading__</title>
	<link rel="stylesheet" type="text/css" href="/css/ccp.css"  />
	<link rel="stylesheet" type="text/css" href="./css/reply.css" />
	
	<!-- BEGINNING OF vars.jsp -->

<script type="text/javascript">
/**
* This include defines a set of commonly used server-related variables in the global ccp.vars object.
*/



var ccp=(typeof ccp=="undefined")?{}:ccp;
ccp.vars=ccp.vars||{};
ccp.constants=ccp.constants||{};
ccp.paths=ccp.paths||{};

(function(){
  var prefs = (typeof gadgets!=="undefined"&&typeof gadgets.Prefs==="function")?new gadgets.Prefs():null;
  var userPref="CCPUser";
  var tokenPref="CCPToken";
  var tokenCookie="JSESSIONIDSSO";
  var getCookie=function(name){
	startIndex=document.cookie.indexOf(name+"=");
	if (startIndex>=0)
	{
		searchStr=document.cookie.substring(startIndex);
		endIndex=searchStr.indexOf(';')>=0?searchStr.indexOf(';'):searchStr.length;
		val=document.cookie.substring(startIndex+name.length+1,endIndex+startIndex);
		return val;
	}
  };
  var setCookie=function(name,value,sessionOnly) {
	var cookieStr=key+'='+value+'; path=/';
	if (!sessionOnly) {
		d=new Date();
		d.setYear(d.getYear()+1901);
		cookieStr=cookieStr+'; expires='+d.toUTCString();
	}
	document.cookie=cookieStr;
  }
  var getUsername=function(){
	var userName=prefs!=null?prefs.getString(userPref):null;
	if(!userName) {
		userName=getCookie(userPref);
	}
	return userName;
  };
  var getToken=function(){
	var token=prefs!=null?prefs.getString(tokenPref):null;
	if(!token) {
		token=getCookie(tokenCookie);
	}
	return token;
  };
  ccp.vars.setUsername=function(username){
	setCookie(userPref,username);
  };
  /** 
  * Determine the address of the CCP server
  * "imgbase" is created as self contained variable so that changes in "serverBase" will not impact it
  **/
  ccp.vars.webappbase = "http://csm10tt.taurus.pri/ccp-webapp/";
  ccp.vars.webappbasehttps = "https://csm10tt.taurus.pri/ccp-webapp/";
  ccp.vars.serverName = "csm10tt.taurus.pri";
  ccp.vars.serverBase = "https://" + ccp.vars.serverName;
  ccp.vars.apibase = ccp.vars.webappbase + "ccp/";
  ccp.vars.imgbase = "https://" + ccp.vars.serverName + "/img/";
  ccp.vars.helpbase = prefs!=null?ccp.vars.serverBase + "/help/"+prefs.getLang() + "_" + prefs.getCountry() + "/":ccp.vars.serverBase + "/help/en_US/";
  ccp.vars.helpbase = ccp.vars.helpbase.replace("en_US","en_ALL"); // Gadget prefs may return en_US instead of en_ALL
  ccp.vars.buildNumber = "10.6.1.1";
  ccp.vars.chatapibase = ccp.vars.serverBase + "/ccp/chat/";
  ccp.vars.username=getUsername();
  ccp.vars.token=getToken();
  
  ccp.constants.extensionFieldURLPrefix="ef_";
  ccp.constants.extensionFieldPrefKey="extensionFields";
  ccp.constants.extensionFieldMaskPrefix="h_";

  ccp.paths.AUTHENTICATION_PING_PATH = ccp.vars.apibase + "authentication/ping";
  
  ccp.constants.timeFormats={en:"h:mm a"};
  ccp.constants.dateFormats={en:"M/d/yy"};

})();


</script>

<!-- END OF vars.jsp -->

	

<script type="text/javascript" src="http://csm10tt.taurus.pri/js/3rdparty/require.js" timeout-msg="A timeout occurred loading the application.  Refresh your browser or try again later.  If the problem persists, you may need to check your network connectivity."></script>

<script>


require.onError=function(error) {
  window._loadErrors=window._loadErrors||[];
  window._loadErrors.push(error);
  var log=typeof gadgets!=="undefined"?function(msg){gadgets.log(new Date().getTime()+": "+msg);}:(typeof console!=="undefined"?function(msg){console.log(new Date().getTime()+": "+msg);}:function(){});

  var msg;
  var scripts = document.getElementsByTagName("script");
  for(var i=0;i<scripts.length;i++) {
    if((msg=scripts[i].getAttribute("timeout-msg")))
      break;
  }
  
  log(error);
  var m=document.getElementById("messages");
  if(m && msg) {
    // Replace the messages element entirely to prevent any other messages from being shown
    m.outerHTML="<table class='error'><tr><td>"+msg+"</td></tr></table>";
  }
};

require.config({
	  baseUrl: ccp.vars.serverBase+"/js",
	  shim: {
	  
		// CCP legacy scripts
		"ccp-utils": { deps:["jquery", "jquery-cookie","jquery-datatables"], exports: "hideLoadingIndicator" },
		"ccp-utils-transferbox": { deps:["jquery"], exports: "jQuery.fn.transferbox" },
		"admin": { exports: "initAdmin" },
		"template": { deps: ["ccp-utils","jquery-datatables","sarissa"], exports: "saveTemplate" },
		"notification": { deps: ["ccp-utils","jquery-datatables","sarissa"], exports: "summaryRequest" },
		"campaign": { deps: ["ccp-utils","jquery-datatables","sarissa"], exports: "summaryRequest" },

		// Reply templates
		"rt-base": { exports: "ccp.base" },
		"rt-facebook": { deps: ["rt-base"], exports: "ccp.facebook" },
		"rt-twitter": { deps: ["rt-base"], exports: "ccp.twitter.TwitterSession" },
		
		// Eventing
		"eventing-converter": { exports: "ccp.Converter" },
		"eventing-utilities": { deps: ["eventing-converter"], exports: "ccp.clientservices.Utilities" },
		"eventing-clientservices": { deps: ["eventing-utilities"], exports: "ccp.clientservices.ClientServices" },
		"eventing-mastertunnel": { deps: ["eventing-clientservices"], exports: "ccp.clientservices.MasterTunnel" },
		
		// jQuery
		"jquery-ui": { deps: ["jquery"], exports: "jQuery.ui" },
		"jquery-cookie": { deps: ["jquery"], exports: "jQuery.cookie" },
		"jquery-base64": { deps: ["jquery"], exports: "jQuery.base64Encode" },
		"jquery-datatables": { deps: ["jquery"], exports: "jQuery" },
		"jquery-splitter": { deps: ["jquery"], exports: "jQuery.fn.splitter" },
		
		// Flot
		"jquery-flot-excanvas": { deps: ["jquery"], exports: "jQuery" },
		"jquery-flot": { deps: ["jquery","jquery-flot-excanvas"], exports: "jQuery.plot" },
		"jquery-flot-image": { deps: ["jquery-flot"], exports: "jQuery.plot.image" },
		
		// Misc.
		"backbone": { deps: ["jquery", "underscore"], exports: "Backbone" },
		"underscore": { exports: "_" },
		"date": { exports: "Date.i18n" },
		"sarissa": { exports: "Sarissa" },
		"parseuri": { exports: "parseUri" },
		"handlebars": { exports: "handlebars" },
		"codemirror": { exports: "CodeMirror" },
		"clike": { deps: ["codemirror"], exports: "CodeMirror" }
	  },
	  
	  paths: {
		// CCP legacy scripts
		"ccp-utils": ccp.vars.serverBase+"/js/ccp-utils",
		"ccp-utils-transferbox": ccp.vars.serverBase+"/js/ccp-utils-transferbox",
		"admin": ccp.vars.serverBase+"/js/admin",
		"template": ccp.vars.serverBase+"/js/template",
		"notification": ccp.vars.serverBase+"/js/notification",
		"campaign": ccp.vars.serverBase+"/js/campaign",
		
		// Reply templates
		"rt-base": ccp.vars.serverBase+"/templates/reply/js/ccp-base",
		"rt-facebook": ccp.vars.serverBase+"/templates/reply/js/ccp-facebook",
		"rt-twitter": ccp.vars.serverBase+"/templates/reply/js/ccp-twitter",

		// Eventing
		"eventing-converter": ccp.vars.serverBase+"/js/ccp/events/converter",
		"eventing-utilities": ccp.vars.serverBase+"/js/ccp/events/clientservices/Utilities",
		"eventing-clientservices": ccp.vars.serverBase+"/js/ccp/events/clientservices/ClientServices",
		"eventing-mastertunnel": ccp.vars.serverBase+"/js/ccp/events/clientservices/MasterTunnel",

		// Flot
		"jquery-flot-excanvas": ccp.vars.serverBase+"/js/3rdparty/flot/excanvas",
		"jquery-flot": ccp.vars.serverBase+"/js/3rdparty/flot/jquery.flot",
		"jquery-flot-image": ccp.vars.serverBase+"/js/3rdparty/flot/jquery.flot.image",
		
		// jQuery
		"jquery-ui": ccp.vars.serverBase+"/js/3rdparty/jquery-ui.custom.min",
		"jquery-cookie": ccp.vars.serverBase+"/js/3rdparty/jquery.cookie",
		"jquery-base64": ccp.vars.serverBase+"/js/3rdparty/jquery.base64",
		"jquery-datatables": ccp.vars.serverBase+"/js/3rdparty/jquery.dataTables",
		"jquery-splitter": ccp.vars.serverBase+"/js/3rdparty/jquery.splitter",

		// Misc.
 		"backbone": ccp.vars.serverBase+"/js/3rdparty/backbone",
 		"underscore": ccp.vars.serverBase+"/js/3rdparty/underscore",
		"date": ccp.vars.serverBase+"/js/3rdparty/Date",
		"sarissa": ccp.vars.serverBase+"/js/3rdparty/sarissa/sarissa-full-0.9.9.4/gr/abiss/js/sarissa/sarissa",
		"parseuri": ccp.vars.serverBase+"/js/3rdparty/parseuri",
		"handlebars": ccp.vars.serverBase+"/js/3rdparty/handlebars",
		"codemirror": ccp.vars.serverBase+"/js/3rdparty/CodeMirror/lib/codemirror",
		"clike": ccp.vars.serverBase+"/js/3rdparty/CodeMirror/mode/clike/clike"
	  },
	waitSeconds: 15,
	enforceDefine: true,
	urlArgs: "bust=" +  ccp.vars.buildNumber, // Comment out if using urlArgs below
//	urlArgs: "bust=" +  (new Date()).getTime(), // Optional but useful to force JS reloading during development

    config: {
        text: {

            //
            // The set of methods in this namespace override methods in
            // text.js. This allows SocialMiner to use gadgets.io to make
            // cross-origin requests to get the text files that store
            // templates.
            //

            /**
             * Override this method to always return true. This ensures
             * createXhr() is called below.
             */
            useXhr: function (url, protocol, hostname, port)
            {
                return (typeof gadgets !== "undefined") && gadgets.io && gadgets.io.makeRequest;
            },

            /**
             * Override createXhr() to create a fake XMLHttpRequest object.
             * This object uses gadgets.io so that croos-domain requests can
             * be made.
             */
            createXhr: function ()
            {
                //On IE "native XMLHTTP support can be disabled". Below is the work around
                if (typeof XMLHttpRequest === "undefined" || !window.XMLHttpRequest) {
				  XMLHttpRequest = function () {
				    try { return new ActiveXObject("Msxml2.XMLHTTP.6.0"); }
				    catch (e) {}
				    try { return new ActiveXObject("Msxml2.XMLHTTP.3.0"); }
				    catch (e) {}
				    try { return new ActiveXObject("Microsoft.XMLHTTP"); }
				    catch (e) {}
				    // Microsoft.XMLHTTP points to Msxml2.XMLHTTP and is redundant
				    throw new Error("This browser does not support XMLHttpRequest.");
				  };
				}
				var xhr = new XMLHttpRequest();
				
                if ( (typeof gadgets !== "undefined") && gadgets.io && gadgets.io.makeRequest )
                {
                    xhr = new Object();
                    xhr.open = function(method, url, async)
                    {
                        this.method = method;
                        this.url = url.replace("https://", "http://");
                        this.async = async;
                    };

                    xhr.onreadystatechange = function(evt)
                    {
                    };

                    xhr.send = function(data)
                    {
                        var params={};
                        params[gadgets.io.RequestParameters.CONTENT_TYPE] = "TEXT";
                        var cb=function(resp)
                        {
                            xhr.readyState = 4;
                            xhr.status = resp.status;
                            xhr.responseText = resp.text;
                            xhr.onreadystatechange(new Object());
                        };
                        gadgets.io.makeRequest(this.url, cb, params);
                    };
                }

                return xhr;
            }
        }
    }
});

</script>
	


<script type="text/javascript">
require(['jquery'], function($) {
  $(document).ready(function(){
    $(document).keydown(function(e){
      if(e.keyCode==27) {
	console.log("Trapping ESC in "+window.location.href);
	return false;
      }
    });
  });
});
</script>
	
<script>
	var CHAT_MESSAGE_MAXLENGTH=10240;
	// Add agent-chat and api to existing RequireJS config
	require.config({
		paths:{
			"agent-chat":"http://csm10tt.taurus.pri/templates/reply/js/agent-chat",
			"api":"http://csm10tt.taurus.pri/templates/reply/js/api"
		}
	});
	
	//Load jQuery and underscore, then load the application
	require(['backbone','jquery','ccp/jquery.placeholder','jquery-cookie'], function(bb,$) {

		$(document).ready(function() {
		
			//Make sure the chat text area doesn't accept more than maxlength number of characters when pasting (for IE only)
			//we compare size of clipboard plus chatbox content with maxlength and truncate pasting text
			$('#chat-box').on('paste', function() {							
				if (window.clipboardData){
					var val;
					val = window.clipboardData.getData('Text');	
					var chatBoxContent = $(this).val();
					if ((val.length+chatBoxContent.length) > CHAT_MESSAGE_MAXLENGTH)
						{	
							var newString=chatBoxContent+val.slice(0,CHAT_MESSAGE_MAXLENGTH-chatBoxContent.length);
							$(this).val(newString);
							alert("__MSG_chat_max_length__");
							return false;
						}
				  }
			});
			// Make sure the chat text area doesn't accept more than maxlength number of characters
			$('#chat-box').live('keyup blur', function() {
					var val = $(this).val();
					if (val.length > CHAT_MESSAGE_MAXLENGTH)
					{
						$(this).val(val.slice(0, CHAT_MESSAGE_MAXLENGTH));
						alert("__MSG_chat_max_length__");
					}
			});

			var AGENT_CHAT_FONT_COOKIE_NAME = "agent-chat-font-size";
			var setChatFontSizeFromCookie = function() {
				var lastSetFontSize = $.cookie(AGENT_CHAT_FONT_COOKIE_NAME);
				if(lastSetFontSize != null)
				{
					setFontSize("#chat-history", lastSetFontSize);
					setFontSize("#chat-box", lastSetFontSize);
					setFontSize("#customer-detail", lastSetFontSize);
				}
				else
					resetCookieChatFontSize();
			};
			
			var setFontSize = function(jQueryElement, size) {
				$(jQueryElement).css("font-size", size + "px");
			};
			
			var resetCookieChatFontSize = function() {
				var options={};
				options.path="/";
				var d=new Date();
				d.setYear(d.getYear()+1901);
				options.expires=d;
				$.cookie(AGENT_CHAT_FONT_COOKIE_NAME, getFontSize("#chat-history"), options);
			};
			
			var getFontSize = function(jQueryElement) {
				return parseFloat($(jQueryElement).css("font-size"), 10);
			};
			
			var onFontSizeUpClick = function() {
				resizeFont("#chat-history", 1);
				resizeFont("#chat-box", 1);
				resizeFont("#customer-detail", 1);
				resetCookieChatFontSize();
			};
			
			var onFontSizeDownClick = function() {
				resizeFont("#chat-history", -1);
				resizeFont("#chat-box", -1);
				resizeFont("#customer-detail", -1);
				resetCookieChatFontSize();
			};
			
			var resizeFont = function(jQueryElement, step) {
				var fontSizeNow = parseFloat($(jQueryElement).css("font-size"), 10);
				var fontSizeNew = (fontSizeNow + step) + "px";
				$(jQueryElement).css("font-size", fontSizeNew);
			};

			//Set Chat area Font Size
			setChatFontSizeFromCookie();
			
			$("#fontSizeDown").click(function() {
				onFontSizeDownClick();
			});
			
			$("#fontSizeUp").click(function() {
				onFontSizeUpClick();
			});
			
			// Adding Hot Keys for buttons
            $(document).keydown( function(event){
                    if(event.ctrlKey && event.shiftKey) {
                            var code = (event.keyCode ? event.keyCode : event.which);
                            //End Button : Hotkey CTRL + SHIFT + E
                            if(code == 101 || code == 69 ){
                                    $("#endButton").trigger("click");
                                    event.preventDefault();
                            }
                            //FONT - : Hotkey CTRL + SHIFT + [
                            if(code ==  219){
                                    onFontSizeDownClick();
                                    event.preventDefault();
                            }
                            //FONT + : Hotkey CTRL + SHIFT + ]
                            if(code == 221){
                                    onFontSizeUpClick();
                                    event.preventDefault();
                            }
                    }
            });
				

			$("#chat-box").placeholder();
	});
		require(['agent-chat','ccp/signin'],function(chat,signin){
			if(!window._loadErrors)
				signin.init({success:function(){chat.init(window.location.href)}});
		});
	});
</script>

<style type="text/css">

.chat-header {
	overflow: hidden;
	overflow-y: hidden;
	text-overflow:ellipsis;
	height: 30px;
	padding-bottom:8px;
}
.chat-footer {
	height: 40px;
}
#chat-chrome {
	width:100%;
}
#chat-history {
	overflow-x:hidden;
	overflow-y:auto;
	background-color:#DAE3E8;
}

textarea#chat-box {
	width:100%;
	height:100%;
	resize:none;
	overflow:auto;
	font-size:12px;
	border:1px solid #AEB4BB;
	padding:0;
	margin:0;
	box-sizing:border-box;
	-moz-box-sizing: border-box;
}
#customer-detail {
	padding:3px;
	overflow:auto;
}
.user-message-bubble {
	width:95%;
	background-color:#ffffff;
	margin:3px 5px 3px 8px;
	border-radius:3px;
    -webkit-box-shadow:0 0 2px 2px #AEB4BB;
    -moz-box-shadow:0 0 2px 2px #AEB4BB;
	box-shadow:0 0 2px 2px #AEB4BB;
	word-wrap: break-word;
	overflow-y:hidden;
}
.chat-agent-username {
	font-weight:bold;
	font-size:90%;
	color:#000000;
	white-space:nowrap;
	overflow:hidden;
	text-overflow:ellipsis;
	padding:1px 3px 1px 3px;
}
.chat-customer-username {
	font-weight:bold;
	font-size:90%;
	white-space:nowrap;
	overflow:hidden;
	text-overflow:ellipsis;
	padding:1px 3px 1px 3px;
	color:#0088C2;
}
.user-message-bubble:after {
   content: ".";
   visibility: hidden;
   display: block;
   height: 0;
   clear: both;
}
.timestamp {
	color:#A9A9A9;
	font-size:75%;
	float:right;
	white-space:nowrap;
	text-align:right;
	padding:0px 3px 0px 2px;
	whitespace:nowrap;
}
.chat-timer {
	vertical-align:middle;
	float:left;
}
.chat-toolbar {
	float:right;
	padding-right: 1px;
}
.single-message {
	margin:3px 3px 3px 3px;
}
#right-column {
	min-width:150px;
	overflow-y:hidden;
}
#left-column {
	min-width:150px;
}
.row-three {
	margin:5px 0px 0px 0px;
}
.splitter {
	height: 100%;
	width: 100%;
	padding:5px;
	box-sizing:border-box;
	-moz-box-sizing: border-box;
	visibility:hidden;
}
.vsplitbar {
	width: 5px;
	background: url(../../img/vgrabber.gif) no-repeat center;
}
.vsplitbar.active {
	background: #aaa url(../../img/vgrabber.gif) no-repeat center;
	opacity: 0.7;
}
#customer-tab {
	overflow-x: hidden;
	overflow-y: hidden;
	text-overflow:ellipsis;
	font-weight: bold;
	max-width:80%;;
	float: left;
	border-top-right-radius: 4px;
	border-top-left-radius: 4px;
	border:1px solid #AEB4BB;
}	

#authorParagraph {
overflow: hidden;
text-overflow:ellipsis;
max-width:100%;
padding: 0px 10px 0px 10px;
white-space:nowrap;
}
.body-style {
	height:97%;
	width:98%;
	vertical-align:top;
	border:1px solid #AEB4BB;
	padding:5px 5px 0px 5px;
}
/* Used for the End button */
.redButton {
      -moz-border-radius: 4px;
      border-radius: 4px;
      -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);
      -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);
      box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);
      border: 1px solid #A51901;
      padding: 2px 4px 2px 4px;
	  background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2YxNGQyNiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNhNTE5MDEiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
	  background: -moz-linear-gradient(top, #f14d26 0%, #a51901 100%); /* FF3.6+ */
	  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f14d26), color-stop(100%,#a51901)); /* Chrome,Safari4+ */
	  background: -webkit-linear-gradient(top, #f14d26 0%,#a51901 100%); /* Chrome10+,Safari5.1+ */
	  background: -o-linear-gradient(top, #f14d26 0%,#a51901 100%); /* Opera 11.10+ */
	  background: -ms-linear-gradient(top, #f14d26 0%,#a51901 100%); /* IE10+ */
	  background: linear-gradient(top, #f14d26 0%,#a51901 100%); /* W3C */
	  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f14d26', endColorstr='#a51901',GradientType=0 ); /* IE6-8 */
      color: #FFF;
	  text-align: center;
      font: 9pt Arial;
      cursor: pointer;
}
.redButton:hover {
      border: 1px solid #A51901;
	  background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2YzYzViZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjElIiBzdG9wLWNvbG9yPSIjZWEyODAzIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMiUiIHN0b3AtY29sb3I9IiNmZjY2MDAiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSI1MCUiIHN0b3AtY29sb3I9IiNlODZjNTciIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSI5OSUiIHN0b3AtY29sb3I9IiNmM2M1YmQiIHN0b3Atb3BhY2l0eT0iMSIvPgogICAgPHN0b3Agb2Zmc2V0PSIxMDAlIiBzdG9wLWNvbG9yPSIjYzcyMjAwIiBzdG9wLW9wYWNpdHk9IjEiLz4KICA8L2xpbmVhckdyYWRpZW50PgogIDxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxIiBoZWlnaHQ9IjEiIGZpbGw9InVybCgjZ3JhZC11Y2dnLWdlbmVyYXRlZCkiIC8+Cjwvc3ZnPg==);
	  background: -moz-linear-gradient(top, #f3c5bd 0%, #ea2803 1%, #ff6600 2%, #e86c57 50%, #f3c5bd 99%, #c72200 100%); /* FF3.6+ */
	  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f3c5bd), color-stop(1%,#ea2803), color-stop(2%,#ff6600), color-stop(50%,#e86c57), color-stop(99%,#f3c5bd), color-stop(100%,#c72200)); /* Chrome,Safari4+ */
	  background: -webkit-linear-gradient(top, #f3c5bd 0%,#ea2803 1%,#ff6600 2%,#e86c57 50%,#f3c5bd 99%,#c72200 100%); /* Chrome10+,Safari5.1+ */
	  background: -o-linear-gradient(top, #f3c5bd 0%,#ea2803 1%,#ff6600 2%,#e86c57 50%,#f3c5bd 99%,#c72200 100%); /* Opera 11.10+ */
	  background: -ms-linear-gradient(top, #f3c5bd 0%,#ea2803 1%,#ff6600 2%,#e86c57 50%,#f3c5bd 99%,#c72200 100%); /* IE10+ */
	  background: linear-gradient(top, #f3c5bd 0%,#ea2803 1%,#ff6600 2%,#e86c57 50%,#f3c5bd 99%,#c72200 100%); /* W3C */
	  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f3c5bd', endColorstr='#c72200',GradientType=0 ); /* IE6-8 */
      -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);
      -moz-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);
      box-shadow: 0 1px 1px rgba(0, 0, 0, 0.15);
}
#icons{
	border:0;
	padding:0;
	margin:0;
	overflow: hidden;
}

#icons button{
	float: right;
	border-style:none;
	background-color: #FFF;
	width: 30px;
	height: 35px;
	padding:0;
}

#icons:after {
   content: ".";
   visibility: hidden;
   display: block;
   height: 0;
   clear: both;
}

.placeholder {
  text-color: #AAA;
}
</style>

<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->

</head>
<body class="body-style">
<div id="messages"></div>
<div class="splitter">
  <div id="left-column" style="position:relative">
          <div class="row-one chat-header" tabindex="-1">
                  <div id="customer-tab" tabindex="1">
                  </div>
          </div>
          <div id="row-two-l" class="row-two" style="border:1px solid #AEB4BB" >
                  <div id="customer-detail" >
                  </div>
          </div>
  </div>

  <div id="right-column">
          <div class="row-one chat-header">
                  <div id="chat-chrome">
                          <span class="chat-timer" tabindex="2">00:00</span>
                          <span class="chat-toolbar"><button id="endButton" type="button" title="__MSG_end_chat__" tabindex="3" class="redButton" disabled="true"><img style="vertical-align:top" src="../../img/Chat_white_12.png" alt="__MSG_end_chat__">&nbsp;__MSG_end__</button></span>
                  </div>
          </div>
          <div id="row-two-r" class="row-two" style="border:1px solid #AEB4BB">
		<div id="chat-history"  aria-live="polite">
		</div>
          </div>
          <div id="icons" class="icon-row" style="height:31px">
          		<img id='tunnelStatus' src='/img/secure_gray.png' title='__MSG_eventing_disconnected__' style='padding-top:19px'/>
                <button id="fontSizeDown" type="button" title="__MSG_chat_decrease_font_size__" tabindex="8"><img src="../../img/font_decrease_24.png" alt="__MSG_chat_decrease_font_size__"/></button>
                <button id="fontSizeUp" type="button" title="__MSG_chat_increase_font_size__" tabindex="7"><img src="../../img/font_increase_24.png" alt="__MSG_chat_increase_font_size__"/></button>
          </div>
          <div id="row-three-r" class="row-three chat-footer" style="height:66px">
                <textarea title="__MSG_connecting_to_chat__" id="chat-box" placeholder="" readonly="true" tabindex="9"  aria-live="polite"></textarea>
          </div>
  </div>
</div>


<script id="contact-template" type="text/template">
        <p tabindex="4">__MSG_customer_name__:&nbsp;{{author}}</p>
        {{#each extensions}}
        <p class="extension-field" tabindex="4"><span class="ef-name">{{name}}</span>:&nbsp;<span class="ef-value">{{value}}</span></p>
        {{/each}}
</script>


<script id="contact-name-template" type="text/template">
        <p id="authorParagraph" tabindex="-1">{{author}}</p>
</script>


<script id="agent-message-template" type="text/template">
<div class="user-message-bubble" tabindex="-1">
        <div class="user-message-layout"  aria-live="polite">
                <div class="chat-agent-username chat-username" tabindex="6">
                        {{user}}
                </div>
        </div>
</div>
</script>


<script id="customer-message-template" type="text/template">
<div class="user-message-bubble" tabindex="-1">
        <div class="user-message-layout">
                <div class="chat-customer-username chat-username" tabindex="6">
                        {{user}}
                </div>
        </div>
</div>
</script>


<script id="single-message-template" type="text/template">
        <div class="single-message"  aria-live="polite">
                <div class="timestamp" tabindex="6">{{timestamp}}</div>
                <div id="single_msgtext_{{id}}" class="message-text" tabindex="6"  aria-live="polite">{{message}}</div>
        </div>
</script>



<script id="link-template" type="text/template">
<a target="_blank" href="{{url}}" tabindex="6">{{text}}</a>
</script>


<script id="email-template" type="text/template">
<a href="{{url}}" tabindex="6">{{text}}</a>
</script>

</body>
</html>
  ]]>
  </Content>
</Module>