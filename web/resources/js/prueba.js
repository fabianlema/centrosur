

$(function dibujarMio() {
  var chart;
  var var1 = new Array();
  var var2 = new Array(30,20,10);
  var1 = document.getElementById('formulario:variable1').value;
  alert(var1);
  //var2 = document.getElementById('formulario:variable2').value;
  
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'prueba',
                type: 'line',
                marginRight: 130,
                marginBottom: 25
            },
            title: {
                text: 'Monthly Average Temperature',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: WorldClimate.com',
                x: -20
            },
            xAxis: {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y +'°C';
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
            series: [{
                name: 'Agente 1',
                data: var1
            }, {
                name: 'Agente 2',
                data: var2
            }]
        });
    });
    
});