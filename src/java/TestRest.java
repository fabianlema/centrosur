
import ec.com.centrosurcall.datos.conexion.Consultas;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Fabian
 */
public class TestRest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TestRest a = new TestRest();
        Consultas consulta = new Consultas();
        try
        {
            ResultSet result = consulta.getSPNew("3", "2016-02-12", "2016-08-01");
            ResultSetMetaData columnas = result.getMetaData();
            int columnCount = columnas.getColumnCount();
            String cadena = "";
            for (int i = 1; i <= columnCount; i++ ) {
                String name = columnas.getColumnName(i);
                cadena += name+"--";
              }            
            System.out.println(cadena);
            if (result != null){
                while (result.next()) {
                    cadena = "";
                    for (int i = 1; i <= columnCount; i++ ) {
                        String dato = result.getString(i);
                        cadena += dato+"--";
                      }
                    System.out.println(cadena);
                }                
            }
        }catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }
}
