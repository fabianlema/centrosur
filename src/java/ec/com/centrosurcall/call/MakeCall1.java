/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.call;
//package jtapivv;

/*
 * To change this template, choose Tools | Templates*
 *
 * @author TaurusTech
 */

import javax.telephony.*;
import javax.telephony.events.*;
import com.cisco.cti.util.Condition;

public final class MakeCall1 {    
                String hostname;
		String login;
		String passwd;
		String src;
		String dst;
                Connection conn[];
                Provider provider;
                JtapiPeer peer;
                String providerString;

    public MakeCall1() {
        
    }
    
    //Inicia la conexion 
    public MakeCall1(String host, String loginUser, String pswd) 
    {
        try{      
                hostname = host;
		login = loginUser;
		passwd = pswd;
                /* start up JTAPI */
		peer = JtapiPeerFactory.getJtapiPeer(null);
                //System.out.println("1");
 		/* connect to the provider */
		providerString = hostname;
		providerString += ";login=" + login;
		providerString += ";passwd=" + passwd;
		provider = peer.getProvider(providerString);
		/* wait for it to come into service */
		//System.out.println("2");
                final Condition	inService = new Condition();
                //System.out.println("2.1");
		provider.addObserver(new ProviderObserver() {
                    public void providerChangedEvent (ProvEv [] eventList) {
			//System.out.println("3");	
                            if (eventList == null) return;
                            //System.out.println("4");
				for (int i = 0; i<eventList.length; ++i) {
				      //System.out.println("4.1 prueba..........."+eventList[i]);
                                      if (eventList[i] instanceof ProvInServiceEv) {
						inService.set();
                                                //System.out.println("prueba..........."+eventList[i]);
                                                //System.out.println("5");
					}
				}
			}
		});
		inService.waitTrue();                 
                //System.out.println("6");
 
		/* get an object for the calling terminal */                
                }   catch (Exception e) {
		    System.out.println(e.getMessage());
		}                
    }

    public void MakeCall2(String host1, String loginUser1, String pswd1, String origen, String destino)  
    {
         src=origen;
                        dst= destino;
                  try{
                        Address srcAddr = provider.getAddress(src);
                        //System.out.println("7");
		        srcAddr.addCallObserver(new CallObserver() {                    
			public void callChangedEvent (CallEv [] eventList) 
                        {  
                        for ( int i = 0; i < eventList.length; i++ ) {
                        //System.out.println("Received ");
                        switch(eventList[i].getID())
                        {
                            
                            case CallActiveEv.ID:
                                //System.out.println("Call Active Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case CallInvalidEv.ID:
                                //System.out.println("Call Invalid Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case ConnAlertingEv.ID:
                                //Timbrado de llamada
                                //System.out.println("Connection Alerting Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case ConnConnectedEv.ID:
                                //Llamada Contestada
                                //System.out.println("Connection Connected Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case ConnCreatedEv.ID:
                                //System.out.println("Connection Created Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case ConnDisconnectedEv.ID:
                                //System.out.println("Connection Disconnected Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case ConnFailedEv.ID:
                                    //System.out.println("Connection Failed Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case ConnInProgressEv.ID:
                                //System.out.println("Connection In Progress Event\n "+"Origen"+ src+" Destino"+dst);
                                break;
                            case ConnUnknownEv.ID:
                                    //System.out.println("Connection Unknown Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case TermConnActiveEv.ID:
                                    //when this event is received, it means we have a call active
                                    //on our terminal.  Change label to "Hang up".
                                    //System.out.println("TerminalConnection Active Event\n"+"Origen"+ src+" Destino"+dst);
                                //handleEvent (eventList[i]);
                                break;
                            case TermConnCreatedEv.ID:
                                    //System.out.println("TerminalConnection Created Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case TermConnDroppedEv.ID:
                                    //when this event is received, it means that our device has
                                    //disconnected from the call.  Change button label to "Dial"
                                //System.out.println("TerminalConnection Dropped Event\n"+"Origen"+ src+" Destino"+dst);
                               // handleEvent (eventList[i]);
                                  
                                break;
                            case TermConnPassiveEv.ID:
                                    //System.out.println("TerminalConnection Passive Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case TermConnRingingEv.ID:
                                    //Terminal timbra
                                    //System.out.println("TerminalConnection Ringing Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case TermConnUnknownEv.ID:
                                    //La conexion con el terminal generado finaliza por razones desconocidas
                                    //System.out.println("TerminalConnection Unknown Event\n"+"Origen"+ src+" Destino"+dst);
                                break;
                            case CallObservationEndedEv.ID:
                                    //Finalizacion de Llamada
                                //    //System.out.println("Call Observation Finaliza Llamada");
                                   // provider.shutdown();
                                break;
                            default:
                              //  //System.out.println("Unknown CallEv: " + eventList[i].getID() + "\n");
                                break;
                            }
                                   
                        }
                      }
		});
 
		/* and make the call */
                
		Call call = provider.createCall();
		//System.out.println("8");
                call.connect(srcAddr.getTerminals()[0], srcAddr, dst);        
                //System.out.println("9");
                
                //System.out.println("estado de proveedor "+provider.getState());
                //System.out.println("terminales de proveedor "+ provider.getTerminals());
                //System.out.println("10");
                //System.out.println("11");
                //System.out.println("12");
                //System.out.println("Prueba inicio!!!!!!!!!!");
                //provider.shutdown();  
                //System.out.println("13");
                //call=null;
                //peer=null;                
                //System.gc();
                //System.out.println("14");
                //System.out.println("Prueba final!!!!!!!!!!");
                  
                  //inService.interrupt(0);
                //System.out.println("15");
                
                }   catch (Exception e) {
                    System.out.println(e.getMessage());
                }
    }
    
    public boolean closeSession(){
        try{
            provider.shutdown();
            peer=null;                
            System.gc();
            return true;
        }   catch (Exception e) {  
            System.out.println(e.getMessage());
            return false;
        }
    }

}