/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.pop3;

/**
 *
 * @author fabia
 */

import ec.com.centrosurcall.datos.DAO.HistorialDAO;
import ec.com.centrosurcall.negocio.controladores.CtrSession;
import java.io.PrintStream;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Flags;
//import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;

public class MsnFromPop3
{
  private HistorialDAO historialDAO = new HistorialDAO();
  CtrSession ctrsession;

  public MsnFromPop3(CtrSession ctrsession)
  {
    this.ctrsession = ctrsession;
  }

  public void getNoValidos() throws Exception
  {
    Properties props = new Properties();

    String host = this.ctrsession.getIp2n();
    String username = this.ctrsession.getUsuario2n() + "@" + this.ctrsession.getDominio2n();
    String password = this.ctrsession.getContrasenia2n();
    String provider = "pop3";

    Session session = Session.getInstance(props, null);
    Store store = session.getStore(provider);
    store.connect(host, username, password);

    Folder inbox = store.getFolder("INBOX");
    if (inbox == null) {
      System.out.println("No INBOX");
      System.exit(1);
    }
    inbox.open(2);

    Message[] messages = inbox.getMessages();

    for (int i = 0; i < messages.length; i++) {
      if (messages[i].getSubject().equals("FAILED")) {
        this.historialDAO.getSPSetIncorrecct(messages[i].getFrom()[0].toString().substring(0, 10), messages[i].getContent().toString().trim());
      }

      messages[i].setFlag(Flags.Flag.DELETED, true);
    }
    inbox.close(false);
    store.close();
  }
}