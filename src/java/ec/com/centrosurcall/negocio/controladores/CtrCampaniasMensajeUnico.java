package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.HistorialDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.modelo.Historial;
import ec.com.centrosurcall.datos.modelo.Parametro;
//import ec.com.centrosurcall.smtp.Smtp2sms;
import ec.com.centrosurcall.utils.FacesUtils;
//import java.io.PrintStream;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.mail.internet.InternetAddress;

@ManagedBean
@ViewScoped
public final class CtrCampaniasMensajeUnico extends MantenimientoGenerico
  implements MantenimientoInterface, Serializable
{
  CtrSession session;
  private Parametro selParametros = new Parametro();
  ParametrosDAO parametrosDAO = new ParametrosDAO();
  private String numero;
  private String mensaje;
  private String opcionBoton = "";
  private SelectItem[] selectItemTipos;
  private HistorialDAO historialDAO = new HistorialDAO();

  public CtrCampaniasMensajeUnico() {
    this.session = ((CtrSession)FacesUtils.getManagedBean("ctrSession"));
    this.session.setTituloSeccion("Listado");
    this.parametroSession.setEsNuevo(Boolean.valueOf(true));
    this.parametroSession.setBoton("btnGrabar");
    setOpcionBoton("Enviar Mensaje");
    this.session.setTituloSeccion("Nuevo");
  }

  public void cargarParametros() {
    this.listaBaseObjetos.clear();
    this.listaObjetos.clear();
    List listaParametros = this.parametrosDAO.getParametros();
    if (listaParametros != null) {
      this.listaBaseObjetos.addAll(listaParametros);
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargarTipos() {
    this.selectItemTipos = new SelectItem[2];
    this.selectItemTipos[0] = new SelectItem("1", "Rango");
    this.selectItemTipos[1] = new SelectItem("2", "Porcentaje");
  }

  public void cargoParametrosEditar() {
    this.opcionBoton = "Guardar";
  }

  public void confPop() {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro desea enviar el mensaje??");
    setMensajeCorrecto("Mensaje enviado");
    setMensajeError("Error al enviar");
  }

  public boolean grabar()
  {
    boolean exito = false; boolean validado = true;
    InternetAddress[] telefono = new InternetAddress[1];
    Parametro prefijos = new ParametrosDAO().getParametroByAtributo("PREFIJOSVALMSN");
    setMensajeError("Error al enviar");
    try
    {
      if (this.numero.length() == 10) {
        String[] listaPrefijos = prefijos.getValor().split(",");
        for (int k = 1; k < listaPrefijos.length; k++)
          if (!this.numero.startsWith(listaPrefijos[k])) {
            validado = false;
            break;
          }
        try
        {
          Integer.parseInt(this.numero);
        }
        catch (NumberFormatException e) {
          validado = false;
        }
        if (validado) {
          if (this.historialDAO.getHistorialNumFailed(this.numero) != null) {
            setMensajeError("El número ingresado no es válido!!");
            validado = false;
          } else {
            telefono[0] = new InternetAddress(this.numero + "@" + this.parametroSession.getDominio2n());
            String mensajeDevuelto = this.parametroSession.getSmtp2sms().send2N(telefono, this.mensaje.length() > 160 ? this.mensaje.substring(0, 160) : this.mensaje);

            System.out.println("Mensaje Devuelto 2N: " + mensajeDevuelto);
            if ((mensajeDevuelto != null) && (mensajeDevuelto.equals("0"))) {
              this.historialDAO.saveHistorial(new Historial(new Date(), this.numero, this.mensaje, "1", Integer.valueOf(0), Integer.valueOf(1), this.session.getUsuario()), 3);
              exito = true;
            }
            else {
              exito = false;
              setMensajeError("Cola de mensajes llena, espere unos minutos e intente otra vez!!");
            }
          }
        } else {
          setMensajeError("Número ingresado incorrecto!!");
          exito = false;
        }
      }
      else {
        setMensajeError("Número ingresado incorrecto!!");
        exito = false;
      }
      return exito;
    } catch (Exception e) {
      System.out.println("Error Mensaje único" + e);
    }return false;
  }

  public void buscarWsql()
  {
  }

  public void cargarColumnasGrupo()
  {
  }

  public String regresar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.session.setObjeto(null);
    this.session.setTituloSeccion("Listado");
    return "frmMensajeria";
  }

  public void auditoria()
  {
  }

  public String refrescar()
  {
    return "";
  }

  public boolean isExitograbar()
  {
    return super.isExitograbar();
  }

  public void imprimir()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public String editar()
  {
    return "";
  }

  public String nuevo()
  {
    return "frmParametros";
  }

  public boolean eliminar()
  {
    return true;
  }

  public Parametro getSelParametros() {
    return this.selParametros;
  }

  public void setSelParametros(Parametro selParametros) {
    this.selParametros = selParametros;
  }

  public String getOpcionBoton() {
    return this.opcionBoton;
  }

  public void setOpcionBoton(String opcionBoton) {
    this.opcionBoton = opcionBoton;
  }

  public SelectItem[] getSelectItemTipos() {
    return this.selectItemTipos;
  }

  public void setSelectItemTipos(SelectItem[] selectItemTipos) {
    this.selectItemTipos = selectItemTipos;
  }

  public String getMensaje() {
    return this.mensaje;
  }

  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }

  public String getNumero() {
    return this.numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }
}