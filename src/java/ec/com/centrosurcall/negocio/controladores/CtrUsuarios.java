/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.SearchResultEntry;
import ec.com.centrosurcall.datos.DAO.CampaniaDAO;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.UsuariosDAO;
import ec.com.centrosurcall.datos.DAO.UsuarioSkillDAO;
import ec.com.centrosurcall.datos.modelo.Campania;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.datos.modelo.Razon;
import ec.com.centrosurcall.datos.modelo.RolUsuario;
import ec.com.centrosurcall.datos.modelo.RolUsuarioId;
import ec.com.centrosurcall.datos.modelo.Rol;
import ec.com.centrosurcall.datos.modelo.Skill;
import ec.com.centrosurcall.datos.modelo.Team;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.datos.modelo.UsuarioSkill;
import ec.com.centrosurcall.datos.modelo.UsuarioSkillId;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;
import ec.com.centrosurcall.ldap.conexion3;
import java.io.IOException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchResult;
//import org.openide.util.Exceptions;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrUsuarios extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Usuario selUsuario = new Usuario();
    private UsuarioSkill selUsuarioSkill = new UsuarioSkill();
    UsuariosDAO usuariosDAO = new UsuariosDAO();
    CampaniaDAO campaniaDAO = new CampaniaDAO();
    UsuarioSkillDAO usuarioSkillDAO = new UsuarioSkillDAO();
    String opcionBoton = "";
    String ouNombre = "";
    private SelectItem[] selectItemTeams;
    private SelectItem[] selectItemSkills;
    private SelectItem[] selectItemRoles;
    private SelectItem[] selectItemOus;
    private SelectItem[] selectItemOus2;
    private SelectItem[] selectItemUsuarios;
    private SelectItem[] selectItemCampanias;
    SelectItem item = new SelectItem();
    Usuario oUsuario = new Usuario();
    private String ouSeleccionado = "";
    private String ouSeleccionado2 = "";
    private List<Object> objTeam;
    private List<Object> objSkill;
    private List<Object> objRol;
    Team team = new Team();
    private List<Usuario> objRol2 = new ArrayList<Usuario>();
    private List<Usuario> objSkill2 = new ArrayList<Usuario>();
    conexion3 conex = new conexion3();
    private boolean verNombre = true;
    private boolean verNombreTxt =false;
    private String idCampania ="0";

    public CtrUsuarios() throws LDAPException {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoUsuarioEditar();
            verNombreTxt=true;
            verNombre=false;
        } else if (parametroSession.getEsNuevo()) {
            cargarOus();
        }
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarUsuarios();
        item.setLabel("-SELECCIONE-");
        item.setValue(null);
        cargarTeams();
        cargarSkills();
        cargarRoles();
        cargarCampanias();
        setMensajeConfirmacion("Seguro desea grabar este Usuario?");
        confPop();
    }

    public void cargarTeams() {
        selectItemTeams = new SelectItem[1];
        selectItemTeams[0] = item;
        objTeam = consulta.getListHql(null, "Team", "estado = 1 order by nombre", null);
        if (objTeam != null && objTeam.size() > 0) {
            int cont = 1;
            selectItemTeams = new SelectItem[objTeam.size() + 1];
            selectItemTeams[0] = item;
            for (Object obj : objTeam) {
                Team cl = (Team) obj;
                selectItemTeams[cont] = new SelectItem(String.valueOf(cl.getId()), cl.getNombre());
                cont++;
            }
        }
    }

    public void cargarSkills() {
        objSkill = consulta.getListHql(null, "Skill", "estado = 1 order by nombreSkill", null);
        if (objSkill != null && objSkill.size() > 0) {
            int cont = 0;
            selectItemSkills = new SelectItem[objSkill.size()];
            for (Object obj : objSkill) {
                Skill cl = (Skill) obj;
                selectItemSkills[cont] = new SelectItem(cl.getId(), cl.getNombreSkill());
                cont++;
            }
        }
    }

    public void cargarRoles() {
        objRol = consulta.getListHql(null, "Rol", "estado = 1 order by nombreRol", null);
        if (objRol != null && objRol.size() > 0) {
            int cont = 0;
            selectItemRoles = new SelectItem[objRol.size()];
            for (Object obj : objRol) {
                Rol cl = (Rol) obj;
                selectItemRoles[cont] = new SelectItem(cl.getId(), cl.getNombreRol());
                cont++;
            }
        }
    }
    
    public void cargarCampanias() {
        List<Campania> listaCampania = campaniaDAO.getCampanias();
        if (listaCampania != null) {
            int cont = 1;
            selectItemCampanias = new SelectItem[listaCampania.size()+1];
            selectItemCampanias[0] = new SelectItem("0", "--SELECCIONE--");
            for (Campania obj : listaCampania) {
                selectItemCampanias[cont] = new SelectItem(String.valueOf(obj.getId()), obj.getNombre());
                cont++;
            }
        }
    }

    public void cargarOus() throws LDAPException {

        //  conex.getallOU();
        //List<SearchResultEntry> lista = conex.getallOU();
        List<Object[]> lista = consulta.getListSql("select * from dbo.parametro where grupo='OU' and estado=1");
        selectItemOus = new SelectItem[lista.size()];
        int cont = 0;
        for (Object[] e : lista) {

            selectItemOus[cont] = new SelectItem(String.valueOf(e[2]), String.valueOf(e[1]));

            cont++;
        }
        if (lista.size() > 0) {
            ouSeleccionado = selectItemOus[0].getValue().toString();
            cargarUsuariosDA();
        }
    }

//    public void cargarOus2() throws LDAPException {
//        
//        //  conex.getallOU();
//        if(ouSeleccionado!=null || !"".equals(ouSeleccionado)){
//            List<SearchResultEntry> lista = conex.getallOU2(ouSeleccionado);
//            selectItemOus2 = new SelectItem[lista.size()];
//            int cont = 0;
//            for (SearchResultEntry e : lista) {
//                selectItemOus2[cont] = new SelectItem(e.getAttributeValue("name"), e.getAttributeValue("name"));
//                cont++;
//            }
//            if(lista.size()>0){f
//                ouSeleccionado2 = selectItemOus2[0].getValue().toString();
//                cargarUsuariosDA();
//            }
//            
//        }
//        
//    }
    public void cargarUsuariosDA() throws LDAPException {

        List<Usuario> objUs;
        objUs = usuariosDAO.getUsuarios();
        List<SelectItem> temporal = new ArrayList<>();
        int contador=0;
        if (ouSeleccionado != null || !"".equals(ouSeleccionado)) {
            if (!"".equals(ouSeleccionado)) {
                try {
                    NamingEnumeration lista = conex.getallbyOU(ouSeleccionado);
                    if (lista!=null){
                        //temporal = new SelectItem[lista.size()];
                        int cont = 0;
                        while (lista != null && lista.hasMore()) {
                            SearchResult e = (SearchResult)lista.next();
                        //for (SearchResultEntry e : lista) {
                            Attributes attrs = e.getAttributes ();
                            boolean bandera=false;
                            for(int i=0;i<=objUs.size()-1;i++){
                                if(objUs.get(i).getNick().equals(String.valueOf((attrs.get("SAMAccountName")!=null?attrs.get("SAMAccountName").get():"")).toUpperCase())){
                                    bandera=true;
                                    break;
                                }
                            }
                            if(bandera==false){
                                if (attrs.get("givenname")!=null){
                                    temporal.add(new SelectItem(String.valueOf((attrs.get("SAMAccountName")!=null?attrs.get("SAMAccountName").get():"")), String.valueOf((attrs.get("sn")!=null?attrs.get("sn").get():"")) + " " + String.valueOf((attrs.get("givenname")!=null?attrs.get("givenname").get():""))));
                                    contador++;
                                }                                    
                            }
                            cont++;
                        }
                        selectItemUsuarios = new SelectItem[contador];
                        cont=0;
                        for(int j=0;j<=temporal.size()-1;j++){
                            if(temporal.get(j)!=null){
                                selectItemUsuarios[cont] = new SelectItem(temporal.get(j).getValue(),temporal.get(j).getLabel());
                                cont++;
                            }
                        }
                        if(selectItemUsuarios.length>0){
                            selUsuario.setNick(String.valueOf(selectItemUsuarios[0].getValue())); 
                        }else{
                            selUsuario.setNick("");
                        }
 
                    }else{
                        selectItemUsuarios = new SelectItem[0];
                        selUsuario.setNick("");
                    }
                } catch (NamingException ex) {
                    System.out.println(ex);
                    //Exceptions.printStackTrace(ex);
                } catch (IOException ex) {
                    System.out.println(ex);
                    //Exceptions.printStackTrace(ex);
                }
            }
        }

    }

    public void cargarUsuarios() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Usuario> listaUsuario = usuariosDAO.getUsuarios();
        if (listaUsuario != null) {
            listaBaseObjetos.addAll(listaUsuario);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargoUsuarioEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Usuario")) {
                if (((Usuario) session.getObjeto()).getId() > 0) {
                    Integer idUsuario = ((Usuario) session.getObjeto()).getId();
                    objRol2 = consulta.getListSql("select a.id from dbo.rol a, dbo.rol_usuario b where b.idUsuario=" + idUsuario + " and b.idRol=a.id and estado=1");
                    objSkill2 = consulta.getListSql("select a.id from dbo.skill a, dbo.usuario_skill b where b.idUsuario=" + idUsuario + " and b.idSkill=a.id and estado=1");
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }

        if (existeObjeto) {
            opcionBoton = "Guardar";
            selUsuario = (Usuario) session.getObjeto();
            if (selUsuario.getCampania()!=null)
                idCampania = String.valueOf(selUsuario.getCampania().getId());
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Usuario");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            for (int i = 0; i <= selectItemUsuarios.length - 1; i++) {
                if (selUsuario.getNick().equals(selectItemUsuarios[i].getValue())) {
                    ouNombre = selectItemUsuarios[i].getLabel();
                }
            }
            selUsuario.setNombre(ouNombre);
            selUsuario.setEstado(1);
            selUsuario.setSeleccionado(false);
            if (!idCampania.equals("0"))
                selUsuario.setCampania(campaniaDAO.getCampaniaByID(Integer.parseInt(idCampania)));
            else
                selUsuario.setCampania(null);
            exito = usuariosDAO.saveUsuario(selUsuario, 0);
            if (exito) {
                List maxId;
                maxId = consulta.getListSql("select max(id) from dbo.usuario");
                for (int i = 0; i <= objSkill2.size() - 1; i++) {
                    Skill skill = new Skill();
                    UsuarioSkill algo = new UsuarioSkill();
                    UsuarioSkillId asd = new UsuarioSkillId();
                    skill.setId(Integer.valueOf(String.valueOf(objSkill2.get(i))));
                    asd.setIdSkill(Integer.valueOf(String.valueOf(objSkill2.get(i))));
                    asd.setIdUsuario(Integer.valueOf(String.valueOf(maxId.get(0))));
                    algo.setId(asd);
                    usuarioSkillDAO.saveUsuarioSkill(algo, 1);
                }
                for (int i = 0; i <= objRol2.size() - 1; i++) {
                    Rol rol = new Rol();
                    RolUsuario algo = new RolUsuario();
                    RolUsuarioId asd = new RolUsuarioId();
                    rol.setId(Integer.valueOf(String.valueOf(objRol2.get(i))));
                    asd.setIdRol(Integer.valueOf(String.valueOf(objRol2.get(i))));
                    asd.setIdUsuario(Integer.valueOf(String.valueOf(maxId.get(0))));
                    algo.setId(asd);
                    usuariosDAO.saveRolUsuario(algo, 1);
                }
            }

        } else {
            if (!idCampania.equals("0"))
                selUsuario.setCampania(campaniaDAO.getCampaniaByID(Integer.parseInt(idCampania)));
            else
                selUsuario.setCampania(null);
            selUsuario.setSeleccionado(false);
            exito = usuariosDAO.saveUsuario(selUsuario, 0);
            if (exito) {
                consulta.getListSql("delete from dbo.usuario_skill where idUsuario=" + selUsuario.getId());
                for (int i = 0; i <= objSkill2.size() - 1; i++) {
                    UsuarioSkill algo = new UsuarioSkill();
                    UsuarioSkillId asd = new UsuarioSkillId();
                    asd.setIdSkill(Integer.valueOf(String.valueOf(objSkill2.get(i))));
                    asd.setIdUsuario(selUsuario.getId());
                    algo.setId(asd);
                    usuarioSkillDAO.saveUsuarioSkill(algo, 1);
                }
                consulta.getListSql("delete from dbo.rol_usuario where idUsuario=" + selUsuario.getId());
                for (int i = 0; i <= objRol2.size() - 1; i++) {
                    RolUsuario algo = new RolUsuario();
                    RolUsuarioId asd = new RolUsuarioId();
                    asd.setIdRol(Integer.valueOf(String.valueOf(objRol2.get(i))));
                    asd.setIdUsuario(selUsuario.getId());
                    algo.setId(asd);
                    usuariosDAO.saveRolUsuario(algo, 1);
                }
            }
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Usuario> respuesta = new ArrayList<Usuario>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Usuario cl = (Usuario) obj;
                if (cl.getNick().toUpperCase().contains(whereSql.toUpperCase()) || cl.getNombre().toUpperCase().contains(whereSql.toUpperCase()) || cl.getExtension().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        listaColumnas.put("nombre", "Nombre del Usuario");
        listaColumnas.put("nick", "Usuario");
        listaColumnas.put("extension", "Extensión");
    }

    @Override
    public String regresar() {
        parametroSession.setEsEditar(false);
        parametroSession.setEsNuevo(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionUsuarios";
    }

    @Override
    public void auditoria() {
        try {
            selUsuario = (Usuario) objetoSeleccionado;
        } catch (Exception e) {
            selUsuario.setId(0);
        }
        if (selUsuario.getId() > 0) {
            creado = selUsuario.getNick();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsEditar(false);
        parametroSession.setEsNuevo(false);
        whereSql = "";
        cargarUsuarios();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Usuario> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Usuario) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmUsuarios";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmUsuarios";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Usuario> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = usuariosDAO.eliminarUsuario(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarUsuarios();
        }
        return exito;
    }

    public List<Usuario> sacarSeleccionado() {
        Usuario selectUsuario = new Usuario();
        List<Usuario> listadoSeleccionado = new ArrayList<Usuario>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectUsuario = (Usuario) o;
            if (selectUsuario.getSeleccionado()) {
                listadoSeleccionado.add(selectUsuario);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Usuario getSelUsuario() {
        return selUsuario;
    }

    public void setSelUsuario(Usuario selUsuario) {
        this.selUsuario = selUsuario;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String nombre;
    private String nick;
    private String extension;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public SelectItem[] getSelectItemTeams() {
        return selectItemTeams;
    }

    public void setSelectItemTeams(SelectItem[] selectItemTeams) {
        this.selectItemTeams = selectItemTeams;
    }

    public SelectItem[] getSelectItemSkills() {
        return selectItemSkills;
    }

    public void setSelectItemSkills(SelectItem[] selectItemSkills) {
        this.selectItemSkills = selectItemSkills;
    }

    public List<Object> getObjSkill() {
        return objSkill;
    }

    public void setObjSkill(List<Object> objSkill) {
        this.objSkill = objSkill;
    }

    public SelectItem[] getSelectItemRoles() {
        return selectItemRoles;
    }

    public void setSelectItemRoles(SelectItem[] selectItemRoles) {
        this.selectItemRoles = selectItemRoles;
    }

    public List<Object> getObjRol() {
        return objRol;
    }

    public void setObjRol(List<Object> objRol) {
        this.objRol = objRol;
    }

    public SelectItem[] getSelectItemOus() {
        return selectItemOus;
    }

    public void setSelectItemOus(SelectItem[] selectItemOus) {
        this.selectItemOus = selectItemOus;
    }

    public SelectItem[] getSelectItemUsuarios() {
        return selectItemUsuarios;
    }

    public void setSelectItemUsuarios(SelectItem[] selectItemUsuarios) {
        this.selectItemUsuarios = selectItemUsuarios;
    }

    public String getOuSeleccionado() {
        return ouSeleccionado;
    }

    public void setOuSeleccionado(String ouSeleccionado) {
        this.ouSeleccionado = ouSeleccionado;
        try {
            cargarUsuariosDA();
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }


    }

    public List<Usuario> getObjRol2() {
        return objRol2;
    }

    public void setObjRol2(List<Usuario> objRol2) {
        this.objRol2 = objRol2;
    }

    public List<Usuario> getObjSkill2() {
        return objSkill2;
    }

    public void setObjSkill2(List<Usuario> objSkill2) {
        this.objSkill2 = objSkill2;
    }

    public SelectItem[] getSelectItemOus2() {
        return selectItemOus2;
    }

    public void setSelectItemOus2(SelectItem[] selectItemOus2) {
        this.selectItemOus2 = selectItemOus2;
    }

    public String getOuSeleccionado2() {
        return ouSeleccionado2;
    }

    public void setOuSeleccionado2(String ouSeleccionado2) {
        this.ouSeleccionado2 = ouSeleccionado2;
        try {
            cargarUsuariosDA();
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
    }

    public boolean isVerNombre() {
        return verNombre;
    }

    public void setVerNombre(boolean verNombre) {
        this.verNombre = verNombre;
    }

    public boolean isVerNombreTxt() {
        return verNombreTxt;
    }

    public void setVerNombreTxt(boolean verNombreTxt) {
        this.verNombreTxt = verNombreTxt;
    }

    public SelectItem[] getSelectItemCampanias() {
        return selectItemCampanias;
    }

    public void setSelectItemCampanias(SelectItem[] selectItemCampanias) {
        this.selectItemCampanias = selectItemCampanias;
    }

    public String getIdCampania() {
        return idCampania;
    }

    public void setIdCampania(String idCampania) {
        this.idCampania = idCampania;
    }
}