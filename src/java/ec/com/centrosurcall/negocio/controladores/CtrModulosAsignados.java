/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.ModulosDAO;
import ec.com.centrosurcall.datos.modelo.Modulo;
import ec.com.centrosurcall.datos.modelo.ModuloId;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrModulosAsignados extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Modulo selModulo = new Modulo();
    ModulosDAO modulosDAO = new ModulosDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemPadres;
    private List<Modulo> objModuloPadre;
    SelectItem item = new SelectItem();
    ModuloId moduloId = new ModuloId();
    private Boolean nomEditable = false;
    private Boolean urlEditable = false;

    public CtrModulosAsignados() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        item.setLabel("-SELECCIONE-");
        item.setValue(-1);
        moduloId = new ModuloId();
        selModulo.setId(moduloId);
        if (parametroSession.getEsNuevo()) {
            nomEditable = false;
            urlEditable = false;
        }
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoModuloEditar();
            nomEditable = true;
            urlEditable = true;
        }
        cargarColumnasGrupo();
        cargarModulos();
        cargarPadres();
        setMensajeConfirmacion("Seguro desea grabar este Módulo?");
        confPop();
    }

    public void cargarPadres() {
        selectItemPadres = new SelectItem[1];
        selectItemPadres[0] = item;
        objModuloPadre = modulosDAO.getModulosPadre();
        if (objModuloPadre != null && objModuloPadre.size() > 0) {
            int cont = 1;
            selectItemPadres = new SelectItem[objModuloPadre.size() + 1];
            selectItemPadres[0] = item;
            for (Object obj : objModuloPadre) {
                Modulo cl = (Modulo) obj;
                selectItemPadres[cont] = new SelectItem(cl.getId().getIdPadre(), cl.getNombre());
                cont++;
            }
        }
    }

    public void cargarModulos() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Modulo> listaModulo = modulosDAO.getModulos();
        if (listaModulo != null) {
            listaBaseObjetos.addAll(listaModulo);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargoModuloEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Modulo")) {
                if (((Modulo) session.getObjeto()).getId().getIdHijo() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Grabar";
            selModulo = (Modulo) session.getObjeto();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Módulo");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;

        if (parametroSession.getEsNuevo()) {
            if (selModulo.getId().getIdPadre() == -1) {
                List maxId;
                maxId = consulta.getListSql(" select max(idPadre) from dbo.modulo");
                moduloId.setIdPadre(Integer.valueOf(String.valueOf(maxId.get(0))) + 1);
                moduloId.setIdHijo(Integer.valueOf(String.valueOf(maxId.get(0))) + 1);
            } else {
                List maxId;
                maxId = consulta.getListSql("select max(idHijo) from dbo.modulo where idPadre=" + selModulo.getId().getIdPadre());
                moduloId.setIdHijo(Integer.valueOf(String.valueOf(maxId.get(0))) + 1);
            }
            selModulo.setId(moduloId);
            selModulo.setEstado(1);
            exito = modulosDAO.saveModulo(selModulo, 1);
        } else {
            //selModulo.setId(moduloId); 
            exito = modulosDAO.saveModulo(selModulo, 0);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Modulo> respuesta = new ArrayList<Modulo>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Modulo cl = (Modulo) obj;
                if (cl.getNombre().toUpperCase().contains(whereSql.toUpperCase()) || cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase()) || cl.getUrl().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        listaColumnas.put("nombre", "Nombre");
        listaColumnas.put("url", "URL");
        listaColumnas.put("descripcion", "Descripción");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionModulos";
    }

    @Override
    public void auditoria() {
        try {
            selModulo = (Modulo) objetoSeleccionado;
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (selModulo.getId().getIdHijo() > 0) {
            creado = selModulo.getNombre();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarModulos();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Modulo> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Modulo) objetoEditar.get(0)).getId().getIdHijo() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmModulos";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }

    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmModulos";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Modulo> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = modulosDAO.eliminarModulo(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarModulos();
        }
        return exito;
    }

    public List<Modulo> sacarSeleccionado() {
        Modulo selectModulo = new Modulo();
        List<Modulo> listadoSeleccionado = new ArrayList<Modulo>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectModulo = (Modulo) o;
            if (selectModulo.getSeleccionado()) {
                listadoSeleccionado.add(selectModulo);
                exito = true;
                break;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Modulo getSelModulo() {
        return selModulo;
    }

    public void setSelModulo(Modulo selModulo) {
        this.selModulo = selModulo;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String nombre;
    private String descripcion;
    private String estado;
    private String url;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public SelectItem[] getSelectItemPadres() {
        return selectItemPadres;
    }

    public void setSelectItemPadres(SelectItem[] selectItemPadres) {
        this.selectItemPadres = selectItemPadres;
    }

    public List<Modulo> getObjModuloPadre() {
        return objModuloPadre;
    }

    public void setObjModuloPadre(List<Modulo> objModuloPadre) {
        this.objModuloPadre = objModuloPadre;
    }

    public Boolean getNomEditable() {
        return nomEditable;
    }

    public void setNomEditable(Boolean nomEditable) {
        this.nomEditable = nomEditable;
    }

    public Boolean getUrlEditable() {
        return urlEditable;
    }

    public void setUrlEditable(Boolean urlEditable) {
        this.urlEditable = urlEditable;
    }
}
