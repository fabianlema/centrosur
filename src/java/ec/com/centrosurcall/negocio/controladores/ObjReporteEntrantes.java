/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author Paul
 */
public class ObjReporteEntrantes {
    private String agente;
    private String numLlamadas;
    private String tiempoTotal;
    
    public void objLlamadasEntrantes(){
        
    }
    
    public void objLlamadasEntrantes(String agente, String numLlamadas, String tiempoTotal){
        agente = this.getAgente();
        numLlamadas = this.getNumLlamadas();
        tiempoTotal = this.getTiempoTotal();
        
    }

    /**
     * @return the agente
     */
    public String getAgente() {
        return agente;
    }

    /**
     * @param agente the agente to set
     */
    public void setAgente(String agente) {
        this.agente = agente;
    }

    /**
     * @return the numLlamadas
     */
    public String getNumLlamadas() {
        return numLlamadas;
    }

    /**
     * @param numLlamadas the numLlamadas to set
     */
    public void setNumLlamadas(String numLlamadas) {
        this.numLlamadas = numLlamadas;
    }

    /**
     * @return the tiempoTotal
     */
    public String getTiempoTotal() {
        return tiempoTotal;
    }

    /**
     * @param tiempoTotal the tiempoTotal to set
     */
    public void setTiempoTotal(String tiempoTotal) {
        this.tiempoTotal = tiempoTotal;
    }
    
    
}
