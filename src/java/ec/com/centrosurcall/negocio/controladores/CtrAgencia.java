/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author fabia
 */

import ec.com.centrosurcall.datos.DAO.AgenciasDAO;
import ec.com.centrosurcall.datos.DAO.EquiposDAO;
import ec.com.centrosurcall.datos.modelo.Agencia;
import ec.com.centrosurcall.datos.modelo.Equipo;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public final class CtrAgencia extends MantenimientoGenerico
  implements MantenimientoInterface, Serializable
{
  CtrSession session;
  private Agencia selAgencia = new Agencia();
  AgenciasDAO agenciasDAO = new AgenciasDAO();
  EquiposDAO equiposDAO = new EquiposDAO();
  String opcionBoton = "";
  private SelectItem[] selectItemEquipos;
  private List<Equipo> objEquipos;
  SelectItem item = new SelectItem();
  private String nombre;
  private String clave;
  private String idEquipo;
  private String url;

  public CtrAgencia()
  {
    this.session = ((CtrSession)FacesUtils.getManagedBean("ctrSession"));
    setOpcionBoton("Grabar");
    this.session.setTituloSeccion("Listado");
    this.item.setLabel("-SELECCIONE-");
    this.item.setValue(Integer.valueOf(-1));
//    if ((!this.parametroSession.getEsNuevo().booleanValue()) || (
//      (this.parametroSession.getEsEditar().booleanValue()) && (this.parametroSession.getObjeto() != null))) {
//      cargoAgenciaEditar();
//    }
//    if (parametroSession.getEsNuevo()) {
//            atriEditable = false;
//        }
        if (this.parametroSession.getEsEditar() && this.parametroSession.getObjeto() != null) {
            cargoAgenciaEditar();
        }
    cargarColumnasGrupo();
    cargarAgencias();
    cargarEquipo();
    setMensajeConfirmacion("Seguro desea grabar esta agencia?");
    confPop();
  }

  public void cargarEquipo() {
    this.selectItemEquipos = new SelectItem[1];
    this.selectItemEquipos[0] = this.item;
    this.objEquipos = this.equiposDAO.getEquipo();
    int cont;
    Iterator i$;
    if ((this.objEquipos != null) && (this.objEquipos.size() > 0)) {
      cont = 1;
      this.selectItemEquipos = new SelectItem[this.objEquipos.size() + 1];
      this.selectItemEquipos[0] = this.item;
      for (i$ = this.objEquipos.iterator(); i$.hasNext(); ) { Object obj = i$.next();
        Equipo cl = (Equipo)obj;
        this.selectItemEquipos[cont] = new SelectItem(Integer.valueOf(cl.getId()), cl.getDescripcion());
        cont++; }
    }
  }

  public void cargarAgencias()
  {
    this.listaBaseObjetos.clear();
    this.listaObjetos.clear();
    List listaAgencia = this.agenciasDAO.getAgencia();
    if (listaAgencia != null) {
      this.listaBaseObjetos.addAll(listaAgencia);
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargoAgenciaEditar() {
    boolean existeObjeto = false;
    try {
      if (this.session.getObjeto().getClass().getName().contains("Agencia")) {
        if (((Agencia)this.session.getObjeto()).getId() > 0)
          existeObjeto = true;
      }
      else {
        this.parametroSession.setEsEditar(Boolean.valueOf(false));
        this.parametroSession.setEsNuevo(Boolean.valueOf(false));
      }
    } catch (Exception e) {
      System.out.println(e.getCause().getMessage());
    }
    if (existeObjeto) {
      this.opcionBoton = "Grabar";
      this.selAgencia = ((Agencia)this.session.getObjeto());
    }
  }

  public void confPop() {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro desea registrar la agencia??");
    setMensajeCorrecto("Registro grabado");
    setMensajeError("Error al Registrar");
  }

  public boolean grabar()
  {
    boolean exito = false;
    if (this.parametroSession.getEsNuevo().booleanValue())
      exito = this.agenciasDAO.saveAgencia(this.selAgencia, 1);
    else {
      exito = this.agenciasDAO.saveAgencia(this.selAgencia, 0);
    }
    return exito;
  }

  public void buscarWsql()
  {
    List respuesta = new ArrayList();
    this.parametroSession.setNumPaginas(1);
    this.listaObjetos.clear();
    if (!this.whereSql.equals("")) {
        for (Object obj : listaBaseObjetos) {
                Agencia cl = (Agencia) obj;
                if (cl.getNombre().toUpperCase().contains(whereSql.toUpperCase()) || cl.getClave().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }        
      this.listaObjetos.addAll(respuesta);
    } else {
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargarColumnasGrupo()
  {
    this.listaColumnas.put("nombre", "Nombre");
    this.listaColumnas.put("clave", "Clave");
    this.listaColumnas.put("idEquipo", "Equipo");
  }

  public String regresar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.session.setObjeto(null);
    this.session.setTituloSeccion("Listado");
    return "frmAdministracionAgencia";
  }

  public void auditoria()
  {
    try {
      this.selAgencia = ((Agencia)this.objetoSeleccionado);
    } catch (Exception e) {
      System.out.println(e.getCause().getMessage());
    }
    if (this.selAgencia.getId() > 0) {
      this.creado = this.selAgencia.getNombre();
      setCreado(this.creado);
      this.fechacreacion = new Date().toString();
      setActualizado("");
      setFechaactualizacion("");
      this.mostrarAuditoriaVal = true;
      mostrarAuditoria();
    }
  }

  public String refrescar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.whereSql = "";
    cargarAgencias();
    this.session.setSeleccionado(Boolean.valueOf(false));
    this.objetoSeleccionado = new Object();
    return "";
  }

  public boolean isExitograbar()
  {
    return super.isExitograbar();
  }

  public void imprimir()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public String editar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(true));
    this.session.setTituloSeccion("Editar");
    List objetoEditar = sacarSeleccionado();
    if (objetoEditar.size() > 0) {
      this.parametroSession.setObjeto(objetoEditar.get(0));
      try {
        if (((Agencia)objetoEditar.get(0)).getId() > 0) {
          this.parametroSession.setBoton("btnActualizar");
          return "frmAgencia";
        }
        return "";
      }
      catch (Exception e) {
        return "";
      }
    }
    return "";
  }

  public String nuevo()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(true));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.parametroSession.setBoton("btnGrabar");
    setOpcionBoton("nuevo");
    this.session.setTituloSeccion("Nuevo");
    return "frmAgencia";
  }

  public void confPopEliminar() {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro quiere eliminar?");
    setMensajeCorrecto("Eliminacion exitosa!");
    setMensajeError("Existen errores en:");
  }

  public boolean eliminar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    boolean exito = false;
    this.listaErrores = new ArrayList();
    List listaAeliminar = sacarSeleccionado();
    if (listaAeliminar.size() > 0) {
      this.listaErrores = this.agenciasDAO.eliminarAgencia(listaAeliminar, this.listaObjetos);
    }
    if (this.listaErrores.isEmpty()) {
      exito = true;
      cargarAgencias();
    }
    return exito;
  }

  public List<Agencia> sacarSeleccionado() {
    Agencia selectAgencia = new Agencia();
    List listadoSeleccionado = new ArrayList();
    boolean exito = false;
    for (Iterator i$ = this.listaObjetos.iterator(); i$.hasNext(); ) { Object o = i$.next();
      selectAgencia = (Agencia)o;
      if (selectAgencia.isSeleccionado()) {
        listadoSeleccionado.add(selectAgencia);
        exito = true;
        break;
      }
    }
    if (exito) {
      return listadoSeleccionado;
    }
    return listadoSeleccionado;
  }

  public Agencia getSelAgencia()
  {
    return this.selAgencia;
  }

  public void setSelAgencia(Agencia selAgencia) {
    this.selAgencia = selAgencia;
  }

  public String getOpcionBoton() {
    return this.opcionBoton;
  }

  public void setOpcionBoton(String opcionBoton) {
    this.opcionBoton = opcionBoton;
  }

  public String getNombre()
  {
    return this.nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getClave() {
    return this.clave;
  }

  public void setClave(String clave) {
    this.clave = clave;
  }

  public String getIdEquipo() {
    return this.idEquipo;
  }

  public void setIdEquipo(String idEquipo) {
    this.idEquipo = idEquipo;
  }

  public SelectItem[] getSelectItemEquipos() {
    return this.selectItemEquipos;
  }

  public void setSelectItemEquipos(SelectItem[] selectItemEquipos) {
    this.selectItemEquipos = selectItemEquipos;
  }

  public List<Equipo> getObjEquipos() {
    return this.objEquipos;
  }

  public void setObjEquipos(List<Equipo> objEquipos) {
    this.objEquipos = objEquipos;
  }
}