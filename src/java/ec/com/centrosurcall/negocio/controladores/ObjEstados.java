/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author Paul
 */
public class ObjEstados {
    private String agentID;
    private String eventDateTime;
    private String gmtOffset;
    private String eventType;
    private String reasonCode;
    private String profileID;
    
    private void objEstados(){
        
    }
    private void objEstados(String agentID, String eventDateTime, String gmtOffset, String eventType, String reasonCode, String profileID){
        agentID = this.agentID;
        eventDateTime = this.eventDateTime;
        gmtOffset = this.gmtOffset;
        eventType = this.eventType;
        reasonCode = this.reasonCode;
        profileID = this.profileID;
    }

    /**
     * @return the agentID
     */
    public String getAgentID() {
        return agentID;
    }

    /**
     * @param agentID the agentID to set
     */
    public void setAgentID(String agentID) {
        this.agentID = agentID;
    }

    /**
     * @return the eventDateTime
     */
    public String getEventDateTime() {
        return eventDateTime;
    }

    /**
     * @param eventDateTime the eventDateTime to set
     */
    public void setEventDateTime(String eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    /**
     * @return the gmtOffset
     */
    public String getGmtOffset() {
        return gmtOffset;
    }

    /**
     * @param gmtOffset the gmtOffset to set
     */
    public void setGmtOffset(String gmtOffset) {
        this.gmtOffset = gmtOffset;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * @param eventType the eventType to set
     */
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    /**
     * @return the reasonCode
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * @param reasonCode the reasonCode to set
     */
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * @return the profileID
     */
    public String getProfileID() {
        return profileID;
    }

    /**
     * @param profileID the profileID to set
     */
    public void setProfileID(String profileID) {
        this.profileID = profileID;
    }
    
}
