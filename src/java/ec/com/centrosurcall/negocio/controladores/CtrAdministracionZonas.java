/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;


import ec.com.centrosurcall.datos.DAO.ZonaAfectadaDAO;
import ec.com.centrosurcall.datos.DAO.ZonaDAO;
import ec.com.centrosurcall.datos.modelo.Alimentador;
import ec.com.centrosurcall.datos.modelo.Subestacion;
import ec.com.centrosurcall.datos.modelo.Zona;
import ec.com.centrosurcall.datos.modelo.ZonaAfectada;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
/**
 *
 * @author carlosguaman65
 */
@ManagedBean
@ViewScoped
public final class CtrAdministracionZonas  extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    private String descripcion;
    private String codigo;
    private List<Object> objZonasAfectadas;
    private List<Object> objAlimentadores;
    private List<Object> objZonas;
    private String mensaje;
    private Set<Alimentador> alimentadores;

    private CtrSession session;
   
    @ManagedProperty(value = "#{subestacionDAO.subestacionesList}")
    private List<Subestacion> listaSubestaciones ;
    private Boolean controlGuardar = false;
    private Subestacion subestacion;
    
    @ManagedProperty(value = "#{alimentadorDAO.alimentadoresList}")
    private List<Alimentador> listaAlimentadores ;
    
    private List<Alimentador> listadoAlimentadoresTemporal= new ArrayList<Alimentador>();
    private List<Alimentador> listaAlimentadoresSeleccionados;
    private String codigosAlimentador[] ;
    
    @ManagedProperty(value = "#{zonaDAO.zonasList}")
    private List<Zona> zonas;

   
     private List<Zona> zonasAfectadas;
     private List<Zona> listadoZonasAfectadas= new ArrayList<Zona>();

    
    
    public CtrAdministracionZonas(){
         session = (CtrSession) FacesUtils.getManagedBean("ctrSession");  
    }
   
    
      public void onSelectZonaItem(ValueChangeEvent event){ 
          listadoZonasAfectadas = (List<Zona>) event.getNewValue();
              for (Zona zona : listadoZonasAfectadas){
                        System.err.println("tttttttt >" + zona.getDescripcion());
              }
      }
    
     public void onSelectAlimentadorItem(ValueChangeEvent event){ 
        listaAlimentadoresSeleccionados = (List<Alimentador>) event.getNewValue();

     }
    
    public void onSelectSubestacionItem(ValueChangeEvent event){ 
        
      if (listaAlimentadoresSeleccionados != null)
          for (Alimentador alimentador : listaAlimentadoresSeleccionados){
                listadoAlimentadoresTemporal.add(alimentador);
          }
     
        listaAlimentadores.clear();
        if (null != event.getNewValue()) {
             Subestacion subestacionSelecionada = (Subestacion) event.getNewValue();
             objAlimentadores = consulta.getListHql(null, "Alimentador", "id_fk_subestacion = " +  subestacionSelecionada.getId(), null);
              if (objAlimentadores != null && objAlimentadores.size() > 0) {
               for (Object obj : objAlimentadores) {
                listaAlimentadores.add((Alimentador) obj);
                }        
              }
        } 
    }
    
    public void guardarNuevaZona(){
   
     ZonaDAO zonaDAO =new ZonaDAO();
     Zona zona = new Zona();
     
    if (listaAlimentadoresSeleccionados  != null)
          for (Alimentador alimentador : listaAlimentadoresSeleccionados){
                listadoAlimentadoresTemporal.add(alimentador);
          }
     listaAlimentadoresSeleccionados = listadoAlimentadoresTemporal;
     alimentadores = new HashSet(listaAlimentadoresSeleccionados);
     List<Alimentador> lst = new ArrayList<Alimentador>(alimentadores);
     zona.setCodigo(codigo);
     zona.setDescripcion(descripcion);  
     zonaDAO.saveZona(zona, alimentadores, 1);
     List <Zona> zonas = zonaDAO.getZonas();
     mensaje = "Estimado usuario, el nombre de su archivo de audio corresponde a: "+ zonas.get(zonas.size()-1).getId()+".wav";
     controlGuardar = true;
     descripcion ="";
     codigo="";
    }
    
    

    public List<Zona> getListaZonasAfectadas(){    
    List<Zona> listaZonasAfectadas = new ArrayList<Zona>();

    
        objZonasAfectadas = consulta.getListHql(null, "ZonaAfectada", "estado = 1 ", null);
        if (objZonasAfectadas != null && objZonasAfectadas.size() > 0) {     
          for (Object obj : objZonasAfectadas) {
                ZonaAfectada zonaAfectada = (ZonaAfectada) obj;
                Zona zona = new Zona();        
                zona.setId(zonaAfectada.getZona().getId());
                objZonas= consulta.getListHql(null, "Zona", "id = "+ zona.getId(), null);
                listaZonasAfectadas.add(zona);
                System.err.println("000000000 "+ zona.getId());
          }
        }
        return  listaZonasAfectadas;
    }
    

     public void actualizarZonasAfectadas(){
      //     System.err.println("wwwwwwwwwwww >");
        ZonaAfectadaDAO gestorZonas = new ZonaAfectadaDAO();
//        for (Zona zona : listadoZonasAfectadas){
//            System.err.println("tttttttt >" + zona.getDescripcion());
//            for (Zona zonaExistente : getListaZonasAfectadas()){
//                 System.err.println("sss >" + zonaExistente.getDescripcion());
//                if (zonaExistente.getId() != zona.getId()){
//                    
//                    ZonaAfectada zonaAfectada = new ZonaAfectada();
//                    zonaAfectada.setUsuario(session.getUsuario());
//                    zonaAfectada.setZona(zona);
//                    zonaAfectada.setEstado(1);
//                    zonaAfectada.setFechaInicio(new Date());
//                    zonaAfectada.setHoraInicio(new Date());
//                    gestorZonas.saveZonaAfectada(zonaAfectada, 1);
//                }
//            }
//       }
        
    }
   @PostConstruct
    public void resetearVariables(){
       
       listaAlimentadores.clear();
    }
    

    public List<Object> getObjZonasAfectadas() {
        return objZonasAfectadas;
    }

    public void setObjZonasAfectadas(List<Object> objZonasAfectadas) {
        this.objZonasAfectadas = objZonasAfectadas;
    }

     
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public List<Zona> getZonasAfectadas() {
        zonasAfectadas = new ArrayList<Zona>();
        List<Zona> comp = getListaZonasAfectadas();

            for (Zona s : zonas) {
                for (Zona sel : comp) {
                    if (sel.getId() == s.getId()) {
                        zonasAfectadas.add(s);
                        break;
                    }
                }
            }  
        return zonasAfectadas;
    }

    public void setZonasAfectadas(List<Zona> zonasAfectadas) {
        this.zonasAfectadas = zonasAfectadas;
    }
    
    public List<Zona> getZonas() {
        return zonas;
    }

    public void setZonas(List<Zona> zonas) {
        this.zonas = zonas;
    }

    public Set<Alimentador> getAlimentadores() {
        return alimentadores;
    }

    public void setAlimentadores(Set<Alimentador> alimentadores) {
        this.alimentadores = alimentadores;
    }

    public List<Subestacion> getListaSubestaciones() {
        return listaSubestaciones;
    }

    public void setListaSubestaciones(List<Subestacion> listaSubestaciones) {
        this.listaSubestaciones = listaSubestaciones;
    }

    public List<Alimentador> getListaAlimentadores() {
        return listaAlimentadores;
    }

    public void setListaAlimentadores(List<Alimentador> listaAlimentadores) {
        this.listaAlimentadores = listaAlimentadores;
    }

    public Subestacion getSubestacion() {
        return subestacion;
    }

    public void setSubestacion(Subestacion subestacion) {
        this.subestacion = subestacion;
    }

    public List<Alimentador> getListadoAlimentadoresTemporal() {
        return listadoAlimentadoresTemporal;
    }

    public void setListadoAlimentadoresTemporal(List<Alimentador> listadoAlimentadoresTemporal) {
        this.listadoAlimentadoresTemporal = listadoAlimentadoresTemporal;
    }


    public List<Object> getObjAlimentadores() {
        return objAlimentadores;
    }

    public void setObjAlimentadores(List<Object> objAlimentadores) {
        this.objAlimentadores = objAlimentadores;
    }

    public List<Alimentador> getListaAlimentadoresSeleccionados() {
        return listaAlimentadoresSeleccionados;
    }


    public void setListaAlimentadoresSeleccionados(List<Alimentador> listaAlimentadoresSeleccionados) {
        this.listaAlimentadoresSeleccionados = listaAlimentadoresSeleccionados;
    }

    public String[] getCodigosAlimentador() {
        return codigosAlimentador;
    }

    public void setCodigosAlimentador(String[] codigosAlimentador) {
        this.codigosAlimentador = codigosAlimentador;
    }

    public List<Object> getObjZonas() {
        return objZonas;
    }

    public void setObjZonas(List<Object> objZonas) {
        this.objZonas = objZonas;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getControlGuardar() {
        return controlGuardar;
    }

    public void setControlGuardar(Boolean controlGuardar) {
        this.controlGuardar = controlGuardar;
    }

    
    
    
    

    @Override
    public String nuevo() {
        return null;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String editar() {
        return null;
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimir() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String refrescar() {
        return null;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void auditoria() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String regresar() {
        return null;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cargarColumnasGrupo() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void buscarWsql() {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
//        public List<Zona> cargarZonas(){
//        
//        List<Zona> zonasAfectadas = new ArrayList<Zona>();
//        String temp ="select *" +
//                                                " from dbo.zona zon" +
//                                                " where zon.id not in  (select  zona.id" +
//                                                " from dbo.zona  zona, dbo.zona_afectada zonaf" +
//                                                " where zona.id = zonaf.id_fk_zonas)";
//         List rows = consulta.getListSql(temp);
//         if (rows != null && rows.size() > 0) {
//            int cont = 0;
////             itemListZonas = new SelectItem[rows.size()];
//            for (int i=0 ; i< rows.size();i++){
//               Object[] row = (Object[])rows.get(i);
//                Integer id = (Integer)row[0];
//                String descripcion = (String)row[1];
//                String codigo = (String) row[3];
//                Zona zona = new Zona((Integer)row[0], (String)row[1]);
//                zona.setCodigo(codigo);
//                zonasAfectadas.add(zona);
//                //System.out.println("AA2:" + as[0] + "-" + as[1]);     
//              //  itemListZonas[cont] = new SelectItem(id,descripcion);
//              //  cont++;
//            }
//         }
//          return zonasAfectadas;
//    }
    
//////////////////////////////////////
    
    //        if (objZonasAfectadas != null && objZonasAfectadas.size() > 0) {
//         
//          for (Object obj : objZonasAfectadas) {
//                ZonaAfectada zonaAfectada = (ZonaAfectada) obj;
//                Zona zona = new Zona();        
//                zona.setId(zonaAfectada.getZona().getId());
//                zona.setCodigo(zonaAfectada.getZona().getCodigo());
//                zona.setDescripcion(zonaAfectada.getZona().getDescripcion());
//                zona.setAlimentador(zonaAfectada.getZona().getAlimentador());
//                zona.setUbicacion(zonaAfectada.getZona().getUbicacion());
//                listaZonasAfectadas.add(zona);
//                System.err.println("000000000 "+ zona.getDescripcion());
//          }
//        }
    
}
