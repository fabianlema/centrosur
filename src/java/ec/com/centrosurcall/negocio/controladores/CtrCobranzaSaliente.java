 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.utils.FacesUtils;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.swing.tree.TreeNode;
import org.richfaces.component.UITree;
import org.richfaces.event.TreeSelectionChangeEvent;
import java.lang.reflect.Field;
import org.richfaces.component.SortOrder;
import ec.com.centrosurcall.call.MakeCall1;
import ec.com.centrosurcall.datos.DAO.ClientesDAO;
import ec.com.centrosurcall.datos.DAO.LlamadasDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.DAO.RazonesDAO;
import ec.com.centrosurcall.datos.DAO.UsuariosDAO;
//import ec.com.centrosurcall.datos.DAO.VisitasDAO;
import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Cliente;
import ec.com.centrosurcall.datos.modelo.Llamada;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.datos.modelo.Razon;
//import ec.com.centrosurcall.datos.modelo.Visita;
//import ec.com.centrosurcall.middle.*;
import ec.com.centrosurcall.negocio.campania.Cola;
import javax.faces.model.SelectItem;

import java.sql.Time;
import java.text.Format;
import java.text.SimpleDateFormat;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author TaurusTech
 */
@ManagedBean
@ViewScoped
public final class CtrCobranzaSaliente implements Serializable {

    //PersonasDAO personasHelper = new PersonasDAO();
    String opcionBoton = "";
    CtrSession session;
//    private Persona selPersona = new Persona();
    private Cliente selCliente;
    //private Cliente selCliente2;
    //private Cliente selClienteSWeb = new Cliente();
//    private Cuota selCuota;
//    private Socio selSocio;
    private Llamada selLlamada = new Llamada();
    private Razon selRazon = new Razon();
//    private Cuenta cuentaSocio;
//    private Garantias garantiasCliente;
    private String idSocio = "";
    private SelectItem[] selectItemRazones;
    private List<Object> objRazon;
    SelectItem item = new SelectItem();
    private List<Object> informacionGarantes = null;
    private List<Object> cuotas = null;
    private List<Object> tasaReajustable = null;
    private List<Object> historialLlamadas = null;
    private List<Object> contactosPersonales = null;
    private List<Object> contactosReferencia = null;
    private List<Object> observacionesFIT = null;
    private List<Object> informacionDetalleCuotas = null;
    //private List<Llamada> listLlamadas;
    private String nombreUsuario;
    private String contestado = "18";
    private String quiencontesta = "1";
    private String msjError = "";
    private String msjNoRegistro = "";
    private String idRazon = "0";
    private String idTelefonoGarante = "";
    private String tipoTelefono = "";
    //private LlamadasDAO llamadasDAO = new LlamadasDAO();
    private ClientesDAO clientesDAO = new ClientesDAO();
    //private ec.com.centrosurcall.datos.modelo.Cliente cliente;
    private String observacion = "";
    private String observacionVisita = "";
    private Time horallamada;
    private boolean programarVisita = false;
    private boolean muestraVisita = false;
    private boolean muestraQContesta = true;
    private boolean muestraRazon = true;
    private boolean mostrarGuardarVisita = true;
    private boolean guardadoObservacion = true;
    private boolean guardadoTelefono = true;
    private boolean esAgente2 = false;
    private boolean ejecutoVisita = true, ejecutoObservacion = true, ejecutoTelefono = true;
    private String aliasUsuario;
    private String observacionObs;
    private String saldoCuentaAhorros;
    private String saldoConfirmar;
    private String msjLlamando = "";
    private String errorMsjObservacion;
    private String errorMsjTelefono;
    private Date fechaVisita;
    private Date fechaCompromiso;
    private String numero;
    private MakeCall1 objLlamar;
    private List<String> listLlamadasRelizadas;
    private static int CONTESTADO = 18;
    private static int NOCONTESTADO = 19;
    private int idComentario;
    private String extension="";
    private Object[] objUsuario;
    private UsuariosDAO usuDAO = new UsuariosDAO();
    private boolean bandera_registrada;
    private String clienteSeleccionado;
    private String tipoBusqueda="1";
    private String busCedula="";
    private String busClicod="";
    private String numTelefonoT="";
    private String operadoraT="";
    private String descipcionT="0";
    private String numTipoT="26";
    private String esRegistrado="24";
    private String parentezcoT="0";
    private List<Cliente> listaBusqueda;
    private Razon razonEquiv;
    private String guion="";
    
    private SelectItem[] selectItems1;
    private SelectItem[] selectItems2;
    private SelectItem[] selectItems3;
    
    //private ec.com.centrosurcall.datos.modelo.Telefono newTelefono = new ec.com.centrosurcall.datos.modelo.Telefono();
        
    public CtrCobranzaSaliente() {
        setMsjLlamando("");
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        session.setNumeroLlamadas(0);
        selLlamada.setRazon(selRazon);
        item.setLabel("-SELECCIONE-");
        item.setValue(9);        
        //cargarRazones();
        cargarRazonesAll();
        
        cargarDatosCliente();
        
        //listLlamadasRelizadas = new ArrayList<String>();
        
        //HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        //Runtime r = Runtime.getRuntime();
    }
    
    public void cargarClienteByNum() throws IOException{
        String url="frmGestionCobranzaEntrante.xhtml?clicod="+clienteSeleccionado;
        FacesContext faces = FacesContext.getCurrentInstance();
        ExternalContext context = faces.getExternalContext();
        context.redirect(url);
        return ;
    }
    
    public void buscarCliente(){
        if(tipoBusqueda.equals("1")){
            listaBusqueda = clientesDAO.getListClientesByIdSocio(busCedula);
        }else if(tipoBusqueda.equals("2")){
            listaBusqueda = new ArrayList<Cliente>();
            /*selCliente2 = clientesDAO.getClienteByID(busClicod);
            listaBusqueda.add(selCliente2);*/
        }
    }
    
    public void abrirPagina() throws IOException{
        String url="frmGestionCobranzaEntrante.xhtml?clicod="+numero;
        FacesContext faces = FacesContext.getCurrentInstance();
        ExternalContext context = faces.getExternalContext();
        context.redirect(url);
        return ;
    }

    public void cargarDatosCliente() {
        Cola cola = new Cola();
        int rolTmp = 0;
        boolean validado = true;
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        
        Parametro prefijos = new ParametrosDAO().getParametroByAtributo("PREFIJOSVALIDOS");
        if (session.getClicodLlamar().equals("")) {
            for (int j = 0; j < session.getRolUsuario().size(); j++) {
                if (session.getRolUsuario().get(j).getId().getIdRol() == session.getAgente1()) {
                    rolTmp = 1;
                    setMostrarGuardarVisita(false);
                    esAgente2 = false;
                } else if (session.getRolUsuario().get(j).getId().getIdRol() == session.getAgente2()) {                    
                    if (rolTmp != 1) {
                        rolTmp = 2;
                    }
                    setMostrarGuardarVisita(true);
                    esAgente2 = true;
                }
            }
            if (rolTmp == 0) {
                System.out.println("El Usuario no tiene un rol válido para recuperación de créditos!!");
                setMsjError("El Usuario no tiene un rol válido para recuperación de créditos!!");
            } else {
                setMsjError("");
                if (rolTmp == 2) {
                    selCliente = cola.getClientes(session.getUsuario().getAlias(), rolTmp, session.getUsuario().getCampania().getId());
                } else {
                    selCliente = cola.getClientes(String.valueOf(session.getUsuario().getId()), rolTmp, session.getUsuario().getCampania().getId());
                }
            }
            session.setChargeInfoCliente(false);
        } else {
            for (int j = 0; j < session.getRolUsuario().size(); j++) {
                if (session.getRolUsuario().get(j).getId().getIdRol() == session.getAgente1()) {
                    rolTmp = 1;
                    setMostrarGuardarVisita(false);
                    esAgente2 = false;
                } else if (session.getRolUsuario().get(j).getId().getIdRol() == session.getAgente2()) {
                    if (rolTmp != 1) {
                        rolTmp = 2;
                    }
                    setMostrarGuardarVisita(true);
                    esAgente2 = true;
                }
            }
            selCliente = clientesDAO.getClienteByID(session.getClicodLlamar().getClicod(), session.getClicodLlamar().getIdCampania());
        }
        //System.out.println("AAAA");
        //System.out.println(selCliente.getTlfCasa());
        try {
            aliasUsuario = session.getUsuario().getAlias();
            //selCliente2 = clientesDAO.getClienteByID(selCliente.getClicod());
            //objUsuario = usuDAO.getUsuariosByAlias(selCliente2.getCusuarioOficialCobranza());
            //session.setAgenteCobranza(selCliente2.getCusuarioOficialCobranza());
            if(objUsuario!=null){
                if(objUsuario[8]!=null){
                    extension = String.valueOf(objUsuario[8]);
                }else{
                    extension ="";
                }
            }
            if (selCliente != null) {
                setMsjError("");
                if (session.getListLlamadasRelizadas() == null) {
                    listLlamadasRelizadas = new ArrayList<String>();
                } else {
                    listLlamadasRelizadas = session.getListLlamadasRelizadas();// new ArrayList<String>();
                }

                //System.out.println("Consultando crédito " + selCliente.getClicod() + " del cliente " + selCliente.getIdCliente());
                session.setClicodLlamar(selCliente.getId());
                session.setClienteLlamar(selCliente);
                validado = true;
                if (session.isIsNoContestado()){
                    setNumero(selCliente.getTlfCasa());
                    setTipoTelefono("TELEFONO");
                    if (getNumero().length() == 7) {
                        String[] listaPrefijos = prefijos.getValor().split(";")[1].split(",");
                        for (int k = 0; k < listaPrefijos.length; k++) {
                            if (!numero.startsWith(listaPrefijos[k])){
                                validado = false;
                            }else{
                                validado = true;
                                break;
                            }
                        }
                        try {
                            Integer.parseInt(getNumero());
                        } catch (NumberFormatException e) {
                            validado = false;
                        }
                        if (validado) {
                            //setNumero(selCliente.getTlfCasa());
                            session.setNumTelefonoLlamar(getNumero());
                            llamarNumero();
                        } else {
                            session.setIsNoContestado(false);
                            session.setClicodLlamar(null);
                            guardarNoValido();
                            cargarDatosCliente();
                        }
                    } else {
                        if (getNumero().length() != 0) {
                            session.setIsNoContestado(false);
                            session.setClicodLlamar(null);
                            guardarNoValido();
                            cargarDatosCliente();
                        }else{
                            session.setIsNoContestado(false);
                            session.setClicodLlamar(null);
                            cargarDatosCliente();
                        }
                    }
                }else{
                    setNumero(selCliente.getTlfCelular());
                    setTipoTelefono("CELULAR");
                    if (getNumero().length() == 10) {
                        String[] listaPrefijos = prefijos.getValor().split(";")[0].split(",");
                        for (int k = 1; k < listaPrefijos.length; k++) {
                            if (!numero.startsWith(listaPrefijos[k])){
                                validado = false;
                            }else {
                                validado = true;
                                break;
                            }
                        }
                        try {
                            Integer.parseInt(getNumero());
                        } catch (NumberFormatException e) {
                            validado = false;
                        }
                        if (validado) {
                            //setNumero(selCliente.getTlfCasa());
                            session.setNumTelefonoLlamar(getNumero());
                            llamarNumero();
                        } else {
                            session.setIsNoContestado(true);                            
                            guardarNoValido();
                            cargarDatosCliente();
                        }
                    } else {
                        if (getNumero().length() != 0) {
                            session.setIsNoContestado(true);                            
                            guardarNoValido();
                            cargarDatosCliente();
                        }else{
                            session.setIsNoContestado(true);
                            cargarDatosCliente();
                        }
                    }
                }
                Date hoy = new Date();
                int tempHora = hoy.getHours();
                String saludo = "";
                if (tempHora < 13)
                    saludo = "Buenos días";
                else if (tempHora < 19)
                    saludo = "Buenas tardes";
                else
                    saludo = "Buenas noches";
                guion = session.getGuion().replace("idSaludo", saludo).replace("\\n", " </br> ").replace("idCliente", selCliente.getNombres()).replace("idAgente", session.getUsuario().getNombre());
                //llamarNumero();
                //System.out.println("NUMEROOOO:" + getNumero());
            } else if (rolTmp != 0) {
                System.out.println("No hay créditos a mostrar");
                setMsjError("No hay Clientes en la cola asignada!!");
                session.setListLlamadasRelizadas(null);
            } else{
                System.out.println("No hay créditos a mostrar");
                setMsjError("La cola asignada se halla vacía!!");
                session.setListLlamadasRelizadas(null);
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void cargarRazones(String padreId) {
        Consultas consulta = new Consultas();
        objRazon = consulta.getListHql(null, "Razon", "estado = 1 and padreId=" + padreId, null);
        selectItemRazones = null;
        idRazon="0";
        esRegistrado="24";
        numTipoT="26";
        if (objRazon != null && objRazon.size() > 0) {
            int cont = 1;
            selectItemRazones = new SelectItem[objRazon.size() + 1];
            selectItemRazones[0] = new SelectItem("0", "--SELECCIONE--");
            for (Object obj : objRazon) {
                Razon cl = (Razon) obj;
                selectItemRazones[cont] = new SelectItem(String.valueOf(cl.getId()), cl.getNombre());
                cont++;
            }
        }
    }
    
    public void cargarRazonesAll() {        
        Consultas consulta = new Consultas();
        objRazon = consulta.getListHql(null, "Razon", "estado=1 and (padreId=1 or padreId=2 or padreId=3) order by padreId, nombre", null);
        if (objRazon != null && objRazon.size() > 0) {            
            List<SelectItem> selection1 = new ArrayList<SelectItem>(0);
            List<SelectItem> selection2 = new ArrayList<SelectItem>(0);
            List<SelectItem> selection3 = new ArrayList<SelectItem>(0);
            for (Object obj : objRazon) {
                Razon cl = (Razon) obj;
                if (cl.getPadreId()==1)
                    selection1.add(new SelectItem(String.valueOf(cl.getId()), cl.getNombre()));
                else if (cl.getPadreId()==2)
                    selection2.add(new SelectItem(String.valueOf(cl.getId()), cl.getNombre()));
                else if (cl.getPadreId()==3)
                    selection3.add(new SelectItem(String.valueOf(cl.getId()), cl.getNombre()));
            }
            selectItems1 = selection1.toArray(new SelectItem[selection1.size()]);
            selectItems2 = selection2.toArray(new SelectItem[selection2.size()]);
            selectItems3 = selection3.toArray(new SelectItem[selection3.size()]);            
        }
    }

    public void selectionChanged(TreeSelectionChangeEvent selectionChangeEvent) {
        List<Object> selection = new ArrayList<Object>(selectionChangeEvent.getNewSelection());
        Object currentSelectionKey = selection.get(0);
        UITree tree = (UITree) selectionChangeEvent.getSource();
        Object storedKey = tree.getRowKey();
        tree.setRowKey(currentSelectionKey);
        try {
            Object currentSelection = (TreeNode) tree.getRowData();
            Class c = currentSelection.getClass();
            Field campoId = c.getField("id");
            int id = (Integer) campoId.get(currentSelection);
            Field campoName = c.getField("name");
            String nombre = (String) campoName.get(currentSelection);

        } catch (Exception e) {
        }
        tree.setRowKey(storedKey);
    }
    
    public List<Object> sacarSeleccionado() {
        //Miro si esta algun seleccionado de la tabla
        //   Persona selectPersona = new Persona();
        List<Object> listadoSeleccionado = new ArrayList<Object>();
        boolean exito = false;
//
//        for (Object o : listaObjetos) {
//            selectPersona = (Persona) o;
//
//            if (selectPersona.getSeleccionado()) {
//                listadoSeleccionado.add(selectPersona);
//                exito = true;
//            }
//        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }
    private SortOrder nombresOrder = SortOrder.unsorted;

    public void sortByNombres() {
        nombresOrder = SortOrder.unsorted;
        if (nombresOrder.equals(SortOrder.ascending)) {
            setNombresOrder(SortOrder.descending);
        } else {
            setNombresOrder(SortOrder.ascending);
        }
    }
    
    public SortOrder getNombresOrder() {
        return nombresOrder;
    }

    public void setNombresOrder(SortOrder nombresOrder) {
        this.nombresOrder = nombresOrder;
    }

//    @Override
//    public void imprimir() {
//        GeneraReporte r = new GeneraReporte();
//        Map parametros = new HashMap();
//        String logoUniversidad = FacesUtils.getExternalContext().getRealPath("/resources/Imagenes/EscudoUniversidad.jpg");
//        parametros.put("LOGO", logoUniversidad);
//        parametros.put("USUARIO", parametroSession.getPersonaLogueado().getNombres());
//        parametros.put("SISTEMA", msgs.getValue("sistema"));
//        parametros.put("NOMREPORTE", msgs.getValue("rptAmbitos"));
//        r.generaPdfListado(listaObjetos, "rptAmbito", this.getClass(), parametros, "ambitos");
//    }
    public void abrirPopup() {
    }
//
//    List<ClaseGeneral> transformarIntitucionesGeneral(List<Persona> listado) {
//        List<ClaseGeneral> listaGen = new ArrayList<ClaseGeneral>();
//        for (Persona objeto : listado) {
//            ClaseGeneral cg = new ClaseGeneral();
//            cg.setCampo1(objeto.getDescripcion());
//            listaGen.add(cg);
//        }
//        return listaGen;
//    }

//    public Persona getSelPersona() {
//        return selPersona;
//    }
//
//    public void setSelPersona(Persona selPersona) {
//        this.selPersona = selPersona;
//    }
    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String nombresCompletosSocio;
    private String direccionPrincipal;
    private String mail;
    private String estado;

    public String getNombresCompletosSocio() {
        return nombresCompletosSocio;
    }

    public void setNombresCompletosSocio(String nombresCompletosSocio) {
        this.nombresCompletosSocio = nombresCompletosSocio;
    }

    public String getDireccionPrincipal() {
        return direccionPrincipal;
    }

    public void setDireccionPrincipal(String direccionPrincipal) {
        this.direccionPrincipal = direccionPrincipal;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(String idSocio) {
        this.idSocio = idSocio;
    }

    public String getSaldoCuentaAhorros() {
        return saldoCuentaAhorros;
    }

    public void setSaldoCuentaAhorros(String saldoCuentaAhorros) {
        this.saldoCuentaAhorros = saldoCuentaAhorros;
    }

    public String getSaldoConfirmar() {
        return saldoConfirmar;
    }

    public void setSaldoConfirmar(String saldoConfirmar) {
        this.saldoConfirmar = saldoConfirmar;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public SelectItem[] getSelectItemRazones() {
        return selectItemRazones;
    }

    public void setSelectItemRazones(SelectItem[] selectItemRazones) {
        //System.out.println("dededede--");
        this.selectItemRazones = selectItemRazones;
    }

    public List<Object> getObjRazon() {
        return objRazon;
    }

    public void setObjRazon(List<Object> objRazon) {
        this.objRazon = objRazon;
    }

    public Llamada getSelLlamada() {
        return selLlamada;
    }

    public void setSelLlamada(Llamada selLlamada) {
        this.selLlamada = selLlamada;
    }

//    public List<Object> getObservacionesFIT() {
//        return observacionesFIT;
//    }
//
//    public void setObservacionesFIT(List<Object> observacionesFIT) {
//        this.observacionesFIT = observacionesFIT;
//    }

    public ec.com.centrosurcall.datos.modelo.Cliente getSelCliente() {
        return selCliente;
    }

    public void setSelCliente(ec.com.centrosurcall.datos.modelo.Cliente selCliente) {
        this.selCliente = selCliente;
    }

    /*public Cliente getSelClienteSWeb() {
        return selClienteSWeb;
    }

    public void setSelClienteSWeb(Cliente selClienteSWeb) {
        this.selClienteSWeb = selClienteSWeb;
    }*/

//    public Cuota getSelCuota() {
//        return selCuota;
//    }
//
//    public void setSelCuota(Cuota selCuota) {
//        this.selCuota = selCuota;
//    }
//
//    public Cuenta getCuentaSocio() {
//        return cuentaSocio;
//    }
//
//    public void setCuentaSocio(Cuenta cuentaSocio) {
//        this.cuentaSocio = cuentaSocio;
//    }
//
//    public Garantias getGarantiasCliente() {
//        return garantiasCliente;
//    }
//
//    public void setGarantiasCliente(Garantias garantiasCliente) {
//        this.garantiasCliente = garantiasCliente;
//    }
//
//    public Socio getSelSocio() {
//        return selSocio;
//    }
//
//    public void setSelSocio(Socio selSocio) {
//        this.selSocio = selSocio;
//    }

    public boolean getMuestraVisita() {
        return muestraVisita;
    }

    public void setMuestraVisita(boolean muestraVisita) {
        //System.out.println("RRRRRRRRRRRR--" + muestraVisita + "--" + programarVisita);
        this.muestraVisita = muestraVisita;
    }

    public String getTipoTelefono() {
        return tipoTelefono;
    }

    public String getMsjLlamando() {
        return msjLlamando;
    }

    public void setMsjLlamando(String msjLlamando) {
        this.msjLlamando = msjLlamando;
    }

    public void setTipoTelefono(String tipoTelefono) {
        this.tipoTelefono = tipoTelefono;
    }

    public boolean getMuestraQContesta() {
        return muestraQContesta;
    }

    public void setMuestraQContesta(boolean muestraQContesta) {
        this.muestraQContesta = muestraQContesta;
    }

    /**
     * @return the nombreConyuge2
     */
    public boolean getProgramarVisita() {
        return programarVisita;
    }

    /**
     * @param nombreConyuge2 the nombreConyuge2 to set
     */
    public void setProgramarVisita(boolean programarVisita) {
        setMuestraVisita(programarVisita);
        this.programarVisita = programarVisita;
    }

    /**
     * @return the informacionGarantes
     */
    public List<Object> getInformacionGarantes() {
        return informacionGarantes;
    }

    /**
     * @param informacionGarantes the informacionGarantes to set
     */
    public void setInformacionGarantes(List<Object> informacionGarantes) {
        this.informacionGarantes = informacionGarantes;
    }

    /**
     * @return the cuotas
     */
    public List<Object> getCuotas() {
        return cuotas;
    }

    /**
     * @param cuotas the cuotas to set
     */
    public void setCuotas(List<Object> cuotas) {
        this.cuotas = cuotas;
    }

    /**
     * @return the cuotas
     */
    public List<Object> getHistorialLlamadas() {
        return historialLlamadas;
    }

    /**
     * @param cuotas the cuotas to set
     */
    public void setHistorialLlamadas(List<Object> historialLlamadas) {
        this.historialLlamadas = historialLlamadas;
    }

    /**
     * @return the cuotas
     */
    public List<Object> getContactosPersonales() {
        return contactosPersonales;
    }

    /**
     * @param cuotas the cuotas to set
     */
    public void setContactosPersonales(List<Object> contactosPersonales) {
        this.contactosPersonales = contactosPersonales;
    }

    /**
     * @return the cuotas
     */
    public List<Object> getContactosReferencia() {
        return contactosReferencia;
    }

    /**
     * @param cuotas the cuotas to set
     */
    public void setContactosReferencia(List<Object> contactosReferencia) {
        this.contactosReferencia = contactosReferencia;
    }

    public String getNombreUsuario() {
        if (session.getUsuario() != null) {
            return session.getUsuario().getNombre();
        } else {
            return "Invitado";
        }
    }

    public void setNombreUsuario(String nombreUsuario) {
        //System.out.println("tttttt--" + contestado);
        this.nombreUsuario = nombreUsuario;
    }

    public void setContestado(String contestado) {
        //System.out.println("Contestooo--" + contestado);
        System.out.println("CO:" + contestado);        
        if (contestado!=null){
            cargarRazones(contestado);
            if (contestado.equals("18"))
                setMuestraRazon(true);
            else
                setMuestraRazon(false);
        }            
        this.contestado = contestado;
    }

    public String getContestado() {
        return contestado;
    }

    public void setQuiencontesta(String quiencontesta) {
        //System.out.println("ContestoooQuien--" + quiencontesta);
        if (Integer.parseInt(quiencontesta) == 1) {
            setMuestraRazon(false);
        } else {
            setMuestraRazon(true);
        }
        this.quiencontesta = quiencontesta;
    }

    public String getQuiencontesta() {
        return quiencontesta;
    }

    public boolean getMuestraRazon() {
        return muestraRazon;
    }

    public void setMuestraRazon(boolean muestraRazon) {
        this.muestraRazon = muestraRazon;
    }

    public void setObservacion(String observacion) {
        //System.out.println("uuuuuuu--" + observacion);
        this.observacion = observacion;
    }

    public String getObservacion() {
        return observacion;
    }

    public String getMsjError() {
        return msjError;
    }

    public void setMsjError(String msjError) {
        this.msjError = msjError;
    }

    public String getMsjNoRegistro() {
        return msjNoRegistro;
    }

    public void setMsjNoRegistro(String msjNoRegistro) {
        this.msjNoRegistro = msjNoRegistro;
    }

    public void setObservacionVisita(String observacionVisita) {
        //System.out.println("uuuuuuu--" + observacionVisita);
        this.observacionVisita = observacionVisita;
    }

    public String getObservacionVisita() {
        return observacionVisita;
    }

    public void setHorallamada(Time horallamada) {
        //System.out.println("uuuuuuu--" + horallamada);        
        this.horallamada = horallamada;
    }

    public Time getHorallamada() {
        Date utilDate = new Date();
        Time sqlTime = new java.sql.Time(utilDate.getTime());
        horallamada = sqlTime;
        return horallamada;
    }

    public Date getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(Date fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public boolean getMostrarGuardarVisita() {
        return mostrarGuardarVisita;
    }

    public void setMostrarGuardarVisita(boolean mostrarGuardarVisita) {
        this.mostrarGuardarVisita = mostrarGuardarVisita;
    }

    public String getIdRazon() {
        return idRazon;
    }

    public void setIdRazon(String idRazon) {
        this.idRazon = idRazon;
    }

    public String getNextCredit() {
        this.bandera_registrada=false;
        if (session.getNumTelefonoLlamar().equals("") || esAgente2) {
            if (session.getNumeroLlamadas() > 0 || esAgente2) {
                session.setListLlamadasRelizadas(null);
                return "frmGestionCobranzaSaliente";
            } else {
                setMsjError("No se ha realizado ninguna llamada al Socios o no se ha registrado la llamada activa razón por la cual no se avanzará a la siguiente llamada!!");
                return null;
            }
        } else {
            setMsjError("No se registró la llamada activa, regístrelo primero antes de avanzar!!");
            return null;
        }
    }

    public String registrarLlamada(boolean fin) {
        try {
            this.bandera_registrada=true;
            //if (ejecutoRegistro) {
            long tempDiff = System.currentTimeMillis() - horallamada.getTime();
            Time tempo = new Time(tempDiff);
            if(numero!=null){
                if (selCliente != null && !numero.equals("")) {
                    //System.out.println("Si hay crédito");
                    RazonesDAO razonesDAO = new RazonesDAO();
                    //System.out.println("ASD:" + getIdRazon());
                    Razon razon = razonesDAO.getRazonByID(getIdRazon()!=null? Integer.parseInt(getIdRazon())!=0? Integer.parseInt(getIdRazon()) : Integer.parseInt(getContestado()) : Integer.parseInt(getContestado()));
                    //Razon razon = razonesDAO.getRazonByID(Integer.parseInt(getIdRazon())!=0? Integer.parseInt(getIdRazon()) : Integer.parseInt(getContestado()));

                    Llamada llamada = new Llamada();
                    llamada.setRazon(razon);
                    llamada.setObservacion(getObservacion() != null ? getObservacion() : "");
                    llamada.setEstadoLlamada(Integer.parseInt(getContestado()));
                    llamada.setUsuario(session.getUsuario());
                    llamada.setCliente(selCliente);
                    llamada.setFechaLlamada(new Date());
                    llamada.setHora(getHorallamada());
                    llamada.setTiempoDuracion(Time.valueOf(Integer.parseInt(getContestado()) == 18 ? "00:" + tempo.getMinutes() + ":" + tempo.getSeconds() : "00:00:00"));
                    llamada.setEstado(1);
                    llamada.setTipoLlamada(numTipoT!=null?Integer.parseInt(numTipoT):0);
                    llamada.setEsRegistrado(esRegistrado!=null?Integer.parseInt(esRegistrado):0);
                    llamada.setNumeroTelefono(getNumero());
                    llamada.setTipoTelefono(getTipoTelefono() != null ? getTipoTelefono() : "");
                    LlamadasDAO llamadasDAO = new LlamadasDAO();
                    llamadasDAO.saveLlamada(llamada, 1);

                    session.setChargeInfoCliente(true);
                    //session.setClicodLlamar("");
                    session.setAgenteCobranza("");
                    if (Integer.parseInt(getContestado()) == NOCONTESTADO) {
                        selCliente.setEstado(0);
                        selCliente.setIntentos(selCliente.getIntentos() + 1);
                        clientesDAO.saveCliente(selCliente, 0);
                        if (session.isIsNoContestado()){
                            session.setIsNoContestado(false);
                            session.setClicodLlamar(null);
                        }else
                            session.setIsNoContestado(true);
                    } else {
                        selCliente.setEstado(2);
                        selCliente.setIntentos(3);
                        clientesDAO.saveCliente(selCliente, 0);
                        session.setIsNoContestado(false);
                        session.setClicodLlamar(null);
                    }
                    if (selCliente.getIntentos() == 3 && selCliente.getEstado() != 2) {
                        setMostrarGuardarVisita(true);
                    } else {
                        setMostrarGuardarVisita(false);
                    }
                    session.setNumTelefonoLlamar("");
                    session.setNumeroLlamadas(session.getNumeroLlamadas() + 1);
                    setMsjNoRegistro("");
                    setMsjError("");
                    setMsjLlamando("Llamada Registrada");
                    horallamada = null;
                    contestado = "18";
                    numero = "";
                    observacion = "";
                } else {
                    setMsjLlamando("No hay cliente asignado");
                    System.out.println("No hay cliente");
                }
            }else{
                setMsjLlamando("No existe un número asignado");
                System.out.print("No existe número");
            }
                
            //}
            //ejecutoRegistro = !ejecutoRegistro;
            if (!fin)
                return "frmGestionCobranzaSalientes";
            else 
                return "frmPrincipal";
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
            return "";
        }
    }
    
    public boolean verificarTelefono() {
        if (getNumTelefonoT() != null){
            if (getNumTelefonoT().length() > 8){
                setErrorMsjTelefono("");
                guardadoTelefono = true;
                return guardadoTelefono;
            }
            else{
                setErrorMsjTelefono("Ingrese un teléfono válido");
                guardadoTelefono = false;
                return guardadoTelefono;
            }            
        }
        else{
            setErrorMsjTelefono("No ha ingresado ningún teléfono");
            guardadoTelefono = false;
            return guardadoTelefono;
        }
    }
    
    public boolean verificarFecha() {
        if (getFechaCompromiso() != null){
            Date hoy = new Date();
            if (daysBetween(hoy, getFechaCompromiso()) == 0 || (getFechaCompromiso().after(hoy) && daysBetween(hoy, getFechaCompromiso()) + 1 < 16)){
                setErrorMsjObservacion("");
                guardadoObservacion = true;
                return guardadoObservacion;
            }
            else{
                setErrorMsjObservacion("Seleccione una fecha menor a 15 dias");
                guardadoObservacion = false;
                return guardadoObservacion;
            }            
        }
        else{
            setErrorMsjObservacion("No ha seleccionado ninguna fecha");
            guardadoObservacion = false;
            return guardadoObservacion;
        }
    }
    
    public int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }
    
    /*public void registrarTelefono() {
        try {
            if (ejecutoTelefono && guardadoTelefono) {
                TelefonosDAO telefonosDAO = new TelefonosDAO();
                ec.com.centrosurcall.datos.modelo.Telefono newTelefonoTmp = new ec.com.centrosurcall.datos.modelo.Telefono();
                newTelefonoTmp.setDescripcion(descipcionT.equals("0") ? null : descipcionT);
                newTelefonoTmp.setEmpresaOperadora(operadoraT.equals("0") ? null : operadoraT);
                //newTelefonoTmp.setIdCliente(selCliente.getIdCliente());
                newTelefonoTmp.setParentezco(parentezcoT.equals("0") ? null : parentezcoT);
                newTelefonoTmp.setTelefono(numTelefonoT);
                newTelefonoTmp.setTipoTelefono(tipoTelefono.equals("0") ? null : tipoTelefono);
                newTelefonoTmp.setEstado(1);
                
                telefonosDAO.saveTelefono(newTelefonoTmp, 1);
                
                newTelefonoTmp.setDescripcion("0");
                newTelefonoTmp.setEmpresaOperadora("");
                //newTelefonoTmp.setIdCliente(selCliente.getIdCliente());
                newTelefonoTmp.setParentezco("0");
                newTelefonoTmp.setTelefono("");
                newTelefonoTmp.setTipoTelefono("0");
            }
            ejecutoTelefono = !ejecutoTelefono;
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
    }*/
    
    public void registrarObservacion() {        
        try {
            if (ejecutoObservacion && guardadoObservacion) {
                Format formatter = new SimpleDateFormat("dd-MM-yyyy");
                //IJep socioImplObs = new JepImpl();
                //socioImplObs.ingresoComentario(this.selCliente.getClicod(), this.aliasUsuario, this.idComentario,"",formatter.format(getFechaCompromiso()), this.observacionObs);
            }
            ejecutoObservacion = !ejecutoObservacion;
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
    }

    /*public void programarVisitas() {
        try {
            if (ejecutoVisita) {
                Visita visita = new Visita();
                visita.setUsuario(session.getUsuario());
                //visita.setCliente(selCliente);
                visita.setFechaMarca(new Date());
                visita.setResultadoVisita(0);
                visita.setEstado(0);

                VisitasDAO visitaDAO = new VisitasDAO();
                visitaDAO.saveVisita(visita, 1);
            }
            ejecutoVisita = !ejecutoVisita;
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
    }*/

    public boolean verificarGuardado() {
        this.bandera_registrada=false;
        if (session.getNumTelefonoLlamar().equals("")) {
            setMsjNoRegistro("");
            return true;
        } else {
            //DESDOCUMENTAR LO DOCUMENTADO Y DOCUMENTAR LO QUE NO ESTA DOCUMENTADO
            setMsjNoRegistro("");
            return true;
            //setMsjNoRegistro("No se podrá realizar otra llamada hasta que registre la llamada activa!!");
            //return false;
        }
    }

    public boolean verificarNoMarcado() {
    this.bandera_registrada=false;
        for (int i = 0; i < listLlamadasRelizadas.size(); i++) {
            if (listLlamadasRelizadas.get(i).equals(numero)) {
                setMsjNoRegistro("Ya se realizó una llamada al numero seleccionado!!");
                return false;
            }
        }
        setMsjNoRegistro("");
        return true;
    }

    public void llamarNumero() {
        try {
            if (!getNumero().equals("0")){
                this.bandera_registrada=false;
                String tmpTelefono = getNumero();
                tipoTelefono = getTipoTelefono();
                session.setNumTelefonoLlamar(tmpTelefono);
                setMsjLlamando("");

                listLlamadasRelizadas.add(getNumero());
                session.setListLlamadasRelizadas(listLlamadasRelizadas);
                
                //System.out.println(session.getPrefijoucxx().getValor() + "" + getNumero());
                session.getObjLlamar().MakeCall2("", "", "", session.getUsuario().getExtension(), session.getPrefijoucxx().getValor() + getNumero());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            //System.out.println(e.getMessage());
        }
    }
    
    /*public void cargarHistorialLlamada() {
        if (informacionDetalleCuotas == null) informacionDetalleCuotas = (List<Object>) (List<?>) socioImpl.detalleCuotasPagadas(selCliente.getClicod());
    }
    
    public void cargarReferencias() {
        try{
            if (contactosReferencia == null) contactosReferencia = (List<Object>) (List<?>) socioImpl.obtenerReferenciasPersonales(selCliente.getIdCliente());
        }catch (Exception e) {
            //System.out.println(e.getMessage());
        }
    }
    
    public void cargarGarantes() {
        try{
            if (informacionDetalleCuotas == null){
                List<Relacion> listRelacion = socioImpl.personasCliente(selCliente.getClicod());
                List<RelacionTmp> relacionTmp = new ArrayList<RelacionTmp>(0);
                Socio garanteTmp;
                SelectItem[] selectItemTelefonos;
                for(int j=0; j < listRelacion.size(); j++){
                    garanteTmp = socioImpl.obtenerDatosSocioGarante(listRelacion.get(j).getIdentificacion());
                    selectItemTelefonos = new SelectItem[listRelacion.get(j).getTelefonos().size() + 1];
                    selectItemTelefonos[0] = new SelectItem("0", "--Seleccione--");
                    for (int k=1; k < selectItemTelefonos.length; k++){
                        selectItemTelefonos[k] = new SelectItem(listRelacion.get(j).getTelefonos().get(k - 1).getNumeroTelefono(), listRelacion.get(j).getTelefonos().get(k - 1).getNumeroTelefono());
                    }
                    relacionTmp.add(new RelacionTmp(listRelacion.get(j).getIdentificacion(), listRelacion.get(j).getNombreLegal(), listRelacion.get(j).getRelacion(), garanteTmp.getDireccionPrincipal(), garanteTmp.getMail(), garanteTmp.getNombresCompletosConyugue(), garanteTmp.getIdentificacionConyugue(), garanteTmp.getProfesion(), selectItemTelefonos));
                }
                if (relacionTmp != null) informacionGarantes = (List<Object>)(List<?>)relacionTmp;
                else informacionGarantes = null;
                //informacionDetalleCuotas = (List<Object>) (List<?>) socioImpl.detalleCuotasPagadas(selCliente.getClicod());
            }
        }catch (Exception e) {
            //System.out.println(e.getMessage());
        }
    }*/

    public List<Object> getTasaReajustable() {
        return tasaReajustable;
    }

    public void setTasaReajustable(List<Object> tasaReajustable) {
        this.tasaReajustable = tasaReajustable;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    /**
     * @return the bandera_registrada
     */
    public boolean isBandera_registrada() {
        
        return bandera_registrada;
        
        
    }

    /**
     * @param bandera_registrada the bandera_registrada to set
     */
    public void setBandera_registrada(boolean bandera_registrada) {
        this.bandera_registrada = bandera_registrada;
    }

    /**
     * @return the observacionesFIT
     */
    public List<Object> getObservacionesFIT() {
        return observacionesFIT;
    }

    /**
     * @param observacionesFIT the observacionesFIT to set
     */
    public void setObservacionesFIT(List<Object> observacionesFIT) {
        this.observacionesFIT = observacionesFIT;
    }

    public String getAliasUsuario() {
        return aliasUsuario;
    }

    public void setAliasUsuario(String aliasUsuario) {
        this.aliasUsuario = aliasUsuario;
    }

    public Date getFechaCompromiso() {
        return fechaCompromiso;
    }

    public void setFechaCompromiso(Date fechaCompromiso) {
        this.fechaCompromiso = fechaCompromiso;
    }

    public int getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(int idComentario) {
        this.idComentario = idComentario;
    }

    public String getObservacionObs() {
        return observacionObs;
    }

    public void setObservacionObs(String observacionObs) {
        this.observacionObs = observacionObs;
    }

    public String getErrorMsjObservacion() {
        return errorMsjObservacion;
    }

    public void setErrorMsjObservacion(String errorMsjObservacion) {
        this.errorMsjObservacion = errorMsjObservacion;
    }

    public String getIdTelefonoGarante() {
        return idTelefonoGarante;
    }

    public void setIdTelefonoGarante(String idTelefonoGarante) {
        setNumero(idTelefonoGarante);
        setTipoTelefono("");
        this.idTelefonoGarante = idTelefonoGarante;
    }

    public List<Object> getInformacionDetalleCuotas() {
        return informacionDetalleCuotas;
    }

    public void setInformacionDetalleCuotas(List<Object> informacionDetalleCuotas) {
        this.informacionDetalleCuotas = informacionDetalleCuotas;
    }

    public String getClienteSeleccionado() {
        return clienteSeleccionado;
    }

    public void setClienteSeleccionado(String clienteSeleccionado) {
        this.clienteSeleccionado = clienteSeleccionado;
    }

    public String getTipoBusqueda() {
        return tipoBusqueda;
    }

    public void setTipoBusqueda(String tipoBusqueda) {
        this.tipoBusqueda = tipoBusqueda;
    }

    public String getBusCedula() {
        return busCedula;
    }

    public void setBusCedula(String busCedula) {
        this.busCedula = busCedula;
    }

    public String getBusClicod() {
        return busClicod;
    }

    public void setBusClicod(String busClicod) {
        this.busClicod = busClicod;
    }

    public List<ec.com.centrosurcall.datos.modelo.Cliente> getListaBusqueda() {
        return listaBusqueda;
    }

    public void setListaBusqueda(List<ec.com.centrosurcall.datos.modelo.Cliente> listaBusqueda) {
        this.listaBusqueda = listaBusqueda;
    }

    public String getErrorMsjTelefono() {
        return errorMsjTelefono;
    }

    public void setErrorMsjTelefono(String errorMsjTelefono) {
        this.errorMsjTelefono = errorMsjTelefono;
    }

    public String getDescipcionT() {
        return descipcionT;
    }

    public void setDescipcionT(String descipcionT) {
        this.descipcionT = descipcionT;
    }

    public String getNumTelefonoT() {
        return numTelefonoT;
    }

    public void setNumTelefonoT(String numTelefonoT) {
        this.numTelefonoT = numTelefonoT;
    }

    public String getNumTipoT() {
        return numTipoT;
    }

    public void setNumTipoT(String numTipoT) {
        this.numTipoT = numTipoT;
    }

    public String getEsRegistrado() {
        return esRegistrado;
    }

    public void setEsRegistrado(String esRegistrado) {
        this.esRegistrado = esRegistrado;
    }

    public String getOperadoraT() {
        return operadoraT;
    }

    public void setOperadoraT(String operadoraT) {
        this.operadoraT = operadoraT;
    }

    public String getParentezcoT() {
        return parentezcoT;
    }

    public void setParentezcoT(String parentezcoT) {
        this.parentezcoT = parentezcoT;
    }
    
    public void guardarNoValido(){
        Llamada llamada = new Llamada();
        llamada.setRazon(razonEquiv);
        llamada.setObservacion(getObservacion() != null ? getObservacion() : "");
        llamada.setEstadoLlamada(18);
        llamada.setUsuario(session.getUsuario());
        llamada.setCliente(selCliente);
        llamada.setFechaLlamada(new Date());
        llamada.setHora(getHorallamada());
        llamada.setTiempoDuracion(Time.valueOf("00:00:00"));
        llamada.setEstado(2);
        llamada.setTipoLlamada(0);
        llamada.setEsRegistrado(0);
        llamada.setNumeroTelefono(getNumero());
        llamada.setTipoTelefono(getTipoTelefono() != null ? getTipoTelefono() : "");
        LlamadasDAO llamadasDAO = new LlamadasDAO();
        llamadasDAO.saveLlamada(llamada, 1);
    }

    public String getGuion() {
        return guion;
    }

    public void setGuion(String guion) {
        this.guion = guion;
    }

    public SelectItem[] getSelectItems1() {
        return selectItems1;
    }

    public void setSelectItems1(SelectItem[] selectItems1) {
        this.selectItems1 = selectItems1;
    }

    public SelectItem[] getSelectItems2() {
        return selectItems2;
    }

    public void setSelectItems2(SelectItem[] selectItems2) {
        this.selectItems2 = selectItems2;
    }

    public SelectItem[] getSelectItems3() {
        return selectItems3;
    }

    public void setSelectItems3(SelectItem[] selectItems3) {
        this.selectItems3 = selectItems3;
    }
}
