/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author fabia
 */

import ec.com.centrosurcall.datos.DAO.AgendaaplisDAO;
import ec.com.centrosurcall.datos.DAO.DateaplisDAO;
import ec.com.centrosurcall.datos.DAO.EquiposDAO;
import ec.com.centrosurcall.datos.DAO.TipoEquiposDAO;
import ec.com.centrosurcall.datos.DAO.UbicacionesDAO;
import ec.com.centrosurcall.datos.modelo.Agendaapli;
import ec.com.centrosurcall.datos.modelo.Dateapli;
import ec.com.centrosurcall.datos.modelo.Equipo;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public final class CtrEquipo extends MantenimientoGenerico
  implements MantenimientoInterface, Serializable
{
  CtrSession session;
  private Equipo selEquipo = new Equipo();
  private Dateapli selDateapli;
  private Agendaapli selAgendaapli;
  EquiposDAO equiposDAO = new EquiposDAO();
  TipoEquiposDAO tipoequiposDAO = new TipoEquiposDAO();
  UbicacionesDAO ubicacionesDAO = new UbicacionesDAO();
  String opcionBoton = "";
  SelectItem item = new SelectItem();
  private List<Dateapli> listDateapli;
  private List<Agendaapli> listAgendaapli;
  private int idDateapliSeleccionado;
  private int idAgendaapliSeleccionado;
  private String vinFilter;
  private String nombre;
  private String descripcion;
  private String ipacceso;

  public CtrEquipo()
  {
    this.session = ((CtrSession)FacesUtils.getManagedBean("ctrSession"));
    setOpcionBoton("Grabar");
    this.session.setTituloSeccion("Listado");
    this.item.setLabel("-SELECCIONE-");
    this.item.setValue(Integer.valueOf(-1));
//    if ((!this.parametroSession.getEsNuevo().booleanValue()) || (
//      (this.parametroSession.getEsEditar().booleanValue()) && (this.parametroSession.getObjeto() != null))) {
//      cargoEquipoEditar();
//    }
    if (this.parametroSession.getEsEditar() && this.parametroSession.getObjeto() != null) {
            cargoEquipoEditar();
        }
    cargarColumnasGrupo();
    cargarEquipos();
    setMensajeConfirmacion("Seguro desea grabar este equipo?");
    confPop();
  }

  public void cargarEquipos() {
    this.listaBaseObjetos.clear();
    this.listaObjetos.clear();
    List listaEquipo = this.equiposDAO.getEquipo();
    if (listaEquipo != null) {
      this.listaBaseObjetos.addAll(listaEquipo);
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargoEquipoEditar() {
    boolean existeObjeto = false;
    try {
      if (this.session.getObjeto().getClass().getName().contains("Equipo")) {
        if (((Equipo)this.session.getObjeto()).getId() > 0)
          existeObjeto = true;
      }
      else {
        this.parametroSession.setEsEditar(Boolean.valueOf(false));
        this.parametroSession.setEsNuevo(Boolean.valueOf(false));
      }
    } catch (Exception e) {
      System.out.println(e.getCause().getMessage());
    }
    if (existeObjeto) {
      this.opcionBoton = "Grabar";
      this.selEquipo = ((Equipo)this.session.getObjeto());
      cargoDateapli();
      cargoAgendaapli();
    }
  }

  public void cargoDateapli() {
    this.listDateapli = new DateaplisDAO().getDateapliEquipoJoin(this.selEquipo.getId());
  }

  public void cargoAgendaapli() {
    this.listAgendaapli = new AgendaaplisDAO().getAgendaapliEquipoJoin(this.selEquipo.getId());
  }

  public void confPop() {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro desea registrar el equipo??");
    setMensajeCorrecto("Registro grabado");
    setMensajeError("Error al Registrar");
  }

  public boolean grabar()
  {
    boolean exito = false;
    if (this.parametroSession.getEsNuevo().booleanValue()) {
      this.selEquipo.setJerarquia("Agencia");
      this.selEquipo.setIdTipoequipo(Integer.valueOf(1));
      this.selEquipo.setIdUbicacion(Integer.valueOf(0));
      exito = this.equiposDAO.saveEquipo(this.selEquipo, 1);
    } else {
      exito = this.equiposDAO.saveEquipo(this.selEquipo, 0);
    }
    return exito;
  }

  public void buscarWsql()
  {
    List respuesta = new ArrayList();
    this.parametroSession.setNumPaginas(1);
    this.listaObjetos.clear();
    if (!this.whereSql.equals("")) {
        for (Object obj : listaBaseObjetos) {
                Equipo cl = (Equipo) obj;
                if (cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase()) || cl.getNombre().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }        
      this.listaObjetos.addAll(respuesta);
    } else {
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargarColumnasGrupo()
  {
    this.listaColumnas.put("nombre", "Hostname");
    this.listaColumnas.put("descripcion", "Descripcion");
    this.listaColumnas.put("ipacceso", "IP");
  }

  public String regresar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.session.setObjeto(null);
    this.session.setTituloSeccion("Listado");
    return "frmAdministracionEquipo";
  }

  public void auditoria()
  {
    try {
      this.selEquipo = ((Equipo)this.objetoSeleccionado);
    } catch (Exception e) {
      System.out.println(e.getCause().getMessage());
    }
    if (this.selEquipo.getId() > 0) {
      this.creado = this.selEquipo.getNombre();
      setCreado(this.creado);
      this.fechacreacion = new Date().toString();
      setActualizado("");
      setFechaactualizacion("");
      this.mostrarAuditoriaVal = true;
      mostrarAuditoria();
    }
  }

  public String refrescar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.whereSql = "";
    cargarEquipos();
    this.session.setSeleccionado(Boolean.valueOf(false));
    this.objetoSeleccionado = new Object();
    return "";
  }

  public boolean isExitograbar()
  {
    return super.isExitograbar();
  }

  public void imprimir()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public String editar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(true));
    this.session.setTituloSeccion("Editar");
    List objetoEditar = sacarSeleccionado();
    if (objetoEditar.size() > 0) {
      this.parametroSession.setObjeto(objetoEditar.get(0));
      try {
        if (((Equipo)objetoEditar.get(0)).getId() > 0) {
          this.parametroSession.setBoton("btnActualizar");
          return "frmEquipo";
        }
        return "";
      }
      catch (Exception e) {
        return "";
      }
    }
    return "";
  }

  public String nuevo()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(true));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.parametroSession.setBoton("btnGrabar");
    setOpcionBoton("nuevo");
    this.session.setTituloSeccion("Nuevo");
    return "frmEquipo";
  }

  public void confPopEliminar() {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro quiere eliminar?");
    setMensajeCorrecto("Eliminacion exitosa!");
    setMensajeError("Existen errores en:");
  }

  public boolean eliminar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    boolean exito = false;
    this.listaErrores = new ArrayList();
    List listaAeliminar = sacarSeleccionado();
    if (listaAeliminar.size() > 0) {
      this.listaErrores = this.equiposDAO.eliminarEquipo(listaAeliminar, this.listaObjetos);
    }
    if (this.listaErrores.isEmpty()) {
      exito = true;
      cargarEquipos();
    }
    return exito;
  }

  public List<Equipo> sacarSeleccionado() {
    Equipo selectEquipo = new Equipo();
    List listadoSeleccionado = new ArrayList();
    boolean exito = false;
    for (Iterator i$ = this.listaObjetos.iterator(); i$.hasNext(); ) { Object o = i$.next();
      selectEquipo = (Equipo)o;
      if (selectEquipo.isSeleccionado()) {
        listadoSeleccionado.add(selectEquipo);
        exito = true;
        break;
      }
    }
    if (exito) {
      return listadoSeleccionado;
    }
    return listadoSeleccionado;
  }

  public void removeDateapli()
  {
    if (new DateaplisDAO().eliminarDateapli(this.selDateapli))
      cargoDateapli();
  }

  public void removeAgendaapli() {
    if (new AgendaaplisDAO().eliminarAgendaapli(this.selAgendaapli))
      cargoAgendaapli();
  }

  public String getVinFilter()
  {
    return this.vinFilter;
  }

  public void setVinFilter(String vinFilter) {
    this.vinFilter = vinFilter;
  }

  public Equipo getSelEquipo()
  {
    return this.selEquipo;
  }

  public void setSelEquipo(Equipo selEquipo) {
    this.selEquipo = selEquipo;
  }

  public String getOpcionBoton() {
    return this.opcionBoton;
  }

  public void setOpcionBoton(String opcionBoton) {
    this.opcionBoton = opcionBoton;
  }

  public String getNombre()
  {
    return this.nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getDescripcion() {
    return this.descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public String getIpacceso() {
    return this.ipacceso;
  }

  public void setIpacceso(String ipacceso) {
    this.ipacceso = ipacceso;
  }

  public List<Dateapli> getListDateapli() {
    return this.listDateapli;
  }

  public void setListDateapli(List<Dateapli> listDateapli) {
    this.listDateapli = listDateapli;
  }

  public List<Agendaapli> getListAgendaapli() {
    return this.listAgendaapli;
  }

  public void setListAgendaapli(List<Agendaapli> listAgendaapli) {
    this.listAgendaapli = listAgendaapli;
  }

  public int getIdDateapliSeleccionado() {
    return this.idDateapliSeleccionado;
  }

  public void setIdDateapliSeleccionado(int idDateapliSeleccionado) {
    this.idDateapliSeleccionado = idDateapliSeleccionado;
    for (Dateapli o : this.listDateapli)
    {
      if (o.getId().intValue() == idDateapliSeleccionado) {
        this.selDateapli = o;
        break;
      }
    }
  }

  public int getIdAgendaapliSeleccionado() {
    return this.idAgendaapliSeleccionado;
  }

  public void setIdAgendaapliSeleccionado(int idAgendaapliSeleccionado) {
    this.idAgendaapliSeleccionado = idAgendaapliSeleccionado;
    for (Agendaapli o : this.listAgendaapli)
    {
      if (o.getId().intValue() == idAgendaapliSeleccionado) {
        this.selAgendaapli = o;
        break;
      }
    }
  }
}