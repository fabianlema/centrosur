/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.HistorialDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.DAO.TemporalDAO;
import ec.com.centrosurcall.datos.modelo.Historial;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.datos.modelo.Temporal;
import ec.com.centrosurcall.datos.modelo.Usuario;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;

import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ViewScoped;
//import javax.faces.model.SelectItem;
import javax.mail.internet.InternetAddress;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

/**
 *
 * @author TaurusTech
 */
@ManagedBean
@ViewScoped
public final class CtrCampaniasMensajeDefinido extends MantenimientoGenerico
  implements Serializable
{
  private List<Object> objColumna = new ArrayList();
  private TemporalDAO temporalDAO = new TemporalDAO();
  private HistorialDAO historialDAO = new HistorialDAO();
  private boolean accion;
  private String msjadv = ""; private String msjinf = "";

  public CtrCampaniasMensajeDefinido() {
    System.out.println("Contructor Definido");
  }

  public void processFileUpload(FileUploadEvent event) throws Exception
  {
    UploadedFile item = event.getUploadedFile();
    List cellDataList = new ArrayList();
    try {
      POIFSFileSystem fsFileSystem = new POIFSFileSystem(item.getInputStream());
      HSSFWorkbook workBook = new HSSFWorkbook(fsFileSystem);
      HSSFSheet hssfSheet = workBook.getSheetAt(0);

      Iterator rowIterator = hssfSheet.rowIterator();

      while (rowIterator.hasNext()) {
        HSSFRow hssfRow = (HSSFRow)rowIterator.next();
        Iterator iterator = hssfRow.cellIterator();
        List cellTempList = new ArrayList();
        while (iterator.hasNext()) {
          HSSFCell hssfCell = (HSSFCell)iterator.next();
          hssfCell.setCellType(1);
          cellTempList.add(hssfCell);
        }
        cellDataList.add(cellTempList);
      }
    } catch (Exception e) {
      System.out.println("Error processFileUpload: " + e);
      e.printStackTrace();
    }
    printToConsole(cellDataList);
  }

  private void printToConsole(List cellDataList)
  {
    List listColumnas = new ArrayList();
    try {
      for (int i = 1; i < cellDataList.size(); i++)
      {
        List cellTempList = (List)cellDataList.get(i);

        String[] temp = new String[2];

        for (int j = 0; j < 2; j++) {
            if (cellTempList.size()>0){
                if (j < cellTempList.size())
                    temp[j] = ((HSSFCell)cellTempList.get(j) != null ? ((HSSFCell)cellTempList.get(j)).toString().trim() : "");
                else {
                    temp[j] = "";
                }               
            }          
        }
        if (cellTempList.size()>0){
            listColumnas.add(temp);                    
        }
      }
      if (listColumnas.size() > 0)
        this.objColumna = listColumnas;
      else
        this.objColumna = null;
    }
    catch (Exception e) {
      System.out.println("Error printToConsole: " + e);
      e.printStackTrace();
    }
  }

  public String regresar()
  {
    return "frmMensajeria";
  }

  public String getMsjadv() {
    return this.msjadv;
  }

  public void setMsjadv(String msjadv) {
    this.msjadv = msjadv;
  }

  public List<Object> getObjColumna() {
    return this.objColumna;
  }

  public void setObjColumna(List<Object> objColumna) {
    this.objColumna = objColumna;
  }

  public String getMsjinf() {
    return this.msjinf;
  }

  public void setMsjinf(String msjinf) {
    this.msjinf = msjinf;
  }

  public boolean isExitograbar()
  {
    return super.isExitograbar();
  }

  public void confPop() {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro desea enviar los mensajes??");
    setMensajeCorrecto("Mensajes enviados");
    setMensajeError("Error al enviar, probablemente no seleccionó la columna del número");
  }

    @Override
    public boolean grabar() {
        boolean bandera = true;
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Parametro prefijos = new ParametrosDAO().getParametroByAtributo("PREFIJOSVALMSN");

                        boolean validado = true;
                        for (int i = 0; i < objColumna.size(); i++) {
                            String msn = ((String[]) (String[]) objColumna.get(i))[1].trim();
                            String numero = ((String[]) (String[]) objColumna.get(i))[0].trim();
                            validado = true;
                            if (numero.length() == 10) {
                                String[] listaPrefijos = prefijos.getValor().split(",");
                                for (int k = 1; k < listaPrefijos.length; k++) {
                                    if (!numero.startsWith(listaPrefijos[k])) {
                                        validado = false;
                                        break;
                                    }
                                }
                                try {
                                    Integer.parseInt(numero);
                                } catch (NumberFormatException e) {
                                    validado = false;
                                }
                                if (validado) {
                                    Temporal histTemp = new Temporal(numero, msn, Boolean.valueOf(true), parametroSession.getUsuario());
                                    temporalDAO.saveTemporal(histTemp, 3);
                                } else {
                                    historialDAO.saveHistorial(new Historial(new Date(), numero, msn, "1", Integer.valueOf(1), Integer.valueOf(1), parametroSession.getUsuario()), 3);
                                }
                            } else if (numero.length() != 0) {
                                historialDAO.saveHistorial(new Historial(new Date(), numero, msn, "1", Integer.valueOf(1), Integer.valueOf(1), parametroSession.getUsuario()), 3);
                            }
                        }
                        historialDAO.getSPBlackList();
                        ejecutarHilo();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            return bandera;
        } catch (Exception e) {
            e.printStackTrace();
            return !bandera;
        }
    }

    public void ejecutarHilo() {
        //Realiza el envío de mensajes a travez de un thread extrayendo la informacion de la base de datos
        System.out.println("Ingresa Hilo");
        accion = true;
        //Parametro svr = new ParametrosDAO().getParametroByAtributo("DELAY");
        //final int sleep = Integer.parseInt(svr.getValor());
        Parametro svr = new ParametrosDAO().getParametroByAtributo("DELAY");
        final int sleep = Integer.parseInt(svr.getValor());
        svr = new ParametrosDAO().getParametroByAtributo("DELAY10");
        final int sleep10 = Integer.parseInt(svr.getValor());
        svr = new ParametrosDAO().getParametroByAtributo("NUMEROMSN");
        final int numeromsns = Integer.parseInt(svr.getValor());
        svr = new ParametrosDAO().getParametroByAtributo("DELAYNUMMSN");
        final int delaymsn = Integer.parseInt(svr.getValor());
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String mensaje = "";

                    int contador = 0;
                    int numeromsn = 0;
                    numeromsn = numeromsns / temporalDAO.getUsuariosTemporal();
                    Usuario uTemp = parametroSession.getUsuario();
                    while (accion) {
                        List tmp = temporalDAO.getSPListaTemporal(uTemp.getId());
                        if (tmp.size() > 0) {
                            InternetAddress[] telefonos = new InternetAddress[tmp.size()];

                            contador += tmp.size();
                            for (int i = 0; i < tmp.size(); i++) {
                                if (i == 0) {
                                    mensaje = ((Temporal) tmp.get(i)).getMensaje().trim();
                                }
                                telefonos[i] = new InternetAddress(((Temporal) tmp.get(i)).getNumero() + "@" + parametroSession.getDominio2n());
                            }

                            String mensajeDevuelto = parametroSession.getSmtp2sms().send2N(telefonos, mensaje.length() > 160 ? mensaje.substring(0, 160) : mensaje);

                            if (mensajeDevuelto.trim().equals("0")) {
                                for (int i = 0; i < tmp.size(); i++) {
                                    historialDAO.saveHistorial(new Historial(new Date(), ((Temporal) tmp.get(i)).getNumero(), ((Temporal) tmp.get(i)).getMensaje(), "1", Integer.valueOf(0), Integer.valueOf(1), uTemp), 3);
                                }
                                Thread.sleep(sleep);
                            } else {
                                for (int i = 0; i < tmp.size(); i++) {
                                    historialDAO.saveHistorial(new Historial(new Date(), ((Temporal) tmp.get(i)).getNumero(), ((Temporal) tmp.get(i)).getMensaje(), "1", Integer.valueOf(1), Integer.valueOf(1), uTemp), 3);
                                }
                                System.out.println("Mensaje devuelto 2N: " + mensajeDevuelto);
                                Thread.sleep(sleep10);
                            }

                            if (numeromsn - contador <= 0) {
                                Thread.sleep(delaymsn);
                                numeromsn = numeromsns / temporalDAO.getUsuariosTemporal();
                                contador = 0;
                            }
                        } else {
                            accion = false;
                            System.out.println("La cola de envío se completó");
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Error en el envío masivo");
                }
            }
        }).start();
    }
}