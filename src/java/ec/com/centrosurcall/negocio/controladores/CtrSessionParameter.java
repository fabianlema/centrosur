/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.utils.FacesUtils;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author carlosguaman65
 */
@ManagedBean(name="parameter")
@SessionScoped
public class CtrSessionParameter {
    private CtrSession session;
    public void  resetParameter(){
       CtrSession  session = (CtrSession) FacesUtils.getManagedBean("ctrSession"); 
       session.getSelectedEventoProcesadoList().clear();
    }


}

