/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.ColumnaDAO;
import ec.com.centrosurcall.datos.DAO.HistorialDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.DAO.TemporalDAO;
import ec.com.centrosurcall.datos.modelo.Columna;
import ec.com.centrosurcall.datos.modelo.Historial;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.datos.modelo.Temporal;
import ec.com.centrosurcall.datos.modelo.Usuario;
//import ec.com.centrosurcall.smtp.Smtp2sms;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;

import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.mail.internet.InternetAddress;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;

/**
 *
 * @author TaurusTech
 */
@ManagedBean
@ViewScoped
public final class CtrCampaniasMensajeMultiple extends MantenimientoGenerico
        implements Serializable {

    private int numColumna;
    private int idColumna;
    private int idColumnaNumero = -1;
    private int indexSelected;
    private String mensaje = "";
    private String[] texto;
    private int[] colSeleccionada;
    private List<Object> objColumna = new ArrayList();
    private List<Columna> objListColumna;
    private SelectItem[] selectItemColumna;
    private SelectItem[] selectedColumnas;
    private TemporalDAO temporalDAO = new TemporalDAO();
    private HistorialDAO historialDAO = new HistorialDAO();
    private ColumnaDAO columnaDAO = new ColumnaDAO();
    private boolean accion;
    private String msjadv = "";
    private String msjinf = "";

    public CtrCampaniasMensajeMultiple() {
        System.out.println("Contructor Multiple");
        cargarColumnas();
    }

    public void cargarColumnas() {
        this.objListColumna = this.columnaDAO.getColumnas();
        this.texto = new String[this.objListColumna.size() + 1];
        this.colSeleccionada = new int[this.objListColumna.size() + 1];
        int cont;
        if ((this.objListColumna != null) && (this.objListColumna.size() > 0)) {
            this.selectItemColumna = new SelectItem[this.objListColumna.size() + 1];
            this.selectItemColumna[0] = new SelectItem(Integer.valueOf(0), "--SELECCIONE--", "--SELECCIONE--");
            cont = 1;
            for (Columna cl : this.objListColumna) {
                this.selectItemColumna[cont] = new SelectItem(Integer.valueOf(cl.getOrden()), cl.getNombre(), cl.getNombre());
                cont++;
            }
        }
    }

    public void processFileUpload(FileUploadEvent event) throws Exception {
        UploadedFile item = event.getUploadedFile();
        List cellDataList = new ArrayList();
        try {
            POIFSFileSystem fsFileSystem = new POIFSFileSystem(item.getInputStream());
            HSSFWorkbook workBook = new HSSFWorkbook(fsFileSystem);
            HSSFSheet hssfSheet = workBook.getSheetAt(0);

            Iterator rowIterator = hssfSheet.rowIterator();

            while (rowIterator.hasNext()) {
                HSSFRow hssfRow = (HSSFRow) rowIterator.next();
                Iterator iterator = hssfRow.cellIterator();
                List cellTempList = new ArrayList();
                while (iterator.hasNext()) {
                    HSSFCell hssfCell = (HSSFCell) iterator.next();
                    hssfCell.setCellType(1);
                    cellTempList.add(hssfCell);
                }
                cellDataList.add(cellTempList);
            }
        } catch (Exception e) {
            System.out.println(new StringBuilder().append("Error processFileUpload: ").append(e).toString());
            e.printStackTrace();
        }
        printToConsole(cellDataList);
    }

    private void printToConsole(List cellDataList) {
        List listColumnas = new ArrayList();
        try {
            for (int i = 1; i < cellDataList.size(); i++) {
                List cellTempList = (List) cellDataList.get(i);

                String[] temp = new String[this.objListColumna.size()];

                for (int j = 0; j < this.objListColumna.size(); j++) {
                    if (cellTempList.size()>0){
                        if (j < cellTempList.size()) {
                            temp[j] = ((HSSFCell) cellTempList.get(j) != null ? ((HSSFCell) cellTempList.get(j)).toString().trim() : "");
                        } else {
                            temp[j] = "";
                        }                        
                    }                    
                }
                if (cellTempList.size()>0){
                    listColumnas.add(temp);                    
                }
            }
            if (listColumnas.size() > 0) {
                this.objColumna = listColumnas;
            } else {
                this.objColumna = null;
            }
        } catch (Exception e) {
            System.out.println(new StringBuilder().append("Error printToConsole: ").append(e).toString());
            e.printStackTrace();
        }
    }

    public String regresar() {
        return "frmMensajeria";
    }

    public void seleccionarColumna() {
        this.mensaje = (this.texto[0] != null ? this.texto[0] : "");
        for (int i = 1; i < this.objListColumna.size() + 1; i++) {
            this.mensaje = new StringBuilder().append(this.mensaje).append(" ").append(this.colSeleccionada[i] != 0 ? ((String[]) (String[]) this.objColumna.get(0))[(this.colSeleccionada[i] - 1)].trim() : "").append(" ").append(this.texto[i] != null ? this.texto[i] : "").append(" ").toString();
        }
        this.mensaje = this.mensaje.trim();
        if (this.mensaje.length() > 160) {
            this.msjinf = "";
            this.msjadv = "Es posible que los mensajes a enviar sobrepasen de los 160 caracteres permitidos";
        } else {
            this.msjinf = new StringBuilder().append("El mensaje de prueba tiene ").append(this.mensaje.length()).append(" caracteres").toString();
            this.msjadv = "";
        }
    }

    public void prueba(int indexSelected) {
        this.indexSelected = indexSelected;
    }

    public int getNumColumna() {
        return this.numColumna;
    }

    public void setNumColumna(int numColumna) {
        this.numColumna = numColumna;
    }

    public String getMensaje() {
        return this.mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public SelectItem[] getSelectedColumnas() {
        return this.selectedColumnas;
    }

    public void setSelectedColumnas(SelectItem[] selectedColumnas) {
        this.selectedColumnas = selectedColumnas;
    }

    public List<Object> getObjColumna() {
        return this.objColumna;
    }

    public void setObjColumna(List<Object> objColumna) {
        this.objColumna = objColumna;
    }

    public SelectItem[] getSelectItemColumna() {
        return this.selectItemColumna;
    }

    public void setSelectItemColumna(SelectItem[] selectItemColumna) {
        this.selectItemColumna = selectItemColumna;
    }

    public int getIdColumna() {
        return this.idColumna;
    }

    public void setIdColumna(int idColumna) {
        if (idColumna != 0) {
            this.colSeleccionada[this.indexSelected] = idColumna;
        } else {
            this.colSeleccionada[this.indexSelected] = 0;
        }
        this.idColumna = idColumna;
    }

    public List<Columna> getObjListColumna() {
        return this.objListColumna;
    }

    public void setObjListColumna(List<Columna> objListColumna) {
        this.objListColumna = objListColumna;
    }

    public String[] getTexto() {
        return this.texto;
    }

    public void setTexto(String[] texto) {
        this.texto = texto;
    }

    public String getMsjadv() {
        return this.msjadv;
    }

    public void setMsjadv(String msjadv) {
        this.msjadv = msjadv;
    }

    public String getMsjinf() {
        return this.msjinf;
    }

    public void setMsjinf(String msjinf) {
        this.msjinf = msjinf;
    }

    public int getIdColumnaNumero() {
        return this.idColumnaNumero;
    }

    public void setIdColumnaNumero(int idColumnaNumero) {
        this.idColumnaNumero = idColumnaNumero;
    }

    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    public void confPop() {
        this.banderaUna = false;
        setMensajeConfirmacion("Seguro desea enviar los mensajes??");
        setMensajeCorrecto("Mensajes enviados");
        setMensajeError("Error al enviar, probablemente no seleccionó la columna del número");
    }

    @Override
    public boolean grabar() {
        //Graba los datos del archivo excel a la base de datos en una tabla temporal
        boolean bandera = true;
        try {
            //TemporalDAO temporalDAO = new TemporalDAO();            
            //msjerror = "";
            setMensajeError("Error al enviar, probablemente no seleccionó la columna del número");
            if (idColumnaNumero != -1 && idColumnaNumero != 0) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            boolean validado = true;

                            Parametro prefijos = new ParametrosDAO().getParametroByAtributo("PREFIJOSVALMSN");
                            System.out.println(new StringBuilder().append("NuMEOR--").append(objColumna.size()).toString());
                            for (int i = 0; i < objColumna.size(); i++) {
                                String msn = texto[0] != null ? texto[0] : "";
                                for (int j = 1; j < objListColumna.size() + 1; j++) {
                                    msn = new StringBuilder().append(msn).append(" ").append(colSeleccionada[j] != 0 ? ((String[]) (String[]) objColumna.get(i))[(colSeleccionada[j] - 1)].trim() : "").append(" ").append(texto[j] != null ? texto[j].trim() : "").append(" ").toString();
                                }
                                String numero = ((String[]) (String[]) objColumna.get(i))[(idColumnaNumero - 1)];
                                validado = true;
                                if (numero.length() == 10) {
                                    String[] listaPrefijos = prefijos.getValor().split(",");
                                    for (int k = 1; k < listaPrefijos.length; k++) {
                                        if (!numero.startsWith(listaPrefijos[k])) {
                                            validado = false;
                                            break;
                                        }
                                    }
                                    try {
                                        Integer.parseInt(numero);
                                    } catch (NumberFormatException e) {
                                        validado = false;
                                    }
                                    if (validado) {
                                        Temporal histTemp = new Temporal(numero, msn, Boolean.valueOf(true), parametroSession.getUsuario());
                                        temporalDAO.saveTemporal(histTemp, 3);
                                    } else {
                                        historialDAO.saveHistorial(new Historial(new Date(), numero, msn, "1", Integer.valueOf(1), Integer.valueOf(1), parametroSession.getUsuario()), 3);
                                    }

                                } else if (numero.length() != 0) {
                                    historialDAO.saveHistorial(new Historial(new Date(), numero, msn, "1", Integer.valueOf(1), Integer.valueOf(1), parametroSession.getUsuario()), 3);
                                }

                            }

                            historialDAO.getSPBlackList();
                            ejecutarHilo();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                //if (bandera == false) setMensajeError("No se puede enviar mensajes a los siguientes números:\n" + msjerror + "\nCorrija el archivo y vuelva a cargar");
                //else                 
                return bandera;
            } else {
                return !bandera;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return !bandera;
        }
    }

    public void ejecutarHilo() {
        //Realiza el envío de mensajes a travez de un thread extrayendo la informacion de la base de datos
        accion = true;
        Parametro svr = new ParametrosDAO().getParametroByAtributo("DELAY");
        final int sleep = Integer.parseInt(svr.getValor());
        svr = new ParametrosDAO().getParametroByAtributo("DELAY10");
        final int sleep10 = Integer.parseInt(svr.getValor());
        svr = new ParametrosDAO().getParametroByAtributo("NUMEROMSN");
        final int numeromsns = Integer.parseInt(svr.getValor());
        svr = new ParametrosDAO().getParametroByAtributo("DELAYNUMMSN");
        final int delaymsn = Integer.parseInt(svr.getValor());
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InternetAddress[] telefono = new InternetAddress[1];
                    int contador = 0;
                    int numeromsn = 0;
                    numeromsn = numeromsns / temporalDAO.getUsuariosTemporal();
                    Usuario uTemp = parametroSession.getUsuario();
                    while (accion) {
                        List listTemp = temporalDAO.getSPFirstTemporal(uTemp.getId());
                        Temporal tmp = listTemp.size() > 0 ? (Temporal) listTemp.get(0) : null;

                        if (tmp != null) {
                            String mensaje = tmp.getMensaje().trim();
                            String numero = tmp.getNumero();
                            telefono[0] = new InternetAddress(numero + "@" + parametroSession.getDominio2n());
                            String mensajeDevuelto = parametroSession.getSmtp2sms().send2N(telefono, mensaje.length() > 160 ? mensaje.substring(0, 160) : mensaje);

                            if (mensajeDevuelto.trim().equals("0")) {
                                historialDAO.saveHistorial(new Historial(new Date(), numero, mensaje, "1", Integer.valueOf(0), Integer.valueOf(1), uTemp), 3);
                                Thread.sleep(sleep);
                            } else {
                                historialDAO.saveHistorial(new Historial(new Date(), numero, mensaje, "1", Integer.valueOf(1), Integer.valueOf(1), uTemp), 3);
                                System.out.println("Mensaje devuelto 2N: " + mensajeDevuelto);
                                Thread.sleep(sleep10);
                            }

                            contador += 1;

                            if (numeromsn - contador <= 0) {
                                Thread.sleep(delaymsn);
                                numeromsn = numeromsns / temporalDAO.getUsuariosTemporal();
                                contador = 0;
                            }
                        } else {
                            accion = false;
                            System.out.println("La cola de envío se completó");
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Error en el envío masivo");
                }
            }
        }).start();
    }
}