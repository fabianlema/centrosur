/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.ColaChatDAO;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import ec.com.centrosurcall.datos.modelo.ColaChat;
import ec.com.centrosurcall.social.RedesSociales;
import ec.com.centrosurcall.utils.FacesUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
//import org.openide.util.Exceptions;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrRedesSociales {

    CtrSession session;
    private ColaChatDAO colaChatDao = new ColaChatDAO();
    private ColaChat redSocial;
    private RedesSociales rs = new RedesSociales();
    private String texto = "NO HAY MENSAJES PARA RESPONDER";
    private String url;
    private String urlRedirect;
    private String tipored;
    private String respuestaPre;
    private String urlChat;
    private String textoError="";
    private Boolean face;
    private Boolean isEstado = false;
    private Boolean comoInterno;
    private Boolean dioErrorEnvio = false;

    public CtrRedesSociales() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        session.setTituloSeccion("Listado");
        
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        //CtrSession session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Runtime r = Runtime.getRuntime();
        //Consultas consulta = new Consultas();
        String codigoivr = request.getParameter("codigoivr");
        try {
            Integer.parseInt(codigoivr);
        } catch (NumberFormatException nfe) {
            codigoivr = "0";
        }
        if (codigoivr.equals("78")){
            cargarRedesSociales("twitter_account");
        }else if (codigoivr.equals("79")){
            cargarRedesSociales("facebook");
        }
    }       

    public void cargarRedesSociales(String redSocialDB) {
        redSocial = colaChatDao.getFirstColaTwitter(redSocialDB);
        if (redSocial != null) {
            redSocial.setEstado(true);
            redSocial.setIdUsuario(session.getUsuario());
            colaChatDao.saveColaChat(redSocial, 0);            
            url = redSocial.getUrlContacto();
            urlRedirect = redSocial.getUrlContacto();;
            texto = rs.getMensaje(redSocial.getIdSocialContact());
            //System.out.println(texto);
            respuestaPre = "";
            if (redSocial.getTipo().equals("twitter_account")) {
                tipored = "Twitter";
                face = false;
                isEstado = false;
                if (redSocial.getTitulo().startsWith("Tweet")) {
                    respuestaPre = "@" + redSocial.getAutor() + " ";
                    isEstado = true;
                }
                urlRedirect = "/CentroSurCM/faces/pages/redirectTwitter.xhtml?url="+redSocial.getUrlContacto();
            } else if (redSocial.getTipo().equals("facebook")) {
                tipored = "Facebook";
                face = true;
            } else {
                tipored = "Chat";
                face = false;
            }
        }    
        
        String pattern = "http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+";
        String cadena = "";
        List<String> result = new ArrayList<String>();
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(texto);
        
        Matcher matcher = r.matcher(texto);
        while (matcher.find()) {
            result.add(matcher.group());
        }
        for (int i=0; i<result.size();i++){
            cadena = "<a href=\"" + result.get(i) + "\" target=\"_blank\">" + result.get(i) + "</a>";
            texto = texto.replace(result.get(i), cadena);
        }
        //System.out.println(texto);
    }

    public String eliminarMails() {
        try {
            redSocial.setAtendido(false);
            colaChatDao.saveColaChat(redSocial, 0);
            return "frmRegistroLlamadas";
        } catch (Exception ex) {
            System.out.println(ex);
            //Exceptions.printStackTrace(ex);
            return "frmRegistroLlamadas";
        }
    }

    public String responder() {
        int resultado = 1;
        try {
            if (redSocial.getTipo().equals("twitter_account")){
                if (redSocial.getTitulo().startsWith("Tweet")){
                    if (comoInterno){
                        String resultadoTmp = rs.responderTweetDirect(respuestaPre, redSocial.getAutor());
                        if (resultadoTmp.equals("0")){
                            resultado=0;
                        }else{
                            dioErrorEnvio = true;
                            textoError = resultadoTmp;
                        }
                    }else{
                        resultado = rs.responderTweetStatus(respuestaPre, redSocial.getAutor(), redSocial.getUrlContacto());
                    }                    
                }else{
                    String resultadoTmp = rs.responderTweetDirect(respuestaPre, redSocial.getAutor());
                    if (resultadoTmp.equals("0")){
                        resultado=0;
                    }
                }            
            }else if (redSocial.getTipo().equals("facebook")){
                resultado = rs.responderFaceStatus(respuestaPre, redSocial.getIdSocialContact());
            }else resultado=1;
            
            if (resultado==0){
                redSocial.setAtendido(true);
                redSocial.setRespuesta(respuestaPre);
                colaChatDao.saveColaChat(redSocial, 0);
                return "frmRegistroLlamadas";
            }else{
                return "";
            }            
        } catch (Exception ex) {
            System.out.println(ex);
            //Exceptions.printStackTrace(ex);
            return "frmRegistroLlamadas";
        }
    }

    public String darLike() {
        int resultado = 0;
        try {
            resultado = rs.responderFaceLike(redSocial.getIdSocialContact());
            redSocial.setRespuesta("");
            if (resultado==0){
                redSocial.setAtendido(true);                
                colaChatDao.saveColaChat(redSocial, 0);
            }            
            return "frmRegistroLlamadas";
        } catch (Exception ex) {
            System.out.println(ex);
            //Exceptions.printStackTrace(ex);
            return "frmRegistroLlamadas";
        }
    }

    public String responderAndLike() {
        int resultado = 0;
        try {
            resultado = rs.responderFaceStatus(respuestaPre, redSocial.getIdSocialContact());
            resultado = rs.responderFaceLike(redSocial.getIdSocialContact());
            redSocial.setRespuesta(respuestaPre);            
            if (resultado==0){
                redSocial.setAtendido(true);                
                colaChatDao.saveColaChat(redSocial, 0);
            }            
            return "frmRegistroLlamadas";
        } catch (Exception ex) {
            System.out.println(ex);
            //Exceptions.printStackTrace(ex);
            return "frmRegistroLlamadas";
        }
    }

    public boolean grabar() {
        boolean exito = false;
//        TipoLlamada tl = tipoLlamadaDAO.getTipoLlamadaByID(tipoLlamadaId);
//        selEvento.setTipoLlamada(tl);
//        selEvento.setEstado(1);
        return exito;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTipored() {
        return tipored;
    }

    public void setTipored(String tipored) {
        this.tipored = tipored;
    }

    public Boolean getFace() {
        return face;
    }

    public void setFace(Boolean face) {
        this.face = face;
    }

    public String getRespuestaPre() {
        return respuestaPre;
    }

    public void setRespuestaPre(String respuestaPre) {
        this.respuestaPre = respuestaPre;
    }

    public String getUrlChat() {
        return urlChat;
    }

    public void setUrlChat(String urlChat) {
        this.urlChat = urlChat;
    }

    public Boolean getIsEstado() {
        return isEstado;
    }

    public void setIsEstado(Boolean isEstado) {
        this.isEstado = isEstado;
    }

    public Boolean getComoInterno() {
        return comoInterno;
    }

    public void setComoInterno(Boolean comoInterno) {
        this.comoInterno = comoInterno;
    }

    public Boolean getDioErrorEnvio() {
        return dioErrorEnvio;
    }

    public void setDioErrorEnvio(Boolean dioErrorEnvio) {
        this.dioErrorEnvio = dioErrorEnvio;
    }

    public String getTextoError() {
        return textoError;
    }

    public void setTextoError(String textoError) {
        this.textoError = textoError;
    }

    public String getUrlRedirect() {
        return urlRedirect;
    }

    public void setUrlRedirect(String urlRedirect) {
        this.urlRedirect = urlRedirect;
    }
}