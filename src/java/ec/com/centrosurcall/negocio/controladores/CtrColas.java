/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.ColasDAO;
import ec.com.centrosurcall.datos.DAO.ColaSkillsDAO;
import ec.com.centrosurcall.datos.modelo.Cola;
import ec.com.centrosurcall.datos.modelo.ColaSkill;
import ec.com.centrosurcall.datos.modelo.ColaSkillId;
import ec.com.centrosurcall.datos.modelo.Skill;
import ec.com.centrosurcall.negocio.campania.CreacionColaCredito;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrColas extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Cola selCola = new Cola();
    ColasDAO ColasDAO = new ColasDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemProductos;
    private SelectItem[] selectItemPrioridades;
    private SelectItem[] selectItemSkills;
    private List<Object> objSkill;
    private List<Cola> objSkill2 = new ArrayList<Cola>();
    private List<Object> objProductos;
    private boolean verTipo1 = false;
    private boolean verTipo2 = false;
    private boolean verTipo3 = false;
    private boolean tipo1 = false;
    private Boolean automatico = false;
    private boolean tipo2 = false;
    private boolean tipo3 = false;
    private boolean activo1 = false;
    private boolean activo2 = false;
    private boolean activo3 = false;
    SelectItem item = new SelectItem();
    ColaSkillsDAO colaSkillDAO = new ColaSkillsDAO();
    CreacionColaCredito creacionColaCredito = new CreacionColaCredito();

    public CtrColas() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarColas();
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoColaEditar();
        }
        item.setLabel("-SELECCIONE-");
        item.setValue(null);
        cargarProductos();
        cargarPrioridades();
        cargarSkills();
        setMensajeConfirmacion("Seguro desea grabar la Campaña?");
        confPop();
    }

    public void cargarProductos() {
        selectItemProductos = new SelectItem[1];
        item.setLabel("TODOS");
        item.setValue("TODOS");
        selectItemProductos[0] = item;
        objProductos = consulta.getListSql("select distinct producto from dbo.credito");
        if (objProductos != null && objProductos.size() > 0) {
            int cont = 1;
            selectItemProductos = new SelectItem[objProductos.size() + 1];
            selectItemProductos[0] = item;
            for (Object obj : objProductos) {
                Object cl = (Object) obj;
                selectItemProductos[cont] = new SelectItem(String.valueOf(cl), String.valueOf(cl));
                cont++;
            }
        }
    }

    public void cargarPrioridades() {
        selectItemPrioridades = new SelectItem[10];
        selectItemPrioridades[0] = new SelectItem("1", "1");
        selectItemPrioridades[1] = new SelectItem("2", "2");
        selectItemPrioridades[2] = new SelectItem("3", "3");
        selectItemPrioridades[3] = new SelectItem("4", "4");
        selectItemPrioridades[4] = new SelectItem("5", "5");
        selectItemPrioridades[5] = new SelectItem("6", "6");
        selectItemPrioridades[6] = new SelectItem("7", "7");
        selectItemPrioridades[7] = new SelectItem("8", "8");
        selectItemPrioridades[8] = new SelectItem("9", "9");
        selectItemPrioridades[9] = new SelectItem("10", "10");
    }

    public void cargarSkills() {
        objSkill = consulta.getListHql(null, "Skill", "estado = 1 order by nombreSkill", null);
        if (objSkill != null && objSkill.size() > 0) {
            int cont = 0;
            selectItemSkills = new SelectItem[objSkill.size()];
            for (Object obj : objSkill) {
                Skill cl = (Skill) obj;
                selectItemSkills[cont] = new SelectItem(cl.getId(), cl.getNombreSkill());
                cont++;
            }
        }
    }

    public void cargarColas() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Cola> listaCola = ColasDAO.getColas();
        if (listaCola != null) {
            listaBaseObjetos.addAll(listaCola);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargoColaEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Cola")) {
                if (((Cola) session.getObjeto()).getId() > 0) {
                    Integer idCola = ((Cola) session.getObjeto()).getId();
                    objSkill2 = consulta.getListSql("select a.id from dbo.skill a, dbo.cola_skill b where b.idCola=" + idCola + " and b.idSkill=a.id and estado=1");
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selCola = (Cola) session.getObjeto();
            if(!"0".equals(selCola.getDesdeTipo1())){
                tipo1=true;
                verTipo1=true;
            }
            if(!"0".equals(selCola.getDesdeTipo2())){
                tipo2=true;
                verTipo2=true;
            }
            if(!"0".equals(selCola.getDesdeTipo3())){
                tipo3=true;
                verTipo3=true;
            }
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar la Campaña");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        String tipos = "";
        if (tipo1 == true) {
            tipos += "1,";
        }else{
            selCola.setDesdeTipo1("0");
            selCola.setHastaTipo1("0");
        }
        if (tipo2 == true) {
            tipos += "2,";
        }else{
            selCola.setDesdeTipo2("0");
            selCola.setHastaTipo2("0");
        }
        if (tipo3 == true) {
            tipos += "3";
            selCola.setDesdeTipo3("1");
            selCola.setHastaTipo3("1");
        }else{
            selCola.setDesdeTipo3("0");
            selCola.setHastaTipo3("0");
        }

        selCola.setPorcentaje(Double.parseDouble("0"));
        selCola.setTipo(tipos);
        if (automatico == false) {
            selCola.setAutomatico(0);
        } else {
            selCola.setAutomatico(1);
        }
        selCola.setEstado(0);
        selCola.setActivo(1);
        if (parametroSession.getEsNuevo()) {
            exito = ColasDAO.saveCola(selCola, 1);
            if (exito) {
                List maxId;
                maxId = consulta.getListSql("select max(id) from dbo.cola");
                for (int i = 0; i <= objSkill2.size() - 1; i++) {
                    Skill skill = new Skill();
                    ColaSkill algo = new ColaSkill();
                    ColaSkillId asd = new ColaSkillId();
                    skill.setId(Integer.valueOf(String.valueOf(objSkill2.get(i))));
                    asd.setIdSkill(Integer.valueOf(String.valueOf(objSkill2.get(i))));
                    asd.setIdCola(Integer.valueOf(String.valueOf(maxId.get(0))));
                    algo.setId(asd);
                    colaSkillDAO.saveColaSkill(algo, 1);
                }
                creacionColaCredito.cola = selCola;
                creacionColaCredito.crearCola(0);
            }
        } else {
            exito = ColasDAO.saveCola(selCola, 0);
            if (exito) {
                consulta.getListSql("delete from dbo.cola_skill where idCola=" + selCola.getId());
                for (int i = 0; i <= objSkill2.size() - 1; i++) {
                    ColaSkill algo = new ColaSkill();
                    ColaSkillId asd = new ColaSkillId();
                    asd.setIdSkill(Integer.valueOf(String.valueOf(objSkill2.get(i))));
                    asd.setIdCola(selCola.getId());
                    algo.setId(asd);
                    colaSkillDAO.saveColaSkill(algo, 1);
                }
                creacionColaCredito.cola = selCola;
                creacionColaCredito.crearCola(1);
            }
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Cola> respuesta = new ArrayList<Cola>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Cola cl = (Cola) obj;
                if (cl.getNombre().toUpperCase().contains(whereSql.toUpperCase()) || cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase()) || cl.getProducto().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        listaColumnas.put("nombre", "Nombre");
        listaColumnas.put("descripcion", "Descripcion");
        listaColumnas.put("producto", "Producto");
    }

    @Override
    public String regresar() {
        parametroSession.setEsEditar(false);
        parametroSession.setEsNuevo(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionCampanas";
    }

    @Override
    public void auditoria() {
        try {
            selCola = (Cola) objetoSeleccionado;
        } catch (Exception e) {
            selCola.setId(0);
        }
        if (selCola.getId() > 0) {
            creado = selCola.getNombre();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsEditar(false);
        parametroSession.setEsNuevo(false);
        whereSql = "";
        cargarColas();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Cola> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Cola) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmCampanias";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmCampanias";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsEditar(false);
        parametroSession.setEsNuevo(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Cola> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = ColasDAO.eliminarColas(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarColas();
        }
        return exito;
    }

    public List<Cola> sacarSeleccionado() {
        Cola selectCola = new Cola();
        List<Cola> listadoSeleccionado = new ArrayList<Cola>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectCola = (Cola) o;
            if (selectCola.getSeleccionado()) {
                listadoSeleccionado.add(selectCola);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Cola getSelCola() {
        return selCola;
    }

    public void setSelCola(Cola selCola) {
        this.selCola = selCola;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String nombre;
    private String descripcion;
    private String desde;
    private String hasta;
    private String producto;
    private Integer prioridad;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getHasta() {
        return hasta;
    }

    public void setHasta(String hasta) {
        this.hasta = hasta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    public SelectItem[] getSelectItemSkills() {
        return selectItemSkills;
    }

    public void setSelectItemSkills(SelectItem[] selectItemSkills) {
        this.selectItemSkills = selectItemSkills;
    }

    public SelectItem[] getSelectItemProductos() {
        return selectItemProductos;
    }

    public void setSelectItemProductos(SelectItem[] selectItemProductos) {
        this.selectItemProductos = selectItemProductos;
    }

    public SelectItem[] getSelectItemPrioridades() {
        return selectItemPrioridades;
    }

    public void setSelectItemPrioridades(SelectItem[] selectItemPrioridades) {
        this.selectItemPrioridades = selectItemPrioridades;
    }

    public List<Object> getObjSkill() {
        return objSkill;
    }

    public void setObjSkill(List<Object> objSkill) {
        this.objSkill = objSkill;
    }

    public List<Object> getObjParametro() {
        return objProductos;
    }

    public void setObjParametro(List<Object> objProductos) {
        this.objProductos = objProductos;
    }

    public boolean isTipo1() {
        return tipo1;
    }

    public void setTipo1(boolean tipo1) {
        setVerTipo1(tipo1);
        this.tipo1 = tipo1;
    }

    public boolean isTipo2() {
        return tipo2;
    }

    public void setTipo2(boolean tipo2) {
        setVerTipo2(tipo2);
        this.tipo2 = tipo2;
    }

    public boolean isTipo3() {
        return tipo3;
    }

    public void setTipo3(boolean tipo3) {
        setVerTipo3(tipo3);
        this.tipo3 = tipo3;
    }

    public Boolean getAutomatico() {
        return automatico;
    }

    public void setAutomatico(Boolean automatico) {
        this.automatico = automatico;
    }

    public boolean getVerTipo1() {
        return this.verTipo1;
    }

    public boolean getVerTipo2() {
        return this.verTipo2;
    }

    public boolean getVerTipo3() {
        return this.verTipo3;
    }

    public void setVerTipo1(boolean verTipo1) {
        this.verTipo1 = verTipo1;
    }

    public void setVerTipo2(boolean verTipo2) {
        this.verTipo2 = verTipo2;
    }

    public void setVerTipo3(boolean verTipo3) {
        this.verTipo3 = verTipo3;
    }

    public void mostrarTipo3() {
        if (tipo3 == true) {
            verTipo3 = false;
        } else {
            verTipo3 = true;
        }
    }

    public List<Cola> getObjSkill2() {
        return objSkill2;
    }

    public void setObjSkill2(List<Cola> objSkill2) {
        this.objSkill2 = objSkill2;
    }

    public boolean isActivo1() {
        return activo1;
    }

    public void setActivo1(boolean activo1) {
        this.activo1 = activo1;
    }

    public boolean isActivo3() {
        if(tipo2==true){
            return true;
        }else{
            return false;
        }
    }

    public void setActivo3(boolean activo3) {
        this.activo3 = activo3;
    }

    public boolean isActivo2() {
        if(tipo3==true){
            return true;
        }else{
            return false;
        }
    }

    public void setActivo2(boolean activo2) {
        this.activo2 = activo2;
    }
}
