/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.CampaniaDAO;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.PreguntaDAO;
import ec.com.centrosurcall.datos.modelo.Campania;
import ec.com.centrosurcall.datos.modelo.Pregunta;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Fabian Lema
 */

@ManagedBean
@ViewScoped
public final class CtrPreguntas extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Pregunta selPregunta = new Pregunta();
    PreguntaDAO preguntaDAO = new PreguntaDAO();
    CampaniaDAO campaniaDAO = new CampaniaDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemCampanias;
    private SelectItem[] selectItemTipo = {new SelectItem(0, "--SELECCIONE--"), new SelectItem(1, "Valor a seleccionar"), new SelectItem(2, "Valor a Ingresar")};
    private String idCampania ="0";
    private boolean esPrimero = true;
    private Integer numPregunta;

    public CtrPreguntas() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarCampanias();
        cargarPreguntas();
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoPreguntaEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar esta Razón?");
        confPop();
    }
    
    public void cargarPreguntas() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Pregunta> listaPregunta = preguntaDAO.getPreguntas();
        if (listaPregunta != null) {
            listaBaseObjetos.addAll(listaPregunta);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargarCampanias() {
        List<Campania> listaCampania = campaniaDAO.getCampanias();
        if (listaCampania != null) {
            int cont = 1;
            selectItemCampanias = new SelectItem[listaCampania.size()+1];
            selectItemCampanias[0] = new SelectItem("0", "--SELECCIONE--");
            for (Campania obj : listaCampania) {
                selectItemCampanias[cont] = new SelectItem(String.valueOf(obj.getId()), obj.getNombre());
                cont++;
            }
        }
    }

    public void cargoPreguntaEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Pregunta")) {
                if (((Pregunta) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selPregunta = (Pregunta) session.getObjeto();
            idCampania = String.valueOf(selPregunta.getCampania().getId());
            numPregunta = selPregunta.getNumeroPregunta();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar la Pregunta");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            //selPregunta.setEstado(true);
            if (!idCampania.equals("0"))
                selPregunta.setCampania(campaniaDAO.getCampaniaByID(Integer.parseInt(idCampania)));
            //selPregunta.setUsuario(parametroSession.getUsuario());
            exito = preguntaDAO.savePregunta(selPregunta, 1);
            parametroSession.setEsNuevo(false);
        } else {
            if (!idCampania.equals("0"))
                selPregunta.setCampania(campaniaDAO.getCampaniaByID(Integer.parseInt(idCampania)));
            exito = preguntaDAO.savePregunta(selPregunta, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Pregunta> respuesta = new ArrayList<Pregunta>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Pregunta cl = (Pregunta) obj;
                if (cl.getTitulo().toUpperCase().contains(whereSql.toUpperCase()) || cl.getTitulo().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
       // listaColumnas.put("id", "Id");
        listaColumnas.put("stringCampania", "Campaña");
        listaColumnas.put("numeroPregunta", "Numero");
        listaColumnas.put("titulo", "Titulo Pregunta");
        listaColumnas.put("tituloPregunta", "Titulo Opcion");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionPreguntas";
    }

    @Override
    public void auditoria() {
        try {
            selPregunta = (Pregunta) objetoSeleccionado;
        } catch (Exception e) {
            selPregunta.setId(0);
        }
        if (selPregunta.getId() > 0) {
            creado = selPregunta.getTitulo();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarPreguntas();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Pregunta> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Pregunta) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmPreguntas";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmPreguntas";
    }

    public void confPopEliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Pregunta> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = preguntaDAO.eliminarPreguntas(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarPreguntas();
        }
        return exito;
    }

    public List<Pregunta> sacarSeleccionado() {
        Pregunta selectPregunta = new Pregunta();
        List<Pregunta> listadoSeleccionado = new ArrayList<Pregunta>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectPregunta = (Pregunta) o;
            if (selectPregunta.getSeleccionado()) {
                listadoSeleccionado.add(selectPregunta);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Pregunta getSelPregunta() {
        return selPregunta;
    }

    public void setSelPregunta(Pregunta selPregunta) {
        this.selPregunta = selPregunta;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String stringCampania;
    private String titulo;
    private String tituloPregunta;
    private Integer numeroPregunta;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStringCampania() {
        return stringCampania;
    }

    public void setStringCampania(String stringCampania) {
        this.stringCampania = stringCampania;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTituloPregunta() {
        return tituloPregunta;
    }

    public void setTituloPregunta(String tituloPregunta) {
        this.tituloPregunta = tituloPregunta;
    }

    public SelectItem[] getSelectItemCampanias() {
        return selectItemCampanias;
    }

    public void setSelectItemCampanias(SelectItem[] selectItemCampanias) {
        this.selectItemCampanias = selectItemCampanias;
    }

    public String getIdCampania() {
        return idCampania;
    }

    public void setIdCampania(String idCampania) {
        this.idCampania = idCampania;
    }

    public SelectItem[] getSelectItemTipo() {
        return selectItemTipo;
    }

    public void setSelectItemTipo(SelectItem[] selectItemTipo) {
        this.selectItemTipo = selectItemTipo;
    }

    public boolean isEsPrimero() {
        return esPrimero;
    }

    public void setEsPrimero(boolean esPrimero) {
        this.esPrimero = esPrimero;
    }

    public Integer getNumPregunta() {
        return numPregunta;
    }

    public void setNumPregunta(Integer numPregunta) {
        this.numPregunta = numPregunta;
        List<Pregunta> temp = preguntaDAO.getPreguntasByCampNPreg(Integer.parseInt(idCampania), numPregunta);
        if (temp.size()>0){
            esPrimero = false;
            selPregunta.setTitulo(temp.get(0).getTitulo());            
        }else{
            esPrimero = true;
        }
        selPregunta.setNumeroPregunta(numPregunta);
    }

    public Integer getNumeroPregunta() {
        return numeroPregunta;
    }

    public void setNumeroPregunta(Integer numeroPregunta) {
        this.numeroPregunta = numeroPregunta;
    }
}
