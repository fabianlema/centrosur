package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.ColumnaDAO;
import ec.com.centrosurcall.datos.modelo.Columna;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public final class CtrColumnas extends MantenimientoGenerico
  implements MantenimientoInterface, Serializable
{
  CtrSession session;
  private Columna selColumnas = new Columna();
  ColumnaDAO columnaDAO = new ColumnaDAO();
  String opcionBoton = "";
  private String id;
  private String nombre;
  private String orden;

  public CtrColumnas()
  {
    this.session = ((CtrSession)FacesUtils.getManagedBean("ctrSession"));
    setOpcionBoton("Grabar");
    this.session.setTituloSeccion("Listado");
    cargarColumnasGrupo();
    cargarColumnas();

    if ((this.parametroSession.getEsEditar().booleanValue()) && (this.parametroSession.getObjeto() != null))
    {
      cargoColumnasEditar();
    }
    setMensajeConfirmacion("Seguro desea grabar esta Columna?");
    confPop();
  }

  public void cargarColumnas() {
    this.listaBaseObjetos.clear();
    this.listaObjetos.clear();
    List listaColumnas = this.columnaDAO.getColumnas();
    if (listaColumnas != null) {
      this.listaBaseObjetos.addAll(listaColumnas);
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargoColumnasEditar()
  {
    boolean existeObjeto = false;
    try {
      if (this.session.getObjeto().getClass().getName().contains("Columna")) {
        if (((Columna)this.session.getObjeto()).getId() > 0)
          existeObjeto = true;
      }
      else {
        this.parametroSession.setEsEditar(Boolean.valueOf(false));
        this.parametroSession.setEsNuevo(Boolean.valueOf(false));
      }
    } catch (Exception e) {
      System.out.println(e.getCause().getMessage());
    }
    if (existeObjeto) {
      this.opcionBoton = "Guardar";
      this.selColumnas = ((Columna)this.session.getObjeto());
    }
  }

  public void confPop()
  {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro desea registrar la Columna");
    setMensajeCorrecto("Registro grabado");
    setMensajeError("Error al Registrar");
  }

  public boolean grabar()
  {
    boolean exito = false;
    if (this.parametroSession.getEsNuevo().booleanValue()) {
      this.selColumnas.setEstado(Boolean.TRUE);
      exito = this.columnaDAO.saveColumna(this.selColumnas, 1);
      this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    } else {
      exito = this.columnaDAO.saveColumna(this.selColumnas, 0);
      this.parametroSession.setEsEditar(Boolean.valueOf(false));
    }
    return exito;
  }

  public void buscarWsql()
  {
    List respuesta = new ArrayList();
    this.parametroSession.setNumPaginas(1);
    this.listaObjetos.clear();
    if (!this.whereSql.equals("")) {
      for (Iterator i$ = this.listaBaseObjetos.iterator(); i$.hasNext(); ) { Object obj = i$.next();
        Columna cl = (Columna)obj;

        respuesta.add(cl);
      }

      this.listaObjetos.addAll(respuesta);
    } else {
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargarColumnasGrupo()
  {
    this.listaColumnas.put("nombre", "Nombre");
    this.listaColumnas.put("orden", "Orden");
  }

  public String regresar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.session.setObjeto(null);
    this.session.setTituloSeccion("Listado");
    return "frmAdministracionColumnas";
  }

  public void auditoria()
  {
    try {
      this.selColumnas = ((Columna)this.objetoSeleccionado);
    } catch (Exception e) {
      this.selColumnas.setId(0);
    }
    if (this.selColumnas.getId() > 0) {
      setCreado(this.creado);
      this.fechacreacion = new Date().toString();
      setActualizado("");
      setFechaactualizacion("");
      this.mostrarAuditoriaVal = true;
      mostrarAuditoria();
    }
  }

  public String refrescar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.whereSql = "";
    cargarColumnas();
    this.session.setSeleccionado(Boolean.valueOf(false));
    this.objetoSeleccionado = new Object();
    return "";
  }

  public boolean isExitograbar()
  {
    return super.isExitograbar();
  }

  public void imprimir()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public String editar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(true));
    this.session.setTituloSeccion("Editar");
    List objetoEditar = sacarSeleccionado();
    if (objetoEditar.size() > 0) {
      this.parametroSession.setObjeto(objetoEditar.get(0));
      try {
        if (((Columna)objetoEditar.get(0)).getId() > 0) {
          this.parametroSession.setBoton("btnActualizar");
          return "frmColumnas";
        }
        return "";
      }
      catch (Exception e) {
        return "";
      }
    }
    return "";
  }

  public String nuevo()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(true));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.parametroSession.setBoton("btnGrabar");
    setOpcionBoton("nuevo");
    this.session.setTituloSeccion("Nuevo");
    return "frmColumnas";
  }

  public void confPopEliminar() {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro quiere eliminar?");
    setMensajeCorrecto("Eliminacion exitosa!");
    setMensajeError("Existen errores en:");
  }

  public boolean eliminar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    boolean exito = false;
    this.listaErrores = new ArrayList();
    List listaAeliminar = sacarSeleccionado();
    if (listaAeliminar.size() > 0) {
      this.listaErrores = this.columnaDAO.eliminarColumnas(listaAeliminar, this.listaObjetos);
    }
    if (this.listaErrores.isEmpty()) {
      exito = true;
      cargarColumnas();
    }
    return exito;
  }

  public List<Columna> sacarSeleccionado() {
    Columna selectColumnas = new Columna();
    List listadoSeleccionado = new ArrayList();
    boolean exito = false;
    for (Iterator i$ = this.listaObjetos.iterator(); i$.hasNext(); ) { Object o = i$.next();
      selectColumnas = (Columna)o;
      if (selectColumnas.getSeleccionado().booleanValue()) {
        listadoSeleccionado.add(selectColumnas);
        exito = true;
      }
    }
    if (exito) {
      return listadoSeleccionado;
    }
    return listadoSeleccionado;
  }

  public Columna getSelColumnas()
  {
    return this.selColumnas;
  }

  public void setSelColumnas(Columna selColumnas) {
    this.selColumnas = selColumnas;
  }

  public String getOpcionBoton() {
    return this.opcionBoton;
  }

  public void setOpcionBoton(String opcionBoton) {
    this.opcionBoton = opcionBoton;
  }

  public String getId()
  {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNombre() {
    return this.nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getOrden() {
    return this.orden;
  }

  public void setOrden(String orden) {
    this.orden = orden;
  }
}