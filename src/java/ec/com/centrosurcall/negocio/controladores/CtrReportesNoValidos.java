/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author fabia
 */
import ec.com.centrosurcall.datos.DAO.HistorialDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Historial;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.pop3.MsnFromPop3;
import ec.com.centrosurcall.reports.GenerarReporte;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.PrintStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public final class CtrReportesNoValidos
        implements Serializable {

    CtrSession session;
    private Date fechaA;
    private Date fechaB;
    private SelectItem[] selectItemGrupos;
    private List<Object> objReporte = new ArrayList();
    private List<Historial> objListHistorial;
    HistorialDAO historialDAO = new HistorialDAO();
    ParametrosDAO paramDAO = new ParametrosDAO();
    Parametro svr = this.paramDAO.getParametroByAtributo("SERVERREPORT");
    Parametro reportNVPdf = this.paramDAO.getParametroByAtributo("REPORTNVPDF");
    Parametro reportNVXls = this.paramDAO.getParametroByAtributo("REPORTNVXLS");

    public CtrReportesNoValidos() {
        this.session = ((CtrSession) FacesUtils.getManagedBean("ctrSession"));
        System.out.println("CONS REpo");
        actualizarReporte();
    }

    public void ejecutarHilo() {
        new Thread("NoValido") {
            @Override
            public void run() {
                try {
                    MsnFromPop3 msnFromPop3 = new MsnFromPop3(session);
                    msnFromPop3.getNoValidos();
                    System.out.println("Ejecuta Hilo");
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        }.start();
    }

    public void cargarGrupos() {
        this.objListHistorial = this.historialDAO.getHistorialSI();
        int cont;
        if ((this.objListHistorial != null) && (this.objListHistorial.size() > 0)) {
            this.selectItemGrupos = new SelectItem[this.objListHistorial.size() + 1];
            this.selectItemGrupos[0] = new SelectItem("0", "--SELECCIONE--", "--SELECCIONE--");
            cont = 1;
            for (Historial cl : this.objListHistorial) {
                this.selectItemGrupos[cont] = new SelectItem(cl.getGrupo(), cl.getGrupo(), cl.getGrupo());
                cont++;
            }
        }
    }

    public void actualizarReporte() {
        this.objReporte.clear();
        if ((this.fechaA != null) && (this.fechaB != null)) {
            Consultas consulta = new Consultas();
            String desde = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.fechaA);
            String hasta = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.fechaB);

            String cadenaSQL = "select * from historial where fecha >= '" + desde + "' and fecha <= '" + hasta + "' and estado = 1 and fallo = 1";
            System.out.println(cadenaSQL);
            this.objReporte = consulta.getListSql(cadenaSQL);
        }
    }

    public void cargarReporte(String tipo) {
        String url = "";
        if ((this.fechaA != null) && (this.fechaB != null)) {
            GenerarReporte r = new GenerarReporte();
            Map parametros = new HashMap();

            parametros.put("fechaA", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.fechaA));
            parametros.put("fechaB", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.fechaB));

            if (tipo.equals("1")) {
                r.generaPdf("ReporteCSur2NInv", parametros, "ReporteCSur2NInv");
            } else {
                r.generaXlsx("ReporteCSur2NInvExcel", parametros, "ReporteCSur2NInvExcel");
            }
        }
    }

    public Date getFechaA() {
        return this.fechaA;
    }

    public void setFechaA(Date fechaA) {
        this.fechaA = fechaA;
    }

    public Date getFechaB() {
        return this.fechaB;
    }

    public void setFechaB(Date fechaB) {
        this.fechaB = fechaB;
    }

    public SelectItem[] getSelectItemGrupos() {
        return this.selectItemGrupos;
    }

    public void setSelectItemGrupos(SelectItem[] selectItemGrupos) {
        this.selectItemGrupos = selectItemGrupos;
    }

    public List<Object> getObjReporte() {
        return this.objReporte;
    }

    public void setObjReporte(List<Object> objReporte) {
        this.objReporte = objReporte;
    }
}
