/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.RegistroSeccionDAO;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.TllamadaDAO;
import ec.com.centrosurcall.datos.modelo.RegistroSeccion;
import ec.com.centrosurcall.datos.modelo.TipoLlamada;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrTllamada extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private TipoLlamada selTipoLlamada = new TipoLlamada();
    TllamadaDAO tipoLlamadaDAO = new TllamadaDAO();
    String opcionBoton = "";
    private SelectItem[] selectIvr;
    final private RegistroSeccionDAO rs = new RegistroSeccionDAO();
    //SelectItem item = new SelectItem();
    //private String idPadre = "0";

    public CtrTllamada() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarTipoLlamada();
        cargarIvr();
        selTipoLlamada.setCodigoIvr(new RegistroSeccion());
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoRazonEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar esta Tipo Llamada?");
        confPop();
    }

    public void cargarTipoLlamada() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<TipoLlamada> listaRol = tipoLlamadaDAO.getTiposLlamada();
        if (listaRol != null) {
            listaBaseObjetos.addAll(listaRol);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargarIvr() {
        List<RegistroSeccion> listaRegistrosSeccion = rs.getRegistroSeccion();
        selectIvr = new SelectItem[listaRegistrosSeccion.size()+1];
        selectIvr[0] = new SelectItem(0, "--SELECCIONE--");
        for (int i=0; i<listaRegistrosSeccion.size(); i++){
            selectIvr[i+1] = new SelectItem(listaRegistrosSeccion.get(i).getId(), listaRegistrosSeccion.get(i).getNombre());
        }
    }

    public void cargoRazonEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("TipoLlamada")) {
                if (((TipoLlamada) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selTipoLlamada = (TipoLlamada) session.getObjeto();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Tipo Llamada");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        selTipoLlamada.setEstado(1);
        if (parametroSession.getEsNuevo()) {
            //selRazon.setEstado(1);
            exito = tipoLlamadaDAO.saveTipoLlamada(selTipoLlamada, 1);
            parametroSession.setEsNuevo(false);
        } else {
            exito = tipoLlamadaDAO.saveTipoLlamada(selTipoLlamada, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<TipoLlamada> respuesta = new ArrayList<TipoLlamada>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                TipoLlamada cl = (TipoLlamada) obj;
                if (cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase()) || cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        // listaColumnas.put("id", "Id");
        listaColumnas.put("stringIvr", "Regitro Llamada");
        listaColumnas.put("descripcion", "Descripción");
        //listaColumnas.put("codigoRazon","Código CentroSur");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionTllamada";
    }

    @Override
    public void auditoria() {
        try {
            selTipoLlamada = (TipoLlamada) objetoSeleccionado;
        } catch (Exception e) {
            selTipoLlamada.setId(0);
        }
        if (selTipoLlamada.getId() > 0) {
            creado = selTipoLlamada.getDescripcion();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarTipoLlamada();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<TipoLlamada> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((TipoLlamada) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmTllamada";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmTllamada";
    }

    public void confPopEliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<TipoLlamada> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = tipoLlamadaDAO.eliminarTipoLlamada(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarTipoLlamada();
        }
        return exito;
    }

    public List<TipoLlamada> sacarSeleccionado() {
        TipoLlamada selectRazon = new TipoLlamada();
        List<TipoLlamada> listadoSeleccionado = new ArrayList<TipoLlamada>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectRazon = (TipoLlamada) o;
            if (selectRazon.getSeleccionado()) {
                listadoSeleccionado.add(selectRazon);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public TipoLlamada getSelTipoLlamada() {
        return selTipoLlamada;
    }

    public void setSelTipoLlamada(TipoLlamada selTipoLlamada) {
        this.selTipoLlamada = selTipoLlamada;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String codigoIvr;
    private String descripcion;
    private String stringIvr;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigoIvr() {
        return codigoIvr;
    }

    public void setCodigoIvr(String codigoIvr) {
        this.codigoIvr = codigoIvr;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public SelectItem[] getSelectIvr() {
        return selectIvr;
    }

    public void setSelectIvr(SelectItem[] selectIvr) {
        this.selectIvr = selectIvr;
    }

    public String getStringIvr() {
        return stringIvr;
    }

    public void setStringIvr(String stringIvr) {
        this.stringIvr = stringIvr;
    }
}