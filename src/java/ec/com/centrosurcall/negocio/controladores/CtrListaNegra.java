package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.HistorialDAO;
import ec.com.centrosurcall.datos.modelo.Historial;
import ec.com.centrosurcall.reports.GenerarReporte;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public final class CtrListaNegra extends MantenimientoGenerico
  implements MantenimientoInterface, Serializable
{
  CtrSession session;
  private Historial selHistorial = new Historial();
  HistorialDAO historialDAO = new HistorialDAO();
  String opcionBoton = "";
  private String id;
  private String numero;

  public CtrListaNegra()
  {
    this.session = ((CtrSession)FacesUtils.getManagedBean("ctrSession"));
    setOpcionBoton("Grabar");
    this.session.setTituloSeccion("Listado");
    cargarColumnasGrupo();
    cargarListaNegra();

    if ((this.parametroSession.getEsEditar().booleanValue()) && (this.parametroSession.getObjeto() != null))
    {
      cargoColumnasEditar();
    }
    setMensajeConfirmacion("Seguro desea grabar esta Columna?");
    confPop();
  }

  public void cargarListaNegra() {
    this.listaBaseObjetos.clear();
    this.listaObjetos.clear();
    List listaColumnas = this.historialDAO.getHistorialListaN();
    if (listaColumnas != null) {
      this.listaBaseObjetos.addAll(listaColumnas);
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargoColumnasEditar()
  {
    boolean existeObjeto = false;
    try {
      if (this.session.getObjeto().getClass().getName().contains("Historial")) {
        if (((Historial)this.session.getObjeto()).getId() > 0)
          existeObjeto = true;
      }
      else {
        this.parametroSession.setEsEditar(Boolean.valueOf(false));
        this.parametroSession.setEsNuevo(Boolean.valueOf(false));
      }
    } catch (Exception e) {
      System.out.println(e.getCause().getMessage());
    }
    if (existeObjeto) {
      this.opcionBoton = "Guardar";
      this.selHistorial = ((Historial)this.session.getObjeto());
    }
  }

  public void confPop()
  {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro desea registrar la Columna");
    setMensajeCorrecto("Registro grabado");
    setMensajeError("Error al Registrar");
  }

  public boolean grabar()
  {
    return false;
  }

  public void buscarWsql()
  {
    List respuesta = new ArrayList();
    this.parametroSession.setNumPaginas(1);
    this.listaObjetos.clear();
    if (!this.whereSql.equals("")) {
      for (Iterator i$ = this.listaBaseObjetos.iterator(); i$.hasNext(); ) { Object obj = i$.next();
        Historial cl = (Historial)obj;
        if (cl.getNumero().contains(this.whereSql.toUpperCase())) {
          respuesta.add(cl);
        }
      }
      this.listaObjetos.addAll(respuesta);
    } else {
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargarColumnasGrupo()
  {
    this.listaColumnas.put("numero", "Numero");
  }

  public String regresar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.session.setObjeto(null);
    this.session.setTituloSeccion("Listado");
    return "frmAdministracionColumnas";
  }

  public void auditoria()
  {
  }

  public String refrescar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.whereSql = "";
    cargarListaNegra();
    this.session.setSeleccionado(Boolean.valueOf(false));
    this.objetoSeleccionado = new Object();
    return "";
  }

  public boolean isExitograbar()
  {
    return super.isExitograbar();
  }

  public void imprimir()
  {
    GenerarReporte r = new GenerarReporte();
    Map parametros = new HashMap();
    r.generaXlsx("ReporteCSur2NBList", parametros, "ReporteCSur2NBList");
  }

  public String editar()
  {
    return "";
  }

  public String nuevo()
  {
    return "";
  }

  public void confPopEliminar() {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro quiere eliminar?");
    setMensajeCorrecto("Eliminacion exitosa!");
    setMensajeError("Existen errores en:");
  }

  public boolean eliminar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    boolean exito = false;
    this.listaErrores = new ArrayList();
    List listaAeliminar = sacarSeleccionado();
    if (listaAeliminar.size() > 0) {
      this.listaErrores = this.historialDAO.eliminarHistorial(listaAeliminar, this.listaObjetos);
    }
    if (this.listaErrores.isEmpty()) {
      exito = true;
      cargarListaNegra();
    }
    return exito;
  }

  public List<Historial> sacarSeleccionado() {
    Historial selectHistorial = new Historial();
    List listadoSeleccionado = new ArrayList();
    boolean exito = false;
    for (Iterator i$ = this.listaObjetos.iterator(); i$.hasNext(); ) { Object o = i$.next();
      selectHistorial = (Historial)o;
      if (selectHistorial.getSeleccionado().booleanValue()) {
        listadoSeleccionado.add(selectHistorial);
        exito = true;
      }
    }
    if (exito) {
      return listadoSeleccionado;
    }
    return listadoSeleccionado;
  }

  public Historial getSelHistorial()
  {
    return this.selHistorial;
  }

  public void setSelHistorial(Historial selHistorial) {
    this.selHistorial = selHistorial;
  }

  public String getOpcionBoton() {
    return this.opcionBoton;
  }

  public void setOpcionBoton(String opcionBoton) {
    this.opcionBoton = opcionBoton;
  }

  public String getId()
  {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNumero() {
    return this.numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }
}