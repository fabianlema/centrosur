/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.InterfacesDAO;
import ec.com.centrosurcall.datos.modelo.Interfaces;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */

@ManagedBean
@ViewScoped
public final class CtrInterfazTrk extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Interfaces selInterfaces = new Interfaces();
    InterfacesDAO interfacesDAO = new InterfacesDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemTipos;
    private Boolean atriEditable = false;

    public CtrInterfazTrk() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarParametros();
        if (parametroSession.getEsNuevo()) {
            atriEditable = false;
        }
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            atriEditable = true;
            cargoParametrosEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar esta interfaz?");
        confPop();
    }

    public void cargarParametros() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Interfaces> listaParametros = interfacesDAO.getInterfaces();
        if (listaParametros != null) {
            listaBaseObjetos.addAll(listaParametros);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargoParametrosEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Interfaces")) {
                if (((Interfaces) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selInterfaces = (Interfaces) session.getObjeto();            
            atriEditable=false;            
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar la Interfaz");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            //selInterfaces.setEstado(Boolean.TRUE);
            exito = interfacesDAO.saveInterfaces(selInterfaces, 1);
            parametroSession.setEsNuevo(false);
        } else {
            exito = interfacesDAO.saveInterfaces(selInterfaces, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Interfaces> respuesta = new ArrayList<Interfaces>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Interfaces cl = (Interfaces) obj;
                if (cl.getNombre().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        listaColumnas.put("nombre", "Nombre");
        listaColumnas.put("troncal", "Troncal");
        listaColumnas.put("segundos", "Segundos");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionInterfaz";
    }

    @Override
    public void auditoria() {
        try {
            selInterfaces = (Interfaces) objetoSeleccionado;
        } catch (Exception e) {
            selInterfaces.setId(0);
        }
        if (selInterfaces.getId() > 0) {
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarParametros();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Interfaces> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Interfaces) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmInterfazTkr";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmInterfazTkr";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Interfaces> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = interfacesDAO.eliminarInterfaces(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarParametros();
        }
        return exito;
    }

    public List<Interfaces> sacarSeleccionado() {
        Interfaces selectParametros = new Interfaces();
        List<Interfaces> listadoSeleccionado = new ArrayList<Interfaces>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectParametros = (Interfaces) o;
            if (selectParametros.getSeleccionado()) {
                listadoSeleccionado.add(selectParametros);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Interfaces getSelInterfaces() {
        return selInterfaces;
    }

    public void setSelInterfaces(Interfaces selInterfaces) {
        this.selInterfaces = selInterfaces;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }

    public SelectItem[] getSelectItemTipos() {
        return selectItemTipos;
    }

    public void setSelectItemTipos(SelectItem[] selectItemTipos) {
        this.selectItemTipos = selectItemTipos;
    }
    private String id;
    private String nombre;
    private String troncal;
    private String segundos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTroncal() {
        return troncal;
    }

    public void setTroncal(String troncal) {
        this.troncal = troncal;
    }

    public String getSegundos() {
        return segundos;
    }

    public void setSegundos(String segundos) {
        this.segundos = segundos;
    }

    public Boolean getAtriEditable() {
        return atriEditable;
    }

    public void setAtriEditable(Boolean atriEditable) {
        this.atriEditable = atriEditable;
    }
}