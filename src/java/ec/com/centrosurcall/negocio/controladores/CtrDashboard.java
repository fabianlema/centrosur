/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;


import com.google.gson.Gson;
import java.io.Serializable;
import java.util.*;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import ec.com.centrosurcall.datos.DAO.RazonesDAO;
import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Razon;
import ec.com.centrosurcall.datos.modelo.Team;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.faces.model.SelectItem;

import java.text.SimpleDateFormat;
//import org.openide.util.Exceptions;

/**
 *
 * @author persona
 */
@ManagedBean
@ViewScoped
public final class CtrDashboard implements Serializable {

    CtrSession session;
    private String opcionBoton = "";
    private Date desde;
    private Date desdeR1;
    private Date hastaR1;
    private Date hasta;
    Team team;
    private List<Object> objReporte1 = new ArrayList<Object>();
    private SelectItem[] selectItemRazones;
    private List<Object> objRazon;
    private String[] razones;
    RazonesDAO razonesDAO = new RazonesDAO();
    SelectItem item = new SelectItem();
    private SelectItem[] efectivas;
    
    private List<Object> objListColumna;
    private List<Object> objReporteAgentes;
    private List<Object> objAgentes;
    
   private List<Object> objetos;
   private List<Object> objetos2;
   private List<Object> objetos3;
   private List<Object> objetos4;
     private Integer efectivasn, idTeam;;
     private Integer noefectivasn=0;
     private Integer total=0;
    private Integer efectivasnouance;
    private Integer noefectivasnouance;
    //private Integer contador;
    Consultas consulta = new Consultas();//   
    private List<Object> objReporte2 = new ArrayList<Object>();// CONTIENE LOS TOTAL DE LLAMADAS POR CADA RAZON
    private Integer contador2;
    private SelectItem[] selectItemTeams;
    
    public CtrDashboard() {
        cargarRazones();
        //dashboard();
  //      TeamsDAO teamdao = new TeamsDAO();
  //      List<TeamUsuario> lista = new ArrayList<>();
  //      lista=teamdao.getTeamsUsuarioList(this.session.getUsuario().getId());
        this.idTeam=3;
        //actualizarReporte2();
        //this.reporteRazones();
        
    }
    
    public void cargarTeams() {
        setSelectItemTeams(new SelectItem[1]);
        getSelectItemTeams()[0] = item;
        Consultas consulta = new Consultas();
        setObjRazon((List<Object>) consulta.getListHql(null, "Team", "estado = 1 and idSupervisor="+this.session.getUsuario().getId()+"order by nombre", null));
        if (getObjRazon() != null && getObjRazon().size() > 0) {
            int cont = 1;
            setSelectItemTeams(new SelectItem[getObjRazon().size() + 1]);
            getSelectItemTeams()[0] = item;
            for (Object obj : getObjRazon()) {
                Team cl = (Team) obj;
                getSelectItemTeams()[cont] = new SelectItem(String.valueOf(cl.getId()), cl.getNombre());
                cont++;
            }
        }
    }
    public void cargarRazones() {
        setSelectItemRazones(new SelectItem[1]);
        getSelectItemRazones()[0] = item;
        Consultas consulta = new Consultas();
        setObjRazon((List<Object>) consulta.getListHql(null, "Razon", "estado = 1 and not id = 1 order by nombre", null));
        if (getObjRazon() != null && getObjRazon().size() > 0) {
            int cont = 1;
            setSelectItemRazones(new SelectItem[getObjRazon().size() + 1]);
            getSelectItemRazones()[0] = item;
            for (Object obj : getObjRazon()) {
                Razon cl = (Razon) obj;
                getSelectItemRazones()[cont] = new SelectItem(cl.getNombre());
                cont++;
            }
        }
    
    }
    
   /*public Integer sumador(){
   contador=contador  + 1;
   return contador;
   } */
   
    public void actualizarReporte2(){
            Date fechaHasta = new Date();
            String fechaActual = new SimpleDateFormat("yyyy-MM-dd").format(fechaHasta);
            List<Razon> objRazon;
            objRazon = razonesDAO.getRazones();
            String cadenaSQL = "select u.nombre AGENTE, " +
            "SUM(case l.estadoLlamada+l.idRazon when 2 then 1 else 0 end) [EFECTIVA],  ";
            for (int i=1; i<=objRazon.size()-1;i++){
                cadenaSQL += "SUM(case l.idRazon when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"], ";
            }
            cadenaSQL += "sum(case l.idUsuario when l.idUsuario then 1 else 0 end) [TOTAL] " +
            "from llamada l left join razon r on l.idRazon = r.id  " +
            "join usuario u on u.id=l.idUsuario " +
            "where l.fechaLlamada>='"+fechaActual+"' and l.fechaLlamada<='"+fechaActual+"' " + 
                    "and u.id IN (select te.idUsuario from team_usuario te where te.idTeam ="+this.idTeam+" and te.idUSuario != 2107)"+
            "group by u.nombre " +
            "order by 1, 2";
            System.out.println(cadenaSQL);
            setObjReporte1((List<Object>) consulta.getListSql(cadenaSQL));
      
    }
    
    public void actualizarReporte(){        
        if(desdeR1!=null && hastaR1!=null){            
            try {
                String desde2 = new SimpleDateFormat("yyyy-MM-dd").format(desdeR1);
                String hasta2 = new SimpleDateFormat("yyyy-MM-dd").format(hastaR1);
                List<Razon> objRazon;
                objRazon = razonesDAO.getRazones();
                String cadenaSQL = "select u.nombre as 'AGENTE', ";
                for (int i=0; i<=objRazon.size()-1;i++){
                    if (objRazon.get(i).getPadreId()==3)
                        cadenaSQL += "SUM(case l.tipoLlamada when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    else if (objRazon.get(i).getPadreId()==2)
                        cadenaSQL += "SUM(case l.esRegistrado when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    else if (objRazon.get(i).getPadreId()==1)
                        cadenaSQL += "SUM(case l.estadoLlamada when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    else
                        cadenaSQL += "SUM(case l.idRazon when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    if (i<=objRazon.size()-2) cadenaSQL += ",";
                }
                cadenaSQL += " from llamada l with (NOLOCK), usuario u with (NOLOCK) " +
                "where l.fechaLlamada>='"+desde2+"' and l.fechaLlamada<='"+hasta2+"' " + 
                "and l.idUsuario=u.id and (l.estado !=2 and l.estado !=3) group by u.nombre " +
                "order by 1, 2";
                //System.out.println(cadenaSQL);
                //objReporte1 = consulta.getListSql(cadenaSQL);
                ResultSet tempReporte = consulta.ejecutarQuery(cadenaSQL);
                if (tempReporte != null){
                    ResultSetMetaData columnas = tempReporte.getMetaData();
                    int columnCount = columnas.getColumnCount();
                    int rowCount;
                    int[] tempSumas = new int[columnCount];
                    objListColumna = new ArrayList<>();
                    for (int i = 1; i < columnCount + 1; i++ ) {
                        objListColumna.add(columnas.getColumnName(i));
                    }
                    objReporteAgentes = new ArrayList<>();
                    objAgentes = new ArrayList<>();
                    List <Object>objReporteAgentesTemp = new ArrayList<>();
                    while (tempReporte.next()) {
                        //rowCount = rowCount + 1;
                        String[] temp = new String[columnCount];
                        for (int i = 1; i < columnCount + 1; i++ ) {
                            temp[i-1]=tempReporte.getString(i);
                            if (i!=1){
                                tempSumas[i-1] = tempSumas[i-1] + tempReporte.getInt(i);
                            }
                        }
                        objReporteAgentesTemp.add(temp);
                    }
                    rowCount = objReporteAgentesTemp.size();
                    
                    for (int i = 0; i < columnCount; i++) {
                        String[] temp = new String[rowCount];
                        if (i!=0){
                            for (int j = 0; j < rowCount; j++) {
                                temp[j] = ((String[])objReporteAgentesTemp.get(j))[i];
                            }
                            objReporteAgentes.add(temp);
                        }else{
                            for (int j = 0; j < rowCount; j++) {
                                temp[j] = ((String[])objReporteAgentesTemp.get(j))[i];
                            }
                            objAgentes.add(temp);
                        }                        
                    }                    
                }
            } catch (Exception ex) {
                    //Exceptions.printStackTrace(ex);
                }
            //objReporte1 = consulta.getListSql(cadenaSQL);
        }
    }
    
    public void actualizarReporteTotal(){        
        if(desdeR1!=null && hastaR1!=null){            
            try {
                String desde2 = new SimpleDateFormat("yyyy-MM-dd").format(desdeR1);
                String hasta2 = new SimpleDateFormat("yyyy-MM-dd").format(hastaR1);
                List<Razon> objRazon;
                objRazon = razonesDAO.getRazones();
                String cadenaSQL = "select 'TODOS LOS AGENTES' as 'AGENTE', ";
                for (int i=0; i<=objRazon.size()-1;i++){
                    if (objRazon.get(i).getPadreId()==3)
                        cadenaSQL += "SUM(case l.tipoLlamada when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    else if (objRazon.get(i).getPadreId()==2)
                        cadenaSQL += "SUM(case l.esRegistrado when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    else if (objRazon.get(i).getPadreId()==1)
                        cadenaSQL += "SUM(case l.estadoLlamada when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    else
                        cadenaSQL += "SUM(case l.idRazon when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    if (i<=objRazon.size()-2) cadenaSQL += ",";
                }
                cadenaSQL += " from llamada l with (NOLOCK) " +
                "where l.fechaLlamada>='"+desde2+"' and l.fechaLlamada<='"+hasta2+"' " + 
                "and (l.estado !=2 and l.estado !=3) " +
                "order by 1, 2";
                //System.out.println(cadenaSQL);
                //objReporte1 = consulta.getListSql(cadenaSQL);
                ResultSet tempReporte = consulta.ejecutarQuery(cadenaSQL);
                if (tempReporte != null){
                    ResultSetMetaData columnas = tempReporte.getMetaData();
                    int columnCount = columnas.getColumnCount();
                    int rowCount;
                    int[] tempSumas = new int[columnCount];
                    objListColumna = new ArrayList<>();
                    for (int i = 1; i < columnCount + 1; i++ ) {
                        objListColumna.add(columnas.getColumnName(i));
                    }
                    objReporteAgentes = new ArrayList<>();
                    objAgentes = new ArrayList<>();
                    List <Object>objReporteAgentesTemp = new ArrayList<>();
                    while (tempReporte.next()) {
                        //rowCount = rowCount + 1;
                        String[] temp = new String[columnCount];
                        for (int i = 1; i < columnCount + 1; i++ ) {
                            temp[i-1]=tempReporte.getString(i);
                            if (i!=1){
                                tempSumas[i-1] = tempSumas[i-1] + tempReporte.getInt(i);
                            }
                        }
                        objReporteAgentesTemp.add(temp);
                    }
                    rowCount = objReporteAgentesTemp.size();
                    
                    for (int i = 0; i < columnCount; i++) {
                        String[] temp = new String[rowCount];
                        if (i!=0){
                            for (int j = 0; j < rowCount; j++) {
                                temp[j] = ((String[])objReporteAgentesTemp.get(j))[i];
                            }
                            objReporteAgentes.add(temp);
                        }else{
                            for (int j = 0; j < rowCount; j++) {
                                temp[j] = ((String[])objReporteAgentesTemp.get(j))[i];
                            }
                            objAgentes.add(temp);
                        }                        
                    }                    
                }
            } catch (Exception ex) {
                    //Exceptions.printStackTrace(ex);
                }
            //objReporte1 = consulta.getListSql(cadenaSQL);
        }
    }
    
   public void reporteRazones(){
        
            Date fechaHasta = new Date();
            String fechaActual = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fechaHasta);
            String cadenaSQL = "select r.nombre, count(*) from llamada l join razon r on l.idRazon = r.id where l.fechaLlamada='"+fechaActual+"' group by r.nombre";
            System.out.println(cadenaSQL);
            setObjReporte2((List<Object>) consulta.getListSql(cadenaSQL));
            this.contador2=this.objReporte2.size();      
    }    
    
    /**
     * @return the opcionBoton
     */
    public String getOpcionBoton() {
        return opcionBoton;
    }

    /**
     * @param opcionBoton the opcionBoton to set
     */
    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }

    /**
     * @return the selectItemRazones
     */
    public SelectItem[] getSelectItemRazones() {
        return selectItemRazones;
    }

    /**
     * @param selectItemRazones the selectItemRazones to set
     */
    public void setSelectItemRazones(SelectItem[] selectItemRazones) {
        this.selectItemRazones = selectItemRazones;
    }

    /**
     * @return the razones
     */
    public String[] getRazones() {
        return razones;
    }

    /**
     * @param razones the razones to set
     */
    public void setRazones(String[] razones) {
        this.razones = razones;
    }

    /**
     * @return the objetos
     */
    public List<Object> getObjetos() {
        return objetos;
    }

    /**
     * @param objetos the objetos to set
     */
    public void setObjetos(List<Object> objetos) {
        this.objetos = objetos;
    }

    /**
     * @return the efectivas
     */
    public SelectItem[] getEfectivas() {
        return efectivas;
    }

    /**
     * @param efectivas the efectivas to set
     */
    public void setEfectivas(SelectItem[] efectivas) {
        this.efectivas = efectivas;
    }

    /**
     * @return the efectivasn
     */
    public Integer getEfectivasn() {
        return efectivasn;
    }

    /**
     * @param efectivasn the efectivasn to set
     */
    public void setEfectivasn(Integer efectivasn) {
        this.efectivasn = efectivasn;
    }

    /**
     * @return the noefectivasn
     */
    public Integer getNoefectivasn() {
        return noefectivasn;
    }

    /**
     * @param noefectivasn the noefectivasn to set
     */
    public void setNoefectivasn(Integer noefectivasn) {
        this.noefectivasn = noefectivasn;
    }

    /**
     * @return the total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * @return the efectivasnouance
     */
    public Integer getEfectivasnouance() {
        return efectivasnouance;
    }

    /**
     * @param efectivasnouance the efectivasnouance to set
     */
    public void setEfectivasnouance(Integer efectivasnouance) {
        this.efectivasnouance = efectivasnouance;
    }

    /**
     * @return the noefectivasnouance
     */
    public Integer getNoefectivasnouance() {
        return noefectivasnouance;
    }

    /**
     * @param noefectivasnouance the noefectivasnouance to set
     */
    public void setNoefectivasnouance(Integer noefectivasnouance) {
        this.noefectivasnouance = noefectivasnouance;
    }

    /**
     * @return the objReporte1
     */
    public List<Object> getObjReporte1() {
        return objReporte1;
    }

    /**
     * @param objReporte1 the objReporte1 to set
     */
    public void setObjReporte1(List<Object> objReporte1) {
        this.objReporte1 = objReporte1;
    }

    /**
     * @return the idTeam
     */
    public Integer getIdTeam() {
        return idTeam;
    }

    /**
     * @param idTeam the idTeam to set
     */
    public void setIdTeam(Integer idTeam) {
        this.idTeam = idTeam;
    }

    /**
     * @return the objReporte2
     */
    public List<Object> getObjReporte2() {
        return objReporte2;
    }

    /**
     * @param objReporte2 the objReporte2 to set
     */
    public void setObjReporte2(List<Object> objReporte2) {
        this.objReporte2 = objReporte2;
    }

    /**
     * @return the contador2
     */
    public Integer getContador2() {
        return contador2;
    }

    /**
     * @param contador2 the contador2 to set
     */
    public void setContador2(Integer contador2) {
        this.contador2 = contador2;
    }

    /**
     * @return the selectItemTeams
     */
    public SelectItem[] getSelectItemTeams() {
        return selectItemTeams;
    }

    /**
     * @param selectItemTeams the selectItemTeams to set
     */
    public void setSelectItemTeams(SelectItem[] selectItemTeams) {
        this.selectItemTeams = selectItemTeams;
    }

    /**
     * @return the objRazon
     */
    public List<Object> getObjRazon() {
        return objRazon;
    }

    /**
     * @param objRazon the objRazon to set
     */
    public void setObjRazon(List<Object> objRazon) {
        this.objRazon = objRazon;
    }

  public String getConsultarReporte1(){
      Gson gson = new Gson();
      return gson.toJson(objReporte1);
  }
  
  public String getConsultarReporte2(){
      Gson gson = new Gson();
      return gson.toJson(objReporte2);
  }

    /**
     * @return the desde
     */
    public Date getDesde() {
        return desde;
    }

    /**
     * @param desde the desde to set
     */
    public void setDesde(Date desde) {
        this.desde = desde;
    }

    /**
     * @return the hasta
     */
    public Date getHasta() {
        return hasta;
    }

    /**
     * @param hasta the hasta to set
     */
    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    /**
     * @return the desdeR1
     */
    public Date getDesdeR1() {
        return desdeR1;
    }

    /**
     * @param desdeR1 the desdeR1 to set
     */
    public void setDesdeR1(Date desdeR1) {
        this.desdeR1 = desdeR1;
    }

    /**
     * @return the hastaR1
     */
    public Date getHastaR1() {
        return hastaR1;
    }

    /**
     * @param hastaR1 the hastaR1 to set
     */
    public void setHastaR1(Date hastaR1) {
        this.hastaR1 = hastaR1;
    }

    /**
     * @return the objetos2
     */
    public List<Object> getObjetos2() {
        return objetos2;
    }

    /**
     * @param objetos2 the objetos2 to set
     */
    public void setObjetos2(List<Object> objetos2) {
        this.objetos2 = objetos2;
    }

    /**
     * @return the objetos3
     */
    public List<Object> getObjetos3() {
        return objetos3;
    }

    /**
     * @param objetos3 the objetos3 to set
     */
    public void setObjetos3(List<Object> objetos3) {
        this.objetos3 = objetos3;
    }

    /**
     * @return the objetos4
     */
    public List<Object> getObjetos4() {
        return objetos4;
    }

    /**
     * @param objetos4 the objetos4 to set
     */
    public void setObjetos4(List<Object> objetos4) {
        this.objetos4 = objetos4;
    }
    
    public String getObjListColumna(){
      Gson gson = new Gson();
      return gson.toJson(objListColumna);
  }
  
  public String getObjReporteAgentes(){
      Gson gson = new Gson();
      return gson.toJson(objReporteAgentes);
  }
  
  public String getObjAgentes(){
      Gson gson = new Gson();
      return gson.toJson(objAgentes);
  }
}