/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.RazonesDAO;
import ec.com.centrosurcall.datos.modelo.Razon;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrRazones extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Razon selRazon = new Razon();
    RazonesDAO razonesDAO = new RazonesDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemRazones;
    //SelectItem item = new SelectItem();
    private String idPadre = "0";

    public CtrRazones() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarRazones();
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoRazonEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar esta Razón?");
        confPop();
    }

    public void cargarRazones() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Razon> listaRazon = razonesDAO.getRazonesY();
        if (listaRazon != null) {
            listaBaseObjetos.addAll(listaRazon);
            listaObjetos.addAll(listaBaseObjetos);
            
            int cont = 0;
            //item.setLabel("RAIZ");
            //item.setValue(0);            
            selectItemRazones = new SelectItem[listaRazon.size()];
            //selectItemRazones[0] = item;
            
            for (Razon obj : listaRazon) {
                selectItemRazones[cont] = new SelectItem(String.valueOf(obj.getId()), obj.getNombre());
                //if (cl.getId()==2) razonEquiv = cl; 
                cont++;
            }
        }
        
        
        /*item.setLabel("-RAIZ-");
        item.setValue(0);
        selectItemRazones = new SelectItem[1];
        selectItemRazones[0] = item;
        objRazon = consulta.getListHql(null, "Razon", "estado = 1 and not id = 1 order by nombre", null);
        if (objRazon != null && objRazon.size() > 0) {
            int cont = 1;
            selectItemRazones = new SelectItem[objRazon.size() + 1];
            selectItemRazones[0] = item;
            for (Object obj : objRazon) {
                Razon cl = (Razon) obj;
                selectItemRazones[cont] = new SelectItem(String.valueOf(cl.getId()), cl.getNombre());
                if (cl.getId()==2) razonEquiv = cl; 
                cont++;
            }
        }*/
    }

    public void cargoRazonEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Razon")) {
                if (((Razon) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selRazon = (Razon) session.getObjeto();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar la Razón");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            selRazon.setEstado(1);
            exito = razonesDAO.saveRazon(selRazon, 1);
            parametroSession.setEsNuevo(false);
        } else {
            exito = razonesDAO.saveRazon(selRazon, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Razon> respuesta = new ArrayList<Razon>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Razon cl = (Razon) obj;
                if (cl.getNombre().toUpperCase().contains(whereSql.toUpperCase()) || cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
       // listaColumnas.put("id", "Id");
        listaColumnas.put("nombre", "Nombre");
        listaColumnas.put("descripcion", "Descripción");
        //listaColumnas.put("codigoRazon","Código CentroSur");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionRazones";
    }

    @Override
    public void auditoria() {
        try {
            selRazon = (Razon) objetoSeleccionado;
        } catch (Exception e) {
            selRazon.setId(0);
        }
        if (selRazon.getId() > 0) {
            creado = selRazon.getNombre();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarRazones();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Razon> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Razon) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmRazones";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmRazones";
    }

    public void confPopEliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Razon> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = razonesDAO.eliminarRazones(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarRazones();
        }
        return exito;
    }

    public List<Razon> sacarSeleccionado() {
        Razon selectRazon = new Razon();
        List<Razon> listadoSeleccionado = new ArrayList<Razon>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectRazon = (Razon) o;
            if (selectRazon.getSeleccionado()) {
                listadoSeleccionado.add(selectRazon);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Razon getSelRazon() {
        return selRazon;
    }

    public void setSelRazon(Razon selRazon) {
        this.selRazon = selRazon;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String nombre;
    private String descripcion;
    private String estado;
    private String codigoRazon;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoRazon() {
        return codigoRazon;
    }

    public void setCodigoRazon(String codigoRazon) {
        this.codigoRazon = codigoRazon;
    }

    public SelectItem[] getSelectItemRazones() {
        return selectItemRazones;
    }

    public void setSelectItemRazones(SelectItem[] selectItemRazones) {
        this.selectItemRazones = selectItemRazones;
    }

    public String getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(String idPadre) {
        this.idPadre = idPadre;
    }
}
