/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author Paul
 */
public class ObjReporteInformix1 {
    private String usuario;
    private String razon1;
    private String razon2;
    private String razon3;
    private String razon4;
    private String razon5;
    private String razon6;
    private String razon7;
    private String razon8;
    private String razon9;
    private String razon10;
    private String razon11;
    private String razon12;
    private String razon13;
    private String razon14;
    private String razon15;
    private String razon16;
    private String razon17;
    private String razon18;
    private String razon19;
    private String razon20;
    private String razon21;
    private String razon22;
    private String razon23;
    private String razon24;
    
    public void ObjReporteInformix1(){
    
    }
    public void ObjReporteInformix1(String usuario, String razon1, String razon2, String razon3, String razon4, String razon5){
        usuario = this.getUsuario();
        razon1 = this.getRazon1();
        razon2 = this.getRazon2();
        razon3 = this.getRazon3();
        razon4 = this.getRazon4();
        razon5 = this.getRazon5();
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the razon1
     */
    public String getRazon1() {
        return razon1;
    }

    /**
     * @param razon1 the razon1 to set
     */
    public void setRazon1(String razon1) {
        this.razon1 = razon1;
    }

    /**
     * @return the razon2
     */
    public String getRazon2() {
        return razon2;
    }

    /**
     * @param razon2 the razon2 to set
     */
    public void setRazon2(String razon2) {
        this.razon2 = razon2;
    }

    /**
     * @return the razon3
     */
    public String getRazon3() {
        return razon3;
    }

    /**
     * @param razon3 the razon3 to set
     */
    public void setRazon3(String razon3) {
        this.razon3 = razon3;
    }

    /**
     * @return the razon4
     */
    public String getRazon4() {
        return razon4;
    }

    /**
     * @param razon4 the razon4 to set
     */
    public void setRazon4(String razon4) {
        this.razon4 = razon4;
    }

    /**
     * @return the razon5
     */
    public String getRazon5() {
        return razon5;
    }

    /**
     * @param razon5 the razon5 to set
     */
    public void setRazon5(String razon5) {
        this.razon5 = razon5;
    }

    /**
     * @return the razon6
     */
    public String getRazon6() {
        return razon6;
    }

    /**
     * @param razon6 the razon6 to set
     */
    public void setRazon6(String razon6) {
        this.razon6 = razon6;
    }

    /**
     * @return the razon7
     */
    public String getRazon7() {
        return razon7;
    }

    /**
     * @param razon7 the razon7 to set
     */
    public void setRazon7(String razon7) {
        this.razon7 = razon7;
    }

    /**
     * @return the razon8
     */
    public String getRazon8() {
        return razon8;
    }

    /**
     * @param razon8 the razon8 to set
     */
    public void setRazon8(String razon8) {
        this.razon8 = razon8;
    }

    /**
     * @return the razon9
     */
    public String getRazon9() {
        return razon9;
    }

    /**
     * @param razon9 the razon9 to set
     */
    public void setRazon9(String razon9) {
        this.razon9 = razon9;
    }

    /**
     * @return the razon10
     */
    public String getRazon10() {
        return razon10;
    }

    /**
     * @param razon10 the razon10 to set
     */
    public void setRazon10(String razon10) {
        this.razon10 = razon10;
    }

    /**
     * @return the razon11
     */
    public String getRazon11() {
        return razon11;
    }

    /**
     * @param razon11 the razon11 to set
     */
    public void setRazon11(String razon11) {
        this.razon11 = razon11;
    }

    /**
     * @return the razon12
     */
    public String getRazon12() {
        return razon12;
    }

    /**
     * @param razon12 the razon12 to set
     */
    public void setRazon12(String razon12) {
        this.razon12 = razon12;
    }

    /**
     * @return the razon13
     */
    public String getRazon13() {
        return razon13;
    }

    /**
     * @param razon13 the razon13 to set
     */
    public void setRazon13(String razon13) {
        this.razon13 = razon13;
    }

    /**
     * @return the razon14
     */
    public String getRazon14() {
        return razon14;
    }

    /**
     * @param razon14 the razon14 to set
     */
    public void setRazon14(String razon14) {
        this.razon14 = razon14;
    }

    /**
     * @return the razon15
     */
    public String getRazon15() {
        return razon15;
    }

    /**
     * @param razon15 the razon15 to set
     */
    public void setRazon15(String razon15) {
        this.razon15 = razon15;
    }

    /**
     * @return the razon16
     */
    public String getRazon16() {
        return razon16;
    }

    /**
     * @param razon16 the razon16 to set
     */
    public void setRazon16(String razon16) {
        this.razon16 = razon16;
    }

    /**
     * @return the razon17
     */
    public String getRazon17() {
        return razon17;
    }

    /**
     * @param razon17 the razon17 to set
     */
    public void setRazon17(String razon17) {
        this.razon17 = razon17;
    }

    /**
     * @return the razon18
     */
    public String getRazon18() {
        return razon18;
    }

    /**
     * @param razon18 the razon18 to set
     */
    public void setRazon18(String razon18) {
        this.razon18 = razon18;
    }

    /**
     * @return the razon19
     */
    public String getRazon19() {
        return razon19;
    }

    /**
     * @param razon19 the razon19 to set
     */
    public void setRazon19(String razon19) {
        this.razon19 = razon19;
    }

    /**
     * @return the razon20
     */
    public String getRazon20() {
        return razon20;
    }

    /**
     * @param razon20 the razon20 to set
     */
    public void setRazon20(String razon20) {
        this.razon20 = razon20;
    }

    /**
     * @return the razon21
     */
    public String getRazon21() {
        return razon21;
    }

    /**
     * @param razon21 the razon21 to set
     */
    public void setRazon21(String razon21) {
        this.razon21 = razon21;
    }

    /**
     * @return the razon22
     */
    public String getRazon22() {
        return razon22;
    }

    /**
     * @param razon22 the razon22 to set
     */
    public void setRazon22(String razon22) {
        this.razon22 = razon22;
    }

    /**
     * @return the razon23
     */
    public String getRazon23() {
        return razon23;
    }

    /**
     * @param razon23 the razon23 to set
     */
    public void setRazon23(String razon23) {
        this.razon23 = razon23;
    }

    /**
     * @return the razon24
     */
    public String getRazon24() {
        return razon24;
    }

    /**
     * @param razon24 the razon24 to set
     */
    public void setRazon24(String razon24) {
        this.razon24 = razon24;
    }
}
