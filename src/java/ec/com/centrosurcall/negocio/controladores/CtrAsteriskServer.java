/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.AteriskServerDAO;
import ec.com.centrosurcall.datos.modelo.AsteriskServer;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */

@ManagedBean
@ViewScoped
public final class CtrAsteriskServer extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private AsteriskServer selAsteriskServer = new AsteriskServer();
    AteriskServerDAO ateriskServerDAO = new AteriskServerDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemTipos;
    private Boolean atriEditable = false;

    public CtrAsteriskServer() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarParametros();
        if (parametroSession.getEsNuevo()) {
            atriEditable = false;
        }
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            atriEditable = true;
            cargoParametrosEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar este servidor asterisk?");
        confPop();
    }

    public void cargarParametros() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<AsteriskServer> listaParametros = ateriskServerDAO.getAsteriskServers();
        if (listaParametros != null) {
            listaBaseObjetos.addAll(listaParametros);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargoParametrosEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("AsteriskServer")) {
                if (((AsteriskServer) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selAsteriskServer = (AsteriskServer) session.getObjeto();            
            atriEditable=false;            
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Servidor Asterisk");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            //selAsteriskServer.setEstado(Boolean.TRUE);
            exito = ateriskServerDAO.saveAsteriskServer(selAsteriskServer, 1);
            parametroSession.setEsNuevo(false);
        } else {
            exito = ateriskServerDAO.saveAsteriskServer(selAsteriskServer, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<AsteriskServer> respuesta = new ArrayList<AsteriskServer>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                AsteriskServer cl = (AsteriskServer) obj;
                if (cl.getHost().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        listaColumnas.put("host", "Host");
        listaColumnas.put("basedatos", "Base de Datos");
        listaColumnas.put("usuario", "Usuario");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionServerAst";
    }

    @Override
    public void auditoria() {
        try {
            selAsteriskServer = (AsteriskServer) objetoSeleccionado;
        } catch (Exception e) {
            selAsteriskServer.setId(0);
        }
        if (selAsteriskServer.getId() > 0) {
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarParametros();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<AsteriskServer> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((AsteriskServer) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmAsteriskServer";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmAsteriskServer";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<AsteriskServer> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = ateriskServerDAO.eliminarAsteriskServers(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarParametros();
        }
        return exito;
    }

    public List<AsteriskServer> sacarSeleccionado() {
        AsteriskServer selectParametros = new AsteriskServer();
        List<AsteriskServer> listadoSeleccionado = new ArrayList<AsteriskServer>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectParametros = (AsteriskServer) o;
            if (selectParametros.getSeleccionado()) {
                listadoSeleccionado.add(selectParametros);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public AsteriskServer getSelAsteriskServer() {
        return selAsteriskServer;
    }

    public void setSelAsteriskServer(AsteriskServer selAsteriskServer) {
        this.selAsteriskServer = selAsteriskServer;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }

    public SelectItem[] getSelectItemTipos() {
        return selectItemTipos;
    }

    public void setSelectItemTipos(SelectItem[] selectItemTipos) {
        this.selectItemTipos = selectItemTipos;
    }
    private String id;
    private String host;
    private String basedatos;
    private String usuario;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getBasedatos() {
        return basedatos;
    }

    public void setBasedatos(String basedatos) {
        this.basedatos = basedatos;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Boolean getAtriEditable() {
        return atriEditable;
    }

    public void setAtriEditable(Boolean atriEditable) {
        this.atriEditable = atriEditable;
    }
}
