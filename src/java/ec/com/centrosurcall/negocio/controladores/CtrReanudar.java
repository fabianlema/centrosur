/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.HistorialDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.DAO.TemporalDAO;
import ec.com.centrosurcall.datos.modelo.Historial;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.datos.modelo.Temporal;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.mail.internet.InternetAddress;

/**
 *
 * @author Fabian
 */
@ManagedBean
@ViewScoped
public final class CtrReanudar extends MantenimientoGenerico implements Serializable {

    CtrSession session;
    private HistorialDAO historialDAO = new HistorialDAO();
    private ParametrosDAO parametro = new ParametrosDAO();
    private Parametro parametroC;
    private String mensaje;
    private Boolean esReanudar;
    private TemporalDAO temporalDAO = new TemporalDAO();
    private boolean accion;

    public CtrReanudar() {
        this.session = ((CtrSession) FacesUtils.getManagedBean("ctrSession"));
        verificarEstado();
    }

    private void verificarEstado() {
        int numeroMensajes = 0;
        parametroC = this.parametro.getParametroByAtributo("CONTINUARENVIANDO");
        List<Temporal> objListTemporal = this.temporalDAO.getTemporal();
        if (objListTemporal != null) {
            numeroMensajes = objListTemporal.size();
        }

        if (parametroC.getValor().equals("1")) {
            if (numeroMensajes > 0) {
                esReanudar = true;
                mensaje = "El envio se esta realizando normalmente y existen " + numeroMensajes + " mensajes enviandose";
            } else {
                esReanudar = false;
                mensaje = "El envio se esta realizando normalmente";
            }
        } else {
            esReanudar = true;
            mensaje = "Existen " + numeroMensajes + " mensajes en cola para ser enviados";
        }
    }

    public void reanudarEnvio() {
        accion = true;
        parametroC.setValor("1");
        parametro.saveParametro(parametroC, 0);
        Parametro svr = parametro.getParametroByAtributo("DELAY");
        final int sleep = Integer.parseInt(svr.getValor());
        svr = parametro.getParametroByAtributo("DELAY10");
        final int sleep10 = Integer.parseInt(svr.getValor());
        svr = parametro.getParametroByAtributo("NUMEROMSN");
        final int numeromsns = Integer.parseInt(svr.getValor());
        svr = parametro.getParametroByAtributo("DELAYNUMMSN");
        final int delaymsn = Integer.parseInt(svr.getValor());
        //new Thread(new CtrReanudar2(this, numeromsns, sleep, sleep10, delaymsn)).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InternetAddress[] telefono = new InternetAddress[1];
                    int contador = 0;
                    int numeromsn = 0;
                    int usuarios = temporalDAO.getUsuariosTemporal();
                    if (usuarios > 0) {
                        numeromsn = numeromsns / usuarios;
                        Usuario uTemp = parametroSession.getUsuario();
                        while (accion) {
                            List listTemp = temporalDAO.getSPFirstTemporal(0);
                            Temporal tmp = listTemp.size() > 0 ? (Temporal) listTemp.get(0) : null;

                            if (tmp != null) {
                                String mensaje = tmp.getMensaje().trim();
                                String numero = tmp.getNumero();
                                telefono[0] = new InternetAddress(numero + "@" + parametroSession.getDominio2n());
                                String mensajeDevuelto = parametroSession.getSmtp2sms().send2N(telefono, mensaje.length() > 160 ? mensaje.substring(0, 160) : mensaje);

                                if (mensajeDevuelto.trim().equals("0")) {
                                    historialDAO.saveHistorial(new Historial(new Date(), numero, mensaje, "1", Integer.valueOf(0), Integer.valueOf(1), uTemp), 3);

                                    Thread.sleep(sleep);
                                } else {
                                    historialDAO.saveHistorial(new Historial(new Date(), numero, mensaje, "1", Integer.valueOf(1), Integer.valueOf(1), uTemp), 3);
                                    System.out.println("Mensaje devuelto 2N: " + mensajeDevuelto);
                                    Thread.sleep(sleep10);
                                }

                                contador += 1;
                                if (mensajeDevuelto.trim().equals("1")) {
                                    accion = false;
                                    System.out.println("La cola de detuvo por error");
                                }

                                if (numeromsn - contador <= 0) {
                                    Thread.sleep(delaymsn);
                                    numeromsn = numeromsns / temporalDAO.getUsuariosTemporal();
                                    contador = 0;
                                }
                            } else {
                                accion = false;
                                System.out.println("La cola de envío se completó");
                            }
                        }
                    } else {
                        accion = false;
                        System.out.println("No hay usuarios en lista de espera");

                    }
                } catch (Exception e) {
                    System.out.println("Error en el envío masivo" + e);
                }
            }
        }).start();

        verificarEstado();
        //historialDao.eliminarHistorial(objReporte);
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Boolean getEsReanudar() {
        return esReanudar;
    }

    public void setEsReanudar(Boolean esReanudar) {
        this.esReanudar = esReanudar;
    }

    public TemporalDAO getTemporalDAO() {
        return temporalDAO;
    }

    public void setTemporalDAO(TemporalDAO temporalDAO) {
        this.temporalDAO = temporalDAO;
    }

    public HistorialDAO getHistorialDAO() {
        return historialDAO;
    }

    public void setHistorialDAO(HistorialDAO historialDAO) {
        this.historialDAO = historialDAO;
    }
}
