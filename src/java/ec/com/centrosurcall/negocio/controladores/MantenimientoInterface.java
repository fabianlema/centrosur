/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author TaurusTech
 */
public interface MantenimientoInterface {
    
    String nuevo();

    String editar();

    void imprimir();

    String refrescar();

    void auditoria();
    
    String regresar();
    
       
    void seleccionarTodos(AjaxBehaviorEvent b);
    

    void cargarColumnasGrupo();
    
    void buscarWsql();
    
      
    

    
}
