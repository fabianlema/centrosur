/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.HistorialDAO;
import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Historial;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Fabian
 */
@ManagedBean
@ViewScoped
public class CtrDeleteListN implements Serializable {

    CtrSession session;
    private Date fechaA;
    private Date fechaB;
    private List<Historial> objReporte = new ArrayList();
    private HistorialDAO historialDao = new HistorialDAO();

    public CtrDeleteListN() {
        this.session = ((CtrSession) FacesUtils.getManagedBean("ctrSession"));
        actualizarFiltro();
    }

    public void actualizarFiltro() {
        this.objReporte.clear();
        if ((this.fechaA != null) && (this.fechaB != null)) {
            Consultas consulta = new Consultas();
            String desde = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.fechaA);
            String hasta = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.fechaB);

            String cadenaSQL = "";
            cadenaSQL = "select * from  Historial WITH(NOLOCK) where grupo='FAILED' and fecha >= '" + desde + "' and fecha <= '" + hasta + "'";
            this.objReporte = historialDao.getHistorialLN(desde, hasta);            
        }
    }

    public void detenerEnvio() {
        historialDao.eliminarHistorial(objReporte);
        actualizarFiltro();
    }

    public Date getFechaA() {
        return fechaA;
    }

    public void setFechaA(Date fechaA) {
        this.fechaA = fechaA;
    }

    public Date getFechaB() {
        return fechaB;
    }

    public void setFechaB(Date fechaB) {
        this.fechaB = fechaB;
    }

    public List<Historial> getObjReporte() {
        return objReporte;
    }

    public void setObjReporte(List<Historial> objReporte) {
        this.objReporte = objReporte;
    }
}
