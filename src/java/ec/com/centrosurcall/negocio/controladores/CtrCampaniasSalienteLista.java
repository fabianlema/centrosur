/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.CampaniaDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.DAO.ClientesDAO;
import ec.com.centrosurcall.datos.modelo.Campania;
//import ec.com.centrosurcall.datos.modelo.Historial;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.datos.modelo.ClienteAux;
import ec.com.centrosurcall.datos.modelo.Usuario;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;

import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
//import javax.faces.model.SelectItem;
import javax.mail.internet.InternetAddress;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;

/**
 *
 * @author TaurusTech
 */
@ManagedBean
@ViewScoped
public final class CtrCampaniasSalienteLista extends MantenimientoGenerico implements Serializable {
    private List<Object> objColumna = new ArrayList<>();
    private ClientesDAO clienteDAO = new ClientesDAO();
//    private HistorialDAO historialDAO = new HistorialDAO();
    private boolean accion;
    private String msjadv = "", msjinf = "";//msjerror = "", ;
    private SelectItem[] selectItemCampanias;
    private CampaniaDAO campaniaDAO = new CampaniaDAO();
    private String idCampania;

    public CtrCampaniasSalienteLista() {
        System.out.println("Contructor Definido");
        cargarCampanias();
    }
    
    public void cargarCampanias(){
        List<Campania> listaCampania = campaniaDAO.getCampanias();
        if (listaCampania != null) {
            int cont = 1;
            selectItemCampanias = new SelectItem[listaCampania.size()+1];
            selectItemCampanias[0] = new SelectItem("0", "--SELECCIONE--");
            for (Campania obj : listaCampania) {
                selectItemCampanias[cont] = new SelectItem(String.valueOf(obj.getId()), obj.getNombre());
                cont++;
            }
        }
    }

    //Carga los datos del archivo excel a la interfaz.
    public void processFileUpload(FileUploadEvent event) throws Exception {
        UploadedFile item = event.getUploadedFile();
        List cellDataList = new ArrayList();
        try {
            POIFSFileSystem fsFileSystem = new POIFSFileSystem(item.getInputStream());
            HSSFWorkbook workBook = new HSSFWorkbook(fsFileSystem);
            HSSFSheet hssfSheet = workBook.getSheetAt(0); //Hoja del archivo excel a cargar

            Iterator rowIterator = hssfSheet.rowIterator();

            while (rowIterator.hasNext()) {
                HSSFRow hssfRow = (HSSFRow) rowIterator.next();
                //Iterator iterator = hssfRow.cellIterator();
                List cellTempList = new ArrayList();
                for (int i=0; i<hssfRow.getLastCellNum();i++){
                    HSSFCell hssfCell = hssfRow.getCell(i);
                    if (hssfCell!=null)
                        hssfCell.setCellType(Cell.CELL_TYPE_STRING);
                    cellTempList.add(hssfCell);
                }
                cellDataList.add(cellTempList);
            }
        } catch (Exception e) {
            System.out.println("Error processFileUpload: " + e);
            e.printStackTrace();
        }
        printToConsole(cellDataList);
    }
    

    //Muestra los datos del archivo excel a la pantalla    
    private void printToConsole(List cellDataList) {
        List<Object> listColumnas = new ArrayList<Object>();
        boolean salir=false;
        //String cadena="";
        try {
            for (int i = 1; i < cellDataList.size(); i++)//Recorrido del numero de filas que tenga el archivo
            {
                List cellTempList = (List) cellDataList.get(i);
                String[] temp = new String[9];
                //cadena = "";
                //System.out.println("TAMAANO:" + cellTempList.size());
                for (int j = 0; j < 9; j++) {//Recorrido de columnas que se haya establecido
                    if (cellTempList.size()>0){
                        if (j==0){
                            if ((HSSFCell) cellTempList.get(j) == null || ((HSSFCell) cellTempList.get(j)).toString().trim().equals("")){
                                salir=true;
                                break;
                            }   
                        }
                        if (j < cellTempList.size()) {
                            //cadena += ((HSSFCell) cellTempList.get(j) != null ? ((HSSFCell) cellTempList.get(j)).toString().trim().equals("")?"N/A":((HSSFCell) cellTempList.get(j)).toString().trim() : "N/A")+"--";
                            temp[j] = ((HSSFCell) cellTempList.get(j) != null ? ((HSSFCell) cellTempList.get(j)).toString().trim().equals("")?"N/A":((HSSFCell) cellTempList.get(j)).toString().trim() : "N/A");
                        } else {
                            //cadena += "N/A1";
                            temp[j] = "N/A";
                        }                        
                    }                    
                }
                //System.out.println("CADENA:" + cadena);
                if (salir) break;
                if (cellTempList.size()>0){
                    listColumnas.add(temp);                    
                }                
            }
            if (listColumnas.size() > 0) {
                objColumna = listColumnas;
            } else {
                objColumna = null;
            }
        } catch (Exception e) {
            System.out.println("Error printToConsole: " + e);
            e.printStackTrace();
        }
    }

    public String regresar() {
        //temporalDAO.dropTable();
        return "frmPrincipal";
    }

    public String getMsjadv() {
        return msjadv;
    }

    public void setMsjadv(String msjadv) {
        this.msjadv = msjadv;
    }

    public List<Object> getObjColumna() {
        return objColumna;
    }

    public void setObjColumna(List<Object> objColumna) {
        this.objColumna = objColumna;
    }

    public String getMsjinf() {
        return msjinf;
    }

    public void setMsjinf(String msjinf) {
        this.msjinf = msjinf;
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea cargar la lista de clientes??");
        setMensajeCorrecto("Lista de llamadas Guardado");
        setMensajeError("Error al enviar, probablemente no seleccionÃ³ la columna del nÃºmero");
    }

    @Override
    public boolean grabar() {
        //Graba los datos del archivo excel a la base de datos en una tabla temporal
        boolean bandera = true;
        try {
            //if (idColumnaNumero != -1 && idColumnaNumero != 0){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Parametro prefijos = new ParametrosDAO().getParametroByAtributo("PREFIJOSVALIDOS");
                        String secuencia, codcli, nombres, apellidos, numero_casa, numero_celular, adicional1, adicional2, adicional3;
                        boolean validado = true;
                        for (int i = 0; i < objColumna.size(); i++) {
                            
                            secuencia = (((String[]) objColumna.get(i))[0]).trim();
                            codcli = (((String[]) objColumna.get(i))[1]).trim();
                            nombres = (((String[]) objColumna.get(i))[2]).trim();
                            apellidos = (((String[]) objColumna.get(i))[3]).trim();
                            numero_casa = (((String[]) objColumna.get(i))[5]).trim();
                            numero_celular = (((String[]) objColumna.get(i))[4]).trim();
                            adicional1 = (((String[]) objColumna.get(i))[6]).trim();
                            adicional2 = (((String[]) objColumna.get(i))[7]).trim();
                            adicional3 = (((String[]) objColumna.get(i))[8]).trim();
                            
                            validado = true;
                            /*if (numero.length() == 10) {
                            if (numero_celular.length() > 5) {
                                /*String[] listaPrefijos = prefijos.getValor().split(",");
                                for (int k = 1; k < listaPrefijos.length; k++) {
                                    if (!numero.startsWith(listaPrefijos[k])){
                                        validado = false;
                                        break;
                                    }
                                }
                                try {
                                    Integer.parseInt(numero_celular);
                                } catch (NumberFormatException e) {
                                    validado = false;
                                }*/
                                if (validado) {
                                    ClienteAux clientTemp = new ClienteAux();
                                    clientTemp.setSecuencia(secuencia);
                                    clientTemp.setClicod(codcli);
                                    clientTemp.setNombres(nombres);
                                    clientTemp.setApellidos(apellidos);
                                    clientTemp.setTlfCasa(numero_casa);
                                    clientTemp.setTlfCelular(numero_celular);
                                    clientTemp.setAdicional1(adicional1);
                                    clientTemp.setAdicional2(adicional2);
                                    clientTemp.setAdicional3(adicional3);
                                    clientTemp.setIdUsuario(0);
                                    clientTemp.setEstado(0);
                                    clientTemp.setIntentos(0);
                                    clientTemp.setIdCampania(Integer.parseInt(idCampania));
                                    clienteDAO.saveClienteAux(clientTemp, 3);
                                } else {
//                                    historialDAO.saveHistorial(new Historial(new Date(), numero_celular, apellidos, "1", 1, 1, parametroSession.getUsuario()), 3);
                                }
                            /*} else {
                                if (numero_celular.length() != 0) {
                                    historialDAO.saveHistorial(new Historial(new Date(), numero_celular, apellidos, "1", 1, 1, parametroSession.getUsuario()), 3);
                                }
                            }*/
                        }
                        clienteDAO.getSPActualizaClientes();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();


            //if (bandera == false) setMensajeError("No se puede enviar mensajes a los siguientes nÃºmeros:\n" + msjerror + "\nCorrija el archivo y vuelva a cargar");
            //else 

            return bandera;
            //}else return !bandera;

        } catch (Exception e) {
            e.printStackTrace();
            return !bandera;
        }
    }

    /*public void ejecutarHilo() {
    //Realiza el envÃ­o de mensajes a travez de un thread extrayendo la informacion de la base de datos
    System.out.println("Ingresa Hilo");
    accion = true;
    //Parametro svr = new ParametrosDAO().getParametroByAtributo("DELAY");
    //final int sleep = Integer.parseInt(svr.getValor());
    Parametro svr = new ParametrosDAO().getParametroByAtributo("DELAY");
    final int sleep = Integer.parseInt(svr.getValor());
    svr = new ParametrosDAO().getParametroByAtributo("DELAY10");
    final int sleep10 = Integer.parseInt(svr.getValor());
    svr = new ParametrosDAO().getParametroByAtributo("NUMEROMSN");
    final int numeromsns = Integer.parseInt(svr.getValor());
    svr = new ParametrosDAO().getParametroByAtributo("DELAYNUMMSN");
    final int delaymsn = Integer.parseInt(svr.getValor());
    new Thread(new Runnable() {
    @Override
    public void run() {
    try {
    String mensajeDevuelto;
    String mensaje = "";//, numero;
    InternetAddress[] telefonos;
    int contador = 0, numeromsn = 0;
    numeromsn = (int)(numeromsns/temporalDAO.getUsuariosTemporal());
    Usuario uTemp = parametroSession.getUsuario();
    while (accion) {
    List<Temporal> tmp = temporalDAO.getSPListaTemporal(uTemp.getId());
    if (tmp.size() > 0) {
    telefonos = new InternetAddress[tmp.size()];
    //System.out.println("TamaÃ±o Vector: " + tmp.size());
    contador = contador + tmp.size();
    for (int i = 0; i < tmp.size(); i++) {
    //Object[] temp = (Object[]) tmp.get(i);
    if (i == 0) {
    mensaje = tmp.get(i).getMensaje().trim();
    }
    telefonos[i] = new InternetAddress(tmp.get(i).getNumero() + "@" + parametroSession.getDominio2n());
    //return new Temporal(Integer.parseInt(temp[0].toString()), temp[1].toString(), temp[2].toString(), Boolean.parseBoolean(temp[3].toString()));
    }
    mensajeDevuelto = parametroSession.getSmtp2sms().send2N(telefonos, mensaje.length() > 160 ? mensaje.substring(0, 160) : mensaje);
    //mensajeDevuelto = "0";
    if (mensajeDevuelto.trim().equals("0")) {
    for (int i = 0; i < tmp.size(); i++) {
    //Object[] temp = (Object[]) tmp.get(i);
    historialDAO.saveHistorial(new Historial(new Date(), tmp.get(i).getNumero(), tmp.get(i).getMensaje(), "1", 0, 1, uTemp), 3);
    }
    //temporalDAO.deleteTemporalMasivoMSN();
    Thread.sleep(sleep);
    } else {
    for (int i = 0; i < tmp.size(); i++) {
    //Object[] temp = (Object[]) tmp.get(i);
    historialDAO.saveHistorial(new Historial(new Date(), tmp.get(i).getNumero(), tmp.get(i).getMensaje(), "1", 1, 1, uTemp), 3);
    }
    //temporalDAO.deleteTemporalMasivoMSN();
    System.out.println("Mensaje devuelto 2N: " + mensajeDevuelto);
    Thread.sleep(sleep10);
    }
    if (numeromsn - contador <= 0){
    Thread.sleep(delaymsn);
    numeromsn = (int)(numeromsns/temporalDAO.getUsuariosTemporal());
    contador = 0;
    }
    } else {
    accion = false;
    System.out.println("La cola de envÃ­o se completÃ³");
    }
    }
    } catch (Exception e) {
    System.out.println("Error en el envÃ­o masivo");
    }
    }
    }).start();
    }*/
    public String getIdCampania() {
        return idCampania;
    }

    public void setIdCampania(String idCampania) {
        this.idCampania = idCampania;
    }

    public SelectItem[] getSelectItemCampanias() {
        return selectItemCampanias;
    }

    public void setSelectItemCampanias(SelectItem[] selectItemCampanias) {
        this.selectItemCampanias = selectItemCampanias;
    }
}