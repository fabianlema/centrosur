/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.HistorialDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.modelo.Parametro;
import java.util.Properties;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.mail.Authenticator;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 *
 * @author Fabian
 */
//@ManagedBean(eager = true)
//@ApplicationScoped
@ManagedBean
@ViewScoped
public class CtrSMS {

    public CtrSMS() {
        new ControlMensajes().start();
    }
}
class ControlMensajes extends Thread {

    private HistorialDAO historialDAO = new HistorialDAO();
    private String dominio2n;
    private String usuario2n;
    private String contrasenia2n;
    private String ip2n;
    private String txtSinSaldo;
    private String host;
    private String puerto;
    private String user;
    private String password;
    private String mail;
    private String mensajeSinSaldo;
    private String mensajeServicio;
    private int numFaileds;
    private ParametrosDAO parametro = new ParametrosDAO();

    public ControlMensajes() {
        this.dominio2n = this.parametro.getParametroByAtributo("DOMINIO").getValor();
        this.usuario2n = this.parametro.getParametroByAtributo("USUARIO").getValor();
        this.contrasenia2n = this.parametro.getParametroByAtributo("CONTRASENIA").getValor();
        this.ip2n = this.parametro.getParametroByAtributo("IP2N").getValor();

        this.host = this.parametro.getParametroByAtributo("HOSTSMTP").getValor();
        this.puerto = this.parametro.getParametroByAtributo("PORTSMTP").getValor();
        this.user = this.parametro.getParametroByAtributo("USERSMTP").getValor();
        this.password = this.parametro.getParametroByAtributo("PASSSMTP").getValor();
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.err.println("Ingresooo Hilo");
                getNoValidos();
                Thread.sleep(1800000);
                //Thread.sleep(60000);
            } catch (Exception ex) {
                System.err.println("Error Hilo:" + ex);
                ex.printStackTrace();
            }
        }
    }

    public void getNoValidos() {
        try {
            this.txtSinSaldo = this.parametro.getParametroByAtributo("TXTSINSALDO").getValor();
            this.mail = this.parametro.getParametroByAtributo("MAILNOTIFICACIONES").getValor();
            this.numFaileds = Integer.parseInt(this.parametro.getParametroByAtributo("NUMSMSFAILED").getValor());
            this.mensajeServicio = this.parametro.getParametroByAtributo("MSJSINSERVICIO").getValor();
            this.mensajeSinSaldo = this.parametro.getParametroByAtributo("MSJSINSALDO").getValor();

            int contFail = 0;
            boolean sinSaldo = false;
            Properties props = new Properties();
            int numeroMensajes = 0;

            //String host = this.ip2n;
            //String username = this.usuario2n + "@" + this.dominio2n;
            //String password = this.contrasenia2n;
            String provider = "pop3";

            Session session = Session.getInstance(props, null);
            Store store = session.getStore(provider);
            store.connect(this.ip2n, this.usuario2n + "@" + this.dominio2n, this.contrasenia2n);

            Folder inbox = store.getFolder("INBOX");
            if (inbox == null) {
                System.out.println("No INBOX");
                //System.exit(1);
            }
            inbox.open(Folder.READ_WRITE);
            Message[] messages = inbox.getMessages();
            
            numeroMensajes = messages.length;
            for (int i = 0; i < numeroMensajes; i++) {
                if (messages[i].getContent().toString().contains(this.txtSinSaldo)) {
                    sinSaldo = true;
                }
                if (messages[i].getSubject().equals("FAILED")) {
                    contFail++;
                    this.historialDAO.getSPSetIncorrecct(messages[i].getFrom()[0].toString().substring(0, 10), messages[i].getContent().toString().trim());
                }
                //messages[i].setFlag(Flag.DELETED, true);
            }
            Parametro parametroC = this.parametro.getParametroByAtributo("CONTINUARENVIANDO");
            if (contFail > this.numFaileds) {
                if (!parametroC.getValor().equals("0")){
                    parametroC.setValor("0");
                    this.parametro.saveParametro(parametroC, 0);
                }                
                sendEmail(mensajeServicio);
                //ENVIAR NOTIFICACION ERROR PROVEEDOR GSM
            }// else {
            //   parametroC.setValor("1");
            //}
            if (numeroMensajes==0 && parametroC.getValor().equals("0")){
                parametroC.setValor("1");
                this.parametro.saveParametro(parametroC, 0);
            }
            if (sinSaldo) {
                sendEmail(mensajeSinSaldo);
                //ENVIAR NOTIFICACION SIN SALDO
            }
            Flags deleted = new Flags(Flags.Flag.DELETED);
            inbox.setFlags(messages, deleted, true);
            inbox.close(true);
            store.close();
        } catch (Exception ex) {
            System.out.println("Error:" + ex);
        }
    }

    public void sendEmail(String mensaje) {
        try {
            Properties properties = new Properties();
            properties.put("mail.smtp.auth", "true");
            properties.setProperty("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", this.host);
            properties.put("mail.smtp.port", this.puerto);
            properties.setProperty("ssl.SocketFactory.provider", "ec.com.centrosur.utils.ZimbraSSLSocketFactory");
            properties.setProperty("mail.smtp.socketFactory.class", "ec.com.centrosur.utils.ZimbraSSLSocketFactory");

            Session session = Session.getInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(user, password);
                }
            });

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(this.user));
            String[] mails = mail.split(",");
            InternetAddress[] direciones = new InternetAddress[mails.length];
            for (int i = 0; i < mails.length; i++) {
                direciones[i] = new InternetAddress(mails[i]);
            }
            //InternetAddress[] direciones = {new InternetAddress(mail)};
            message.addRecipients(Message.RecipientType.TO, direciones);
            message.setSubject("Notificacion Aplicacion SMS");
            message.setContent(mensaje, "text/plain; charset=UTF-8");
            Transport.send(message);
        } catch (Exception ex) {
            System.out.println("Error mail:" + ex);
        }
    }
}
