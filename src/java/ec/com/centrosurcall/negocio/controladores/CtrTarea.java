/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author fabia
 */

import ec.com.centrosurcall.datos.DAO.AgenciasDAO;
import ec.com.centrosurcall.datos.DAO.AgendaaplisDAO;
import ec.com.centrosurcall.datos.DAO.ComandosDAO;
import ec.com.centrosurcall.datos.DAO.DateaplisDAO;
import ec.com.centrosurcall.datos.DAO.EquiposDAO;
import ec.com.centrosurcall.datos.DAO.TareasDAO;
import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Agencia;
import ec.com.centrosurcall.datos.modelo.Agendaapli;
import ec.com.centrosurcall.datos.modelo.Comando;
import ec.com.centrosurcall.datos.modelo.Dateapli;
import ec.com.centrosurcall.datos.modelo.Equipo;
import ec.com.centrosurcall.datos.modelo.Tarea;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.PrintStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public final class CtrTarea extends MantenimientoGenerico
  implements MantenimientoInterface, Serializable
{
  CtrSession session;
  private Tarea selTarea = new Tarea();
  TareasDAO tareasDAO = new TareasDAO();
  String opcionBoton = "";
  private List<Integer> list;
  private int idComandoSeleccionado;
  private int idEquipo;
  SelectItem item = new SelectItem();

  private List<Comando> listComandos = new ArrayList(0);
  private Comando selComando;
  private boolean isNewComand = false;
  private Date fechaSeleccionada;
  private String descripcionRegistro;
  private SelectItem[] selectItemEquipo;
  private List<Equipo> objEquipos;
  private int horaAgenda = 0; private int minutoAgenda = 0;
  private int horaAgendaFE = 0; private int minutoAgendaFE = 0;
  private List<Integer> dias;
  private List<Integer> meses;
  private boolean isEditable = false; private boolean mostrarComando = true;
  private String descripcion;

  public CtrTarea()
  {
    this.session = ((CtrSession)FacesUtils.getManagedBean("ctrSession"));
    setOpcionBoton("Grabar");
    this.session.setTituloSeccion("Listado");
    this.item.setLabel("-SELECCIONE-");
    this.item.setValue(null);
    
    
    this.mostrarComando = true; 
//    if (this.parametroSession.getUsuario().getIdRol().intValue() == 4) 
//        this.mostrarComando = true; 
//    else
//      this.mostrarComando = false;
    
    
    if ((this.parametroSession.getEsEditar().booleanValue()) && (this.parametroSession.getObjeto() != null)) {
      cargoTareaEditar();
      this.isEditable = true;
    }
    else {
      this.mostrarComando = false;
    }cargarColumnasGrupo();
    cargarTareas();
    cargarEquipos();
    setMensajeConfirmacion("Seguro desea grabar esta tarea?");
    confPop();

    this.list = new ArrayList();
    this.list.add(Integer.valueOf(1));
    this.list.add(Integer.valueOf(2));
    this.list.add(Integer.valueOf(3));
  }

  public void cargarEquipos() {
    this.selectItemEquipo = new SelectItem[1];
    this.selectItemEquipo[0] = this.item;
    this.objEquipos = new EquiposDAO().getEquipo();
    int cont;
    Iterator i$;
    if ((this.objEquipos != null) && (this.objEquipos.size() > 0)) {
      cont = 1;
      this.selectItemEquipo = new SelectItem[this.objEquipos.size() + 1];
      this.selectItemEquipo[0] = this.item;
      for (i$ = this.objEquipos.iterator(); i$.hasNext(); ) { Object obj = i$.next();
        Equipo cl = (Equipo)obj;
        this.selectItemEquipo[cont] = new SelectItem(Integer.valueOf(cl.getId()), cl.getNombre());
        cont++;
      }
    }
//    if (this.parametroSession.getUsuario().getIdRol().intValue() != 4) {
//      Agencia aux = new AgenciasDAO().getAgenciaXLogin(this.parametroSession.getUsuario().getLogin());
//      if (aux != null) this.idEquipo = aux.getIdEquipo().intValue(); 
//    }
  }

  public void cargarTareas()
  {
    this.listaBaseObjetos.clear();
    this.listaObjetos.clear();
    List listaTarea = this.tareasDAO.getTareas();
    if (listaTarea != null) {
      this.listaBaseObjetos.addAll(listaTarea);
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargoTareaEditar() {
    boolean existeObjeto = false;
    try {
      if (this.session.getObjeto().getClass().getName().contains("Tarea")) {
        if (((Tarea)this.session.getObjeto()).getId() > 0)
          existeObjeto = true;
      }
      else {
        this.parametroSession.setEsEditar(Boolean.valueOf(false));
        this.parametroSession.setEsNuevo(Boolean.valueOf(false));
      }
    } catch (Exception e) {
      System.out.println(e.getCause().getMessage());
    }
    if (existeObjeto) {
      this.opcionBoton = "Grabar";
      this.selTarea = ((Tarea)this.session.getObjeto());
      cargoComandos();
    }
  }

  public void cargoComandos() {
    this.listComandos = new ComandosDAO().getComandos(this.selTarea.getId());
  }

  public void confPop() {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro desea registrar la tarea??");
    setMensajeCorrecto("Registro grabado");
    setMensajeError("Error al Registrar");
  }

  public boolean grabar()
  {
    boolean exito = false;
    int IdTarea;
    int cont;
    Iterator i$;
    if (this.parametroSession.getEsNuevo().booleanValue()) {
      exito = this.tareasDAO.saveTarea(this.selTarea, 1);
      if (!this.isEditable) {
        IdTarea = Integer.valueOf(String.valueOf(this.consulta.getListSql("SELECT MAX(id_Tarea) FROM Tarea").get(0))).intValue();
        cont = 0;
        for (i$ = this.listComandos.iterator(); i$.hasNext(); ) { Object objeto = i$.next();

          Comando obj = (Comando)objeto;

          obj.setIdTarea(Integer.valueOf(IdTarea));
          obj.setSecuencia(Integer.valueOf(cont + 1));
          exito = new ComandosDAO().saveComando(obj, 1);
          cont++; }
      }
    }
    else
    {
      exito = this.tareasDAO.saveTarea(this.selTarea, 0);
    }
    return exito;
  }

  public void buscarWsql()
  {
    List respuesta = new ArrayList();
    this.parametroSession.setNumPaginas(1);
    this.listaObjetos.clear();
    if (!this.whereSql.equals("")) {
        for (Object obj : listaBaseObjetos) {
                Tarea cl = (Tarea) obj;
                if (cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }        
      this.listaObjetos.addAll(respuesta);
    } else {
      this.listaObjetos.addAll(this.listaBaseObjetos);
    }
  }

  public void cargarColumnasGrupo()
  {
    this.listaColumnas.put("descripcion", "Descripción");
  }

  public String regresar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.session.setObjeto(null);
    this.session.setTituloSeccion("Listado");
    return "frmAdministracionTarea";
  }

  public void auditoria()
  {
    try {
      this.selTarea = ((Tarea)this.objetoSeleccionado);
    } catch (Exception e) {
      System.out.println(e.getCause().getMessage());
    }
    if (this.selTarea.getId() > 0) {
      this.creado = this.selTarea.getDescripcion();
      setCreado(this.creado);
      this.fechacreacion = new Date().toString();
      setActualizado("");
      setFechaactualizacion("");
      this.mostrarAuditoriaVal = true;
      mostrarAuditoria();
    }
  }

  public String refrescar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.whereSql = "";
    cargarTareas();
    this.session.setSeleccionado(Boolean.valueOf(false));
    this.objetoSeleccionado = new Object();
    return "";
  }

  public boolean isExitograbar()
  {
    return super.isExitograbar();
  }

  public void imprimir()
  {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public String editar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(true));
    this.session.setTituloSeccion("Editar");
    List objetoEditar = sacarSeleccionado();
    if (objetoEditar.size() > 0) {
      this.parametroSession.setObjeto(objetoEditar.get(0));
      try {
        if (((Tarea)objetoEditar.get(0)).getId() > 0) {
          this.parametroSession.setBoton("btnActualizar");
          return "frmTareas";
        }
        return "";
      }
      catch (Exception e) {
        return "";
      }
    }
    return "";
  }

  public String nuevo()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(true));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    this.parametroSession.setBoton("btnGrabar");
    setOpcionBoton("nuevo");
    this.session.setTituloSeccion("Nuevo");
    return "frmTareas";
  }

  public void confPopEliminar() {
    this.banderaUna = false;
    setMensajeConfirmacion("Seguro quiere eliminar?");
    setMensajeCorrecto("Eliminacion exitosa!");
    setMensajeError("Existen errores en:");
  }

  public boolean eliminar()
  {
    this.parametroSession.setEsNuevo(Boolean.valueOf(false));
    this.parametroSession.setEsEditar(Boolean.valueOf(false));
    boolean exito = false;
    this.listaErrores = new ArrayList();
    List listaAeliminar = sacarSeleccionado();
    if (listaAeliminar.size() > 0) {
      this.listaErrores = this.tareasDAO.eliminarTarea(listaAeliminar, this.listaObjetos);
    }
    if (this.listaErrores.isEmpty()) {
      exito = true;
      cargarTareas();
    }
    return exito;
  }

  public List<Tarea> sacarSeleccionado() {
    Tarea selectTarea = new Tarea();
    List listadoSeleccionado = new ArrayList();
    boolean exito = false;
    for (Iterator i$ = this.listaObjetos.iterator(); i$.hasNext(); ) { Object o = i$.next();
      selectTarea = (Tarea)o;
      if (selectTarea.isSeleccionado()) {
        listadoSeleccionado.add(selectTarea);
        exito = true;
        break;
      }
    }
    if (exito) {
      return listadoSeleccionado;
    }
    return listadoSeleccionado;
  }

  public void remove()
  {
    if (!this.list.isEmpty())
      this.list.remove(this.list.size() - 1);
  }

  public List<Integer> getList() {
    return this.list;
  }

  public void add() {
    this.list.add(Integer.valueOf(this.list.size() + 1));
  }

  public void guardarComando() {
    boolean exito = false;
    if (this.isEditable) {
      if (!this.isNewComand) {
        exito = new ComandosDAO().saveComando(this.selComando, 0);
      } else {
        List maxId = this.consulta.getListSql("SELECT MAX(secuencia) FROM Comando WHERE id_tarea = " + this.selTarea.getId());
        this.selComando.setIdTarea(Integer.valueOf(this.selTarea.getId()));
        this.selComando.setSecuencia(Integer.valueOf(maxId.get(0) != null ? Integer.valueOf(String.valueOf(maxId.get(0))).intValue() + 1 : 1));
        exito = new ComandosDAO().saveComando(this.selComando, 1);
        cargoComandos();
      }
    }
    else
    {
      this.listComandos.add(this.selComando);
    }
  }

  public void guardarFechaEspecifica() {
    Date fecha = getFechaSeleccionada();
    Dateapli dateapli = new Dateapli();
    dateapli.setAnio(Integer.valueOf(Integer.parseInt(new SimpleDateFormat("yyyy").format(fecha))));
    dateapli.setDescripcion(this.descripcionRegistro);
    dateapli.setDiames(Integer.valueOf(Integer.parseInt(new SimpleDateFormat("dd").format(fecha))));
    dateapli.setHora(Integer.valueOf(this.horaAgendaFE));
    dateapli.setIdTarea(Integer.valueOf(this.selTarea.getId()));
    dateapli.setIdEquipo(Integer.valueOf(this.idEquipo));
    dateapli.setMes(Integer.valueOf(Integer.parseInt(new SimpleDateFormat("MM").format(fecha)) - 1));
    dateapli.setMinuto(Integer.valueOf(this.minutoAgendaFE));
    new DateaplisDAO().saveDateapli(dateapli, 0);

    this.descripcionRegistro = "";
    setFechaSeleccionada(null);
    this.horaAgendaFE = 0;
    this.minutoAgendaFE = 0;
  }

  public void guardarAgenda() {
    for (int i = 0; i < this.dias.size(); i++) {
      for (int j = 0; j < this.meses.size(); j++) {
        Agendaapli agendaapli = new Agendaapli();
        agendaapli.setDescripcion(this.descripcionRegistro);
        agendaapli.setDiasemana(Integer.valueOf(Integer.parseInt(String.valueOf(this.dias.get(i)))));
        agendaapli.setHora(Integer.valueOf(this.horaAgenda));
        agendaapli.setIdEquipo(Integer.valueOf(this.idEquipo));
        agendaapli.setIdTarea(Integer.valueOf(this.selTarea.getId()));
        agendaapli.setMes(Integer.valueOf(Integer.parseInt(String.valueOf(this.meses.get(j)))));
        agendaapli.setMinuto(Integer.valueOf(this.minutoAgenda));
        new AgendaaplisDAO().saveAgendaapli(agendaapli, 0);
      }
    }
    this.dias = null;
    this.meses = null;
    this.descripcionRegistro = "";
    this.horaAgenda = 0;
    this.minutoAgenda = 0;
  }

  public Tarea getSelTarea() {
    return this.selTarea;
  }

  public void setSelTarea(Tarea selTarea) {
    this.selTarea = selTarea;
  }

  public String getOpcionBoton() {
    return this.opcionBoton;
  }

  public void setOpcionBoton(String opcionBoton) {
    this.opcionBoton = opcionBoton;
  }

  public String getDescripcion()
  {
    return this.descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

  public int getIdComandoSeleccionado() {
    return this.idComandoSeleccionado;
  }

  public void setIdComandoSeleccionado(int idComandoSeleccionado)
  {
    this.idComandoSeleccionado = idComandoSeleccionado;

    if (idComandoSeleccionado == 0) {
      setSelComando(new Comando());
      this.isNewComand = true;
    } else {
      for (Comando o : this.listComandos)
      {
        if (o.getId().intValue() == idComandoSeleccionado) {
          setSelComando(o);
          break;
        }
      }
      this.isNewComand = false;
    }
  }

  public List<Comando> getListComandos() {
    return this.listComandos;
  }

  public void setListComandos(List<Comando> listComandos) {
    this.listComandos = listComandos;
  }

  public Comando getSelComando() {
    return this.selComando;
  }

  public void setSelComando(Comando selComando) {
    this.selComando = selComando;
  }

  public Date getFechaSeleccionada() {
    return this.fechaSeleccionada;
  }

  public void setFechaSeleccionada(Date fechaSeleccionada) {
    this.fechaSeleccionada = fechaSeleccionada;
  }

  public String getDescripcionRegistro() {
    return this.descripcionRegistro;
  }

  public void setDescripcionRegistro(String descripcionRegistro) {
    this.descripcionRegistro = descripcionRegistro;
  }

  public SelectItem[] getSelectItemEquipo() {
    return this.selectItemEquipo;
  }

  public void setSelectItemEquipo(SelectItem[] selectItemEquipo) {
    this.selectItemEquipo = selectItemEquipo;
  }

  public int getIdEquipo() {
    return this.idEquipo;
  }

  public void setIdEquipo(int idEquipo) {
    this.idEquipo = idEquipo;
  }

  public int getHoraAgenda() {
    return this.horaAgenda;
  }

  public void setHoraAgenda(int horaAgenda) {
    this.horaAgenda = horaAgenda;
  }

  public int getMinutoAgenda() {
    return this.minutoAgenda;
  }

  public void setMinutoAgenda(int minutoAgenda) {
    this.minutoAgenda = minutoAgenda;
  }

  public List<Integer> getDias() {
    return this.dias;
  }

  public void setDias(List<Integer> dias) {
    this.dias = dias;
  }

  public List<Integer> getMeses() {
    return this.meses;
  }

  public void setMeses(List<Integer> meses) {
    this.meses = meses;
  }

  public boolean isIsEditable() {
    return this.isEditable;
  }

  public void setIsEditable(boolean isEditable) {
    this.isEditable = isEditable;
  }

  public boolean isMostrarComando() {
    return this.mostrarComando;
  }

  public void setMostrarComando(boolean mostrarComando) {
    this.mostrarComando = mostrarComando;
  }

  public int getHoraAgendaFE() {
    return this.horaAgendaFE;
  }

  public void setHoraAgendaFE(int horaAgendaFE) {
    this.horaAgendaFE = horaAgendaFE;
  }

  public int getMinutoAgendaFE() {
    return this.minutoAgendaFE;
  }

  public void setMinutoAgendaFE(int minutoAgendaFE) {
    this.minutoAgendaFE = minutoAgendaFE;
  }
}