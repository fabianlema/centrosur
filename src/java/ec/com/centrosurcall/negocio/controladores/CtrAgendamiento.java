/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;


import ec.com.centrosurcall.datos.DAO.AgendamientosDAO;
import ec.com.centrosurcall.datos.DAO.ClientesDAO;
import ec.com.centrosurcall.datos.DAO.UsuariosDAO;
//import ec.com.centrosurcall.datos.DAO.VisitasDAO;
import ec.com.centrosurcall.datos.modelo.Agendamiento;
import ec.com.centrosurcall.datos.modelo.Cliente;
import ec.com.centrosurcall.datos.modelo.Usuario;
//import ec.com.centrosurcall.datos.modelo.Visita;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.Serializable;
import java.sql.Time;

import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public final class CtrAgendamiento extends MantenimientoGenerico implements Serializable {
    private String nomAgente;
    private String numeroCliente = "";
    private String horas = "00";
    private String minutos = "00";
    private String msjError = "";
 //   private Date fechaAgendamiento;
    private String idUsuario2;
    private SelectItem[] selectItemUsuarios;
    private List<Object[]> objUsuario;
    private SelectItem items = new SelectItem();
    private CtrSession session;
    private String agenteCobranza;
    private UsuariosDAO usDAO = new UsuariosDAO();
    private Usuario obAgente2;
    private List<Object> llamadasAgendadas;
    private List<Object> visitasAgendadas;
    private String clicod;
    private Date fechaVisita;
    private String observacion;
    private boolean asignacionAgente = false;
    private boolean habilitarcheck = false;

    public CtrAgendamiento() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        //numeroCliente = session.getClicodLlamar();
        try{
            obAgente2 = usDAO.getUsuarioByAliasObj(session.getAgenteCobranza());
            agenteCobranza = obAgente2.getNombre();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        if(session.getRolUsuario().get(0).getId().getIdRol() == 4){
            asignacionAgente=true;
            habilitarcheck=true;
        }
        
        nomAgente = session.getUsuario().getNick();
        if (session.getClicodLlamar().equals("")) msjError = "Seleccione algún crédito en el formulario Cobranzas Salientes primero!!";
        else msjError = "";
        items.setLabel("-SELECCIONE-");
        items.setValue(null);
        cargarUsuarios();  
        cargarLlamadasAgendadas();
        cargarVisitasAgendadas();
    }
    
    public void cargarLlamadasAgendadas(){
        try{
            llamadasAgendadas = consulta.getListSql("select a.fecha, a.hora, a.clicod, b.nombre from dbo.agendamiento a, dbo.usuario b where idUsuario2="+session.getUsuario().getId()+" and idUsuario=b.id and a.estado=1 and a.fecha>=CONVERT (date, GETDATE()) and a.hora>=CONVERT (time, GETDATE()) order by a.fecha");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
    }
    
    public void cargarVisitasAgendadas(){
        try{
            visitasAgendadas = consulta.getListSql("select a.id, a.fechaMarca, a.clicod, IIF(a.resultadoVisita = 0, 'PENDIENTE', 'VISITADO') visita from dbo.visita a where idUSuario="+session.getUsuario().getId()+" AND fechaVisita IS NULL");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
    }
    
    public void registrarVisita(String id){
        try{
            System.out.println("Consul--" + getFechaVisita() + "--" + getObservacion() + "--" + id);
            
//            VisitasDAO visitaDAO = new VisitasDAO();
//            Visita visita = visitaDAO.getVisitaByID(Integer.parseInt(id));
//            visita.setFechaVisita(getFechaVisita());
//            visita.setObservaciones(getObservacion() != null ? getObservacion() : "");
//            visita.setResultadoVisita(1);            
//            visitaDAO.saveVisita(visita, 0);
            
            cargarVisitasAgendadas();
            
            setFechaVisita(null);
            setObservacion("");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
    }
    
    public final void cargarUsuarios(){
        setSelectItemUsuarios(new SelectItem[1]);
        getSelectItemUsuarios()[0] = items;
        
        objUsuario = consulta.getListSql("select a.id,a.nombre from usuario a, rol_usuario b where b.idUsuario=a.id and b.idRol=4 and a.estado=1 order by a.nombre");
        if (objUsuario != null && objUsuario.size() > 0) {
            int cont = 1;
            setSelectItemUsuarios(new SelectItem[objUsuario.size() + 1]);
            getSelectItemUsuarios()[0] = items;
            for (Object[] obj : objUsuario) {
               // Usuario cl = (Usuario) obj;
                getSelectItemUsuarios()[cont] = new SelectItem(String.valueOf(obj[0]), String.valueOf(obj[1]));
                cont++;
            }
        }
    }

    public String getHoras() {
        return horas;
    }

    public void setHoras(String horas) {
        this.horas = horas;
    }

    public String getMinutos() {
        return minutos;
    }

    public void setMinutos(String minutos) {
        this.minutos = minutos;
    }
    
    public String getIdUsuario2() {
        return idUsuario2;
    }

    public void setIdUsuario2(String idUsuario2) {
        this.idUsuario2 = idUsuario2;
    }
    
    public String getNomAgente(){        
        return nomAgente;
    }
    
    public void setNomAgente(String nomAgente){
        this.nomAgente = nomAgente;
    }

    public String getNumeroCliente() {        
        return numeroCliente;
    }
    
    public void setAgenteCobranza(String agenteCobranza){
        this.agenteCobranza = agenteCobranza;
    }

    public String getAgenteCobranza() {        
        return agenteCobranza;
    }
    

    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public SelectItem[] getSelectItemUsuarios() {
        return selectItemUsuarios;
    }

    public void setSelectItemUsuarios(SelectItem[] selectItemUsuarios) {
        this.selectItemUsuarios = selectItemUsuarios;
    }

    public String getMsjError() {        
        return msjError;
    }

    public void setMsjError(String msjError) {
        this.msjError = msjError;
    }
    
    public String agendarLlamada(){
        //System.out.println(getNomAgente() + "--" + getNumeroCliente() + "--" + getIdUsuario2() + "--" + getFechaAgendamiento() + "--" + getHoras() + "--" + getMinutos());
        Agendamiento agendamiento = new Agendamiento();
        agendamiento.setUsuarioByIdUsuario(session.getUsuario());
        agendamiento.setUsuarioByIdUsuario2(getAsignacionAgente() ? obAgente2 : null);
        agendamiento.setFecha(new Date());
        agendamiento.setHora(Time.valueOf(getHoras() + ":" + getMinutos() + ":00"));
        //agendamiento.setClicod(getNumeroCliente());
        agendamiento.setObservacion("");
        agendamiento.setEstado(1);
        
        AgendamientosDAO agendamientosDAO = new AgendamientosDAO();
        agendamientosDAO.saveAgendamiento(agendamiento, 1);
        
        ClientesDAO creditosDAO = new ClientesDAO();
        Cliente credito = session.getClienteLlamar();
        credito.setEstado(5);
        creditosDAO.saveCliente(credito, 0);
        session.setClicodLlamar(null);
        
        /*setHoras("0");
        setMinutos("0");
        setNumeroCliente("");
        setFechaAgendamiento(null);
        setIdUsuario2("0");*/
        //setMsjError("Crédito agendado correctamente");
        return "frmGestionCobranzaSaliente";
    }

    public List<Object> getLlamadasAgendadas() {
        return llamadasAgendadas;
    }

    public void setLlamadasAgendadas(List<Object> llamadasAgendadas) {
        this.llamadasAgendadas = llamadasAgendadas;
    }

    public List<Object> getVisitasAgendadas() {
        return visitasAgendadas;
    }

    public void setVisitasAgendadas(List<Object> visitasAgendadas) {
        this.visitasAgendadas = visitasAgendadas;
    }

    public String getClicod() {
        return clicod;
    }

    public void setClicod(String clicod) {
        this.clicod = clicod;
    }

    public Date getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(Date fechaVisita) {       
        this.fechaVisita = fechaVisita;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public boolean getAsignacionAgente() {
        return asignacionAgente;
    }

    public void setAsignacionAgente(boolean asignacionAgente) {
        this.asignacionAgente = asignacionAgente;
    }

    public boolean isHabilitarcheck() {
        return habilitarcheck;
    }

    public void setHabilitarcheck(boolean habilitarcheck) {
        this.habilitarcheck = habilitarcheck;
    }
}