 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.BandejaEntradaDAO;
import ec.com.centrosurcall.datos.DAO.ColaChatDAO;
import ec.com.centrosurcall.datos.DAO.CorreoDAO;
import ec.com.centrosurcall.datos.DAO.DatosCorreoDAO;
import ec.com.centrosurcall.datos.DAO.EventoEnfocadoDAO;
import ec.com.centrosurcall.datos.DAO.EventoProcesadoDAO;
import ec.com.centrosurcall.datos.DAO.LlamadaEntranteDAO;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.RolesDAO;
import ec.com.centrosurcall.datos.DAO.ModulosDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.modelo.BandejaEntrada;
import ec.com.centrosurcall.datos.modelo.ColaChat;
import ec.com.centrosurcall.datos.modelo.DatosCorreo;
import ec.com.centrosurcall.datos.modelo.DcorreoRol;
//import ec.com.centrosurcall.datos.DAO.TipoLlamadaDAO;
//import ec.com.centrosurcall.datos.modelo.CredencialesCorreo;
import ec.com.centrosurcall.datos.modelo.EstadoLlamada;
import ec.com.centrosurcall.datos.modelo.Evento;
import ec.com.centrosurcall.datos.modelo.EventoProcesado;
import ec.com.centrosurcall.datos.modelo.LlamadaEntrante;
import ec.com.centrosurcall.datos.modelo.Rol;
import ec.com.centrosurcall.datos.modelo.TipoLlamada;
import ec.com.centrosurcall.utils.Base64Converter;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.search.FlagTerm;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.io.FileUtils;
//import org.openide.util.Exceptions;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrRegistroLlamada extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    // @ManagedProperty(value = "#{tipoLlamadaDAO.listaTiposLlamada}")
    @ManagedProperty(value = "#{tipoLlamadaDAO.tiposLlamada}")
    private List<TipoLlamada> tipoLlamadas;
    @ManagedProperty(value = "#{eventoDAO.eventosList}")
    private List<Evento> eventos;
    private Evento selectedEvent;
    private TipoLlamada selectedType;
    private List<Boolean> estados;
    CtrSession session;
    private Rol selRol = new Rol();
    RolesDAO RolesDAO = new RolesDAO();
    ModulosDAO ModulosDAO = new ModulosDAO();
    DatosCorreoDAO datosCorreoDAO = new DatosCorreoDAO();
//    TipoLlamadaDAO tipoDAO = new TipoLlamadaDAO();
    EventoEnfocadoDAO eventoEnfocadoDAO = new EventoEnfocadoDAO();
    EventoProcesadoDAO eventoProcesadoDAO;
    String opcionBoton = "";
    Evento eventoSelecionado;
    EventoProcesado eventoSelecionadoProcesado;
    private List<TipoLlamada> objModulo2 = new ArrayList<TipoLlamada>();
    private List<TipoLlamada> listTipoLlamada = new ArrayList<TipoLlamada>();
    private SelectItem selectItemModulos[];
    private String etiqueta;
    private List<Object> objModulo;
    private List<Object> objModuloEvento;
    private SelectItem selectItemEventos[];
    private Object item;
    private Boolean switchEstados = false;
    private Integer stateFlag = null;
    private Boolean estadoFinal = null;
    private Boolean eventoNoGuardado = true;
    private String observacion = null;
    private String textFilter = "";
    private String currentItem = "";
    private String currentType = "";
    private String currentTypeEvent = "";
    private String forwardField = "";
    private String copyField = "";
    private String remitente = "";
    private Integer codigoIvr = 0;
    private Boolean controlEncolar = true;
    //*********************************************
    private Boolean controlEnvio = false;
    private String contenido = "";
    private String inboxContenido = "No existe nueva correspondencia";
    private String asunto = "";
    private String userSocial = "";
    private String passSocial = "";
    private String hostImap = "";
    private String usernameImap = "";
    private String passwordImap = "";
    private String providerSmtp = "";
    private String hostSmtp = "";
    private String usernameSmtp = "";
    private String passwordSmtp = "";
    private String portSmtp = "";
    private Boolean controlReenvio = false;
    private Integer idCorreo;
    private Integer contInLine = 0;
    private String para = "";
    //BandejaEntrada correo;
    Session sesionMail;
    Message message;
    EstadoLlamada estadoCorreo;
    //private List<String> listadoImagenes = new ArrayList();
    private List<String> listadoAdjuntos = new ArrayList();
    private List<String> listadoAdjuntosReenvio = new ArrayList();
    private List<String> listadoInLine;
    CorreoDAO objCorreo = new CorreoDAO();
//    private Set selectedEventoProcesadoList = new HashSet(0);
    // private List<EventoProcesado> eventoProcesadoList= new ArrayList();
    private List<SelectItem> tiposList;
    private Date horaInicio;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
    private Boolean viewControl = true;
    private Boolean selectedRadio;
    //  private Boolean stateSelected= false;
    LlamadaEntrante llamadaEntrante = new LlamadaEntrante();
    //private Integer responderReenviar = 0;
    private List<BodyPart> listBody = new ArrayList();
    private Store store;
    private Folder emailFolder;
    private String urlChat;
    private String mensajeAbandono = "";
    private Integer tamanioMax;
    private String nickUsuario = "";

    @PostConstruct
    public void enlazarDirecciones() {

        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        //CtrSession session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Runtime r = Runtime.getRuntime();
        //Consultas consulta = new Consultas();
        String codigoivr = request.getParameter("codigoivr");
        try {
            Integer.parseInt(codigoivr);
        } catch (NumberFormatException nfe) {
            codigoivr = "1";
        }
        session.setCodigoivr(codigoivr);
        //System.out.println("EE:" + codigoivr);
        setCodigoIvr(Integer.parseInt(codigoivr));
        if (session.getCodigoivr().startsWith("77")) {
            ParametrosDAO parametroDao = new ParametrosDAO();
//            hostImap = parametroDao.getParametroByAtributo("HOSTIMAP").getValor();
//            usernameImap = parametroDao.getParametroByAtributo("USERIMAP").getValor();
//            passwordImap = parametroDao.getParametroByAtributo("PASSIMAP").getValor();
//
//            providerSmtp = parametroDao.getParametroByAtributo("PROVIDERSMTP").getValor();
//            hostSmtp = parametroDao.getParametroByAtributo("HOSTSMTP").getValor();
//            usernameSmtp = parametroDao.getParametroByAtributo("USERSMTP").getValor();
//            portSmtp = parametroDao.getParametroByAtributo("PORTSMTP").getValor();
//            passwordSmtp = parametroDao.getParametroByAtributo("PASSSMTP").getValor();
            tamanioMax = Integer.parseInt(parametroDao.getParametroByAtributo("TAMANIOMAX").getValor());

            //estadoCorreo = new EstadoLlamada();
            //CorreoDAO correo = new CorreoDAO();
            estadoCorreo = objCorreo.getEstadoCorreo();
            estadoCorreo.setEstado(1);
            objCorreo.guardarEstadoCorreo(estadoCorreo, 0);
            try {
                responderMail(codigoivr);
                new Thread("CorreoRol") {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(5000);
                            estadoCorreo.setEstado(0);
                            objCorreo.guardarEstadoCorreo(estadoCorreo, 0);
                            System.out.println("Inside Thread");
                        } catch (InterruptedException ex) {
                            System.out.println(ex);
                            //Exceptions.printStackTrace(ex);
                        }
                    }
                }.start();
            } catch (Exception ex) {
                System.out.println(ex);
                //Exceptions.printStackTrace(ex);
            }
        }
        try {
            viewControl = tipoLlamadas != null && tipoLlamadas.size()>0;
            /*eventoNoGuardado = true;
            if (eventos!=null && eventos.size()==1) {
                eventoNoGuardado = false;
                session.setEventoSeleccionado(eventos.get(0));
            }*/
            //System.out.println(eventos.size());
            
            if (session.getCodigoivr().startsWith("77")) {
                etiqueta = "Mensaje de Correo";
                viewControl = false;
            }
        } catch (Exception e) {
            System.err.println("Error al asignar etiqueta");
        }
        /*if (codigoivr.compareTo("77") == 0) {
         controlInterfaz = true;
         } else {
         controlInterfaz = false;
         //controlInterfazC = false;
         }*/

        if (codigoivr.compareTo("78") == 0 || codigoivr.compareTo("79") == 0 || codigoivr.compareTo("80") == 0) {
            //controlInterfazC = true;
            ParametrosDAO parametroDao = new ParametrosDAO();
            userSocial = parametroDao.getParametroByAtributo("USERSOCIAL").getValor();
            passSocial = parametroDao.getParametroByAtributo("PASSSOCIAL").getValor();

            estadoCorreo = objCorreo.getEstadoCorreo();
            estadoCorreo.setEstado(1);
            objCorreo.guardarEstadoCorreo(estadoCorreo, 0);
            System.out.println("Audio Inicia");
            try {
//                if (codigoivr.compareTo("78") == 0) {
//                    recuperarTwitter();
//                } else if (codigoivr.compareTo("79") == 0) {
//                    recuperarFacebook();
//                } else 
                if (codigoivr.compareTo("80") == 0) {
                    recuperarChat("chat");
                }
                new Thread("Chat") {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(5000);
                            estadoCorreo.setEstado(0);
                            objCorreo.guardarEstadoCorreo(estadoCorreo, 0);
                            System.out.println("Audio Finaliza");
                        } catch (InterruptedException ex) {
                            System.out.println(ex);
                            //Exceptions.printStackTrace(ex);
                        }
                    }
                }.start();

                //estadoCorreo.setEstado(0);
                //objCorreo.guardarEstadoCorreo(estadoCorreo, 0);
            } catch (Exception ex) {
                System.out.println(ex);
                //Exceptions.printStackTrace(ex);
            }
        }
    }

    public CtrRegistroLlamada() {
        //System.out.println("CONSTUCTOOOOOOOR ELECTRICO");
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        horaInicio = new Date();
        tipoLlamadas = null;
        ///***************************************************************
        //CredencialesCorreo credenciales = objCorreo.getCredencialesByID(1);
        //host=  "smtp.gmail.com";
        ///******************************************************************

        if (session.getStateSelected()) {
            eventoSelecionadoProcesado = new EventoProcesado();
            eventoSelecionadoProcesado.setEstado(session.getEstadoFinal());
            eventoSelecionadoProcesado.setObservacion(session.getObservacion());
            eventoSelecionadoProcesado.setEvento(session.getEventoSeleccionado());
            eventoSelecionadoProcesado.setLlamadaEntrante(session.getLlamadaEntrante());
//                selectedEventoProcesadoList.add(eventoSelecionadoProcesado);
            session.getSelectedEventoProcesadoList().add(eventoSelecionadoProcesado);
            eventoNoGuardado = false;
            session.setObservacion(null);
            session.setEstadoFinal(null);
            session.setEventoSeleccionado(null);
            session.setLlamadaEntrante(null);
            session.setStateSelected(false);
        }
    }

    public void registrarLlamada() {
        if (session.getStateSelected()) {
            guardarEventoProcesado();
        }
        llamadaEntrante.setUsuario(session.getUsuario());
        llamadaEntrante.setFecha(new Date());
        llamadaEntrante.setHoraInicio(horaInicio);
        Date horaFin = new Date();
        llamadaEntrante.setHoraFin(horaFin);
        long milliseconds = horaFin.getTime() - horaInicio.getTime();
        int seconds = (int) (milliseconds / 1000.0);
        //    System.err.println("TIMEEEEE  "+ seconds);
        llamadaEntrante.setTiempoDuracion(seconds);
        llamadaEntrante.setEventoProcesados(session.getSelectedEventoProcesadoList());
        LlamadaEntranteDAO llE = new LlamadaEntranteDAO();
        llE.saveLlamadaEntranteProcesadas(llamadaEntrante, session.getSelectedEventoProcesadoList(), 1);
        switchEstados = false;

        eventoNoGuardado = true;
        estadoFinal = null;
        session.getSelectedEventoProcesadoList().clear();
    }

    public Rol getSelRol() {
        return selRol;
    }

    public void setSelRol(Rol selRol) {
        this.selRol = selRol;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String nombreRol;
    private String descripcion;
    private String estado;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Object> getObjModulo() {
        return objModulo;
    }

    public void setObjModulo(List<Object> objModulo) {
        this.objModulo = objModulo;
    }

    public SelectItem[] getSelectItemModulos() {
        return selectItemModulos;
    }

    public void setSelectItemModulos(SelectItem[] selectItemModulos) {
        this.selectItemModulos = selectItemModulos;
    }

    public List<TipoLlamada> getObjModulo2() {
        return objModulo2;
    }

    public void setObjModulo2(List<TipoLlamada> objModulo2) {
        this.objModulo2 = objModulo2;
    }

    public List<Object> getObjModuloEvento() {
        return objModuloEvento;
    }

    public void setObjModuloEvento(List<Object> objModuloEvento) {
        this.objModuloEvento = objModuloEvento;
    }

    public SelectItem[] getSelectItemEventos() {
        return selectItemEventos;
    }

    public Boolean getControlEncolar() {
        return controlEncolar;
    }

    public void setControlEncolar(Boolean controlEncolar) {
        this.controlEncolar = controlEncolar;
    }

    public void setSelectItemEventos(SelectItem[] selectItemEventos) {
        this.selectItemEventos = selectItemEventos;
    }

    public Integer getStateFlag() {
        return stateFlag;
    }

    public void setStateFlag(Integer stateFlag) {
        this.stateFlag = stateFlag;
    }

    /**
     * @return the item
     */
    public Object getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(SelectItem item) {
        this.item = item;
    }

    public String getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(String currentItem) {
        this.currentItem = currentItem;
    }

    public String getCurrentType() {
        return currentType;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        session.setObservacion(observacion);
        this.observacion = observacion;
    }

    public List<SelectItem> getTiposList() {
        return tiposList;
    }

    public void setTiposList(List<SelectItem> tiposList) {
        this.tiposList = tiposList;
    }

    public String getTextFilter() {
        return textFilter;
    }

    public void setTextFilter(String textFilter) {
        this.textFilter = textFilter;
    }

    public String getCurrentTypeEvent() {
        return currentTypeEvent;
    }

    public void setCurrentTypeEvent(String currentTypeEvent) {
        //  System.out.println("Current TypeeEventtt");        
        this.currentTypeEvent = currentTypeEvent;
    }

    public List<TipoLlamada> getListTipoLlamada() {
        return listTipoLlamada;
    }

    public void setListTipoLlamada(List<TipoLlamada> listTipoLlamada) {
        this.listTipoLlamada = listTipoLlamada;
    }

    public Evento getSelectedEvent() {
        return selectedEvent;
    }

    public void setSelectedEvent(Evento selectedEvent) {
        this.selectedEvent = selectedEvent;
    }

    public Boolean getEventoNoGuardado() {
        return eventoNoGuardado;
    }

    public void setEventoNoGuardado(Boolean eventoGuardado) {
        this.eventoNoGuardado = eventoGuardado;
    }

    public Integer getIdCorreo() {
        return idCorreo;
    }

    public void setIdCorreo(Integer idCorreo) {
        this.idCorreo = idCorreo;
    }

    public List<Evento> getEventos() {
        return eventos;
    }

    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }

    public List<TipoLlamada> getTipoLlamadas() {
        return tipoLlamadas;
    }

    public void setTipoLlamadas(List<TipoLlamada> tipoLlamadas) {
        this.tipoLlamadas = tipoLlamadas;
    }

    public TipoLlamada getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(TipoLlamada selectedType) {
        this.selectedType = selectedType;
    }

    public void guardarEventoProcesado() {

        eventoSelecionadoProcesado = new EventoProcesado();
        eventoSelecionadoProcesado.setEstado(estadoFinal);
        eventoSelecionadoProcesado.setObservacion(observacion);
        eventoSelecionadoProcesado.setEvento(eventoSelecionado);
        eventoSelecionadoProcesado.setLlamadaEntrante(llamadaEntrante);
//                selectedEventoProcesadoList.add(eventoSelecionadoProcesado);
        session.getSelectedEventoProcesadoList().add(eventoSelecionadoProcesado);
        eventoNoGuardado = false;
        observacion = null;
        session.setStateSelected(false);
    }

    public void onSelectEventItem(ValueChangeEvent event) {
        //System.out.println(event.getNewValue());
        if (session.getStateSelected()) {
            guardarEventoProcesado();
        }
        if (event.getNewValue() != null) {
            eventoSelecionado = (Evento) event.getNewValue();
            session.setEventoSeleccionado(eventoSelecionado);
            session.setLlamadaEntrante(llamadaEntrante);

            if (eventoEnfocadoDAO.getEventoEnfocadoByID(eventoSelecionado.getId())) {
                switchEstados = true;
                selectedRadio = true;
            } else {
                switchEstados = false;
            }
            if (switchEstados) {
                eventoNoGuardado = true;
            } else {
                guardarEventoProcesado();
            }
            setStateFlag(null);
            estadoFinal = null;
        }
    }

    public void onCheckReenvioLlamada() {
        if (controlReenvio) {
            controlReenvio = false;
        } else {
            controlReenvio = true;
        }
    }

    public void onSelectTipoLlamadaItem(ValueChangeEvent event) {
        switchEstados = false;
        eventos.clear();
        if (session.getStateSelected()) {
            guardarEventoProcesado();
        }

        if (null != event.getNewValue()) {
            TipoLlamada tipoSelecionado = (TipoLlamada) event.getNewValue();
            objModuloEvento = consulta.getListHql(null, "Evento", "idfkrazon = " + tipoSelecionado.getId() + " and estado!=0", null);
            if (objModuloEvento != null && objModuloEvento.size() > 0) {
                for (Object obj : objModuloEvento) {
                    eventos.add((Evento) obj);
                }
            }
        }
        /*eventoNoGuardado = true;
        if (eventos != null && eventos.size()==1) {
            eventoNoGuardado = false;
            session.setEventoSeleccionado(eventos.get(0));
        }*/
    }

    public void onSelectEstadoLlamadaOption(ValueChangeEvent event) {
        Integer opcion = (Integer) event.getNewValue();
//        eventoSelecionadoProcesado = new  EventoProcesado();

        if (opcion == 1) {
            estadoFinal = true;
        } else {
            estadoFinal = false;
        }
        session.setStateSelected(true);
        session.setEstadoFinal(estadoFinal);
        eventoNoGuardado = false;


    }
///***************************************************************************************

    public void encolarMensaje() {
        try {
            message.setFlag(Flags.Flag.ANSWERED, true);
            message.setFlag(Flags.Flag.DELETED, true);
            inboxContenido = "";
            asunto = "";
            remitente = "";
            controlEnvio = true;
            contenido = "";
            copyField = "";
            forwardField = "";
            controlEncolar = true;
            /*correo.setEstado(true);
             correo.setUsuario(session.getUsuario());
             objCorreo.guardarCorreoDescargado(correo, 0);*/
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }

    }

    public void responderMail(String codigoIvr) {
        for (int ii = 0; ii < session.getRolUsuario().size(); ii++) {
            List<DcorreoRol> listTemp = datosCorreoDAO.getDcorreoRoles(String.valueOf(session.getRolUsuario().get(ii).getId().getIdRol()));
            for (int jj = 0; jj < listTemp.size(); jj++) {
                DatosCorreo correoTemp = datosCorreoDAO.getDatosCorreoByID(listTemp.get(jj).getId().getIdDcorreo());
                if (correoTemp != null) {
                    if (correoTemp.getCodigoivr().equals(codigoIvr)) {
                        hostImap = correoTemp.getHostmail();
                        usernameImap = correoTemp.getUsermail();
                        passwordImap = correoTemp.getPassmail();

                        providerSmtp = correoTemp.getProveedor();
                        hostSmtp = correoTemp.getHostmail();
                        usernameSmtp = correoTemp.getUsermail();
                        portSmtp = correoTemp.getPortrmail();
                        passwordSmtp = correoTemp.getPassmail();
                        ///////////////////////////////
                        autenticarEntidad(providerSmtp);
                        try {
                            listadoInLine = new ArrayList();
                            contInLine = 0;
                            store = sesionMail.getStore(providerSmtp);
                            store.connect(hostImap, usernameImap, passwordImap);
                            emailFolder = store.getFolder("INBOX");
                            emailFolder.open(Folder.READ_WRITE);
                            //emailFolder.open(Folder.READ_WRITE);

                            Flags seen = new Flags(Flags.Flag.SEEN);
                            FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
                            Message messages[] = emailFolder.search(unseenFlagTerm);

                            if (messages.length == 0) {
                                System.out.println("No messages found.");

                            } else {
                                /*correo = objCorreo.getCorreoByEstado();
                                 System.err.println("NUmero mail: " + correo.getNumeroMail());
                                 message = emailFolder.getMessage(correo.getNumeroMail());*/
                                message = messages[messages.length - 1];
                                message.setFlag(Flags.Flag.SEEN, true);
                                System.out.println("Flagged: " + message.getFlags().contains(Flags.Flag.FLAGGED));
                                System.err.println("Seen: " + message.getFlags().contains(Flags.Flag.SEEN));
                                //System.out.println("anwered: " + message.getFlags().contains(Flags.Flag.ANSWERED));

                                //inboxContenido = getTextFromMessage(message);
                                asunto = message.getSubject();
                                remitente = "";
                                para = "";
                                Address[] in = message.getFrom();
                                for (Address address : in) {
                                    if (!remitente.equals("")) {
                                        remitente = remitente + ";";
                                    }
                                    remitente = remitente + MimeUtility.decodeText(address.toString());
                                }
                                Address[] out = message.getAllRecipients();
                                for (Address address : out) {
                                    if (!para.equals("")) {
                                        para = para + ";";
                                    }
                                    para = para + MimeUtility.decodeText(address.toString());
                                }

                                if (message.isMimeType("multipart/MIXED")) {
                                    Multipart multipart = (Multipart) message.getContent();
                                    System.out.println("Count:" + multipart.getCount());
                                    for (int j = 0; j < multipart.getCount(); j++) {
                                        BodyPart bodyPart = multipart.getBodyPart(j);
                                        if (j == 0) {
                                            writePart(bodyPart);
                                        } else {
                                            //if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()) && !StringUtils.isNotBlank(bodyPart.getFileName())) {
                                            //continue; // dealing with attachments only
                                            //} 
                                            listBody.add(bodyPart);
                                            /*InputStream is = bodyPart.getInputStream();
                                             File f = new File("C:/src/down/" + bodyPart.getFileName());
                                             FileOutputStream fos = new FileOutputStream(f);
                                             byte[] buf = new byte[4096];
                                             int bytesRead;
                                             while ((bytesRead = is.read(buf)) != -1) {
                                             fos.write(buf, 0, bytesRead);
                                             }
                                             fos.close();*/
                                            listadoAdjuntos.add(bodyPart.getFileName());
                                            //attachments.add(f);
                                        }
                                    }
                                } else {
                                    writePart(message);
                                }
                                //message.setFlag(Flags.Flag.FLAGGED, true);                
                            }

                            //emailFolder.close(true);
                            //store.close();

                        } catch (NoSuchProviderException ex) {
                            System.err.println("Proveedor incorrecto");
                        } catch (MessagingException ex) {
                            System.err.println("Error al descargar los mensajes");
                        } catch (Exception ex) {
                            System.err.println("Error al obtener el contenido");
                            ex.printStackTrace();
                        }
                        break;
                    }
                }
            }
        }
    }

    public FileDataSource getDataSource(String path) {
        FileDataSource fd = new FileDataSource(path);
        return fd;
    }

    public void autenticarEntidad(String proveedor) {
        sesionMail = Session.getInstance(getReadProperties(proveedor), new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usernameImap, passwordImap);
            }
        });
    }

    public void autenticarEntidadEnvio() {
        sesionMail = Session.getInstance(getSendProperties(), new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usernameSmtp, passwordSmtp);
            }
        });
    }

    public Properties getReadProperties(String proveedor) {
        Properties properties = new Properties();
        properties.setProperty("mail.imap.starttls.enable", "true");
        properties.setProperty("ssl.SocketFactory.provider", "ec.com.centrosurcall.utils.ZimbraSSLSocketFactory");
        properties.setProperty("mail.imap.socketFactory.class", "ec.com.centrosurcall.utils.ZimbraSSLSocketFactory");
        if (proveedor.equals("imaps")) {
            properties.setProperty("mail.imaps.ssl.trust", "*");
        }
        return properties;
    }

    public Properties getSendProperties() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", hostSmtp);
        properties.put("mail.smtp.port", portSmtp);
        properties.setProperty("ssl.SocketFactory.provider", "ec.com.centrosurcall.utils.ZimbraSSLSocketFactory");
        properties.setProperty("mail.smtp.socketFactory.class", "ec.com.centrosurcall.utils.ZimbraSSLSocketFactory");
        return properties;
    }

    /*public Properties getReadProperties() {
     Properties properties = new Properties();
     properties.put("mail.store.protocol", "imaps");
     properties.put("mail.imap.host", hostImap);
     properties.put("mail.imap.port", portImap);
     properties.put("mail.imap.starttls.enable", "true");
     return properties;
     }
     public Properties getSendProperties() {
     Properties properties = System.getProperties();
     properties.put("mail.smtp.starttls.enable", true); // added this line
     properties.put("mail.smtp.host", hostSmtp);
     //properties.put("mail.smtp.user", "fabian.lema@taurustech.ec");
     //properties.put("mail.smtp.password", "fabgalpre87");
     properties.put("mail.smtp.port", portSmtp);
     properties.put("mail.smtp.auth", true);
     return properties;
     }*/
    public Integer getCodigoIvr() {
        //System.out.println("DDD:" + codigoIvr);
        return codigoIvr;
    }

    public void setCodigoIvr(Integer codigoIvr) {
        this.codigoIvr = codigoIvr;
    }

    public String getContenido() {
        return contenido;
    }

    public String getForwardField() {
        return forwardField;
    }

    public void setForwardField(String forwardField) {
        this.forwardField = forwardField;
    }

    public String getCopyField() {
        return copyField;
    }

    public void setCopyField(String copyField) {
        this.copyField = copyField;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getInboxContenido() {
        return inboxContenido;
    }

    public void setInboxContenido(String inboxContenido) {
        this.inboxContenido = inboxContenido;
    }

    public String getAsunto() {
        return asunto;
    }

    public String getRemitente() {
        return remitente;
    }

    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

///****************************************************************************************
    public void onSelectCancelarButton() {
        switchEstados = false;
        eventoNoGuardado = false;
    }

    public Boolean getEstadoFinal() {
        return estadoFinal;
    }

    public void setEstadoFinal(Boolean estadoFinal) {
        this.estadoFinal = estadoFinal;
    }

    public List<Boolean> getEstados() {
        return estados;
    }

    public Boolean getControlEnvio() {
        return controlEnvio;
    }

    public void setControlEnvio(Boolean controlEnvio) {
        this.controlEnvio = controlEnvio;
    }

    public void setEstados(List<Boolean> estados) {
        this.estados = estados;
    }

    public Boolean getSwitchEstados() {
        return switchEstados;
    }

    public Boolean getControlReenvio() {
        return controlReenvio;
    }

    public void setControlReenvio(Boolean controlReenvio) {
        this.controlReenvio = controlReenvio;
    }

    public void setSwitchEstados(Boolean switchEstados) {
        this.switchEstados = switchEstados;
    }

    public Boolean getSelectedRadio() {
        return selectedRadio;
    }

    public void setSelectedRadio(Boolean selectedRadio) {
        this.selectedRadio = selectedRadio;
    }

    public List<String> getListadoAdjuntos() {
        return listadoAdjuntos;
    }

    public void setListadoAdjuntos(List<String> listadoAdjuntos) {
        this.listadoAdjuntos = listadoAdjuntos;
    }

    public String getUrlChat() {
        return urlChat;
    }

    public void setUrlChat(String urlChat) {
        this.urlChat = urlChat;
    }

    public String getMensajeAbandono() {
        return mensajeAbandono;
    }

    public void setMensajeAbandono(String mensajeAbandono) {
        this.mensajeAbandono = mensajeAbandono;
    }

    @Override
    public String nuevo() {
        return null;
        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String editar() {
        return null;
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimir() {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String refrescar() {
        return null;
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void auditoria() {
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String regresar() {
        return null;
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cargarColumnasGrupo() {
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void buscarWsql() {
        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public void dynamicRender(AjaxBehaviorEvent event) {
        setStateFlag(null);
        UIComponent target = event.getComponent().findComponent("j_idt47:estado");
        // FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("foo:bar");
        if (selectedRadio) {
            target.clearInitialState();
            target.processUpdates(FacesContext.getCurrentInstance());
            //     target.get
            selectedRadio = false;
        }

        FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add(target.getClientId());

    }

    public Boolean getViewControl() {
        return viewControl;
    }

    public void setViewControl(Boolean viewControl) {
        this.viewControl = viewControl;
    }

    public void writePart(Part p) throws Exception {
        if (p instanceof Message) //Call methos writeEnvelope
        {
            writeEnvelope((Message) p);
        }

        //System.out.println("----------------------------");
        //System.out.println("CONTENT-TYPE: " + p.getContentType());

        //check if the content is plain text
        if (p.isMimeType("text/plain")) {
            System.out.println("This is plain text");
            //System.out.println("---------------------------");
            //System.out.println((String) p.getContent());
            inboxContenido = (String) p.getContent();
            /*String cadenaTemp = inboxContenido;
             System.out.println("22222222222222222");
             int t = 0;
             String[] as = cadenaTemp.split("cid:");
             //d = d.substring(t);
             for (int i = 0; i < as.length - 1; i++) {
             t = cadenaTemp.indexOf("cid:", t + 1);
             cadenaTemp = cadenaTemp.substring(t);
             Pattern pattern = Pattern.compile("src=\".*?\\.(?:png|jpg|gif)\"|src=\"cid:.*?\"");
             Matcher matcher = pattern.matcher(cadenaTemp);
             if (matcher.find()) {
             listadoInLine.add(cadenaTemp.substring(matcher.start(), matcher.end()));
             }
             System.out.println("222:" + t);
             System.out.println("111:" + cadenaTemp);
             }*/
        } //check if the content has attachment
        else if (p.isMimeType("multipart/*")) {
            System.out.println("This is a Multipart");
            //System.out.println("---------------------------");
            Multipart mp = (Multipart) p.getContent();
            int count = mp.getCount();
            for (int i = 0; i < count; i++) {
                writePart(mp.getBodyPart(i));
            }
        } //check if the content is a nested message
        else if (p.isMimeType("message/rfc822")) {
            System.out.println("This is a Nested Message");
            //System.out.println("---------------------------");
            writePart((Part) p.getContent());
        } //check if the content is an inline image
        else if (p.isMimeType("image/jpeg") || p.isMimeType("image/png")) {
            String path = new File("").getAbsolutePath();
            System.out.println("--------> image/jpeg");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.out.println("TAMAÑO:" + p.getSize() + "--MAX:" + tamanioMax);
            if (p.getSize() < tamanioMax) {
                byte[] bArray = toBytes(p.getDataHandler());
                //rutaImagen= path +"/resources/"+ "prueba1";
                //listadoImagenes.remove(0);
                //p.getContentType();
                //FileOutputStream f2 = new FileOutputStream("C:/src/down/" + p.getFileName());

                //f2.write(bArray);
                //f2.close();

                OutputStream base64OutputStream = MimeUtility.encode(baos, "base64");
                base64OutputStream.write(bArray);
                base64OutputStream.close();
            } else {
                listBody.add((BodyPart) p);
                listadoAdjuntos.add(p.getFileName());
                baos = null;
            }
            //System.out.println("encoded String " + baos.toString());

            //String path = "/CentroSurCM/imagenes";

            //contenido = content;
            //while (matcher.find()) {
            //if (matcher.find()) {
            //String nombre = inboxContenido.substring(matcher.start(), matcher.end());
            //System.err.println("Nombre archivo " + nombre + "###############");
            //if (nombre.contains("cid:")) {
            if (baos != null) {
                System.out.println("RR:" + listadoInLine.get(contInLine) + "--" + contInLine);
                inboxContenido = inboxContenido.replace(listadoInLine.get(contInLine), "src=\"data:image/png;base64," + baos.toString() + "\"");
            }
            //} else {
            //String nombreAsignado = nombre.substring(nombre.indexOf("\"") + 1, nombre.lastIndexOf("\""));
            //listadoImagenes.add(nombreAsignado);
            //inboxContenido = inboxContenido.replaceFirst(nombre, "src=\"" + path + "/" + nombreAsignado + "\"");
            //}
            //}
            //inboxContenido = contenido;
            //System.out.println("COOOOOOOOOOO:" + contenido);
            //System.out.println("--------> Fin image/jpeg");
            contInLine++;
        } else if (p.getContentType().contains("image/")) {
            System.out.println("content type" + p.getContentType());
            File f = new File("image" + new Date().getTime() + ".jpg");
            DataOutputStream output = new DataOutputStream(
                    new BufferedOutputStream(new FileOutputStream(f)));
            com.sun.mail.util.BASE64DecoderStream test =
                    (com.sun.mail.util.BASE64DecoderStream) p
                    .getContent();
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = test.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
        } else {
            Object o = p.getContent();
            if (o instanceof String) {
                System.out.println("This is a string");
                //System.out.println("---------------------------");
                //System.out.println((String) o);
                inboxContenido = (String) o;
                String cadenaTemp = inboxContenido;
                System.out.println("11111111111111111111111111111");
                int t = 0;
                String[] as = cadenaTemp.split("cid:");
                //d = d.substring(t);
                for (int i = 0; i < as.length - 1; i++) {
                    Pattern pattern = Pattern.compile("src=\".*?\\.(?:png|jpg|gif)\"|src=\"cid:.*?\"");
                    Matcher matcher = pattern.matcher(cadenaTemp);
                    if (matcher.find()) {
                        listadoInLine.add(cadenaTemp.substring(matcher.start(), matcher.end()));
                    }
                    //System.out.println("222:" + t);
                    //System.out.println("111:" + cadenaTemp);
                    t = cadenaTemp.indexOf("cid:", 1);
                    cadenaTemp = cadenaTemp.substring(t);
                }
            } else if (o instanceof InputStream) {
                System.out.println("This is just an input stream");
                //System.out.println("---------------------------");
                InputStream is = (InputStream) o;
                is = (InputStream) o;
                int c;
                while ((c = is.read()) != -1) {
                    System.out.write(c);
                }
            } else {
                System.out.println("This is an unknown type");
                //System.out.println("---------------------------");
                //System.out.println(o.toString());
            }
        }
    }

    public void writeEnvelope(Message m) throws Exception {
        System.out.println("This is the message envelope");
        System.out.println("---------------------------");
        Address[] a;

        // FROM
        if ((a = m.getFrom()) != null) {
            for (int j = 0; j < a.length; j++) {
                System.out.println("FROM: " + a[j].toString());
            }
        }

        // TO
        if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
            for (int j = 0; j < a.length; j++) {
                System.out.println("TO: " + a[j].toString());
            }
        }

        // SUBJECT
        if (m.getSubject() != null) {
            System.out.println("SUBJECT: " + m.getSubject());
        }

    }

    public byte[] toBytes(DataHandler handler) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        handler.writeTo(output);
        return output.toByteArray();
    }

    public String downloadFile(String nameFile) {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + nameFile + "\"");

            for (int j = 0; j < listBody.size(); j++) {
                if (listBody.get(j).getFileName().equals(nameFile)) {
                    try (BufferedInputStream bIn = new BufferedInputStream(listBody.get(j).getInputStream())) {
                        int rLength = -1;
                        byte[] buffer = new byte[1000];
                        while ((rLength = bIn.read(buffer, 0, 100)) != -1) {
                            response.getOutputStream().write(buffer, 0, rLength);
                        }
                    }
                    break;
                }
            }

            response.getOutputStream().flush();
            response.getOutputStream().close();
            response.flushBuffer();
            context.responseComplete();
            //FacesContext.getCurrentInstance().responseComplete();

        } catch (Exception e) {
            // Log the error
            e.printStackTrace();
        }
        return null;
    }

    /*public void save() {
     String nombreArch;
     try {
     System.out.println("Engresooo");
     InputStream input = file.getInputStream();
     String[] name = file.getFileName().split("\\\\");
     if (name.length>1){
     nombreArch = name[name.length-1];
     }else nombreArch = name[0];
     listadoAdjuntosReenvio.add(nombreArch);
     Files.copy(input, new File("C:/src/tmp/" + nombreArch).toPath());
     }
     catch (Exception e) {
     System.out.println("Error processFileUpload: " + e);
     e.printStackTrace();
     }
     }*/
    public void processFileUpload(FileUploadEvent event) throws Exception {
        UploadedFile item = event.getUploadedFile();
        String nombreArch;
        System.out.println("Entrooo");
        try {
            String[] name = item.getName().split("\\\\");
            if (name.length > 1) {
                nombreArch = name[name.length - 1];
            } else {
                nombreArch = name[0];
            }
            listadoAdjuntosReenvio.add(nombreArch);
            FileUtils.writeByteArrayToFile(new File("C:/src/tmp/" + nombreArch), item.getData());
        } catch (Exception e) {
            System.out.println("Error processFileUpload: " + e);
            e.printStackTrace();
        }
        //printToConsole(cellDataList);
    }

    public String enviarMails() {
        try {
            if (message != null) {
                new Thread("Mail") {
                    @Override
                    public void run() {
                        try {
                            reenviarMails();
                            BandejaEntrada be = new BandejaEntrada();
                            be.setRemitente(remitente);
                            be.setListaDistribucion(para);
                            be.setEstado(true);
                            be.setLeido(true);
                            be.setRespondido(true);
                            be.setHora(new Date());
                            be.setAsunto(asunto);
                            be.setUsuario(session.getUsuario());
                            be.setContenido("");
                            (new BandejaEntradaDAO()).saveBandejaEntrada(be, 1);
                            message.setFlag(Flags.Flag.ANSWERED, true);
                            eliminarArchivos();
                        } catch (Exception ex) {
                            System.out.println(ex);
                            //Exceptions.printStackTrace(ex);
                        }
                    }
                }.start();
            }
            return "frmRegistroLlamadas";
        } catch (Exception ex) {
            System.out.println(ex);
            //Exceptions.printStackTrace(ex);
            return "frmRegistroLlamadas";
        }
    }

    /*public void responderMails() {
     try {
     autenticarEntidadEnvio();
     Message forward = new MimeMessage(sesionMail);
     forward.setRecipients(Message.RecipientType.TO,
     InternetAddress.parse(message.getFrom()[0].toString()));
     forward.setSubject("Fwd: " + message.getSubject());
     forward.setFrom(new InternetAddress(usernameSmtp));

     Multipart multipart = new MimeMultipart();
     BodyPart stringPart = new MimeBodyPart();
     stringPart.setContent(contenido, "text/html; charset=utf-8");// .setText(contenido);
     multipart.addBodyPart(stringPart);
     for (int i = 0; i < listadoAdjuntosReenvio.size(); i++) {
     MimeBodyPart attachPart = new MimeBodyPart();
     attachPart.attachFile(new File("C:/src/tmp/" + listadoAdjuntosReenvio.get(i)));
     multipart.addBodyPart(attachPart);
     }

     forward.setContent(multipart);
     forward.saveChanges();
     Transport.send(forward);

     System.out.println("Mensaje Enviado..");
     } catch (Exception mex) {
     mex.printStackTrace();
     }
     }*/
    public void reenviarMails() {
        InternetAddress[] addresses;
        int conta = 0;
        try {
            autenticarEntidadEnvio();

            Message forward = new MimeMessage(sesionMail);
            String[] forw = forwardField.split(";");
            String[] copy = copyField.split(";");

            //InternetAddress[] addresses = new InternetAddress[forw.length];
            if (forwardField.trim().equals("")) {
                System.out.println("11");
                if (!copyField.trim().equals("")) {
                    System.out.println("22");
                    addresses = new InternetAddress[copy.length + 1];
                    addresses[0] = (InternetAddress) message.getFrom()[0];
                    for (int j = 0; j < copy.length; j++) {
                        addresses[j + 1] = new InternetAddress(copy[j]);
                    }
                } else {
                    System.out.println("33");
                    addresses = new InternetAddress[1];
                    addresses[0] = (InternetAddress) message.getFrom()[0];
                }
            } else {
                System.out.println("44");
                if (!copyField.trim().equals("")) {
                    System.out.println("55");
                    addresses = new InternetAddress[forw.length + copy.length];
                    conta = 0;
                    for (int j = 0; j < copy.length; j++) {
                        addresses[conta] = new InternetAddress(copy[j]);
                        conta++;
                    }
                    for (int j = 0; j < forw.length; j++) {
                        addresses[conta] = new InternetAddress(forw[j]);
                        conta++;
                    }
                } else {
                    System.out.println("66");
                    addresses = new InternetAddress[forw.length];
                    for (int j = 0; j < forw.length; j++) {
                        addresses[j] = new InternetAddress(forw[j]);
                    }
                }
            }
            //addresses = new InternetAddress[forw.length + copy.length];
            forward.setRecipients(Message.RecipientType.TO, addresses);
            forward.setSubject("Fwd: " + message.getSubject());
            forward.setFrom(new InternetAddress(usernameSmtp));
            Multipart multipart = new MimeMultipart();
            if (message.isMimeType("multipart/MIXED")) {
                System.out.println("MIXED");
                Multipart multiparte = (Multipart) message.getContent();
                for (int i = 0; i < multiparte.getCount(); i++) {
                    BodyPart bodyPart = multiparte.getBodyPart(i);
                    if (i == 0) {
                        if (bodyPart.isMimeType("text/plain")) {
                            BodyPart stringPart = new MimeBodyPart();
                            stringPart.setText(contenido + armarHeader() + bodyPart.getContent().toString());
                            multipart.addBodyPart(stringPart);
                        } else {
                            Multipart multipartBody = new MimeMultipart();
                            Multipart mp = (Multipart) bodyPart.getContent();
                            for (int j = 0; j < mp.getCount(); j++) {
                                if (j == 0) {
                                    BodyPart stringPart1 = new MimeBodyPart();
                                    stringPart1.setContent(contenido + armarHeader(), "text/html; charset=utf-8");
                                    multipartBody.addBodyPart(stringPart1);
                                }
                                BodyPart multiPartes = mp.getBodyPart(j);
                                if (!multiPartes.isMimeType("text/plain")) {
                                    multipartBody.addBodyPart(multiPartes);
                                }
                                //multipartBody.addBodyPart(multiPartes);
                            }
                            BodyPart stringPartrr = new MimeBodyPart();
                            stringPartrr.setContent(multipartBody);
                            multipart.addBodyPart(stringPartrr);
                        }

                    } else {
                        multipart.addBodyPart(bodyPart);
                    }
                }
            } else {
                if (message.isMimeType("text/plain")) {
                    BodyPart stringPart = new MimeBodyPart();
                    stringPart.setText(contenido + armarHeader() + message.getContent().toString());
                    multipart.addBodyPart(stringPart);
                } else {
                    Multipart multipartBody = new MimeMultipart();
                    Multipart mp = (Multipart) message.getContent();
                    for (int j = 0; j < mp.getCount(); j++) {
                        if (j == 0) {
                            BodyPart stringPart1 = new MimeBodyPart();
                            stringPart1.setContent(contenido + armarHeader(), "text/html; charset=utf-8");
                            multipartBody.addBodyPart(stringPart1);
                        }
                        BodyPart multiPartes = mp.getBodyPart(j);
                        if (!multiPartes.isMimeType("text/plain")) {
                            multipartBody.addBodyPart(multiPartes);
                        }
                    }
                    BodyPart stringPartrr = new MimeBodyPart();
                    stringPartrr.setContent(multipartBody);
                    multipart.addBodyPart(stringPartrr);
                }
            }

            for (int i = 0; i < listadoAdjuntosReenvio.size(); i++) {
                MimeBodyPart attachPart = new MimeBodyPart();
                attachPart.attachFile(new File("C:/src/tmp/" + listadoAdjuntosReenvio.get(i)));
                multipart.addBodyPart(attachPart);
            }

            forward.setContent(multipart);
            forward.saveChanges();

            Transport.send(forward);
            System.out.println("Mensaje Enviado..");


        } catch (Exception mex) {
            mex.printStackTrace();
        }
    }

    public String armarHeader() {
        try {
            SimpleDateFormat formato =
                    new SimpleDateFormat("EEEE d MMMM yyyy HH:mm:ss", new Locale("es", "ES"));
            return "<br/> <br/>"
                    + "__________________________________________________" + "<br/> <br/>"
                    + (message.getReceivedDate() != null ? "Fecha Recepción: " + formato.format(message.getReceivedDate()) + "<br/> " : "")
                    + "Remite: " + remitente + "<br/> "
                    + "Para: " + para + "<br/> "
                    + "Asunto: FWD: " + asunto + "<br/> <br/> ";
        } catch (Exception ex) {
            System.out.println(ex);
            //Exceptions.printStackTrace(ex);
            return "";
        }
    }

    public String eliminarMails() {
        try {
            if (message != null) {
                BandejaEntrada be = new BandejaEntrada();
                be.setRemitente(remitente);
                be.setListaDistribucion(para);
                be.setEstado(false);
                be.setLeido(true);
                be.setRespondido(false);
                be.setHora(new Date());
                be.setAsunto(asunto);
                be.setUsuario(session.getUsuario());
                be.setContenido("");
                (new BandejaEntradaDAO()).saveBandejaEntrada(be, 1);
                message.setFlag(Flags.Flag.DELETED, true);
                eliminarArchivos();
            }
            return "frmRegistroLlamadas";
        } catch (Exception ex) {
            System.out.println(ex);
            //Exceptions.printStackTrace(ex);
            return "frmRegistroLlamadas";
        }
    }

    public void eliminarArchivos() {
        try {
            for (int i = 0; i < listadoAdjuntosReenvio.size(); i++) {
                File f = new File("C:/src/tmp/" + listadoAdjuntosReenvio.get(i));
                f.delete();
            }
            listadoAdjuntosReenvio.clear();
            listadoAdjuntos.clear();
            emailFolder.close(true);
            store.close();
        } catch (MessagingException ex) {
            System.out.println(ex);
            //Exceptions.printStackTrace(ex);
        }
    }

    public void recuperarChat(String chatDB) {
        ColaChatDAO colaChatDao = new ColaChatDAO();
        ColaChat redSocial = colaChatDao.getFirstColaTwitter(chatDB);
        ParametrosDAO parametroDao = new ParametrosDAO();
        String servidor = parametroDao.getParametroByAtributo("HOSTSOCIAL").getValor();
        if (redSocial != null) {
            redSocial.setEstado(true);
            redSocial.setAtendido(true);
            redSocial.setIdUsuario(session.getUsuario());
            colaChatDao.saveColaChat(redSocial, 0);
            nickUsuario = session.getUsuario().getNick();
            urlChat = "https://" + servidor + "/reply-template-container.jsp?templateUrl=http%3A//" + servidor + "/templates/reply/cisco_agent_chat.jsp&scRefUrl=http%3A//" + servidor + "/ccp-webapp/ccp/socialcontact/" + redSocial.getIdSocialContact() + "&country=EC&lang=es&CCPAlias=" + session.getUsuario().getNick() + "&CCPToken=" + redSocial.getTitulo();
            //urlChat = "https://"+servidor+"/reply-template-container.jsp?templateUrl=http%3A//"+servidor+"/templates/reply/cisco_agent_chat.jsp&scRefUrl=http%3A//"+servidor+"/ccp-webapp/ccp/socialcontact/"+redSocial.getIdSocialContact()+"&CCPToken="+redSocial.getTitulo();

            System.out.println("AAA:" + urlChat);
        }
    }

    public void recuperarChat() {
        ColaChatDAO colaChatDao = new ColaChatDAO();
        ColaChat colaChat = colaChatDao.getFirstColaChat();
        String url;
        if (colaChat != null) {
            url = colaChat.getUrl();
            //cadena = "https://csmpre10.centrosur.gob.ec/reply-template-container.jsp?templateUrl=http://CSMPRE10.centrosur.gob.ec/templates/reply/cisco_agent_chat.jsp&scRefUrl=" + url + "&CCPAlias=" + session.getUsuario().getNick().toLowerCase() + "&origin=https://ccxpre10.centrosur.local";
            //setUrlChat(cadena);
            if (!verifyUrl(colaChat.getUrlContacto(), "chat")) {
                setUrlChat("");
                colaChat.setAtendido(false);
                setMensajeAbandono("El Cliente abandonó el Chat");
            } else {
                setUrlChat(url);
                colaChat.setAtendido(true);
            }
            colaChat.setEstado(true);
            colaChat.setIdUsuario(session.getUsuario());
            colaChatDao.saveColaChat(colaChat, 0);
        } else {
            setMensajeAbandono("El Cliente abandonó el Chat");
            setUrlChat("");
        }
    }

    public void recuperarTwitter() {
        ColaChatDAO colaChatDao = new ColaChatDAO();
        ColaChat colaChat = colaChatDao.getFirstColaTwitter("twitter");
        String url;
        if (colaChat != null) {
            url = colaChat.getUrl();
            //cadena = "https://csmpre10.centrosur.gob.ec/reply-template-container.jsp?templateUrl=http://CSMPRE10.centrosur.gob.ec/templates/reply/cisco_agent_chat.jsp&scRefUrl=" + url + "&CCPAlias=" + session.getUsuario().getNick().toLowerCase() + "&origin=https://ccxpre10.centrosur.local";
            //setUrlChat(cadena);
            if (!verifyUrl(colaChat.getUrlContacto(), "twitter_account")) {
                setUrlChat("");
                colaChat.setAtendido(false);
                setMensajeAbandono("El tweet fué respondido");
            } else {
                setUrlChat(url);
                colaChat.setAtendido(true);
            }
            colaChat.setEstado(true);
            colaChat.setIdUsuario(session.getUsuario());
            colaChatDao.saveColaChat(colaChat, 0);
        } else {
            setMensajeAbandono("El tweet fué respondido");
            setUrlChat("");
        }
    }

    public void recuperarFacebook() {
        ColaChatDAO colaChatDao = new ColaChatDAO();
        ColaChat colaChat = colaChatDao.getFirstColaFacebook();
        String url;
        if (colaChat != null) {
            url = colaChat.getUrl();
            //cadena = "https://csmpre10.centrosur.gob.ec/reply-template-container.jsp?templateUrl=http://CSMPRE10.centrosur.gob.ec/templates/reply/cisco_agent_chat.jsp&scRefUrl=" + url + "&CCPAlias=" + session.getUsuario().getNick().toLowerCase() + "&origin=https://ccxpre10.centrosur.local";
            //setUrlChat(cadena);
            if (!verifyUrl(colaChat.getUrlContacto(), "facebook")) {
                setUrlChat("");
                colaChat.setAtendido(false);
                setMensajeAbandono("La notificación fué respondida");
            } else {
                setUrlChat(url);
                colaChat.setAtendido(true);
            }
            colaChat.setEstado(true);
            colaChat.setIdUsuario(session.getUsuario());
            colaChatDao.saveColaChat(colaChat, 0);
        } else {
            setMensajeAbandono("La notificación fué respondida");
            setUrlChat("");
        }
    }

    public Boolean verifyUrl(String urlContacto, String tipo) {
        Boolean aux = false;
        HostnameVerifier hv = new HostnameVerifier() {
            @Override
            public boolean verify(String urlHostName, SSLSession session) {
                System.out.println("Warning: URL Host: " + urlHostName
                        + " vs. " + session.getPeerHost());
                return true;
            }
        };
        try {
            trustAllHttpsCertificates();
        } catch (Exception e) {
            System.out.println("Trustall" + e.getStackTrace());
        }
        HttpsURLConnection.setDefaultHostnameVerifier(hv);

        try {
            URL url = new URL(urlContacto);
            String userPassword = userSocial + ":" + passSocial;
            String encoding = Base64Converter.encode(userPassword.getBytes("UTF-8"));
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            uc.setRequestProperty("Authorization", String.format("Basic %s", encoding));

            int responseCode = uc.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                //InputSource xml = new InputSource(url.openStream());
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(uc.getInputStream());
                doc.getDocumentElement().normalize();
                NodeList nodes = doc.getElementsByTagName("SocialContact");

                for (int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);

                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        if (getValue("sourceType", element).equals(tipo)) {
                            System.out.println("Estado Tipo: " + getValue("status", element));
                            if (getValue("status", element).equals("unread")) {
                                aux = true;
                            }
                        }
                    }
                }
            } else {
                System.out.println("GET request not worked:" + responseCode);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            //pw.println("Invalid URL");
        } catch (IOException e) {
            e.printStackTrace();
            //pw.println("Error reading URL");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aux;
    }

    private static String getValue(String tag, Element element) {
        NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodes.item(0);
        return node.getNodeValue();
    }

    public static class TempTrustedManager implements
            javax.net.ssl.TrustManager, javax.net.ssl.X509TrustManager {

        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public boolean isServerTrusted(
                java.security.cert.X509Certificate[] certs) {
            return true;
        }

        public boolean isClientTrusted(
                java.security.cert.X509Certificate[] certs) {
            return true;
        }

        public void checkServerTrusted(
                java.security.cert.X509Certificate[] certs, String authType)
                throws java.security.cert.CertificateException {
            return;
        }

        public void checkClientTrusted(
                java.security.cert.X509Certificate[] certs, String authType)
                throws java.security.cert.CertificateException {
            return;
        }
    }

    private static void trustAllHttpsCertificates() throws Exception {
        javax.net.ssl.TrustManager[] trustAllCerts =
                new javax.net.ssl.TrustManager[1];
        javax.net.ssl.TrustManager tm = new TempTrustedManager();
        trustAllCerts[0] = tm;
        javax.net.ssl.SSLContext sc =
                javax.net.ssl.SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, null);

        javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
                sc.getSocketFactory());
    }

    public String getNickUsuario() {
        return nickUsuario;
    }

    public void setNickUsuario(String nickUsuario) {
        this.nickUsuario = nickUsuario;
    }
}