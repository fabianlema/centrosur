/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.EventoLlamadaDAO;
import ec.com.centrosurcall.datos.DAO.RegistroSeccionDAO;
import ec.com.centrosurcall.datos.DAO.TllamadaDAO;
import ec.com.centrosurcall.datos.modelo.Evento;
import ec.com.centrosurcall.datos.modelo.RegistroSeccion;
import ec.com.centrosurcall.datos.modelo.TipoLlamada;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */

@ManagedBean
@ViewScoped
public final class CtrEventos extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Evento selEvento = new Evento();
    EventoLlamadaDAO razonesDAO = new EventoLlamadaDAO();
    TllamadaDAO tipoLlamadaDAO = new TllamadaDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemRazones;
    //SelectItem item = new SelectItem();    
    private SelectItem[] selectIvr;
    private SelectItem[] selectTipoLlamada;
    private Integer codigoIvr;
    private Integer tipoLlamadaId;
    final private RegistroSeccionDAO rs = new RegistroSeccionDAO();

    public CtrEventos() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarEventos();
        cargarIvr();
        //cargarTipoLlamada();
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoRazonEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar esta Razón?");
        confPop();
    }
    
    public void cargarIvr() {
        //selectIvr = new SelectItem[9];
        List<RegistroSeccion> listaRegistrosSeccion = rs.getRegistroSeccion();
        selectIvr = new SelectItem[listaRegistrosSeccion.size()+1];
        selectIvr[0] = new SelectItem(0, "--SELECCIONE--");
        for (int i=0; i<listaRegistrosSeccion.size(); i++){
            selectIvr[i+1] = new SelectItem(listaRegistrosSeccion.get(i).getId(), listaRegistrosSeccion.get(i).getNombre());
        }
        
        selectTipoLlamada = new SelectItem[1];        
        selectTipoLlamada[0] = new SelectItem(0, "--SELECCIONE--");
    }

    public void cargarEventos() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Evento> listaRazon = razonesDAO.getEventos();
        if (listaRazon != null) {
            listaBaseObjetos.addAll(listaRazon);
            listaObjetos.addAll(listaBaseObjetos);           
        }
    }
    
    public void cargarTipoLlamada(int codigoIVR) {
        List<TipoLlamada> listaTipoLlamada = tipoLlamadaDAO.getTiposLlamada(codigoIVR);
        int cont = 1;
        if (listaTipoLlamada != null) {            
            selectTipoLlamada = new SelectItem[listaTipoLlamada.size() + 1];
            selectTipoLlamada[0] = new SelectItem(0, "--SELECCIONE--");
            for (TipoLlamada obj : listaTipoLlamada) {
                selectTipoLlamada[cont] = new SelectItem(String.valueOf(obj.getId()), obj.getDescripcion());
                cont++;
            }
        }
    }

    public void cargoRazonEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Evento")) {
                if (((Evento) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selEvento = (Evento) session.getObjeto();
            tipoLlamadaId = selEvento.getTipoLlamada().getId();            
            setCodigoIvr(tipoLlamadaDAO.getTiposLlIvr(selEvento.getId()));
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar la Razón");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        TipoLlamada tl = tipoLlamadaDAO.getTipoLlamadaByID(tipoLlamadaId);
        selEvento.setTipoLlamada(tl);
        selEvento.setEstado(1);
        if (parametroSession.getEsNuevo()) {
            exito = razonesDAO.saveEvento(selEvento, 1);
            parametroSession.setEsNuevo(false);
        } else {
            exito = razonesDAO.saveEvento(selEvento, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Evento> respuesta = new ArrayList<Evento>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Evento cl = (Evento) obj;
                if (cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase()) || cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
       // listaColumnas.put("id", "Id");
        listaColumnas.put("stringLlamada", "Tipo Llamada");
        listaColumnas.put("descripcion", "Descripción");
        //listaColumnas.put("codigoRazon","Código CentroSur");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionEventos";
    }

    @Override
    public void auditoria() {
        try {
            selEvento = (Evento) objetoSeleccionado;
        } catch (Exception e) {
            selEvento.setId(0);
        }
        if (selEvento.getId() > 0) {
            creado = selEvento.getDescripcion();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarEventos();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Evento> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Evento) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmEventos";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmEventos";
    }

    public void confPopEliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Evento> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = razonesDAO.eliminarEventos(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarEventos();
        }
        return exito;
    }

    public List<Evento> sacarSeleccionado() {
        Evento selectRazon = new Evento();
        List<Evento> listadoSeleccionado = new ArrayList<Evento>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectRazon = (Evento) o;
            if (selectRazon.getSeleccionado()) {
                listadoSeleccionado.add(selectRazon);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Evento getSelEvento() {
        return selEvento;
    }

    public void setSelEvento(Evento selEvento) {
        this.selEvento = selEvento;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String descripcion;
    private String stringLlamada;
    private TipoLlamada tipoLlamada;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public SelectItem[] getSelectItemRazones() {
        return selectItemRazones;
    }

    public void setSelectItemRazones(SelectItem[] selectItemRazones) {
        this.selectItemRazones = selectItemRazones;
    }

    public String getStringLlamada() {
        return stringLlamada;
    }

    public void setStringLlamada(String stringLlamada) {
        this.stringLlamada = stringLlamada;
    }

    public TipoLlamada getTipoLlamada() {
        return tipoLlamada;
    }

    public void setTipoLlamada(TipoLlamada tipoLlamada) {
        this.tipoLlamada = tipoLlamada;
    }

    public SelectItem[] getSelectIvr() {
        return selectIvr;
    }

    public void setSelectIvr(SelectItem[] selectIvr) {
        this.selectIvr = selectIvr;
    }

    public SelectItem[] getSelectTipoLlamada() {
        return selectTipoLlamada;
    }

    public void setSelectTipoLlamada(SelectItem[] selectTipoLlamada) {
        this.selectTipoLlamada = selectTipoLlamada;
    }

    public Integer getCodigoIvr() {
        return codigoIvr;
    }

    public void setCodigoIvr(Integer codigoIvr) {
        cargarTipoLlamada(codigoIvr);
        this.codigoIvr = codigoIvr;
    }

    public Integer getTipoLlamadaId() {
        return tipoLlamadaId;
    }

    public void setTipoLlamadaId(Integer tipoLlamadaId) {
        this.tipoLlamadaId = tipoLlamadaId;
    }
}