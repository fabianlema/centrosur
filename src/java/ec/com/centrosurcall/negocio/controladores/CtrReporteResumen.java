/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.utils.ColumnaTemporal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
//import org.openide.util.Exceptions;

//import jxl.*;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrReporteResumen extends MantenimientoGenerico implements Serializable {
    private List<ColumnaTemporal> objListColumna;
    private List<Object> objReporteAgentes = new ArrayList<>();
    
    public CtrReporteResumen() throws SQLException {
        actualizarReporte3();
    }
    
    public void actualizarReporte3(){
            try {                
                String cadenaSQL = "select SUM(CASE WHEN intentos=0 and estado=0 THEN 1 ELSE 0 END) as 'Clientes Por llamar', " + 
                        "SUM(CASE WHEN intentos>0 and intentos<3 and estado=0 THEN 1 ELSE 0 END) as 'Clientes Volver llamar', " + 
                        "SUM(CASE WHEN estado!=0 THEN 1 ELSE 0 END) as 'Clientes No Volver Llamar', COUNT(*) as 'Total Clientes' from cliente";
                ResultSet tempReporte = consulta.ejecutarQuery(cadenaSQL);
                if (tempReporte != null){
                    ResultSetMetaData columnas = tempReporte.getMetaData();
                    int columnCount = columnas.getColumnCount();
                    objListColumna = new ArrayList<>();
                    for (int i = 1; i < columnCount + 1; i++ ) {
                        ColumnaTemporal temp = new ColumnaTemporal(i-1, columnas.getColumnName(i));
                        objListColumna.add(temp);
                    }
                    objReporteAgentes = new ArrayList<>();
                    while (tempReporte.next()) {
                        String[] temp = new String[columnCount];
                        for (int i = 1; i < columnCount + 1; i++ ) {
                            temp[i-1]=tempReporte.getString(i);
                        }
                        objReporteAgentes.add(temp);
                    }
                    
                }
                
            } catch (SQLException ex) {
                System.out.println(ex);
                //Exceptions.printStackTrace(ex);
            }
    }

    public List<ColumnaTemporal> getObjListColumna() {
        return objListColumna;
    }

    public void setObjListColumna(List<ColumnaTemporal> objListColumna) {
        this.objListColumna = objListColumna;
    }

    public List<Object> getObjReporteAgentes() {
        return objReporteAgentes;
    }

    public void setObjReporteAgentes(List<Object> objReporteAgentes) {
        this.objReporteAgentes = objReporteAgentes;
    }
}