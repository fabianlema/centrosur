/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.DAO.UsuariosDAO;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.RazonesDAO;
import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.datos.modelo.Razon;
import ec.com.centrosurcall.datos.modelo.Team;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.reports.GenerarReporte;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import ec.com.centrosurcall.utils.ColumnaTemporal;
import ec.com.centrosurcall.utils.ResultSetToExcel;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import javax.faces.model.SelectItem;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
//import org.openide.util.Exceptions;

//import jxl.*;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrReportes extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Razon selRazon = new Razon();
    RazonesDAO razonesDAO = new RazonesDAO();
    String opcionBoton = "";
    private List<Object> objReporte1 = new ArrayList<>();
    private Date desde;
    private Date hasta;
    private Date fecha11;
    private Date fecha12;
    private Date fecha21;
    private Date fecha22;
    private Date fecha31;
    private Date fecha32;
    private Date fecha41;
    private Date fecha42;
    private Date fechaA;
    private Date fechaB;
    private Date fechaSumDesde;
    private Date fechaSumHasta;
    ParametrosDAO paramDAO = new ParametrosDAO();
    UsuariosDAO usuarioDAO = new UsuariosDAO();
    Parametro svr = paramDAO.getParametroByAtributo("SERVERREPORT");
    Parametro report1 = paramDAO.getParametroByAtributo("REPORT1");
    Parametro report2 = paramDAO.getParametroByAtributo("REPORT2");
    Parametro report3 = paramDAO.getParametroByAtributo("REPORT3");
    Parametro report4 = paramDAO.getParametroByAtributo("REPORT4");
    Parametro report5 = paramDAO.getParametroByAtributo("REPORT5");
    Parametro report6 = paramDAO.getParametroByAtributo("REPORT6");
    Parametro report7 = paramDAO.getParametroByAtributo("REPORT7");
    Parametro report8 = paramDAO.getParametroByAtributo("REPORT8");
    Parametro report9 = paramDAO.getParametroByAtributo("REPORT9");
    Parametro report10 = paramDAO.getParametroByAtributo("REPORT10");
    private Integer idUsuario, idTeam;
    private Integer idTeamSum;
    private SelectItem[] selectItemUsuarios;
    private SelectItem[] selectItemTeams;
    SelectItem item = new SelectItem();
    
    private List<ColumnaTemporal> objListColumna;
    private List<Object> objReporteAgentes = new ArrayList<>();
    private List<Object> objReporteTotales = new ArrayList<>();
    
    private List<Object> objRazon;
    private List<ObjEstados> reporteSumarizado = new ArrayList<>();
    private List<ObjReporteInformix1> reporteInformix1 = new ArrayList<>();
    private List<ObjRazones> reporteRazones;
    ObjReporteEntrantes obj = new ObjReporteEntrantes();
    
    private List<ObjLlamadasEntrantes> llamadasEntrantes = new ArrayList<>();
    private List<ObjReporteEntrantes>  reporteLlamadasEntrantes = new ArrayList<>();
    CtrInformix informix = new CtrInformix();
    private String cadenaSQLR;
    
    public CtrReportes() throws SQLException {
        System.out.println("Constructor");
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        item.setLabel("-SELECCIONE-");
        item.setValue(null);
        cargarColumnasGrupo();
        cargarTeams();
        //cargoRazonEditar();
        //cargarReporte1();
        //cargarReporte2();
        cargarUsuarios();
        setMensajeConfirmacion("Seguro desea grabar esta Razón?");
        confPop();
        /*try{
            informix.cargarDriver();
            //informix.crearConexion("172.16.15.5","db_cra", "ccxtelf01_uccx", "uccxhruser", "password.1");
            informix.crearConexion("131.150.30.244", "db_cra", "CCXDOS01_uccx", "uccxhruser", "password.1");            
        }catch(ClassNotFoundException e){
            System.out.println(e.getMessage());
        }*/
    
    }
    
    public void actualizarReporte1(){
        if(desde!=null && hasta!=null){
            try {
                String desde2 = new SimpleDateFormat("yyyy-MM-dd").format(desde);
                String hasta2 = new SimpleDateFormat("yyyy-MM-dd").format(hasta);
                //List<Razon> objRazon;
                //objRazon = razonesDAO.getRazonesV();
                String cadenaSQL = "select nombre as 'USUARIO', fechaLlamada as 'FECHA', llamadas as 'NUMERO DE LLAMADAS', convert(varchar(8), (segundos+ CONVERT(DATETIME,minutos)),108) as 'TIEMPO TOTAL', CONVERT(CHAR(8),DATEADD(ss,(DATEDIFF(second,0,cast(convert(varchar(8), (segundos+ CONVERT(DATETIME,minutos)),108) as datetime))/llamadas),0),108) as 'PROMEDIO' from vistaTiempos WITH (NOLOCK) " +
                "where fechaLlamada>='"+desde2+"' and fechaLlamada<='"+hasta2+"'  order by 1";
                //System.out.println(cadenaSQL);
                //objReporte1 = consulta.getListSql(cadenaSQL);
                ResultSet tempReporte = consulta.ejecutarQuery(cadenaSQL);
                if (tempReporte != null){
                    ResultSetMetaData columnas = tempReporte.getMetaData();
                    int columnCount = columnas.getColumnCount();
                    objListColumna = new ArrayList<>();
                    for (int i = 1; i < columnCount + 1; i++ ) {
                        ColumnaTemporal temp = new ColumnaTemporal(i-1, columnas.getColumnName(i));
                        objListColumna.add(temp);
                    }
                    objReporteAgentes = new ArrayList<>();
                    while (tempReporte.next()) {
                        String[] temp = new String[columnCount];
                        for (int i = 1; i < columnCount + 1; i++ ) {
                            temp[i-1]=tempReporte.getString(i);
                        }
                        objReporteAgentes.add(temp);
                    }                    
                    
                }
                
            } catch (SQLException ex) {
                System.out.println(ex);
                //Exceptions.printStackTrace(ex);
            } catch (Exception ex) {
                System.out.println(ex);
                //Exceptions.printStackTrace(ex);
            }
        }
    }
    
    public void actualizarReporte2(){
        if(desde!=null && hasta!=null){
            try {
                String desde2 = new SimpleDateFormat("yyyy-MM-dd").format(desde);
                String hasta2 = new SimpleDateFormat("yyyy-MM-dd").format(hasta);
                List<Razon> objRazon;
                objRazon = razonesDAO.getRazones();
                cadenaSQLR = "select u.nombre as 'AGENTE', ";
                for (int i=0; i<=objRazon.size()-1;i++){
                    if (objRazon.get(i).getPadreId()==3)
                        cadenaSQLR += "SUM(case l.tipoLlamada when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    else if (objRazon.get(i).getPadreId()==2)
                        cadenaSQLR += "SUM(case l.esRegistrado when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    else if (objRazon.get(i).getPadreId()==1)
                        cadenaSQLR += "SUM(case l.estadoLlamada when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    else
                        cadenaSQLR += "SUM(case l.idRazon when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"]";
                    if (i<=objRazon.size()-2) cadenaSQLR += ",";
                }
                cadenaSQLR += " from llamada l with (NOLOCK), usuario u with (NOLOCK) " +
                "where l.fechaLlamada>='"+desde2+"' and l.fechaLlamada<='"+hasta2+"' " + 
                "and l.idUsuario=u.id and (l.estado !=2 and l.estado !=3) group by u.nombre " +
                "order by 1, 2";
                //System.out.println(cadenaSQL);
                //objReporte1 = consulta.getListSql(cadenaSQL);
                ResultSet tempReporte = consulta.ejecutarQuery(cadenaSQLR);
                if (tempReporte != null){
                    ResultSetMetaData columnas = tempReporte.getMetaData();
                    int columnCount = columnas.getColumnCount();
                    int[] tempSumas = new int[columnCount];
                    objListColumna = new ArrayList<>();
                    for (int i = 1; i < columnCount + 1; i++ ) {
                        ColumnaTemporal temp = new ColumnaTemporal(i-1, columnas.getColumnName(i));
                        objListColumna.add(temp);
                    }
                    objReporteAgentes = new ArrayList<>();
                    //tempReporte.beforeFirst();
                    while (tempReporte.next()) {
                        String[] temp = new String[columnCount];
                        for (int i = 1; i < columnCount + 1; i++ ) {
                            temp[i-1]=tempReporte.getString(i);
                            if (i!=1){
                                tempSumas[i-1] = tempSumas[i-1] + tempReporte.getInt(i);
                            }
                        }
                        objReporteAgentes.add(temp);
                    }
                    String[] temp = new String[columnCount];
                    for (int i = 0; i < columnCount; i++) {
                        if (i!=0){
                            temp[i]=String.valueOf(tempSumas[i]);
                        }else{
                            temp[i]="TOTAL";
                        }
                    }                    
                    objReporteAgentes.add(temp);
                }                
            } catch (SQLException ex) {
                System.out.println(ex);
                //Exceptions.printStackTrace(ex);
            } catch (Exception ex) {
                System.out.println(ex);
                //Exceptions.printStackTrace(ex);
            }
        }
    }
    
    public void actualizarReporte3(){
            try {                
                String cadenaSQL = "select SUM(CASE WHEN intentos=0 and estado=0 THEN 1 ELSE 0 END) as 'Clientes Por llamar', " + 
                        "SUM(CASE WHEN intentos>0 and intentos<3 and estado=0 THEN 1 ELSE 0 END) as 'Clientes llamados', " + 
                        "SUM(CASE WHEN intentos>2 and estado!=0 THEN 1 ELSE 0 END) as 'Clientes llamadoss', COUNT(*) as 'Total Clientes' from cliente";
                ResultSet tempReporte = consulta.ejecutarQuery(cadenaSQL);
                if (tempReporte != null){
                    ResultSetMetaData columnas = tempReporte.getMetaData();
                    int columnCount = columnas.getColumnCount();
                    int[] tempSumas = new int[columnCount];
                    objListColumna = new ArrayList<>();
                    for (int i = 1; i < columnCount + 1; i++ ) {
                        ColumnaTemporal temp = new ColumnaTemporal(i-1, columnas.getColumnName(i));
                        objListColumna.add(temp);
                    }
                    objReporteAgentes = new ArrayList<>();
                    while (tempReporte.next()) {
                        String[] temp = new String[columnCount];
                        for (int i = 1; i < columnCount + 1; i++ ) {
                            temp[i-1]=tempReporte.getString(i);
                            if (i!=1){
                                tempSumas[i-1] = tempSumas[i-1] + tempReporte.getInt(i);
                            }
                        }
                        objReporteAgentes.add(temp);
                    }
                    String[] temp = new String[columnCount];
                    for (int i = 0; i < columnCount; i++) {                        
                        if (i!=0){
                            temp[i]=String.valueOf(tempSumas[i]);
                        }else{
                            temp[i]="TOTAL";
                        }
                    }
                    objReporteAgentes.add(temp);
                    
                }
                
            } catch (SQLException ex) {
                System.out.println(ex);
                //Exceptions.printStackTrace(ex);
            }
    }
    
    public void cargarReporte1(){
        String cadenaSQL="select b.fechaLlamada, b.hora,b.tiempoDuracion,b.estadoLlamada, a.idCliente, b.numeroTelefono, a.numCredito, c.nick, d.nombre, b.observacion "+
                        "from dbo.llamada b "+
                        "join dbo.credito a on b.numCredito=a.numCredito "+
                        "join dbo.usuario c on b.idUsuario=c.id "+
                        "join dbo.razon d on b.idRazon = d.id "+
                        "order by b.fechaLlamada, b.hora ";
        listaObjetos = consulta.getListSql(cadenaSQL);
    }
    public void cargarReporte2(){
        List<Razon> objRazon;
        objRazon = razonesDAO.getRazones();
        String cadenaSQL = "select u.'nombre AGENTE, " +
        "SUM(case l.estadoLlamada+l.idrazon when 2 then 1 else 0 end) [EFECTIVA],  ";
        for (int i=1; i<=objRazon.size()-1;i++){
            cadenaSQL += "SUM(case l.idRazon when "+objRazon.get(i).getId() +" then 1 else 0 end) ["+objRazon.get(i).getNombre()+"], ";
        }
	cadenaSQL += "sum(case l.idUsuario when l.idUsuario then 1 else 0 end) [TOTAL] " +
        "from llamada l left join razon r on l.idRazon = r.id  " +
	"join usuario u on u.id=l.idUsuario " +
        "where l.fechaLlamada>='2013-04-01' and l.fechaLlamada<='2013-04-30' " +
        "group by u.nombre " +
        "order by 1, 2";
        objReporte1 = consulta.getListSql(cadenaSQL);
    }
    
    /*public void cargarRazones() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Razon> listaRazon = razonesDAO.getRazonesV();
        if (listaRazon != null) {
            listaBaseObjetos.addAll(listaRazon);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }*/
       
     public void cargarUsuarios() {
        selectItemUsuarios = new SelectItem[1];
        selectItemUsuarios[0] = item;
        Consultas consulta = new Consultas();
        //Select * from usuario where id in (Select idUsuario from team_usuario where idTeam IN (Select id from team where idSupervisor= 22 and estado=1)) and estado=1
        setObjRazon((List<Object>) consulta.getListHql(null, "Usuario", "id in (Select tu.id.idUsuario from TeamUsuario tu where tu.id.idTeam IN (Select t.id from Team t where t.idSupervisor= "+this.session.getUsuario().getId()+" and t.estado=1)) and estado=1", null));
       
        if (getObjRazon() != null && getObjRazon().size() > 0) {
            int cont = 1;
            selectItemUsuarios = new SelectItem[getObjRazon().size() + 1];
            selectItemUsuarios[0] = item;
            for (Object obj : getObjRazon()) {
                Usuario cl = (Usuario) obj;
                selectItemUsuarios[cont] = new SelectItem(String.valueOf(cl.getId()), cl.getNombre());
                cont++;
            }
        }
    }
    public void consultaLlamadasEntrantes() throws ParseException{
        llamadasEntrantes=new ArrayList<>();
        reporteLlamadasEntrantes = new ArrayList<>();
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<Object[]> listUsuarios = usuarioDAO.getUsuariosByTeam(idTeamSum);
        try{
            if(fechaSumDesde!=null && fechaSumHasta!=null){
                for(int j=0;j<listUsuarios.size();j++){
                    obj = new ObjReporteEntrantes();
                    Object[] usuario = listUsuarios.get(j);
                    List<ObjLlamadasEntrantes> reporteTemp = new ArrayList<>();
                    reporteTemp = informix.reporteLlamadasEntrantes(String.valueOf(usuario[1]),fechaSumDesde,fechaSumHasta,"");
                    llamadasEntrantes.addAll(reporteTemp);
                    obj.setAgente(String.valueOf(usuario[6]));
                    Double ttotal=Double.valueOf(0); 
                    for(int i=0;i<reporteTemp.size();i++){
                        Double tparcial;
                        tparcial = Double.valueOf(reporteTemp.get(i).getTiempo())/60;
                        ttotal = ttotal + tparcial;
                    }
                    ttotal = ttotal*1000/1000;
                    ttotal = Math.rint(ttotal*1000)/1000;
                    obj.setTiempoTotal(ttotal + " min");
                    obj.setNumLlamadas(String.valueOf(reporteTemp.size())); 
                    getReporteLlamadasEntrantes().add(obj);
                }
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
    
     public void cargarTeams() {
        selectItemTeams = new SelectItem[1];
        selectItemTeams[0] = item;
        Consultas consulta = new Consultas();
        setObjRazon((List<Object>) consulta.getListHql(null, "Team", "estado = 1 and idSupervisor="+this.session.getUsuario().getId()+"order by nombre", null));
        if (getObjRazon() != null && getObjRazon().size() > 0) {
            int cont = 1;
            selectItemTeams = new SelectItem[getObjRazon().size() + 1];
            selectItemTeams[0] = item;
            for (Object obj : getObjRazon()) {
                Team cl = (Team) obj;
                selectItemTeams[cont] = new SelectItem(String.valueOf(cl.getId()), cl.getNombre());
                cont++;
            }
        }
    }
    
  public void cargarReporte(String tipo) {
        if (desde != null && hasta != null) {
            try {
                GenerarReporte r = new GenerarReporte();
                /*Map parametros = new HashMap();
                parametros.put("fechaA", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(desde));
                parametros.put("fechaB", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(hasta));
                //if (!autorizado)
                parametros.put("idUsuario", session.getUsuario().getId());
                //r.generaXlsxListado(objReporteAgentes   , report1.getValor(), parametros, report1.getValor());
                //r.generaXlsx(report1.getValor(), parametros, report1.getValor());
                ResultSet tempReporte = consulta.ejecutarQuery(cadenaSQLR);*/
                ResultSet tempReporte = consulta.ejecutarQuery(cadenaSQLR);
                r.generaXlsxListado(tempReporte, report1.getValor(), report1.getValor(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(desde), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(hasta));
            } catch (Exception ex) {
                System.out.println(ex);
                //Exceptions.printStackTrace(ex);
            }
        }
        else{
//         return "page no found" ;
        }
    }
  
  public void cargarReporteEfe(String tipo) {
        if (desde != null && hasta != null) {
            GenerarReporte r = new GenerarReporte();
            Map parametros = new HashMap();
            parametros.put("fechaA", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(desde));
            parametros.put("fechaB", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(hasta));
            //if (!autorizado)
            parametros.put("idUsuario", session.getUsuario().getId());
            r.generaXlsxListado(objReporteAgentes, report7.getValor(), parametros, report7.getValor());
            //r.generaXlsx(report7.getValor(), parametros, report7.getValor());
        }
        else{
//         return "page no found" ;
        }
    }
  
  public void cargarReporteMEER(String tipo) {
        if (desde != null && hasta != null) {
            GenerarReporte r = new GenerarReporte();
            Map parametros = new HashMap();
            parametros.put("fechaA", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(desde));
            parametros.put("fechaB", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(hasta));
            //if (!autorizado)
            parametros.put("idUsuario", session.getUsuario().getNombre());
            
            r.generaXlsx(report2.getValor(), parametros, report2.getValor());
        }
        else{
//         return "page no found" ;
        }
    }
    
    public void cargarReporte_1(String tipo) {
        System.out.println("1111");
        if (fecha11 != null && fecha12 != null) {
            GenerarReporte r = new GenerarReporte();
            Map parametros = new HashMap();
            parametros.put("fechaA", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha11));
            parametros.put("fechaB", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha12));
            //if (!autorizado)
            parametros.put("idUsuario", session.getUsuario().getId());
            
            r.generaXlsx(report3.getValor(), parametros, report3.getValor());
        }
        else{
//         return "page no found" ;
        }
    }
    
    public void cargarReporte_2(String tipo) {
        System.out.println("2222");
        if (fecha21 != null && fecha22 != null) {
            GenerarReporte r = new GenerarReporte();
            Map parametros = new HashMap();
            parametros.put("fechaA", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha21));
            parametros.put("fechaB", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha22));
            //if (!autorizado)
            parametros.put("idUsuario", session.getUsuario().getId());
            
            r.generaXlsx(report4.getValor(), parametros, report4.getValor());
        }
        else{
//         return "page no found" ;
        }
    }
    
    public void cargarReporte_3(String tipo) {
        System.out.println("333");
        if (fecha31 != null && fecha32 != null) {
            GenerarReporte r = new GenerarReporte();
            Map parametros = new HashMap();
            parametros.put("fechaA", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha31));
            parametros.put("fechaB", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha32));
            //if (!autorizado)
            parametros.put("idUsuario", session.getUsuario().getId());
            
            r.generaXlsx(report5.getValor(), parametros, report5.getValor());
        }
        else{
//         return "page no found" ;
        }
    }
    
    public void cargarReporte_4(String tipo) {
        System.out.println("4444");
        if (fecha41 != null && fecha42 != null) {
            GenerarReporte r = new GenerarReporte();
            Map parametros = new HashMap();
            parametros.put("fechaA", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha41));
            parametros.put("fechaB", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fecha42));
            //if (!autorizado)
            parametros.put("idUsuario", session.getUsuario().getId());
            
            r.generaXlsx(report6.getValor(), parametros, report6.getValor());
        }
        else{
//         return "page no found" ;
        }
    }
    
     public String cargarReporte_5(String tipo, Integer idUsuario, Integer idRazon) {
//        private String urlreporte1;       
         String url="";
        if (this.fechaA != null && this.fechaB != null) {            
            String fechaA2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaA);
            String fechaB2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaB);
            String horaA2 = new SimpleDateFormat("HH:mm:ss").format(fechaA.getTime());
            String horaB2 = new SimpleDateFormat("HH:mm:ss").format(fechaB.getTime());
            //http://172.16.15.10:8080/CoopJEP/JReport?reporte=LlamadasEfectivasUsuario&parametros=idUsuario=3~fechaA=2013-01-01~fechaB=2013-04-29~horaA=12:00:00~horaB=20:00:00&tipo=1
            http://172.16.15.10:8080/CoopJEP/JReport?reporte=LlamadasPorCategoriaUsuario&parametros=idRazon=1~fechaA=2013-01-01~fechaB=2013-04-29~horaA=12:00:00~horaB=20:00:00~idUsuario=3&tipo=1
            url="http://" + svr.getValor() + "" + report5.getValor() + "&parametros=idRazon="+idRazon+"~fechaA=" + fechaA2 + "~fechaB=" + fechaB2 + "~horaA=" + horaA2 + "~horaB=" + horaB2 + "~idUsuario="+idUsuario+"&tipo="+tipo+"";
            return url;
        }
        else{
         return "page no found" ;
        }
        
    }
   
     
     ///REPORTES DE EFICIENCIA DE LLAMADAS EFECTIVAS
    public String cargarReporte2_1(String tipo) {
//        private String urlreporte1;
       
         String url="";
        if (this.fechaA != null && this.fechaB != null) {
            
            String fechaA2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaA);
            String fechaB2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaB);
            String horaA2 = new SimpleDateFormat("HH:mm:ss").format(fechaA.getTime());
            String horaB2 = new SimpleDateFormat("HH:mm:ss").format(fechaB.getTime());
            url="http://" + svr.getValor() + "" + report6.getValor() + "&parametros=fechaA=" + fechaA2 + "~fechaB=" + fechaB2 + "~horaA=" + horaA2 + "~horaB=" + horaB2 + "~idTeam="+this.idTeam+"&tipo="+tipo+"";
            return url;
        }
        else{
         return "page no found" ;
        }
        
    }
     public String cargarReporte2_2(String tipo) {
//        private String urlreporte1;
       
         String url="";
        if (this.fechaA != null && this.fechaB != null) {
            
            String fechaA2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaA);
            String fechaB2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaB);
            String horaA2 = new SimpleDateFormat("HH:mm:ss").format(fechaA.getTime());
            String horaB2 = new SimpleDateFormat("HH:mm:ss").format(fechaB.getTime());
          //  http://172.16.15.10:8080/CoopJEP/JReport?reporte=LlamadasPorCategoria&parametros=idRazon=1~fechaA=2013-01-01~fechaB=2013-04-29~horaA=12:00:00~horaB=20:00:00&tipo=1
            url="http://" + svr.getValor() + "" + report7.getValor() + "&parametros=fechaA=" + fechaA2 + "~fechaB=" + fechaB2 + "~horaA=" + horaA2 + "~horaB=" + horaB2 + "~idTeam="+this.idTeam+"&tipo="+tipo+"";
            return url;
        }
        else{
         return "page no found" ;
        }
        
    }
//     public String cargarReporte2_3(String tipo, Integer idUsuario) {
////        private String urlreporte1;
//       
//         String url="";
//        if (this.fechaA != null && this.fechaB != null) {
//            
//            String fechaA2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaA);
//            String fechaB2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaB);
//            String horaA2 = new SimpleDateFormat("HH:mm:ss").format(fechaA.getTime());
//            String horaB2 = new SimpleDateFormat("HH:mm:ss").format(fechaB.getTime());
//            //http://172.16.15.10:8080/CoopJEP/JReport?reporte=LlamadasEfectivasUsuario&parametros=idUsuario=3~fechaA=2013-01-01~fechaB=2013-04-29~horaA=12:00:00~horaB=20:00:00&tipo=1
//            url="http://" + svr.getValor() + "" + report8.getValor() + "&parametros=idUsuario="+idUsuario+"~fechaA=" + fechaA2 + "~fechaB=" + fechaB2 + "~horaA=" + horaA2 + "~horaB=" + horaB2 + "&tipo="+tipo+"";
//            return url;
//        }
//        else{
//         return "page no found" ;
//        }
//        
//    }
     public String cargarReporte2_3(String tipo) {
//        private String urlreporte1;
       
         String url="";
        if (this.fechaA != null && this.fechaB != null) {
            
            String fechaA2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaA);
            String fechaB2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaB);
            String horaA2 = new SimpleDateFormat("HH:mm:ss").format(fechaA.getTime());
            String horaB2 = new SimpleDateFormat("HH:mm:ss").format(fechaB.getTime());
          
            url="http://" + svr.getValor() + "" + report8.getValor() + "&parametros=fechaA=" + fechaA2 + "~fechaB=" + fechaB2 + "~horaA=" + horaA2 + "~horaB=" + horaB2 + "~idTeam="+this.idTeam+"&tipo="+tipo+"";
            return url;
        }
        else{
         return "page no found" ;
        }
        
    }
//      public String cargarReporte2_4(String tipo) {
////        private String urlreporte1;
//       
//         String url="";
//        if (this.fechaA != null && this.fechaB != null) {
//            
//            String fechaA2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaA);
//            String fechaB2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaB);
//            String horaA2 = new SimpleDateFormat("HH:mm:ss").format(fechaA.getTime());
//            String horaB2 = new SimpleDateFormat("HH:mm:ss").format(fechaB.getTime());
//        //    http://172.16.15.10:8080/CoopJEP/JReport?reporte=LlamadasEfectivasTotal&parametros=fechaA=2013-01-01~fechaB=2013-04-29~horaA=12:00:00~horaB=20:00:00&tipo=1
//            url="http://" + svr.getValor() + "" + report9.getValor() + "&parametros=fechaA=" + fechaA2 + "~fechaB=" + fechaB2 + "~horaA=" + horaA2 + "~horaB=" + horaB2 + "&tipo="+tipo+"";
//            return url;
//        }
//        else{
//         return "page no found" ;
//        }
//        
//    }
      public String cargarReporte2_4(String tipo, Integer idUsuario) {
//        private String urlreporte1;
       
         String url="";
        if (this.fechaA != null && this.fechaB != null) {
            
            String fechaA2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaA);
            String fechaB2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaB);
            String horaA2 = new SimpleDateFormat("HH:mm:ss").format(fechaA.getTime());
            String horaB2 = new SimpleDateFormat("HH:mm:ss").format(fechaB.getTime());
            //http://172.16.15.10:8080/CoopJEP/JReport?reporte=LlamadasEfectivasUsuario&parametros=idUsuario=3~fechaA=2013-01-01~fechaB=2013-04-29~horaA=12:00:00~horaB=20:00:00&tipo=1
            url="http://" + svr.getValor() + "" + report9.getValor() + "&parametros=idUsuario="+idUsuario+"~fechaA=" + fechaA2 + "~fechaB=" + fechaB2 + "~horaA=" + horaA2 + "~horaB=" + horaB2 + "&tipo="+tipo+"";
            return url;
        }
        else{
         return "page no found" ;
        }
        
    }
      public String cargarReporte2_5(String tipo) {
//        private String urlreporte1;
       
         String url="";
        if (this.fechaA != null && this.fechaB != null) {
            
            String fechaA2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaA);
            String fechaB2 = new SimpleDateFormat("yyyy-MM-dd").format(this.fechaB);
            String horaA2 = new SimpleDateFormat("HH:mm:ss").format(fechaA.getTime());
            String horaB2 = new SimpleDateFormat("HH:mm:ss").format(fechaB.getTime());
            //http://172.16.15.10:8080/CoopJEP/JReport?reporte=LlamadasEfectivasUsuario&parametros=idUsuario=3~fechaA=2013-01-01~fechaB=2013-04-29~horaA=12:00:00~horaB=20:00:00&tipo=1
        //    http://172.16.15.10:8080/CoopJEP/JReport?reporte=LlamadasPorCategoriaUsuario&parametros=idRazon=1~fechaA=2013-01-01~fechaB=2013-04-29~horaA=12:00:00~horaB=20:00:00~idUsuario=3&tipo=1
            url="http://" + svr.getValor() + "" + report10.getValor() + "&parametros=fechaA=" + fechaA2 + "~fechaB=" + fechaB2 + "~horaA=" + horaA2 + "~horaB=" + horaB2 + "&tipo="+tipo+"";
            return url;
        }
        else{
         return "page no found" ;
        }
        
    }
      
    public void cargarReporteEfectividad(String tipo) throws IOException {
        //GeneraReporte r = new GeneraReporte();
        //Map parametros = new HashMap();
        //parametros.put("NOMREPORTE", "reporteLlamadasEntrantes");
        //r.generaPdfListado(reporteLlamadasEntrantes, "reporteLlamadasEntrantes", this.getClass(), parametros, "Reporte_llamadas_entrantes");
    }
     
    
    public void cargoRazonEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Razon")) {
                if (((Razon) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selRazon = (Razon) session.getObjeto();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar la RazÃ³n");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            selRazon.setEstado(1);
            exito = razonesDAO.saveRazon(selRazon, 1);
            parametroSession.setEsNuevo(false);
        } else {
            exito = razonesDAO.saveRazon(selRazon, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Razon> respuesta = new ArrayList<Razon>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Razon cl = (Razon) obj;
                if (cl.getNombre().toUpperCase().contains(whereSql.toUpperCase()) || cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        listaColumnas.put("fechaLlamada", "Fecha");
        listaColumnas.put("hora", "Hora");
    }
    
    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getFechaLlamada() {
        return fechaLlamada;
    }

    public void setFechaLlamada(String fechaLlamada) {
        this.fechaLlamada = fechaLlamada;
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionRazones";
    }

    @Override
    public void auditoria() {
        try {
            selRazon = (Razon) objetoSeleccionado;
        } catch (Exception e) {
            selRazon.setId(0);
        }
        if (selRazon.getId() > 0) {
            creado = selRazon.getNombre();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        //cargarRazones();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Razon> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Razon) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmRazones";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmRazones";
    }

    public void confPopEliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Razon> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = razonesDAO.eliminarRazones(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            //cargarRazones();
        }
        return exito;
    }

    public List<Razon> sacarSeleccionado() {
        Razon selectRazon = new Razon();
        List<Razon> listadoSeleccionado = new ArrayList<Razon>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectRazon = (Razon) o;
            if (selectRazon.getSeleccionado()) {
                listadoSeleccionado.add(selectRazon);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Razon getSelRazon() {
        return selRazon;
    }

    public void setSelRazon(Razon selRazon) {
        this.selRazon = selRazon;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String hora;
    private String fechaLlamada;
    private String estado;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    public List<Object> getObjReporte1() {
        return objReporte1;
    }

    public void setObjReporte1(List<Object> objReporte1) {
        this.objReporte1 = objReporte1;
    }

    public Date getHasta() {
        return hasta;
    }

    public void setHasta(Date hasta) {
        this.hasta = hasta;
    }

    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }

  
    
    

    /**
     * @return the fechaA
     */
    public Date getFechaA() {
        return fechaA;
    }

    /**
     * @param fechaA the fechaA to set
     */
    public void setFechaA(Date fechaA) {
        System.out.println("fecha a "+fechaA);
        this.fechaA = fechaA;
    }

    /**
     * @return the fechaB
     */
    public Date getFechaB() {
        return fechaB;
    }

    /**
     * @param fechaB the fechaB to set
     */
    public void setFechaB(Date fechaB) {
        System.out.println("fecha b "+fechaB);
        this.fechaB = fechaB;
    }

    /**
     * @return the idUsuario
     */
    public Integer getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the selectItemUsuarios
     */
    public SelectItem[] getSelectItemUsuarios() {
        return selectItemUsuarios;
    }

    /**
     * @param selectItemUsuarios the selectItemUsuarios to set
     */
    public void setSelectItemUsuarios(SelectItem[] selectItemUsuarios) {
        this.selectItemUsuarios = selectItemUsuarios;
    }

    /**
     * @return the objRazon
     */
    public List<Object> getObjRazon() {
        return objRazon;
    }

    /**
     * @param objRazon the objRazon to set
     */
    public void setObjRazon(List<Object> objRazon) {
        this.objRazon = objRazon;
    }

    /**
     * @return the selectItemTeams
     */
    public SelectItem[] getSelectItemTeams() {
        return selectItemTeams;
    }

    /**
     * @param selectItemTeams the selectItemTeams to set
     */
    public void setSelectItemTeams(SelectItem[] selectItemTeams) {
        this.selectItemTeams = selectItemTeams;
    }

    /**
     * @return the idTeam
     */
    public Integer getIdTeam() {
        return idTeam;
    }

    /**
     * @param idTeam the idTeam to set
     */
    public void setIdTeam(Integer idTeam) {
        this.idTeam = idTeam;
    }

    /**
     * @return the reporteSumarizado
     */
    public List<ObjEstados> getReporteSumarizado() {
        return reporteSumarizado;
    }

    /**
     * @param reporteSumarizado the reporteSumarizado to set
     */
    public void setReporteSumarizado(List<ObjEstados> reporteSumarizado) {
        this.reporteSumarizado = reporteSumarizado;
    }

    /**
     * @return the fechaSumDesde
     */
    public Date getFechaSumDesde() {
        return fechaSumDesde;
    }

    /**
     * @param fechaSumDesde the fechaSumDesde to set
     */
    public void setFechaSumDesde(Date fechaSumDesde) {
        this.fechaSumDesde = fechaSumDesde;
    }

    /**
     * @return the fechaSumHasta
     */
    public Date getFechaSumHasta() {
        return fechaSumHasta;
    }

    /**
     * @param fechaSumHasta the fechaSumHasta to set
     */
    public void setFechaSumHasta(Date fechaSumHasta) {
        this.fechaSumHasta = fechaSumHasta;
    }

    /**
     * @return the idTeamSum
     */
    public Integer getIdTeamSum() {
        return idTeamSum;
    }

    /**
     * @param idTeamSum the idTeamSum to set
     */
    public void setIdTeamSum(Integer idTeamSum) {
        this.idTeamSum = idTeamSum;
    }

/**
     * @return the reporteInformix1
     */
    public List<ObjReporteInformix1> getReporteInformix1() {
        return reporteInformix1;
    }

    /**
     * @param reporteInformix1 the reporteInformix1 to set
     */
    public void setReporteInformix1(List<ObjReporteInformix1> reporteInformix1) {
        this.reporteInformix1 = reporteInformix1;
    }

    /**
     * @return the llamadasEntrantes
     */
    public List<ObjLlamadasEntrantes> getLlamadasEntrantes() {
        return llamadasEntrantes;
    }

    /**
     * @param llamadasEntrantes the llamadasEntrantes to set
     */
    public void setLlamadasEntrantes(List<ObjLlamadasEntrantes> llamadasEntrantes) {
        this.llamadasEntrantes = llamadasEntrantes;
    }

    /**
     * @return the reporteLlamadasEntrantes
     */
    public List<ObjReporteEntrantes> getReporteLlamadasEntrantes() {
        return reporteLlamadasEntrantes;
    }

    /**
     * @param reporteLlamadasEntrantes the reporteLlamadasEntrantes to set
     */
    public void setReporteLlamadasEntrantes(List<ObjReporteEntrantes> reporteLlamadasEntrantes) {
        this.reporteLlamadasEntrantes = reporteLlamadasEntrantes;
    }

    public List<ColumnaTemporal> getObjListColumna() {
        return objListColumna;
    }

    public void setObjListColumna(List<ColumnaTemporal> objListColumna) {
        this.objListColumna = objListColumna;
    }

    public List<Object> getObjReporteAgentes() {
        return objReporteAgentes;
    }

    public void setObjReporteAgentes(List<Object> objReporteAgentes) {
        this.objReporteAgentes = objReporteAgentes;
    }

    public List<Object> getObjReporteTotales() {
        return objReporteTotales;
    }

    public void setObjReporteTotales(List<Object> objReporteTotales) {
        this.objReporteTotales = objReporteTotales;
    }

    public Date getFecha11() {
        return fecha11;
    }

    public void setFecha11(Date fecha11) {
        this.fecha11 = fecha11;
    }

    public Date getFecha12() {
        return fecha12;
    }

    public void setFecha12(Date fecha12) {
        this.fecha12 = fecha12;
    }

    public Date getFecha21() {
        return fecha21;
    }

    public void setFecha21(Date fecha21) {
        this.fecha21 = fecha21;
    }

    public Date getFecha22() {
        return fecha22;
    }

    public void setFecha22(Date fecha22) {
        this.fecha22 = fecha22;
    }

    public Date getFecha31() {
        return fecha31;
    }

    public void setFecha31(Date fecha31) {
        this.fecha31 = fecha31;
    }

    public Date getFecha32() {
        return fecha32;
    }

    public void setFecha32(Date fecha32) {
        this.fecha32 = fecha32;
    }

    public Date getFecha41() {
        return fecha41;
    }

    public void setFecha41(Date fecha41) {
        this.fecha41 = fecha41;
    }

    public Date getFecha42() {
        return fecha42;
    }

    public void setFecha42(Date fecha42) {
        this.fecha42 = fecha42;
    }
}