/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.DepartamentoDAO;
import ec.com.centrosurcall.datos.modelo.CentroCosto;
import ec.com.centrosurcall.datos.modelo.Ciudad;
import ec.com.centrosurcall.datos.modelo.Departamento;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrDepartamento extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Departamento selDepartamento = new Departamento();
    DepartamentoDAO departamentoDAO = new DepartamentoDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemCiudad;
    private SelectItem[] selectItemCentroCosto;
    private SelectItem[] selectItemDepartamento;
    private Boolean atriEditable = false, esGuion = false;
    private String idCiudad = "0";
    private String idCentroCosto = "0";
    private String idDepartamento = "0";
    private List<Object> objCiudad;
    private List<Object> objCentroCosto;
    //private List<Object> objDepartamento;
    private SelectItem item = new SelectItem("0", "--SELECCIONE--");

    public CtrDepartamento() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarParametros();
        cargarCiudad();
        cargarCentroCosto();
        if (parametroSession.getEsNuevo()) {
            atriEditable = false;
        }
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            atriEditable = true;
            cargoParametrosEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar este Codigo?");
        confPop();
    }

    public void cargarCiudad() {
        selectItemCiudad = new SelectItem[1];
        selectItemCiudad[0] = item;
        objCiudad = consulta.getListHql(null, "Ciudad", "id>0 order by nombre", null);
        if (objCiudad != null && objCiudad.size() > 0) {
            int cont = 1;
            selectItemCiudad = new SelectItem[objCiudad.size() + 1];
            selectItemCiudad[0] = item;
            for (Object obj : objCiudad) {
                Ciudad cl = (Ciudad) obj;
                selectItemCiudad[cont] = new SelectItem(String.valueOf(cl.getId()), cl.getNombre());
                cont++;
            }
        }
    }
    
    public void cargarCentroCosto() {
        selectItemCentroCosto = new SelectItem[1];
        selectItemCentroCosto[0] = item;
        objCentroCosto = consulta.getListHql(null, "CentroCosto", "id>0 order by nombre", null);
        if (objCentroCosto != null && objCentroCosto.size() > 0) {
            int cont = 1;
            selectItemCentroCosto = new SelectItem[objCentroCosto.size() + 1];
            selectItemCentroCosto[0] = item;
            for (Object obj : objCentroCosto) {
                CentroCosto cl = (CentroCosto) obj;
                selectItemCentroCosto[cont] = new SelectItem(String.valueOf(cl.getId()), cl.getNombre());
                cont++;
            }
        }
    }

    public void cargarParametros() {
        selectItemDepartamento = new SelectItem[1];
        selectItemDepartamento[0] = item;
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Departamento> listaParametros = departamentoDAO.getDepartamento();
        if (listaParametros != null) {
            listaBaseObjetos.addAll(listaParametros);
            listaObjetos.addAll(listaBaseObjetos);
            int cont = 1;
            selectItemDepartamento = new SelectItem[listaParametros.size() + 1];
            selectItemDepartamento[0] = item;
            for (Departamento obj : listaParametros) {
                selectItemDepartamento[cont] = new SelectItem(String.valueOf(obj.getId()), obj.getStringDepartamento());
                cont++;
            }
        }
    }

    public void cargoParametrosEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Departamento")) {
                if (((Departamento) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selDepartamento = (Departamento) session.getObjeto();
            idCiudad = String.valueOf(selDepartamento.getCiudad().getId());
            idCentroCosto = String.valueOf(selDepartamento.getCentroCosto().getId());
            if (selDepartamento.getDepartamento()!=null){
                idDepartamento = String.valueOf(selDepartamento.getDepartamento().getId());
            }            
            atriEditable = false;
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Codigo de llamada entrante");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        Ciudad ciuTemp = new Ciudad();
        ciuTemp.setId(Integer.parseInt(idCiudad));
        
        CentroCosto ceCostoTemp = new CentroCosto();
        ceCostoTemp.setId(Integer.parseInt(idCentroCosto));
        
        if (!idDepartamento.equals("0")){
            Departamento departamentoTemp = new Departamento();
            departamentoTemp.setId(Integer.parseInt(idDepartamento));    
            selDepartamento.setDepartamento(departamentoTemp);
        }
        
        selDepartamento.setCiudad(ciuTemp);
        selDepartamento.setCentroCosto(ceCostoTemp);
        
        if (parametroSession.getEsNuevo()) {
            //selDepartamento.setEstado(Boolean.TRUE);            
            exito = departamentoDAO.saveDepartamento(selDepartamento, 1);
            parametroSession.setEsNuevo(false);
        } else {
            exito = departamentoDAO.saveDepartamento(selDepartamento, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Departamento> respuesta = new ArrayList<Departamento>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Departamento cl = (Departamento) obj;
                if (cl.getNombre().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        listaColumnas.put("codigo", "Codigo");
        listaColumnas.put("stringCiudad", "Ciudad");
        listaColumnas.put("stringCentroCosto", "Cento Costo");
        listaColumnas.put("stringDepartamento", "Nombre");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionDepartamento";
    }

    @Override
    public void auditoria() {
        try {
            selDepartamento = (Departamento) objetoSeleccionado;
        } catch (Exception e) {
            selDepartamento.setId(0);
        }
        if (selDepartamento.getId() > 0) {
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarParametros();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Departamento> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Departamento) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmDepartamento";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmDepartamento";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Departamento> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = departamentoDAO.eliminarDepartamento(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarParametros();
        }
        return exito;
    }

    public List<Departamento> sacarSeleccionado() {
        Departamento selectParametros = new Departamento();
        List<Departamento> listadoSeleccionado = new ArrayList<Departamento>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectParametros = (Departamento) o;
            if (selectParametros.getSeleccionado()) {
                listadoSeleccionado.add(selectParametros);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Departamento getSelDepartamento() {
        return selDepartamento;
    }

    public void setSelDepartamento(Departamento selDepartamento) {
        this.selDepartamento = selDepartamento;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }

    public SelectItem[] getSelectItemCiudad() {
        return selectItemCiudad;
    }

    public void setSelectItemCiudad(SelectItem[] selectItemCiudad) {
        this.selectItemCiudad = selectItemCiudad;
    }

    public SelectItem[] getSelectItemCentroCosto() {
        return selectItemCentroCosto;
    }

    public void setSelectItemCentroCosto(SelectItem[] selectItemCentroCosto) {
        this.selectItemCentroCosto = selectItemCentroCosto;
    }

    public SelectItem[] getSelectItemDepartamento() {
        return selectItemDepartamento;
    }

    public void setSelectItemDepartamento(SelectItem[] selectItemDepartamento) {
        this.selectItemDepartamento = selectItemDepartamento;
    }

    public String getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(String idCiudad) {
        this.idCiudad = idCiudad;
    }

    public String getIdCentroCosto() {
        return idCentroCosto;
    }

    public void setIdCentroCosto(String idCentroCosto) {
        this.idCentroCosto = idCentroCosto;
    }

    public String getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(String idDepartamento) {
        this.idDepartamento = idDepartamento;
    }
    private String id;
    private String codigo;
    private String stringCiudad;
    private String stringCentroCosto;
    private String stringDepartamento;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getStringCiudad() {
        return stringCiudad;
    }

    public void setStringCiudad(String stringCiudad) {
        this.stringCiudad = stringCiudad;
    }

    public String getStringCentroCosto() {
        return stringCentroCosto;
    }

    public void setStringCentroCosto(String stringCentroCosto) {
        this.stringCentroCosto = stringCentroCosto;
    }

    public String getStringDepartamento() {
        return stringDepartamento;
    }

    public void setStringDepartamento(String stringDepartamento) {
        this.stringDepartamento = stringDepartamento;
    }
   
    public Boolean getAtriEditable() {
        return atriEditable;
    }

    public void setAtriEditable(Boolean atriEditable) {
        this.atriEditable = atriEditable;
    }
}
