package ec.com.centrosurcall.negocio.controladores;

import java.sql.*; 
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;
import javax.swing.*;

public class CtrInformix{
    String cmd = null;
    int rc;
    ResultSet resul;
    private Connection connection;

    public void cargarDriver() throws ClassNotFoundException{
        try{
            Class.forName("com.informix.jdbc.IfxDriver");
        }catch(ClassNotFoundException excepcion){
            JOptionPane.showMessageDialog(null, "ERROR: falló la carga del Informix JDBC driver. \n" + excepcion.getMessage(), "Atención", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void crearConexion(String nombreMaquina, String nombreBase, String nombreInstancia, String nombreUsuario, String passwordUsuario) throws SQLException{
        try{
            String cadenaConexion = "jdbc:informix-sqli://" + nombreMaquina + ":1504/" + nombreBase + ":INFORMIXSERVER=" + nombreInstancia + ";user=" + nombreUsuario + ";password=" + passwordUsuario;
            System.out.println(cadenaConexion);
            connection = DriverManager.getConnection(cadenaConexion);
        }catch(SQLException excepcion){
            JOptionPane.showMessageDialog(null, "ERROR: No se pudo establecer la conexión con la base. \n" + excepcion.getMessage(), "Atención", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public List reporte1(String var1, String var2, String var3) throws SQLException{
        try {
            String cadenaSQL = "select * from Resource";
            try (PreparedStatement pstmt = connection.prepareStatement(cadenaSQL); ResultSet objInformix = pstmt.executeQuery()) {
                while(objInformix.next()){
                    String resultado = objInformix.getString("resourceID") + "/"+
                        objInformix.getString("profileID") + "/"+
                        objInformix.getString("resourceLoginID") + "/"+
                        objInformix.getString("resourceName") +"/"+
                        objInformix.getString("resourceGroupID") +"/"+
                        objInformix.getString("resourceType") +"/"+
                        objInformix.getString("active")+"/"+
                        objInformix.getString("autoAvail")+"/"+
                        objInformix.getString("extension")+"/"+
                        objInformix.getString("assignedTeamID")+"/"+
                        objInformix.getString("orderInRG")+"/"+
                        objInformix.getString("dateInactive")+"/"+
                        objInformix.getString("resourceSkillMapID")+"/"+
                        objInformix.getString("resourceFirstName")+"/"+
                        objInformix.getString("resourceLastName")
                        ;
                    System.out.println(resultado);
                }
            }
            return null;
        }catch(SQLException excepcion){
            JOptionPane.showMessageDialog( null, "ERROR: fallo la ejecución de la sentencia. \n" + excepcion.getMessage(), "Atención", JOptionPane.INFORMATION_MESSAGE);
            return null;
        }
    }
    
    public List<ObjLlamadasEntrantes> reporteLlamadasEntrantes(String usuario, Date fechaDesde, Date fechaHasta, String var3) throws SQLException{
         List<ObjLlamadasEntrantes> listaObjetos = new ArrayList();
        
        String fDesde = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fechaDesde);
        String fHasta = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fechaHasta);
        usuario = usuario.toLowerCase();
        try {
            String cadenaSQL = " select a.resourcename, b.originatordn, b.startdatetime, b.enddatetime, b.connecttime " +
                "from ContactCallDetail b, Resource a " +
                "where b.startDateTime between '"+fDesde+"' and '"+fHasta+"'  " +
                "and a.ResourceId=b.destinationId " +
                "and b.destinationtype=1 and b.contacttype=1 " +
                "and a.resourceloginid='"+usuario+"' " +
                "order by b.startDateTime";
             System.out.println(cadenaSQL);
            try (PreparedStatement pstmt = connection.prepareStatement(cadenaSQL); ResultSet objInformix = pstmt.executeQuery()) {
                while(objInformix.next()){
                    ObjLlamadasEntrantes objeto = new ObjLlamadasEntrantes();
                    objeto.setAgente(objInformix.getString("resourcename"));
                    objeto.setNumTelefono(objInformix.getString("originatordn"));
                    objeto.setFechaDesde(objInformix.getString("startdatetime"));
                    objeto.setFechaHasta(objInformix.getString("enddatetime"));
                    objeto.setTiempo(objInformix.getString("connecttime"));
                    
                    listaObjetos.add(objeto);
                }
            }
            return listaObjetos;
        }catch(SQLException excepcion){
            JOptionPane.showMessageDialog( null, "ERROR: fallo la ejecución de la sentencia. \n" + excepcion.getMessage(), "Atención", JOptionPane.INFORMATION_MESSAGE);
            return null;
        }
   

    }

     public List<ObjEstados> reporteEstadosNoReady(String usuario, Date fechaDesde, Date fechaHasta, String var3) throws SQLException{
        List<ObjEstados> listaObjetos = new ArrayList();
          Calendar fTemp = Calendar.getInstance();
        fTemp.setTime(fechaDesde);
        fTemp.add(Calendar.HOUR, 5);
        fechaDesde = fTemp.getTime();
        fTemp.setTime(fechaHasta);
        fTemp.add(Calendar.HOUR, 5);
        fechaHasta = fTemp.getTime();
        String fDesde = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fechaDesde);
        String fHasta = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(fechaHasta);
        usuario = usuario.toLowerCase();
        try {
            //String cadenaSQL = "select * from AgentStateDetail b join Resource a on b.agentID=a.resourceID where b.eventDateTime>='"+fDesde+"' and b.eventDateTime<='"+fHasta+"' and a.resourceLoginID='"+usuario+"' order by a.resourceName, b.eventDateTime";
            String cadenaSQL = "select * from AgentStateDetail b join Resource a on b.agentID=a.resourceID where b.eventDateTime between '"+fDesde+"' and '"+fHasta+"' and (b.eventType=2 or b.eventType=3) and a.resourceLoginID='"+usuario+"' order by a.resourceName, b.eventDateTime";
            System.out.println(cadenaSQL);
            try (PreparedStatement pstmt = connection.prepareStatement(cadenaSQL); ResultSet objInformix = pstmt.executeQuery()) {
                while(objInformix.next()){
                    ObjEstados objeto = new ObjEstados();
                    objeto.setAgentID(objInformix.getString("resourceName"));
                    objeto.setEventDateTime(objInformix.getString("eventDateTime"));
                    objeto.setGmtOffset(objInformix.getString("gmtOffset"));
                    objeto.setEventType(objInformix.getString("eventType"));
                    objeto.setReasonCode(objInformix.getString("reasonCode"));
                    objeto.setProfileID(objInformix.getString("profileID"));
                    listaObjetos.add(objeto);
                }
            }
            return listaObjetos;
        }catch(SQLException excepcion){
            JOptionPane.showMessageDialog( null, "ERROR: fallo la ejecución de la sentencia. \n" + excepcion.getMessage(), "Atención", JOptionPane.INFORMATION_MESSAGE);
            return null;
        }
    }
    public List<ObjRazones> consultarRazones() throws SQLException{
        List<ObjRazones> listaObjetos = new ArrayList<>();
        try {                                                                         
            String cadenaSQL = "select * from EEMReasonCodeDescription";
            try (PreparedStatement pstmt = connection.prepareStatement(cadenaSQL); ResultSet objInformix = pstmt.executeQuery()) {
                while(objInformix.next()){
                    ObjRazones objeto = new ObjRazones();
                    objeto.setReasonCodeID(objInformix.getString("reasonCodeID"));
                    objeto.setReasonCodeDescription(objInformix.getString("reasonCodeDescription"));
                    listaObjetos.add(objeto);
                }
            }
            return listaObjetos;
        }catch(SQLException excepcion){
            JOptionPane.showMessageDialog( null, "ERROR: fallo la ejecución de la sentencia. \n" + excepcion.getMessage(), "Atención", JOptionPane.INFORMATION_MESSAGE);
            return null;
        }
    }
    
    public void ejecutarUpdate(String sentenciaPasada) throws SQLException{
        try{
            PreparedStatement sentenciaPreparada = null;
            sentenciaPreparada = connection.prepareStatement(sentenciaPasada, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            sentenciaPreparada.executeUpdate();
        }catch(SQLException excepcion){
            JOptionPane.showMessageDialog( null, "ERROR: fallo la ejecución de la sentencia. \n" + excepcion.getMessage(), "Atención", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}