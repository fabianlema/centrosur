package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.HistorialDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.DAO.TemporalDAO;
import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Historial;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.datos.modelo.RolUsuario;
import ec.com.centrosurcall.datos.modelo.RolUsuarioId;
import ec.com.centrosurcall.datos.modelo.Temporal;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.pop3.MsnFromPop3;
import ec.com.centrosurcall.reports.GenerarReporte;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.PrintStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public final class CtrReportesMsn
        implements Serializable {

    CtrSession session;
    private Date fechaA;
    private Date fechaB;
    private int encoEnvi;
    private int encoNoEnvi;
    private int noEncoEnvi;
    private int noEncoNoEnvi;
    private String idGrupo;
    private SelectItem[] selectItemGrupos;
    private List<Object> objReporte = new ArrayList();
    private List<Historial> objListHistorial;
    private List<Temporal> objListTemporal;
    HistorialDAO historialDAO = new HistorialDAO();
    ParametrosDAO paramDAO = new ParametrosDAO();
    TemporalDAO temporal = new TemporalDAO();
    Parametro svr = this.paramDAO.getParametroByAtributo("SERVERREPORT");
    Parametro reportPdf = this.paramDAO.getParametroByAtributo("REPORTPDF");
    Parametro reportXls = this.paramDAO.getParametroByAtributo("REPORTXLS");
    Parametro reportPdfA = this.paramDAO.getParametroByAtributo("REPORTPDFADM");
    Parametro reportXlsA = this.paramDAO.getParametroByAtributo("REPORTXLSADM");
    private int numeroMensajes;
    private boolean autorizado = false;
    private boolean tempBoolean = false;

    public CtrReportesMsn() {
        this.session = ((CtrSession) FacesUtils.getManagedBean("ctrSession"));
        ejecutarHilo();
        this.encoEnvi = 0;
        this.encoNoEnvi = 0;
        this.noEncoEnvi = 0;
        this.noEncoNoEnvi = 0;

        for (int j = 0; j < this.session.getRolUsuario().size(); j++) {
            if (((RolUsuario) this.session.getRolUsuario().get(j)).getId().getIdRol() == this.session.getAdministrador()) {
                this.autorizado = true;
                j = this.session.getRolUsuario().size();
            } else {
                this.autorizado = false;
            }
        }
        if (this.autorizado) {
            this.objListTemporal = this.temporal.getTemporal();
        } else {
            this.objListTemporal = this.temporal.getTemporalXUsuario(this.session.getUsuario().getId());
        }
        this.numeroMensajes = this.objListTemporal.size();

        actualizarReporte();
    }

    public void ejecutarHilo() {
        new Thread() {
            @Override
            public void run() {
                try {
                    MsnFromPop3 msnFromPop3 = new MsnFromPop3(session);
                    msnFromPop3.getNoValidos();
                    System.out.println("Ejecuta Hilo");
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        }.start();
    }    

    public void cargarGrupos() {
        this.objListHistorial = this.historialDAO.getHistorialSI();
        int cont;
        if ((this.objListHistorial != null) && (this.objListHistorial.size() > 0)) {
            this.selectItemGrupos = new SelectItem[this.objListHistorial.size() + 1];
            this.selectItemGrupos[0] = new SelectItem("0", "--SELECCIONE--", "--SELECCIONE--");
            cont = 1;
            for (Historial cl : this.objListHistorial) {
                this.selectItemGrupos[cont] = new SelectItem(cl.getGrupo(), cl.getGrupo(), cl.getGrupo());
                cont++;
            }
        }
    }

    public void actualizarReporte() {
        this.objReporte.clear();
        this.encoEnvi = 0;
        this.encoNoEnvi = 0;
        this.noEncoEnvi = 0;
        this.noEncoNoEnvi = 0;
        if ((this.fechaA != null) && (this.fechaB != null)) {
            Consultas consulta = new Consultas();
            String desde = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.fechaA);
            String hasta = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.fechaB);

            String cadenaSQL = "";
            if (this.autorizado) {
                cadenaSQL = "select fallo, estado, count(*) from historial where fecha >= '" + desde + "' and fecha <= '" + hasta + "' and estado!=2 and fallo!=2 group by estado, fallo";
            } else {
                cadenaSQL = "select fallo, estado, count(*) from historial where fecha >= '" + desde + "' and fecha <= '" + hasta + "' and estado!=2 and fallo!=2 and idUsuario = " + this.session.getUsuario().getId() + " group by estado, fallo";
            }
            System.out.println(cadenaSQL);
            List objReporteTmp = consulta.getListSql(cadenaSQL);
            if ((objReporteTmp != null) && (objReporteTmp.size() > 0)) {
                for (int i = 0; i < objReporteTmp.size(); i++) {
                    Object[] temp = (Object[]) (Object[]) objReporteTmp.get(i);
                    if ((temp[0].toString().equals("1")) && (temp[1].toString().equals("0"))) {
                        this.encoEnvi = Integer.parseInt(temp[2].toString());
                    } else if ((temp[0].toString().equals("1")) && (temp[1].toString().equals("1"))) {
                        this.encoNoEnvi = Integer.parseInt(temp[2].toString());
                    } else if ((temp[0].toString().equals("0")) && (temp[1].toString().equals("0"))) {
                        this.noEncoEnvi = Integer.parseInt(temp[2].toString());
                    } else if ((temp[0].toString().equals("0")) && (temp[1].toString().equals("1"))) {
                        this.noEncoNoEnvi = Integer.parseInt(temp[2].toString());
                    }
                }
                String[] row1 = {"ENVIADOS", String.valueOf(this.encoEnvi)};
                String[] row2 = {"NO ENVIADOS", String.valueOf(this.encoNoEnvi)};

                String[] row5 = {"TOTAL", String.valueOf(this.encoEnvi + this.encoNoEnvi)};
                this.objReporte.add(row1);
                this.objReporte.add(row2);

                this.objReporte.add(row5);
            } else {
                String[] row1 = {"--", "--"};
                this.objReporte.add(row1);
            }
        } else {
            String[] row1 = {"--", "--"};
            this.objReporte.add(row1);
        }
    }

    public void cargarReporte(String tipo) {
        if ((this.fechaA != null) && (this.fechaB != null)) {
            GenerarReporte r = new GenerarReporte();
            Map parametros = new HashMap();

            parametros.put("fechaA", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.fechaA));
            parametros.put("fechaB", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.fechaB));
            if (!this.autorizado) {
                parametros.put("idUsuario", Integer.valueOf(this.session.getUsuario().getId()));
            }

            if (tipo.equals("1")) {
                if (this.autorizado) {
                    r.generaPdf("ReporteCSur2NA", parametros, "ReporteCSur2NA");
                } else {
                    r.generaPdf("ReporteCSur2N", parametros, "ReporteCSur2N");
                }
            } else if (this.autorizado) {
                r.generaXlsx("ReporteCSur2NExcelA", parametros, "ReporteCSur2NExcelA");
            } else {
                r.generaXlsx("ReporteCSur2NExcel", parametros, "ReporteCSur2NExcel");
            }
        }
    }

    public void detenerEnvio() {
        if (this.tempBoolean) {
            System.out.println("Deteneeeer");
            if (this.temporal.eliminarList(this.objListTemporal)) {
                this.numeroMensajes = 0;
            }

            this.tempBoolean = (!this.tempBoolean);
        } else {
            this.tempBoolean = (!this.tempBoolean);
        }
    }

    public Date getFechaA() {
        return this.fechaA;
    }

    public void setFechaA(Date fechaA) {
        this.fechaA = fechaA;
    }

    public Date getFechaB() {
        return this.fechaB;
    }

    public void setFechaB(Date fechaB) {
        this.fechaB = fechaB;
    }

    public String getIdGrupo() {
        return this.idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public SelectItem[] getSelectItemGrupos() {
        return this.selectItemGrupos;
    }

    public void setSelectItemGrupos(SelectItem[] selectItemGrupos) {
        this.selectItemGrupos = selectItemGrupos;
    }

    public List<Object> getObjReporte() {
        return this.objReporte;
    }

    public void setObjReporte(List<Object> objReporte) {
        this.objReporte = objReporte;
    }

    public int getEncoEnvi() {
        return this.encoEnvi;
    }

    public void setEncoEnvi(int encoEnvi) {
        this.encoEnvi = encoEnvi;
    }

    public int getEncoNoEnvi() {
        return this.encoNoEnvi;
    }

    public void setEncoNoEnvi(int encoNoEnvi) {
        this.encoNoEnvi = encoNoEnvi;
    }

    public int getNoEncoEnvi() {
        return this.noEncoEnvi;
    }

    public void setNoEncoEnvi(int noEncoEnvi) {
        this.noEncoEnvi = noEncoEnvi;
    }

    public int getNoEncoNoEnvi() {
        return this.noEncoNoEnvi;
    }

    public void setNoEncoNoEnvi(int noEncoNoEnvi) {
        this.noEncoNoEnvi = noEncoNoEnvi;
    }

    public int getNumeroMensajes() {
        return this.numeroMensajes;
    }

    public void setNumeroMensajes(int numeroMensajes) {
        this.numeroMensajes = numeroMensajes;
    }

    public boolean isAutorizado() {
        return this.autorizado;
    }

    public void setAutorizado(boolean autorizado) {
        this.autorizado = autorizado;
    }
}