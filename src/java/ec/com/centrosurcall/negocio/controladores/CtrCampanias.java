/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.CampaniaDAO;
import ec.com.centrosurcall.datos.modelo.Campania;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Fabian Lema
 */

@ManagedBean
@ViewScoped
public final class CtrCampanias extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Campania selCampania = new Campania();
    CampaniaDAO campaniaDAO = new CampaniaDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemCampanias;
    //SelectItem item = new SelectItem();

    public CtrCampanias() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarCampanias();
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoCampaniaEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar esta Razón?");
        confPop();
    }

    public void cargarCampanias() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Campania> listaCampania = campaniaDAO.getCampanias();
        if (listaCampania != null) {
            listaBaseObjetos.addAll(listaCampania);
            listaObjetos.addAll(listaBaseObjetos);            
//            int cont = 0;
//            //item.setLabel("RAIZ");
//            //item.setValue(0);            
//            selectItemCampanias = new SelectItem[listaCampania.size()];
//            //selectItemRazones[0] = item;
//            
//            for (Campania obj : listaCampania) {
//                selectItemCampanias[cont] = new SelectItem(String.valueOf(obj.getId()), obj.getNombre());
//                //if (cl.getId()==2) razonEquiv = cl; 
//                cont++;
//            }
        }
        
        
        /*item.setLabel("-RAIZ-");
        item.setValue(0);
        selectItemRazones = new SelectItem[1];
        selectItemRazones[0] = item;
        objRazon = consulta.getListHql(null, "Razon", "estado = 1 and not id = 1 order by nombre", null);
        if (objRazon != null && objRazon.size() > 0) {
            int cont = 1;
            selectItemRazones = new SelectItem[objRazon.size() + 1];
            selectItemRazones[0] = item;
            for (Object obj : objRazon) {
                Razon cl = (Razon) obj;
                selectItemRazones[cont] = new SelectItem(String.valueOf(cl.getId()), cl.getNombre());
                if (cl.getId()==2) razonEquiv = cl; 
                cont++;
            }
        }*/
    }

    public void cargoCampaniaEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Campania")) {
                if (((Campania) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selCampania = (Campania) session.getObjeto();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar la Campania");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            selCampania.setEstado(true);
            selCampania.setUsuario(parametroSession.getUsuario());
            exito = campaniaDAO.saveCampania(selCampania, 1);
            parametroSession.setEsNuevo(false);
        } else {
            selCampania.setUsuario(parametroSession.getUsuario());
            exito = campaniaDAO.saveCampania(selCampania, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Campania> respuesta = new ArrayList<Campania>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Campania cl = (Campania) obj;
                if (cl.getNombre().toUpperCase().contains(whereSql.toUpperCase()) || cl.getNombre().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
       // listaColumnas.put("id", "Id");
        listaColumnas.put("nombre", "Nombre");
        //listaColumnas.put("descripcion", "Descripción");
        //listaColumnas.put("codigoRazon","Código CentroSur");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionCampanias";
    }

    @Override
    public void auditoria() {
        try {
            selCampania = (Campania) objetoSeleccionado;
        } catch (Exception e) {
            selCampania.setId(0);
        }
        if (selCampania.getId() > 0) {
            creado = selCampania.getNombre();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarCampanias();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Campania> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Campania) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmCampanias";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmCampanias";
    }

    public void confPopEliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Campania> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = campaniaDAO.eliminarCampanias(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarCampanias();
        }
        return exito;
    }

    public List<Campania> sacarSeleccionado() {
        Campania selectCampania = new Campania();
        List<Campania> listadoSeleccionado = new ArrayList<Campania>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectCampania = (Campania) o;
            if (selectCampania.getSeleccionado()) {
                listadoSeleccionado.add(selectCampania);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Campania getSelCampania() {
        return selCampania;
    }

    public void setSelCampania(Campania selCampania) {
        this.selCampania = selCampania;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String nombre;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public SelectItem[] getSelectItemCampanias() {
        return selectItemCampanias;
    }

    public void setSelectItemCampanias(SelectItem[] selectItemCampanias) {
        this.selectItemCampanias = selectItemCampanias;
    }
}
