/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrParametros extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Parametro selParametros = new Parametro();
    ParametrosDAO parametrosDAO = new ParametrosDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemTipos;
    private Boolean atriEditable = false, esGuion = false;

    public CtrParametros() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarParametros();
        cargarTipos();
        if (parametroSession.getEsNuevo()) {
            atriEditable = false;
        }
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            atriEditable = true;
            cargoParametrosEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar este Parámetro?");
        confPop();
    }

    public void cargarParametros() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Parametro> listaParametros = parametrosDAO.getParametros();
        if (listaParametros != null) {
            listaBaseObjetos.addAll(listaParametros);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargarTipos() {
        selectItemTipos = new SelectItem[2];
        selectItemTipos[0] = new SelectItem("1", "Rango");
        selectItemTipos[1] = new SelectItem("2", "Porcentaje");
    }

    public void cargoParametrosEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Parametro")) {
                if (((Parametro) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selParametros = (Parametro) session.getObjeto();
            if("LDAP".equals(selParametros.getGrupo()) || "UCCX".equals(selParametros.getGrupo())){
                atriEditable=true;
            }else{
                atriEditable=false;
            }
            if("GUION".equals(selParametros.getAtributo())) esGuion = true;
            else esGuion = false;
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Parámetro");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            selParametros.setEstado(Boolean.TRUE);
            exito = parametrosDAO.saveParametro(selParametros, 1);
            parametroSession.setEsNuevo(false);
        } else {
            exito = parametrosDAO.saveParametro(selParametros, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Parametro> respuesta = new ArrayList<Parametro>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Parametro cl = (Parametro) obj;
                if (cl.getAtributo().toUpperCase().contains(whereSql.toUpperCase()) || cl.getValor().toUpperCase().contains(whereSql.toUpperCase()) || cl.getGrupo().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        listaColumnas.put("atributo", "Atributo");
        listaColumnas.put("valor", "Valor");
        listaColumnas.put("grupo", "Grupo");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionParametros";
    }

    @Override
    public void auditoria() {
        try {
            selParametros = (Parametro) objetoSeleccionado;
        } catch (Exception e) {
            selParametros.setId(0);
        }
        if (selParametros.getId() > 0) {
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarParametros();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Parametro> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Parametro) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmParametros";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmParametros";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Parametro> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = parametrosDAO.eliminarParametros(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarParametros();
        }
        return exito;
    }

    public List<Parametro> sacarSeleccionado() {
        Parametro selectParametros = new Parametro();
        List<Parametro> listadoSeleccionado = new ArrayList<Parametro>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectParametros = (Parametro) o;
            if (selectParametros.getSeleccionado()) {
                listadoSeleccionado.add(selectParametros);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Parametro getSelParametros() {
        return selParametros;
    }

    public void setSelParametros(Parametro selParametros) {
        this.selParametros = selParametros;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }

    public SelectItem[] getSelectItemTipos() {
        return selectItemTipos;
    }

    public void setSelectItemTipos(SelectItem[] selectItemTipos) {
        this.selectItemTipos = selectItemTipos;
    }
    private String id;
    private String atributo;
    private String valor;
    private String grupo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAtributo() {
        return atributo;
    }

    public void setAtributo(String atributo) {
        this.atributo = atributo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public Boolean getAtriEditable() {
        return atriEditable;
    }

    public void setAtriEditable(Boolean atriEditable) {
        this.atriEditable = atriEditable;
    }

    public Boolean getEsGuion() {
        return esGuion;
    }

    public void setEsGuion(Boolean esGuion) {
        this.esGuion = esGuion;
    }
}
