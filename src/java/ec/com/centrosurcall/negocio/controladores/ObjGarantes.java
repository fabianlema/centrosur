/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author andresmoli
 */
public class ObjGarantes {
    private String cCuenta;
    private String nombreGarante;
    private String direccion;
    private String nombreConyuge;
    private String telefono;
    private String relacion;

    /**
     * @return the cCuenta
     */
    public String getcCuenta() {
        return cCuenta;
    }

    /**
     * @param cCuenta the cCuenta to set
     */
    public void setcCuenta(String cCuenta) {
        this.cCuenta = cCuenta;
    }

    /**
     * @return the nombreGarante
     */
    public String getNombreGarante() {
        return nombreGarante;
    }

    /**
     * @param nombreGarante the nombreGarante to set
     */
    public void setNombreGarante(String nombreGarante) {
        this.nombreGarante = nombreGarante;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the nombreConyuge
     */
    public String getNombreConyuge() {
        return nombreConyuge;
    }

    /**
     * @param nombreConyuge the nombreConyuge to set
     */
    public void setNombreConyuge(String nombreConyuge) {
        this.nombreConyuge = nombreConyuge;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the relacion
     */
    public String getRelacion() {
        return relacion;
    }

    /**
     * @param relacion the relacion to set
     */
    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }
}
