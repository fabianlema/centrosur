/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author Paul
 */
public class ObjRazones {
    private String reasonCodeID;
    private String reasonCodeDescription;
    
    public void objRazones(){
        
    }
    
    public void objRazones(String reasonCodeID, String reasonCodeDescription){
        reasonCodeID = this.getReasonCodeID();
        reasonCodeDescription = this.getReasonCodeDescription();
    }

    /**
     * @return the reasonCodeID
     */
    public String getReasonCodeID() {
        return reasonCodeID;
    }

    /**
     * @param reasonCodeID the reasonCodeID to set
     */
    public void setReasonCodeID(String reasonCodeID) {
        this.reasonCodeID = reasonCodeID;
    }

    /**
     * @return the reasonCodeDescription
     */
    public String getReasonCodeDescription() {
        return reasonCodeDescription;
    }

    /**
     * @param reasonCodeDescription the reasonCodeDescription to set
     */
    public void setReasonCodeDescription(String reasonCodeDescription) {
        this.reasonCodeDescription = reasonCodeDescription;
    }
    
    
}
