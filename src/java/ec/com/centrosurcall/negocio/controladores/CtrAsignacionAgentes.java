/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;



import ec.com.centrosurcall.datos.DAO.UsuarioDisponibleDAO;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.datos.modelo.UsuarioDisponible;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;
/**
 *
 * @author carlosguaman65
 */
@ManagedBean
@ViewScoped
public final class CtrAsignacionAgentes  extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    private String estado;

    private List<Object> objAgentesAsignados;
    private List<Object> objAgentes;
    


    private CtrSession session;
    
    @ManagedProperty(value = "#{asignacionAgentesDAO.agentesList}")
    private List<Usuario> agentes;
     
    private List<Usuario> listadoAgentesAsignados;
     private List<Usuario> agentesAsignados;
   
    
    
    public CtrAsignacionAgentes(){
         session = (CtrSession) FacesUtils.getManagedBean("ctrSession");  
         System.err.println("Constructor");
    }
   
    
      public void onSelectAgenteItem(ValueChangeEvent event){ 
         listadoAgentesAsignados =((List<Usuario>) event.getNewValue());

      }
    
       
    public List<Usuario> getListaAgentesAsignados(){ 
        System.err.println("Obtener la lista de comparacion");
        List<Usuario> listaAgentesAsignados = new ArrayList<Usuario>();    
        objAgentesAsignados = consulta.getListHql(null, "UsuarioDisponible", "estado = 1 ", null);
        if (objAgentesAsignados != null && objAgentesAsignados.size() > 0) {     
          for (Object obj : objAgentesAsignados) {
                UsuarioDisponible agentesAsignados = (UsuarioDisponible) obj;
                Usuario usuario = new Usuario();        
                usuario.setId(agentesAsignados.getUsuario().getId());
                objAgentes= consulta.getListHql(null, "Usuario", "id = "+ usuario.getId(), null);
                listaAgentesAsignados.add(usuario);
              //  System.err.println("000000000 "+ zona.getId());
          }
        }
        return  listaAgentesAsignados;
    }
    

     public void actualizarAgentesAsignados(){
   
        UsuarioDisponibleDAO gestorAgentes = new UsuarioDisponibleDAO();
        List<Usuario> listadoAgentesActuales = getListaAgentesAsignados();
        for (Usuario usuario : listadoAgentesAsignados){
            System.err.println("tttttttt >" + usuario.getNombre());
            for (Usuario usuarioExistente : listadoAgentesActuales){
                
                if (usuarioExistente.getId() != usuario.getId()){
                   
                    //ZonaAfectada zonaAfectada = ;
                    gestorAgentes.saveUsuarioDisponible(crearUsuarioDisponible(usuario), 1);
                     System.err.println("Zona cambiada a Zona Afectada ");
                }
            }
       }
        listadoAgentesAsignados.clear();
    }
     
    public UsuarioDisponible crearUsuarioDisponible(Usuario usuario){
    
         UsuarioDisponible usuarioDisponible= new UsuarioDisponible();
                    usuarioDisponible.setUsuario(usuario);
                    usuarioDisponible.setEstado(false);
       
        return usuarioDisponible;
    } 
     
     
   @PostConstruct
    public void resetearVariables(){
       
     //  zonas.clear();
    }
    

    public List<Object> getObjAgentesAsignados() {
        return objAgentesAsignados;
    }

    public void setObjAgentesAsignados(List<Object> objZonasAfectadas) {
        this.objAgentesAsignados = objZonasAfectadas;
    }



    public List<Usuario> getAgentesAsignados() {
        System.err.println("llamada a zonas afectadas");
        agentesAsignados = new ArrayList<Usuario>();
        List<Usuario> comp = getListaAgentesAsignados();

            for (Usuario s : agentes) {
                for (Usuario sel : comp) {
                    if (sel.getId() == s.getId()) {
                        agentesAsignados.add(s);
                        break;
                    }
                }
            }  
        return agentesAsignados;
    }

    public void setAgentesAsignados(List<Usuario> agentesAsignados) {
         System.err.println("insertar  zonas afectadas");
         for (Usuario za : agentesAsignados){
             System.err.println("ZAfectada: "+za.getNombre());
         }
         
         System.err.println("_________________________________");
        this.agentesAsignados = agentesAsignados;
    }
    
    public List<Usuario> getAgentes() {
        
         System.err.println("llamada a zonas");
        return agentes;
    }

    public void setAgentes(List<Usuario> agentes) {
         System.err.println("insertar zonas");
        for (Usuario za : agentes){
             System.err.println("Zonas: "+ za.getNombre());
         }
        
       System.err.println("_________________________________");
       
        
        this.agentes = agentes;
    }

    
    public List<Object> getObjAgentes() {
        return objAgentes;
    }

    public void setObjAgentes(List<Object> objAgentes) {
        this.objAgentes = objAgentes;
    }

    public List<Usuario> getListadoAgentesAsignados() {
        return listadoAgentesAsignados;
    }

    public void setListadoAgentesAsignados(List<Usuario> listadoAgentesAsignados) {
        this.listadoAgentesAsignados = listadoAgentesAsignados;
    }


    
    
    
    

    @Override
    public String nuevo() {
        return null;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String editar() {
        return null;
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimir() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String refrescar() {
        return null;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void auditoria() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String regresar() {
        return null;
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cargarColumnasGrupo() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void buscarWsql() {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
