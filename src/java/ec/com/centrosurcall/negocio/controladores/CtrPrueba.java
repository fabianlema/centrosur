/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author carlosguaman65
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.RolesDAO;
import ec.com.centrosurcall.datos.DAO.ModulosDAO;
import ec.com.centrosurcall.datos.DAO.ZonaDAO;
import ec.com.centrosurcall.datos.modelo.DetalleMod;
import ec.com.centrosurcall.datos.modelo.Modulo;
import ec.com.centrosurcall.datos.modelo.ModuloId;
import ec.com.centrosurcall.datos.modelo.Rol;
import ec.com.centrosurcall.datos.modelo.Zona;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrPrueba extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Zona setZona = new Zona();
    ZonaDAO zonasDAO = new ZonaDAO();
    //ZonaAfectadaDAO zonaAfectadaDAO = new ZonaAfectadaDAO();
    String opcionBoton = "";
    private List<Object> objModulo;
    private List<Zona> objModulo2 = new ArrayList<Zona>();
    private SelectItem selectItemModulos[];

    public CtrPrueba() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarRoles();
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoRolEditar();
        }
        cargarModulos();
        setMensajeConfirmacion("Seguro desea grabar este Rol?");
        confPop();
    }

    public void cargarRoles() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Zona> listaZona = zonasDAO.getZonasList();
        if (listaZona != null) {
            listaBaseObjetos.addAll(listaZona);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargarModulos() {
        objModulo = consulta.getListHql(null, "Zona", "estado = 1 ", null);
        if (objModulo != null && objModulo.size() > 0) {
            int cont = 0;
            selectItemModulos = new SelectItem[objModulo.size()];
            for (Object obj : objModulo) {
                Modulo cl = (Modulo) obj;
                selectItemModulos[cont] = new SelectItem(cl.getId().getIdPadre() + "" + cl.getId().getIdHijo(), cl.getNombre(), String.valueOf(cl.getId().getIdPadre()));
                cont++;
            }
        }
    }

    public void cargoRolEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Rol")) {
                if (((Rol) session.getObjeto()).getId() > 0) {
                    Integer idRol = ((Rol) session.getObjeto()).getId();
                    objModulo2 = consulta.getListSql("﻿select *" +
                                                " from dbo.zona zon" +
                                                " where zon.id not in  ( select  zona.id" +
                                                " from dbo.zona  zona, dbo.zona_afectada zonaf" +
                                                " where zona.id = zonaf.id_fk_zonas)");
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            setZona = (Zona) session.getObjeto();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Rol");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
       return true;
    }

    @Override
    public void buscarWsql() {
        List<Rol> respuesta = new ArrayList<Rol>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Rol cl = (Rol) obj;
                if (cl.getNombreRol().toUpperCase().contains(whereSql.toUpperCase()) || cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
      //  listaColumnas.put("id", "Id");
        listaColumnas.put("nombreRol", "Nombre");
        listaColumnas.put("descripcion", "Descripción");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "frmAdministracionRoles";
    }

    @Override
    public void auditoria() {
        try {
            setZona = (Zona) objetoSeleccionado;
        } catch (Exception e) {
            setZona.setId(0);
        }
        if (setZona.getId() > 0) {
            creado = setZona.getDescripcion();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarRoles();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        return "";
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmRoles";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
            return true;
    }



    public Zona getSelRol() {
        return setZona;
    }

    public void setSelRol(Zona setZona) {
        this.setZona = setZona;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String nombreRol;
    private String descripcion;
    private String estado;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Object> getObjModulo() {
        return objModulo;
    }

    public void setObjModulo(List<Object> objModulo) {
        this.objModulo = objModulo;
    }

    public SelectItem[] getSelectItemModulos() {
        return selectItemModulos;
    }

    public void setSelectItemModulos(SelectItem[] selectItemModulos) {
        this.selectItemModulos = selectItemModulos;
    }

    public List<Zona> getObjModulo2() {
        return objModulo2;
    }

    public void setObjModulo2(List<Zona> objModulo2) {
        this.objModulo2 = objModulo2;
    }
}
