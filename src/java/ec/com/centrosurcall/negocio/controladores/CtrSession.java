/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.call.MakeCall1;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.modelo.Cliente;
import ec.com.centrosurcall.datos.modelo.ClienteId;
import ec.com.centrosurcall.datos.modelo.DetalleMod;
import ec.com.centrosurcall.datos.modelo.Evento;
import ec.com.centrosurcall.datos.modelo.LlamadaEntrante;
import ec.com.centrosurcall.datos.modelo.Parametro;
import ec.com.centrosurcall.datos.modelo.Rol;
import ec.com.centrosurcall.datos.modelo.RolUsuario;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.datos.modelo.UsuarioSkill;
import ec.com.centrosurcall.smtp.Smtp2sms;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
//import java.util.logging.Level;
//import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
//import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author TaurusTech
 */
@ManagedBean
@SessionScoped
public class CtrSession implements Serializable {

    private Object objeto;
    private Object objeto2;// objeto Auxiliar 
    private Object objeto3;// objeto Auxiliar 
    private Boolean esEditar = false;
    private Boolean esNuevo = false;
    private Boolean seleccionado = false;
    //para el logueo
    private String descripcion = "CentroSurCM";
    private Integer menuId;
    private String pagina = "index.xhtml";
    // para el paginator
    private int numPaginas = 1;
    private int numTotalPaginas = 1;
    private int maximoFila = 10;
    private boolean disableProximaPagina = false;
    private boolean disableAnteriorPagina = false;
    private boolean disablePaginaInicio = false;
    private boolean disablePaginaFin = false;
    private boolean chargeInfoCliente = true;
    private boolean isSessionOn = true;
    //Mensaje Pop
    private String mensajePopM;
    private boolean showPopM = false;
    private boolean isNoContestado = false;
    // E --> error, I --> información, A --> Advertencia
    private String imagenPopM;
    private String tituloPopM;
    private String tituloSeccion = "";
    private String paginaActual;
    private boolean showPopD = false;
    protected String boton = "btnGrabar";
    private String tabla;
    private Usuario usuario;
    private String numeroCliente;
    private ClienteId clicodLlamar = null;
    private String numTelefonoLlamar = "";
    private String agenteCobranza = "";
    private String guion = "";
    private int agente1;
    private int agente2;
    private int administrador;
    private int numeroLlamadas = 0;
    private List<UsuarioSkill> usuarioSkill;
    private List<RolUsuario> rolUsuario;
    private List<DetalleMod> moduloUsuario;
    private List<String> listLlamadasRelizadas = null;
    private Cliente clienteLlamar;
    private MakeCall1 objLlamar;
    private Parametro prefijoucxx;
    private String codigoivr;
    private String etiquetaTiposLlamada;
    private Set selectedEventoProcesadoList = new HashSet(0);
    private Boolean estadoVista = true;
    private Boolean stateSelected = false;
    private String observacion = null;
    private Evento eventoSeleccionado;
    private LlamadaEntrante llamadaEntrante;
    private Boolean estadoFinal;
    private int numeroCorreo;
    private String dominio2n;
    private String usuario2n;
    private String contrasenia2n;
    private String ip2n;
    private ParametrosDAO parametro = new ParametrosDAO();
    private Smtp2sms smtp2sms;

    public CtrSession() {
        showPopM = false;
        showPopD = false;
        //ControlCorreo control= new ControlCorreo();
        //    control.start();

        // new MailScheduler();
    }

    public Boolean getEstadoVista() {
        return estadoVista;
    }

    public void setEstadoVista(Boolean estadoVista) {
        this.estadoVista = estadoVista;
    }

    public void inicializarClickCall() {
        Parametro svr = parametro.getParametroByAtributo("SERVERUCCX");
        Parametro usuarioucxx = parametro.getParametroByAtributo("USUARIOUCCX");
        Parametro pwdusuariouccx = parametro.getParametroByAtributo("PWDUSUARIOUCCX");
        prefijoucxx = parametro.getParametroByAtributo("PREFIJOUCCX");
        objLlamar = new MakeCall1(svr.getValor(), usuarioucxx.getValor(), pwdusuariouccx.getValor());
        guion = parametro.getParametroByAtributo("GUION").getValor();
        System.out.println("Click to call inicializado");
    }

    public void inicializarMSN2n() {
        System.out.println("CtrSession");
        this.showPopM = false;
        this.showPopD = false;
        this.dominio2n = this.parametro.getParametroByAtributo("DOMINIO").getValor();
        this.usuario2n = this.parametro.getParametroByAtributo("USUARIO").getValor();
        this.contrasenia2n = this.parametro.getParametroByAtributo("CONTRASENIA").getValor();
        this.ip2n = this.parametro.getParametroByAtributo("IP2N").getValor();
        this.smtp2sms = new Smtp2sms();
    }

    public void cerrarSesion() {
        String url = "/CentroSurCM/faces/index.xhtml";
        try {

            FacesContext faces = FacesContext.getCurrentInstance();
            ExternalContext context = faces.getExternalContext();
            final HttpServletRequest request = (HttpServletRequest) context.getRequest();
//     request.getSession(false).invalidate();            
            //context.redirect(url);
            context.redirect(url);
            faces.responseComplete();

            Runtime r = Runtime.getRuntime();
            r.gc();
        } catch (Exception e) {
            System.out.println("Error en redirect");
        }
    }

    public void irURL(String pagina1) {
        try {
//            FacesContext faces = FacesContext.getCurrentInstance();
//            ExternalContext context = faces.getExternalContext();
//            final HttpServletRequest request = (HttpServletRequest) context.getRequest();
//            context.redirect(url);
//            faces.responseComplete();
            FacesContext faces = FacesContext.getCurrentInstance();
            ExternalContext context = faces.getExternalContext();
            String ctxPath =
                    ((ServletContext) context.getContext()).getContextPath();
            context.getRequest();
            String url = ctxPath + pagina1;
            context.redirect(url);
            faces.responseComplete();
        } catch (Exception e) {
        }
    }

    public void setMensajes(char tipo, String mensaje) {
        showPopM = true;
        mensajePopM = mensaje;
        if (tipo == 'E') {
            imagenPopM = "../resources/Imagenes/iconError.png";
            tituloPopM = "Error";
        } else if (tipo == 'I') {
            imagenPopM = "../resources/Imagenes/iconInformation.png";
            tituloPopM = "Información";
        } else if (tipo == 'A') {
            imagenPopM = "../resources/Imagenes/iconWarning.png";
            tituloPopM = "Advertencia";
        }
    }

    public void cerrarPopMensajes() {
        showPopM = false;
    }

    public int getNumTotalPaginas() {
        return numTotalPaginas;
    }

    public void setNumTotalPaginas(int numTotalPaginas) {
        this.numTotalPaginas = numTotalPaginas;
    }

    public int getMaximoFila() {
        return maximoFila;
    }

    public void setMaximoFila(int maximoFila) {
        this.maximoFila = maximoFila;
    }

    public boolean isDisableAnteriorPagina() {
        if (numPaginas > 1) {
            disableAnteriorPagina = false;
        } else {
            disableAnteriorPagina = true;
        }
        return disableAnteriorPagina;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setDisableAnteriorPagina(boolean disableAnteriorPagina) {
        this.disableAnteriorPagina = disableAnteriorPagina;
    }

    public boolean isDisableProximaPagina() {
        if (numTotalPaginas > 1) {
            if (numTotalPaginas == numPaginas) {
                disableProximaPagina = true;
            } else {
                disableProximaPagina = false;
            }

        } else {
            disableProximaPagina = true;
        }
        return disableProximaPagina;
    }

    public void setDisableProximaPagina(boolean disableProximaPagina) {
        this.disableProximaPagina = disableProximaPagina;
    }

    public boolean isDisablePaginaFin() {
        if (numPaginas < numTotalPaginas) {
            disablePaginaFin = false;
        } else {
            disablePaginaFin = true;
        }
        return disablePaginaFin;
    }

    public void setDisablePaginaFin(boolean disablePaginaFin) {
        this.disablePaginaFin = disablePaginaFin;
    }

    public boolean isDisablePaginaInicio() {
        if (numPaginas > 1) {
            disablePaginaInicio = false;
        } else {
            disablePaginaInicio = true;
        }
        return disablePaginaInicio;
    }

    public void setDisablePaginaInicio(boolean disablePaginaInicio) {
        this.disablePaginaInicio = disablePaginaInicio;
    }

    //***
    public String getPagina() {
        return pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    public Object getObjeto() {
        return objeto;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

    public Boolean getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(Boolean seleccionado) {
        this.seleccionado = seleccionado;
    }

    public int getNumPaginas() {
        return numPaginas;
    }

    public void setNumPaginas(int numPaginas) {
        this.numPaginas = numPaginas;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getTituloSeccion() {
        return tituloSeccion;
    }

    public void setTituloSeccion(String tituloSeccion) {
        this.tituloSeccion = tituloSeccion;
    }

    public Object getObjeto2() {
        return objeto2;
    }

    public void setObjeto2(Object objeto2) {
        this.objeto2 = objeto2;
    }

    public Boolean getEsEditar() {
        return esEditar;
    }

    public void setEsEditar(Boolean esEditar) {
        this.esEditar = esEditar;
    }

    public Object getObjeto3() {
        return objeto3;
    }

    public void setObjeto3(Object objeto3) {
        this.objeto3 = objeto3;
    }

    public String getImagenPopM() {
        return imagenPopM;
    }

    public void setImagenPopM(String imagenPopM) {
        this.imagenPopM = imagenPopM;
    }

    public String getMensajePopM() {
        return mensajePopM;
    }

    public void setMensajePopM(String mensajePopM) {
        this.mensajePopM = mensajePopM;
    }

    public boolean isShowPopM() {
        return showPopM;
    }

    public void setShowPopM(boolean showPopM) {
        this.showPopM = showPopM;
    }

    public String getTituloPopM() {
        return tituloPopM;
    }

    public void setTituloPopM(String tituloPopM) {
        this.tituloPopM = tituloPopM;
    }

    public String getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(String paginaActual) {
        this.paginaActual = paginaActual;
    }

    public Boolean getEsNuevo() {
        return esNuevo;
    }

    public void setEsNuevo(Boolean esNuevo) {
        this.esNuevo = esNuevo;
    }

    public boolean isShowPopD() {
        return showPopD;
    }

    public Evento getEventoSeleccionado() {
        return eventoSeleccionado;
    }

    public void setEventoSeleccionado(Evento eventoSeleccionado) {
        this.eventoSeleccionado = eventoSeleccionado;
    }

    public LlamadaEntrante getLlamadaEntrante() {
        return llamadaEntrante;
    }

    public void setLlamadaEntrante(LlamadaEntrante llamadaEntrante) {
        this.llamadaEntrante = llamadaEntrante;
    }

    public Boolean getEstadoFinal() {
        return estadoFinal;
    }

    public void setEstadoFinal(Boolean estadoFinal) {
        this.estadoFinal = estadoFinal;
    }

    public void setShowPopD(boolean showPopD) {
        this.showPopD = showPopD;
    }

    public String getBoton() {
        return boton;
    }

    public void setBoton(String boton) {
        this.boton = boton;
    }

    public String getTabla() {
        return tabla;
    }

    public Boolean getStateSelected() {
        return stateSelected;
    }

    public void setStateSelected(Boolean stateSelected) {
        this.stateSelected = stateSelected;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }
    private boolean bandEditar;

    public boolean isBandEditar() {
        return bandEditar;
    }

    public void setBandEditar(boolean bandEditar) {
        this.bandEditar = bandEditar;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<UsuarioSkill> getUsuarioSkill() {
        return usuarioSkill;
    }

    public void setUsuarioSkill(List<UsuarioSkill> usuarioSkill) {
        this.usuarioSkill = usuarioSkill;
    }

    public String getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public int getNumeroLlamadas() {
        return numeroLlamadas;
    }

    public void setNumeroLlamadas(int numeroLlamadas) {
        this.numeroLlamadas = numeroLlamadas;
    }

    public ClienteId getClicodLlamar() {
        return clicodLlamar;
    }

    public void setClicodLlamar(ClienteId clicodLlamar) {
        this.clicodLlamar = clicodLlamar;
    }

    public String getNumTelefonoLlamar() {
        return numTelefonoLlamar;
    }

    public void setNumTelefonoLlamar(String numTelefonoLlamar) {
        this.numTelefonoLlamar = numTelefonoLlamar;
    }

    public void setChargeInfoCliente(boolean chargeInfoCliente) {
        this.chargeInfoCliente = chargeInfoCliente;
    }

    public boolean isChargeInfoCliente() {
        return this.chargeInfoCliente;
    }

    public List<RolUsuario> getRolUsuario() {
        return rolUsuario;
    }

    public void setRolUsuario(List<RolUsuario> rolUsuario) {
        this.rolUsuario = rolUsuario;
    }

    public List<String> getListLlamadasRelizadas() {
        return listLlamadasRelizadas;
    }

    public void setListLlamadasRelizadas(List<String> listLlamadasRelizadas) {
        this.listLlamadasRelizadas = listLlamadasRelizadas;
    }

    public int getAgente1() {
        return agente1;
    }

    public void setAgente1(int agente1) {
        this.agente1 = agente1;
    }

    public int getAgente2() {
        return agente2;
    }

    public void setAgente2(int agente2) {
        this.agente2 = agente2;
    }

    public void setAgentes(List<Rol> agentes) {
        setAdministrador(((Rol)agentes.get(0)).getId());
        setAgente1(agentes.get(1).getId());
        setAgente2(agentes.get(2).getId());
    }

    public boolean isIsSessionOn() {
        return isSessionOn;
    }

    public void setIsSessionOn(boolean isSessionOn) {
        this.isSessionOn = isSessionOn;
    }

    public void closeSession() {
        objeto = null;
        objeto2 = null;
        objeto3 = null;
        esEditar = false;
        esNuevo = false;
        seleccionado = false;
        descripcion = "CentroSurCM";
        menuId = 0;
        pagina = "index.xhtml";
        numPaginas = 1;
        numTotalPaginas = 1;
        maximoFila = 10;
        disableProximaPagina = false;
        disableAnteriorPagina = false;
        disablePaginaInicio = false;
        disablePaginaFin = false;
        chargeInfoCliente = true;
        mensajePopM = "";
        showPopM = false;
        imagenPopM = "";
        tituloPopM = "";
        tituloSeccion = "";
        paginaActual = "";
        showPopD = false;
        boton = "btnGrabar";
        tabla = "";
        usuario = null;
        numeroCliente = "";
        clicodLlamar = null;
        setAgenteCobranza("");
        usuarioSkill = null;
        rolUsuario = null;

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = request.getSession(true);
        //this.parametroSession = (CtrSession) FacesContext.getCurrentInstance().getELContext().getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(),null, "ctrSession");
        session.setAttribute("usuarioId", null);
        objLlamar.closeSession();
        //session.getAttribute("usuarioId") = null;
    }

    /**
     * @return the moduloUsuario
     */
    public List<DetalleMod> getModuloUsuario() {
        return moduloUsuario;
    }

    /**
     * @param moduloUsuario the moduloUsuario to set
     */
    public void setModuloUsuario(List<DetalleMod> moduloUsuario) {
        this.moduloUsuario = moduloUsuario;
    }

    public Cliente getClienteLlamar() {
        return clienteLlamar;
    }

    public void setClienteLlamar(Cliente clienteLlamar) {
        this.clienteLlamar = clienteLlamar;
    }

    public int getNumeroCorreo() {
        return numeroCorreo;
    }

    public void setNumeroCorreo(int numeroCorreo) {
        this.numeroCorreo = numeroCorreo;
    }

    public String getAgenteCobranza() {
        return agenteCobranza;
    }

    public void setAgenteCobranza(String agenteCobranza) {
        this.agenteCobranza = agenteCobranza;
    }

    public MakeCall1 getObjLlamar() {
        return objLlamar;
    }

    public void setObjLlamar(MakeCall1 objLlamar) {
        this.objLlamar = objLlamar;
    }

    public Parametro getPrefijoucxx() {
        return prefijoucxx;
    }

    public void setPrefijoucxx(Parametro prefijoucxx) {
        this.prefijoucxx = prefijoucxx;
    }

    public boolean isIsNoContestado() {
        return isNoContestado;
    }

    public void setIsNoContestado(boolean isNoContestado) {
        this.isNoContestado = isNoContestado;
    }

    public String getGuion() {
        return guion;
    }

    public void setGuion(String guion) {
        this.guion = guion;
    }

    public String getCodigoivr() {
        return codigoivr;
    }

    public void setCodigoivr(String codigoivr) {
        this.codigoivr = codigoivr;
    }

    public Set getSelectedEventoProcesadoList() {
        return selectedEventoProcesadoList;
    }

    public void setSelectedEventoProcesadoList(Set selectedEventoProcesadoList) {
        this.selectedEventoProcesadoList = selectedEventoProcesadoList;
    }

    public String getEtiquetaTiposLlamada() {
        return etiquetaTiposLlamada;
    }

    public void setEtiquetaTiposLlamada(String etiquetaTiposLlamada) {
        this.etiquetaTiposLlamada = etiquetaTiposLlamada;
    }

    public String getDominio2n() {
        return this.dominio2n;
    }

    public void setDominio2n(String dominio2n) {
        this.dominio2n = dominio2n;
    }

    public String getUsuario2n() {
        return this.usuario2n;
    }

    public void setUsuario2n(String usuario2n) {
        this.usuario2n = usuario2n;
    }

    public String getContrasenia2n() {
        return this.contrasenia2n;
    }

    public void setContrasenia2n(String contrasenia2n) {
        this.contrasenia2n = contrasenia2n;
    }

    public String getIp2n() {
        return this.ip2n;
    }

    public void setIp2n(String ip2n) {
        this.ip2n = ip2n;
    }

    public Smtp2sms getSmtp2sms() {
        return this.smtp2sms;
    }

    public void setSmtp2sms(Smtp2sms smtp2sms) {
        this.smtp2sms = smtp2sms;
    }

    public int getAdministrador() {
        return this.administrador;
    }

    public void setAdministrador(int administrador) {
        this.administrador = administrador;
    }
}
/*class ControlCorreo extends Thread{
    
    private String host= "";
    private String username="";
    private String password="";
    private String provider="";
        
    private String rutaImagen ="";
    private  String remitente ="";
    private  String para ="";
    private  String asunto ="";
    private  String  contenido ="";
    private Boolean controlBandeja=true;
     private List <String>  listadoImagenes = new ArrayList();
   
    
     Session session;
    CorreoDAO objCorreo = new CorreoDAO();
   
    public ControlCorreo(){
     CredencialesCorreo credenciales = objCorreo.getCredencialesByID(1);
     
     host=  credenciales.getHost();
     username=credenciales.getUsername();
     password= credenciales.getPassword();
     provider= credenciales.getProvider();
    
    }
    
    public void run(){
        while(true){
            try {
               descargarCorreo();
                Thread.sleep(49999);
            } catch (InterruptedException ex) {
                System.err.println("Error Hilo");
                ex.printStackTrace();
            }
        }
    }

    public void descargarCorreo() {

    //      List <BandejaEntrada> correos = objCorreo.getCorreoByEstado();
          //  System.err.println(">>>>>>>>> Revisando nueva correspondecia");
            autenticarEntidad();
            Store store;
           
              try {
                  store = session.getStore(provider);
                  store.connect(host, username, password);
                  Folder emailFolder = store.getFolder("INBOX");
                  emailFolder.open(Folder.READ_WRITE);
                 
                  Message[] messages = emailFolder.getMessages();
                  for (int i= 0; i < messages.length; i++){
                     Message message = messages[i];
                       System.err.println("Flagged: "+ message.getFlags().contains(Flags.Flag.FLAGGED));
                        if (!message.isSet(Flags.Flag.FLAGGED)){
                         message.setFlag(Flags.Flag.FLAGGED, true);
                          message.setFlag(Flags.Flag.SEEN, false);
                         System.err.println("Ha llegado un mail "+ message.getFlags().contains(Flags.Flag.FLAGGED));
                         obtenerContenido(message);
                         BandejaEntrada correo = guardarCorreo(contenido, 
                                                                remitente,
                                                                para, 
                                                                message.getMessageNumber(), 
                                                                asunto);
                          objCorreo.guardarCorreoDescargado(correo, 1);
                          sendGet() ;
                          //  System.err.println("*******************************LLAMANDO****************************");
                         }
                  }
//                  if (!correos.isEmpty()){
//                        for (BandejaEntrada correo : correos){
//                             Message mensaje = emailFolder.getMessage(correo.getNumeroMail());
//                             if (mensaje.isSet(Flags.Flag.FLAGGED) && !mensaje.isSet(Flags.Flag.SEEN)){
//                                 sendGet();
//                             }
//                             
//                        }
//                       
//                  }
                  
               emailFolder.close(false);
               store.close();
               controlBandeja=true;
              } catch (NoSuchProviderException ex) {
                  System.err.println("Proveedor incorrecto");
              }
               catch ( MessagingException ex) {
                   System.err.println("Error al descargar los mensajes");
              } catch (Exception ex) {
                  
                  System.err.println("Error al obtener el contenido");
                  ex.printStackTrace();
                 
              }

  }
    
public void obtenerContenido(Part p) throws Exception {
      if (p instanceof Message)
         //Call methos writeEnvelope
         writeEnvelope((Message) p);


      //check if the content is plain text
      if (p.isMimeType("text/plain")) {
         //System.out.println((String) p.getContent());
      } 
      //check if the content has attachment
      else if (p.isMimeType("multipart/*")) {
         
         Multipart mp = (Multipart) p.getContent();
         int count = mp.getCount();
         for (int i = 0; i < count; i++){
            obtenerContenido(mp.getBodyPart(i));}
      } 
      //check if the content is a nested message
      else if (p.isMimeType("message/rfc822")) {
         obtenerContenido((Part) p.getContent());
      } 
      //check if the content is an inline image
      else if (p.isMimeType("image/jpeg")) {
         byte[] bArray = toBytes(p.getDataHandler());
         String path =  new File("").getAbsolutePath();
         rutaImagen= path +"/resources/"+ listadoImagenes.get(0);
         listadoImagenes.remove(0);
         FileOutputStream f2 = new FileOutputStream(rutaImagen);
         f2.write(bArray);
         f2.close();
      } 
      else if (p.getContentType().contains("image/")) {
         String path =  new File("").getAbsolutePath(); 
      //   System.out.println("content type" + p.getContentType());

        File f = new File(path +"/resources/"+ listadoImagenes.get(0));
         listadoImagenes.remove(0);
         DataOutputStream output = new DataOutputStream(
            new BufferedOutputStream(new FileOutputStream(f)));
            com.sun.mail.util.BASE64DecoderStream test = 
                 (com.sun.mail.util.BASE64DecoderStream) p
                  .getContent();
         byte[] buffer = new byte[1024];
         int bytesRead;
         while ((bytesRead = test.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
         }
         output.close();
      } 
      else {
         Object o = p.getContent();
         if (o instanceof String) {
            String content = (String) o;
             String path = "http://192.168.204.37:8080/CentroSurCM/images";
            Pattern pattern = Pattern.compile("src=\".*?\\.(?:png|jpg|gif)\"|src=\"cid:.*?\"");
             Matcher matcher = pattern.matcher(content );
             contenido = content;
             while (matcher.find()) {
             String nombre =content .substring(matcher.start(), matcher.end()) ;
           //   System.err.println("Nombre archivo " + nombre +"###############") ;
             if (nombre.contains("cid:")){
                String nombreAsignado =nombre.substring(nombre.indexOf(":")+1,nombre.lastIndexOf("\""))+".jpg";
                listadoImagenes.add(nombreAsignado);
               contenido  =contenido  .replaceFirst(nombre, "src=\""+path + "/" + nombreAsignado +"\"");
             }else{
                    
                String nombreAsignado =nombre.substring(nombre.indexOf("\"")+1,nombre.lastIndexOf("\""));
                listadoImagenes.add(nombreAsignado);
                contenido =contenido .replaceFirst(nombre, "src=\""+ path + "/" + nombreAsignado +"\"");
               }
             
          //       System.err.println("Contenido post:  " + contenido);
            }

         } 
         else if (o instanceof InputStream) {
            InputStream is = (InputStream) o;
            is = (InputStream) o;
            int c;
            while ((c = is.read()) != -1)
               System.out.write(c);
         } 
         else {
            System.out.println(o.toString());
         }
      }
    }
   
       

    
    public void writeEnvelope(Message m) throws Exception {
      Address[] a;

      // FROM
      if ((a = m.getFrom()) != null) {
         for (int j = 0; j < a.length; j++)
             remitente = a[j].toString();
      }

      // TO
      if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
         for (int j = 0; j < a.length; j++)
            para = a[j].toString();
      }

      // SUBJECT
      if (m.getSubject() != null)
           asunto = m.getSubject();

   }  
    
  public byte[] toBytes(DataHandler handler) throws IOException {
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    handler.writeTo(output);
    return output.toByteArray();
    }  
    
    
    
    
   public void autenticarEntidad(){
     session = Session.getDefaultInstance(getReadProperties(), null);

    }
   
       
    public Properties  getReadProperties(){
        Properties properties = new Properties();
        return  properties;
     }
    
   
   
      public BandejaEntrada guardarCorreo(String contenido,
                                        String remitente,
                                        String listaDistribucion,
                                        int numeroMail,
                                        String asunto){
        BandejaEntrada mail = new BandejaEntrada();
  
        mail.setContenido(contenido);
        mail.setEstado(false);
        mail.setLeido(false);
        mail.setRespondido(false);
        mail.setHora(new Date());
        mail.setListaDistribucion(listaDistribucion);
        mail.setRemitente(remitente);
        mail.setNumeroMail(numeroMail);
        mail.setAsunto(asunto);
        return mail;
    }
   
     
      
 private void sendGet() throws Exception {

		String url = "http://172.19.129.16:9080/CSADICOEMAIL";
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");
                int responseCode = con.getResponseCode();
		//add request header
		//con.setRequestProperty("User-Agent", USER_AGENT);

		
//		System.out.println("\nSending 'GET' request to URL : " + url);
//		System.out.println("Response Code : " + responseCode);
//
//		BufferedReader in = new BufferedReader(
//		        new InputStreamReader(con.getInputStream()));
//		String inputLine;
//		StringBuffer response = new StringBuffer();
//
//		while ((inputLine = in.readLine()) != null) {
//			response.append(inputLine);
//		}
//		in.close();
//
//		//print result
//		System.out.println(response.toString());

	}
     
      
}*/