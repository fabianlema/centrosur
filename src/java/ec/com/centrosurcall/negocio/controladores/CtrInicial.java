/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.RolesDAO;
import ec.com.centrosurcall.datos.DAO.UsuariosDAO;
import ec.com.centrosurcall.datos.DAO.ModulosDAO;
import ec.com.centrosurcall.utils.Login;
import java.io.Serializable;
import java.security.MessageDigest;
//import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author TaurusTech
 */
@ManagedBean
@SessionScoped
public final class CtrInicial implements Serializable {

    Log log = LogFactory.getLog(this.getClass());
    protected CtrSession parametroSession;
//    protected CtrAplicacion parametroAplicacion;
    private String inicializar = new String();
    private String usuarioP;
    private String contraseniaP;
    private String mensajerror = "";
   

    public CtrInicial() {
        validar();
    }

    public void validar() {
        try {
            String url;
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            HttpSession session = request.getSession(true);
            this.parametroSession = (CtrSession) FacesContext.getCurrentInstance().getELContext().getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(), null, "ctrSession");
            if (session.getAttribute("usuarioId") == null) {
                this.prerender();
            } else {
                url = "/CentroSurCM/faces/pages/frmPrincipal.xhtml";
                FacesContext faces = FacesContext.getCurrentInstance();
                ExternalContext context = faces.getExternalContext();
                context.redirect(url);
            }
        } catch (Exception e) {
            System.out.println("CtrInicio mensaje: " + e.getMessage());
            e.printStackTrace();
        }

    }

    public void prerender() throws Exception {
        Runtime r = Runtime.getRuntime();
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().
                getExternalContext().
                getRequest();
        //   String usuarioP = "";
        //   String contraseniaP = "";
        try {
            //       usuarioP = request.getParameter("usuario");
            //        contraseniaP = request.getParameter("contrasenia");
        } catch (Exception e) {
        }
        if (usuarioP != null && contraseniaP != null) {
            verificarLogin(usuarioP, contraseniaP);
        } else {
            String url;
            FacesContext faces = FacesContext.getCurrentInstance();
            ExternalContext context = faces.getExternalContext();
            url = "/CentroSurCM/faces/index.xhtml";
            context.redirect(url);
        }
        r.gc();
    }

    public void verificarLogin(String usuario, String contrasenia) throws Exception {
        boolean logeo = false;
        Login logueo = new Login();

        if (usuario == null || contrasenia == null) {
            logeo = false;
            usuario = "";
            contrasenia = "";
        } else {            
            if (!usuario.equals("agenteccx")){
                logeo = logueo.login(usuario, contrasenia);
                if (logeo == false) {
                    this.mensajerror = "EL usuario o contraseña no es validado en DA";
                }
            }else{
                if (contrasenia.equals("4dmin2017")) logeo = true;
                else{
                    logeo = false;
                    this.mensajerror = "Clave del usuario administrador incorrecto";
                } 
            }                
        }

        String url;
        if (logeo) {
            RolesDAO rd = new RolesDAO();
            url = "/CentroSurCM/faces/pages/frmPrincipal.xhtml";
            parametroSession.setUsuario(new UsuariosDAO().getUsuario(usuario));    
            parametroSession.setRolUsuario(rd.getRolesUsuario(parametroSession.getUsuario().getId()));
            parametroSession.setAgentes(rd.getIdAgentesRoles());
            parametroSession.inicializarClickCall();
            parametroSession.inicializarMSN2n();
            if (parametroSession.getRolUsuario().isEmpty() ) {
                mensajerror = "El usuario ingresado no cuenta con los permisos suficientes";
                FacesContext faces = FacesContext.getCurrentInstance();
                ExternalContext context = faces.getExternalContext();
                url = "/CentroSurCM/faces/index.xhtml";
                context.redirect(url);
            } else {               
                ModulosDAO md = new ModulosDAO();
                String cadenaIn = md.getDetalleIdsString(parametroSession.getUsuario().getId());
                parametroSession.setModuloUsuario(md.getModulosxRol(cadenaIn));
                //    parametroSession.setUsuarioSkill(new UsuarioSkillDAO().getUsuarioSkills(parametroSession.getUsuario().getId()));
                try {
                    HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().
                            getExternalContext().
                            getRequest();
                          
                    FacesContext faces = FacesContext.getCurrentInstance();
                    ExternalContext context = faces.getExternalContext();
                    context.redirect(url);
                    faces.responseComplete();
                    HttpSession session = request.getSession(true);
                    session.setAttribute("usuarioId", String.valueOf(parametroSession.getUsuario().getId()));

                } catch (Exception e) {
                    log.info("errorVerificarLog" + e.getMessage());
                }
            }

        } else {
            mensajerror = "El usuario o la Contrasenia son Incorrectos, comuniquese con el Administrador";
            FacesContext faces = FacesContext.getCurrentInstance();
            ExternalContext context = faces.getExternalContext();
            url = "/CentroSurCM/faces/index.xhtml";
            context.redirect(url);
        }

    }

    public void destroy() {
        Runtime r = Runtime.getRuntime();

        // Step 2: determine the current amount of free memory
        long freeMem = r.freeMemory();
        System.out.println("antes cerrar: " + freeMem);


        r.gc();
        freeMem = r.freeMemory();
        System.out.println("luego del gc gc():    " + freeMem);
    }

    public String encriptarMD5(String palabra) {
        String pe = "";
        try {
            pe = hash(palabra);
        } catch (Exception e) {
            throw new Error("Error: Al encriptar el password");
        }
        return pe.toUpperCase();
    }

    /**
     * Encripta un String con el algoritmo MD5. Reemplazar la palabara MENOR por
     * el simbolo de menor
     *
     * @return String
     * @throws Exception
     */
    private String hash(String clear) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] b = md.digest(clear.getBytes());
        int size = b.length;
        StringBuffer h = new StringBuffer(size);
        for (int i = 0; i < size; i++) {
            int u = b[i] & 255;
            if (u < 16) {
                h.append("0" + Integer.toHexString(u));
            } else {
                h.append(Integer.toHexString(u));
            }
        }
        return h.toString();
    }

    public String getInicializar() {
        return inicializar;
    }

    public void setInicializar(String inicializar) {
        this.inicializar = inicializar;
    }

    public String getUsuarioP() {
        return usuarioP;
    }

    public void setUsuarioP(String usuarioP) {
        this.usuarioP = usuarioP;
    }

    public String getContraseniaP() {
        return contraseniaP;
    }

    public void setContraseniaP(String contraseniaP) {
        this.contraseniaP = contraseniaP;
    }

    /**
     * @return the mensajerror
     */
    public String getMensajerror() {
        return mensajerror;
    }

    /**
     * @param mensajerror the mensajerror to set
     */
    public void setMensajerror(String mensajerror) {
        this.mensajerror = mensajerror;
    }
}
