/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.ModulosDAO;
import ec.com.centrosurcall.datos.DAO.RegistroSeccionDAO;
import ec.com.centrosurcall.datos.DAO.UsuariosDAO;
import ec.com.centrosurcall.datos.modelo.DetalleMod;
import ec.com.centrosurcall.datos.modelo.Modulo;
import ec.com.centrosurcall.datos.modelo.ModuloId;
import ec.com.centrosurcall.datos.modelo.RegistroSeccion;
import ec.com.centrosurcall.utils.FacesUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author TaurusTech
 */
@ManagedBean
@SessionScoped
public final class CtrModulos extends MantenimientoGenerico{
    public List<DetalleMod> lstModulosPadres = new ArrayList<DetalleMod>();
    public List<DetalleMod> lstModulos = new ArrayList<DetalleMod>();
    private ModulosDAO md = new ModulosDAO();
    private RegistroSeccionDAO rs = new RegistroSeccionDAO();
    //List<Object> rol;
    UsuariosDAO usDAO = new UsuariosDAO();
    public LinkedHashMap<String, String> listaColumnas = new LinkedHashMap();
    public List<Modulo> listaSubModulos = new ArrayList<Modulo>();
    
    CtrSession session;
    
    public CtrModulos() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        cargarModulosPadres();
    }
    String nombreModuloSel="";
    public  String seleccionarModulo(Object keysel){
        String objsel=keysel.toString();
        nombreModuloSel=listaColumnas.get(keysel).toString();
        //rol = consulta.getListSql("select idRol from dbo.rol_usuario where idUsuario="+parametroSession.getUsuario().getId());
        String cadenaHijosIn = md.getHijosIdsString(Integer.valueOf(objsel),parametroSession.getUsuario().getId());
        listaSubModulos=md.getModulosHijosxId(Integer.valueOf(objsel),cadenaHijosIn);
        //listaSubModulos = new ModulosDAO().getModulosPadre();
        
        if (listaSubModulos.get(0).getUrl().equals("frmRegistroLlamadas.xhtml")) {
            List<RegistroSeccion> listaRegistrosSeccion = rs.getRegistroSeccion();
            List<Modulo> listaSubModulosAux = new ArrayList<Modulo>();
            listaSubModulosAux.add(listaSubModulos.get(0));
            for (int i=0; i<listaRegistrosSeccion.size(); i++){
                listaSubModulosAux.add(new Modulo(new ModuloId(Integer.parseInt(objsel), listaRegistrosSeccion.get(i).getId()), listaRegistrosSeccion.get(i).getNombre(), "frmRegistroLlamada.xhtml?codigoivr="+String.valueOf(listaRegistrosSeccion.get(i).getId()), "", "", Integer.valueOf("1"), Boolean.valueOf(false)));
            }
            listaSubModulos = listaSubModulosAux;
        }
        
        if(listaSubModulos.size()>0){
            return listaSubModulos.get(0).getUrl();
        }
        return "frmPrincipal.xhtml";
    }

    public void cargarModulosPadres(){
        try{
         if(parametroSession.getUsuario()!=null){             
            System.out.println("Cargando modulos del usuario " + parametroSession.getUsuario().getId());
            //rol = consulta.getListSql("select idRol from dbo.rol_usuario where idUsuario=" + parametroSession.getUsuario().getId());
            String cadenaInPadre = md.getDetallePadreIdsString(parametroSession.getUsuario().getId());
            lstModulosPadres=md.getModulosxUsuario(cadenaInPadre);
            
            String cadenaIn = md.getDetalleIdsString(parametroSession.getUsuario().getId());
            //parametroSession.setModuloUsuario(md.getModulosxRol(cadenaIn));
            
            lstModulos=md.getModulosxRol(cadenaIn);
            listaColumnas = new LinkedHashMap();
            for (DetalleMod mod : lstModulosPadres){
                listaColumnas.put(String.valueOf(mod.getModulo().getId().getIdPadre()), String.valueOf(mod.getModulo().getNombre()));
            }
         }else{
             session.cerrarSesion();
             parametroSession.cerrarSesion();
         }
        }catch(Exception e){
            System.out.println(e.getCause().getMessage());
        }

    }
        
    @Override
    public LinkedHashMap<String, String> getListaColumnas() {
        if (!session.isIsSessionOn()){
            cargarModulosPadres();
            session.setIsSessionOn(true);
        }
        return listaColumnas;
    }

    @Override
    public void setListaColumnas(LinkedHashMap<String, String> listaColumnas) {
        this.listaColumnas = listaColumnas;
    }

    public String getNombreModuloSel() {
        return nombreModuloSel;
    }

    public void setNombreModuloSel(String nombreModuloSel) {
        this.nombreModuloSel = nombreModuloSel;
    }

    public List<Modulo> getListaSubModulos() {
        return listaSubModulos;
    }

    public void setListaSubModulos(List<Modulo> listaSubModulos) {
        this.listaSubModulos = listaSubModulos;
    }
    

    
}
