/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

/**
 *
 * @author Paul
 */
public class ObjLlamadasEntrantes {
    private String agente;
    private String numTelefono;
    private String fechaDesde;
    private String fechaHasta;
    private String tiempo;
    
    public void objLlamadasEntrantes(){
    
    }

    /**
     * @return the agente
     */
    public String getAgente() {
        return agente;
    }

    /**
     * @param agente the agente to set
     */
    public void setAgente(String agente) {
        this.agente = agente;
    }

    /**
     * @return the numTelefono
     */
    public String getNumTelefono() {
        return numTelefono;
    }

    /**
     * @param numTelefono the numTelefono to set
     */
    public void setNumTelefono(String numTelefono) {
        this.numTelefono = numTelefono;
    }

    /**
     * @return the fechaDesde
     */
    public String getFechaDesde() {
        return fechaDesde;
    }

    /**
     * @param fechaDesde the fechaDesde to set
     */
    public void setFechaDesde(String fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    /**
     * @return the fechaHasta
     */
    public String getFechaHasta() {
        return fechaHasta;
    }

    /**
     * @param fechaHasta the fechaHasta to set
     */
    public void setFechaHasta(String fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    /**
     * @return the tiempo
     */
    public String getTiempo() {
        return tiempo;
    }

    /**
     * @param tiempo the tiempo to set
     */
    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }
}
