/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.DatosCorreoDAO;
import ec.com.centrosurcall.datos.modelo.DatosCorreo;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrDatosCorreo extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private DatosCorreo selDatosCorreo = new DatosCorreo();
    DatosCorreoDAO datoCorreoDAO = new DatosCorreoDAO();
    String opcionBoton = "";
    //SelectItem item = new SelectItem();

    public CtrDatosCorreo() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarDatosCorreos();
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoDatosCorreoEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar estos Datos del Correo?");
        confPop();
    }

    public void cargarDatosCorreos() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<DatosCorreo> listaDatosCorreo = datoCorreoDAO.getDatosCorreos();        
        if (listaDatosCorreo != null) {
            listaBaseObjetos.addAll(listaDatosCorreo);
            listaObjetos.addAll(listaBaseObjetos); 
        }
    }

    public void cargoDatosCorreoEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("DatosCorreo")) {
                if (((DatosCorreo) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selDatosCorreo = (DatosCorreo) session.getObjeto();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar los Datos del Correo");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            selDatosCorreo.setEstado(true);
            exito = datoCorreoDAO.saveDatosCorreo(selDatosCorreo, 1);
            parametroSession.setEsNuevo(false);
        } else {
            exito = datoCorreoDAO.saveDatosCorreo(selDatosCorreo, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<DatosCorreo> respuesta = new ArrayList<DatosCorreo>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                DatosCorreo cl = (DatosCorreo) obj;
                if (cl.getHostmail().toUpperCase().contains(whereSql.toUpperCase()) || cl.getUsermail().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
       // listaColumnas.put("id", "Id");
        listaColumnas.put("usermail", "Correo");
        listaColumnas.put("hostmail", "Host");
        //listaColumnas.put("codigoDatosCorreo","Código CentroSur");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionDatosCorreo";
    }

    @Override
    public void auditoria() {
        try {
            selDatosCorreo = (DatosCorreo) objetoSeleccionado;
        } catch (Exception e) {
            selDatosCorreo.setId(0);
        }
        if (selDatosCorreo.getId() > 0) {
            creado = selDatosCorreo.getUsermail();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarDatosCorreos();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<DatosCorreo> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((DatosCorreo) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmDatosCorreo";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmDatosCorreo";
    }

    public void confPopEliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<DatosCorreo> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = datoCorreoDAO.eliminarDatosCorreos(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarDatosCorreos();
        }
        return exito;
    }

    public List<DatosCorreo> sacarSeleccionado() {
        DatosCorreo selectDatosCorreo = new DatosCorreo();
        List<DatosCorreo> listadoSeleccionado = new ArrayList<DatosCorreo>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectDatosCorreo = (DatosCorreo) o;
            if (selectDatosCorreo.getSeleccionado()) {
                listadoSeleccionado.add(selectDatosCorreo);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public DatosCorreo getSelDatosCorreo() {
        return selDatosCorreo;
    }

    public void setSelDatosCorreo(DatosCorreo selDatosCorreo) {
        this.selDatosCorreo = selDatosCorreo;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String usermail;
    private String hostmail;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsermail() {
        return usermail;
    }

    public void setUsermail(String usermail) {
        this.usermail = usermail;
    }

    public String getHostmail() {
        return hostmail;
    }

    public void setHostmail(String hostmail) {
        this.hostmail = hostmail;
    }
}
