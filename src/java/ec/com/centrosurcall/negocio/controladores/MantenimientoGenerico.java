/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.utils.ClaseGeneral;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.FuncionesComunes;
import ec.com.centrosurcall.utils.MensajeConfirmacion;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.validator.ValidatorException;
import org.richfaces.component.UIDataTable;
import org.richfaces.component.UIExtendedDataTable;

/**
 *
 * @author usuario
 */
public class MantenimientoGenerico extends MensajeConfirmacion {

    //Parametros que se deben definir en el mantenimiento:}
    //
    public List<Object> listaObjetos = new ArrayList<Object>();
    public List<Object> listaBaseObjetos = new ArrayList<Object>();
    public Object objetoSeleccionado = new Object();
    public String whereSql = "";
    public Collection<Object> objetoSeleccionar = new ArrayList<Object>();
    public boolean seleccionadoCheck = false;
    public CtrSession parametroSession;
    public LinkedHashMap<String, String> listaColumnas = new LinkedHashMap();
    public Consultas consulta = new Consultas();
    public String boton = "btnGrabar";
    //Utilidades
    public boolean mostrarAuditoriaVal = false;
    public boolean mostrarDetalle = false;
    //Auditoria:
    public String creado;
    public String fechacreacion;
    public String actualizado;
    public String fechaactualizacion;

    public MantenimientoGenerico() {

        parametroSession = (CtrSession) FacesUtils.getManagedBean("ctrSession");


    }

    public void reset() {
        objetoSeleccionado = new Object();
        whereSql = "";
        objetoSeleccionar = new ArrayList<Object>();
        seleccionadoCheck = false;
        consulta = new Consultas();
        //Utilidades

        mostrarAuditoriaVal = false;

    }

    public String getWhereSql() {
        return whereSql;
    }

    public void setWhereSql(String whereSql) {
        this.whereSql = whereSql;

    }
    FuncionesComunes fg = new FuncionesComunes();

    public void seleccionarTodos(AjaxBehaviorEvent b) {
        HtmlSelectBooleanCheckbox sf = (HtmlSelectBooleanCheckbox) b.getComponent();
        seleccionadoCheck = Boolean.valueOf(sf.getValue().toString());
        int page = parametroSession.getNumPaginas();
        int numerofilasxpagina = 10;
        listaObjetos = fg.seleccionarTodos(seleccionadoCheck, page, numerofilasxpagina, listaObjetos);

    }
//    public void selectionListener(AjaxBehaviorEvent event) {
//
//        UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
//        int fila = dataTable.getRowIndex();
//        int pag = parametroSession.getNumPaginas() - 1;
//        int idLista = (pag * parametroSession.getMaximoFila()) + fila;
//        Object originalKey = dataTable.getRowKey();
//        objetoSeleccionado = listaObjetos.get(idLista);
//
//        //pongo el seleccionado en true:
//        try {
//            Class<?> c = objetoSeleccionado.getClass();
//            Method metodo = c.getMethod("setSeleccionado", boolean.class);
//            metodo.invoke(objetoSeleccionado, true);
//
//        } catch (Exception e) {
//        }
//
//
//
//        parametroSession.setObjeto(objetoSeleccionado);
//        dataTable.setRowKey(originalKey);
//
//    }
    private Collection<Object> selection;

    public Collection<Object> getSelection() {
        return selection;
    }

    public void setSelection(Collection<Object> selection) {
        this.selection = selection;
    }

    public void selectionListener(AjaxBehaviorEvent event) {
        UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
        Object originalKey = dataTable.getRowKey();
        if (originalKey != null) {
            for (Object selectionKey : selection) {
                dataTable.setRowKey(selectionKey);

                parametroSession.setObjeto(listaObjetos.get(selection.hashCode()));

            }
            dataTable.setRowKey(originalKey);
        }

    }
    //Usado para Validaciones:
    /*
     *
     * Daniel Autor:
     */

    public void validateEmail(FacesContext facesContext, UIComponent uIComponent, Object object) throws ValidatorException {
        String enteredEmail = (String) object;
        Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        Matcher m = p.matcher(enteredEmail);
        boolean matchFound = m.matches();

        if (!matchFound) {
            FacesMessage message = new FacesMessage();
            message.setSummary("Invalid Email ID.");
            throw new ValidatorException(message);
        }
    }
    //Metodos para Confirmacion del Toolbar
    //Variable valida pop
    public boolean exitograbar = false;
    public boolean banderaUna = false;

    public boolean isExitograbar() {
        exitograbar = confirmarInstitucion();
        return exitograbar;
    }

    public void setExitograbar(boolean exitograbar) {
        this.exitograbar = exitograbar;
    }

    public boolean confirmarInstitucion() {
        boolean exito = false;
        if (banderaUna) {

            exito = grabar();

        } else {
            banderaUna = true;
        }

        return exito;

    }
    //Para la parte de eliminar:
    //Variable valida pop
    public boolean exitoeliminar = false;
    public boolean banderapopeliminar = false;

    public boolean isExitoeliminar() {
        exitoeliminar = eliminarInstitucion();
        return exitoeliminar;
    }

    public void setExitoeliminar(boolean exitoeliminar) {
        this.exitoeliminar = exitoeliminar;
    }

    public boolean eliminarInstitucion() {
        boolean exito = false;
        if (banderapopeliminar) {
            exito = eliminar();
        } else {
            banderapopeliminar = true;
        }

        return exito;

    }
    public List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

    public List<ClaseGeneral> getListaErrores() {
        return listaErrores;
    }

    public void setListaErrores(List<ClaseGeneral> listaErrores) {
        this.listaErrores = listaErrores;
    }

    public boolean grabar() {
        return false;
    }

    public boolean eliminar() {
        return false;
    }

    public void mostrarAuditoria() {
        mostrarAuditoriaVal = true;
    }

    public void cerrarAuditoria() {
        mostrarAuditoriaVal = false;
    }

    public void mostrarDetalle() {
        mostrarDetalle = true;
    }

    public void cerrarDetalle() {
        mostrarDetalle = false;
    }
    boolean mostrarPopExito = false;
    boolean mostrarPopError = false;

    public void eventoGrabacionPop() {
        mostrarError();

    }

    public boolean isMostrarPopError() {
        return mostrarPopError;
    }

    public void setMostrarPopError(boolean mostrarPopError) {
        this.mostrarPopError = mostrarPopError;
    }

    public boolean isMostrarPopExito() {
        return mostrarPopExito;
    }

    public void setMostrarPopExito(boolean mostrarPopExito) {
        this.mostrarPopExito = mostrarPopExito;
    }

    public void mostrarError() {

        if (mostrarPopError) {
            mostrarPopError = false;
        } else {
            mostrarPopError = true;
        }


    }

    public void mostrarExito() {
        if (mostrarPopExito) {
            mostrarPopExito = false;
        } else {
            mostrarPopExito = true;
        }
    }

    public void cerrarTodos() {

        mostrarPopExito = false;
        mostrarPopError = false;

    }

    public LinkedHashMap<String, String> getListaColumnas() {
        return listaColumnas;
    }

    public void setListaColumnas(LinkedHashMap<String, String> listaColumnas) {
        this.listaColumnas = listaColumnas;
    }

    public Collection<Object> getObjetoSeleccionar() {
        return objetoSeleccionar;
    }

    public void setObjetoSeleccionar(Collection<Object> objetoSeleccionar) {
        this.objetoSeleccionar = objetoSeleccionar;
    }

    public boolean isSeleccionadoCheck() {
        return seleccionadoCheck;
    }

    public void setSeleccionadoCheck(boolean seleccionadoCheck) {
        this.seleccionadoCheck = seleccionadoCheck;
    }

    public List<Object> getListaObjetos() {
        return listaObjetos;
    }

    public void setListaObjetos(List<Object> listaObjetos) {
        this.listaObjetos = listaObjetos;
    }

    public Object getObjetoSeleccionado() {
        return objetoSeleccionado;
    }

    public void setObjetoSeleccionado(Object objetoSeleccionado) {
        this.objetoSeleccionado = objetoSeleccionado;
    }

    public boolean isMostrarAuditoriaVal() {
        return mostrarAuditoriaVal;
    }

    public void setMostrarAuditoriaVal(boolean mostrarAuditoriaVal) {
        this.mostrarAuditoriaVal = mostrarAuditoriaVal;
    }

    public boolean isMostrarDetalle() {
        return mostrarDetalle;
    }

    public void setMostrarDetalle(boolean mostrarDetalle) {
        this.mostrarDetalle = mostrarDetalle;
    }

    public String getActualizado() {
        return actualizado;
    }

    public void setActualizado(String actualizado) {
        this.actualizado = actualizado;
    }

    public String getCreado() {
        return creado;
    }

    public void setCreado(String creado) {
        this.creado = creado;
    }

    public String getFechaactualizacion() {
        return fechaactualizacion;
    }

    public void setFechaactualizacion(String fechaactualizacion) {
        this.fechaactualizacion = fechaactualizacion;
    }

    public String getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(String fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public CtrSession getParametroSession() {
        return parametroSession;
    }

    public void setParametroSession(CtrSession parametroSession) {
        this.parametroSession = parametroSession;
    }

    public List<Object> getListaBaseObjetos() {
        return listaBaseObjetos;
    }

    public void setListaBaseObjetos(List<Object> listaBaseObjetos) {
        this.listaBaseObjetos = listaBaseObjetos;
    }

    public String getBoton() {
        return boton;
    }

    public void setBoton(String boton) {
        this.boton = boton;
    }
}
