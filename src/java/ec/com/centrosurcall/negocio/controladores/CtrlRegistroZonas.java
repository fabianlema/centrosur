/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.ZonaAfectadaDAO;
import ec.com.centrosurcall.datos.DAO.ZonaDAO;
import ec.com.centrosurcall.datos.modelo.Alimentador;
import ec.com.centrosurcall.datos.modelo.HoraReposicion;
import ec.com.centrosurcall.datos.modelo.Subestacion;
import ec.com.centrosurcall.datos.modelo.Zona;
import ec.com.centrosurcall.datos.modelo.ZonaAfectada;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author carlosguaman65
 */
@ManagedBean
@ViewScoped
public final class CtrlRegistroZonas extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    private String descripcion;
    private String codigo;
    private List<Object> objZonasAfectadas;
    private List<Object> objAlimentadores;
    private List<Object> objZonas;
    private Set<Alimentador> alimentadores;
    private String buscar;
    private CtrSession session;
    ZonaDAO zonaDAO = new ZonaDAO();
    private Subestacion subestacion;
    @ManagedProperty(value = "#{alimentadorDAO.alimentadoresList}")
    private List<Alimentador> listaAlimentadores;
    private Boolean controGuardar = false;
    //  private List<Alimentador> listadoAlimentadoresTemporal= new ArrayList<Alimentador>();
    private Alimentador alimentadorSeleccionado;
    private String codigosAlimentador[];
    @ManagedProperty(value = "#{zonaDAO.zonasList}")
    private List<Zona> zonas;
    private List<Zona> listadoZonasAfectadas;
    @ManagedProperty(value = "#{zonaDAO.zonasAfectadasList}")
    private List<Zona> zonasAfectadas;
    Alimentador alimentadorActual;
    private Zona zonaDisponibleSeleccionada;
    private Zona zonaAfectadaSeleccionada;
    private String horaActual = "";
    private List<String> listaHoras = new ArrayList();
    private HoraReposicion horaRep;

    @PostConstruct
    public void init() {
        zonas.clear();        
    }

    public CtrlRegistroZonas() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        horaRep = (HoraReposicion)(consulta.getListHql(null, "HoraReposicion", "id=1", null).get(0));
        if (horaRep.getDescripcion().equals("0")) horaActual = "--SELECCIONE--";
        else horaActual = horaRep.getDescripcion();
        //    zonas.clear();
        System.err.println("Constructor:CtrlRegistroZonas");
    }

    public void onSelectZonaItem(ValueChangeEvent event) {
        listadoZonasAfectadas = ((List<Zona>) event.getNewValue());

    }

    public void onSelectAlimentadorItem(ValueChangeEvent event) {
        controGuardar = false;
        ZonaDAO gestorZonas = new ZonaDAO();
        zonas.clear();
        if (null != event.getNewValue()) {

            alimentadorActual = (Alimentador) event.getNewValue();
            List<Zona> zonasPorAlimentador = gestorZonas.getZonasByAlimentadores(alimentadorActual.getId());
            for (Zona zona : zonasPorAlimentador) {
                //  System.err.println(">>>>>>>>>>> "+  zona.getDescripcion());
                zonas.add(zona);
            }
            //Collections.sort(zonas);

        }
    }

    public void onSelectZonasDisponiblesItem(ValueChangeEvent event) {
        controGuardar = false;
        ZonaAfectadaDAO gestorZonas = new ZonaAfectadaDAO();
        ZonaDAO zonasDAO = new ZonaDAO();
        if (null != event.getNewValue()) {
            Zona zona = (Zona) event.getNewValue();
            //   System.err.println("Click en zonas  disp>> ");
            gestorZonas.saveZonaAfectada(crearZonaAfectada(zona), 1);
            zonas.clear();

            List<Zona> zonasDisponibles = zonasDAO.getZonasByAlimentadores(alimentadorActual.getId());

            for (Zona zonaDisponible : zonasDisponibles) {
                zonas.add(zonaDisponible);
            }
            zonasAfectadas.clear();
            List<Zona> zonasAfectadasActuales = getListaZonasAfectadas();
            for (Zona zonaAfectadaActual : zonasAfectadasActuales) {
                zonasAfectadas.add(zonaAfectadaActual);
            }
        }
    }

    public void onSelectZonaAfectadaItem(ValueChangeEvent event) {
        ZonaAfectadaDAO gestorZonas = new ZonaAfectadaDAO();
        ZonaDAO zonaDAO = new ZonaDAO();
        if (null != event.getNewValue()) {
            Zona zona = (Zona) event.getNewValue();
            ZonaAfectada zonaAfectada = gestorZonas.getZonaAfectadaByZona(zona.getId());
            zonaAfectada.setEstado(0);
            boolean a = gestorZonas.saveZonaAfectada(zonaAfectada, 0);
            zonasAfectadas.clear();
            List<Zona> zonasAfectadasActuales = zonaDAO.getZonasAfectadasList();
            for (Zona zonaAfectadaActual : zonasAfectadasActuales) {
                zonasAfectadas.add(zonaAfectadaActual);
            }
            zonas.clear();
            List<Zona> zonasDisponibles = zonaDAO.getZonasByAlimentadores(alimentadorActual.getId());
            for (Zona zonaDisponible : zonasDisponibles) {
                zonas.add(zonaDisponible);
            }
        }
    }

    public List<Zona> getListaZonasAfectadas() {
        List<Zona> listaZonasAfectadas = new ArrayList<Zona>();
        objZonasAfectadas = consulta.getListHql(null, "ZonaAfectada", "estado = 1 ", null);
        if (objZonasAfectadas != null && objZonasAfectadas.size() > 0) {
            for (Object obj : objZonasAfectadas) {
                ZonaAfectada zonaAfectada = (ZonaAfectada) obj;
                Zona zona = zonaDAO.getZonaByID(zonaAfectada.getZona().getId());

                listaZonasAfectadas.add(zona);
            }
        }        
        return listaZonasAfectadas;
    }

    public void actualizarZonasAfectadas() {
        zonasAfectadas.clear();
        ZonaAfectadaDAO gestorZonas = new ZonaAfectadaDAO();
        List<ZonaAfectada> zonaAfectadas = gestorZonas.getZonasAfectadas();
        for (ZonaAfectada zonaAfectada : zonaAfectadas) {
            zonaAfectada.setEstado(0);
            gestorZonas.saveZonaAfectada(zonaAfectada, 0);
        }
        
        horaRep.setDescripcion("0");
        gestorZonas.saveHoraReposicion(horaRep, 0);
        horaActual = "--SELECCIONE--";

        controGuardar = true;
//        ZonaAfectadaDAO gestorZonas = new ZonaAfectadaDAO();
//        List<Zona> listaZonasActuales = getListaZonasAfectadas();
//       
//        Set <Zona>nuevosElementos  = new HashSet<Zona>(listadoZonasAfectadas);
//        Set <Zona>elementosActuales  = new HashSet<Zona>(listaZonasActuales);
// 
//
//        for (Zona zona : nuevosElementos){
//              for (Zona zonaActual: elementosActuales){
//                  if (zona.getId() == zonaActual.getId()){
//                      listadoZonasAfectadas.remove(zona);
//                  }
//              }
//        }
//
//        for (Zona zona : elementosActuales){
//            for (Zona zonaNueva : nuevosElementos)
//                if (zona.getId() == zonaNueva.getId()){
//                     listaZonasActuales.remove(zona);
//                }
//         
//        }
//            
//        for  (Zona zona : listadoZonasAfectadas){
//            gestorZonas.saveZonaAfectada(crearZonaAfectada(zona), 0);
//        }
//             
//        
//        for (Zona zona : listaZonasActuales){
//             ZonaAfectada zonaAfectada = gestorZonas.getZonaAfectadaByZona(zona.getId());
//             zonaAfectada.setEstado(0);
//             gestorZonas.saveZonaAfectada(zonaAfectada, 0);
//        }
//
//        listadoZonasAfectadas.clear();
//        zonasAfectadas.clear();

    }

    public void onSelectHoraReposicion(ValueChangeEvent evt) {
        ZonaAfectadaDAO gestorZonas = new ZonaAfectadaDAO();
        String currtentValue = (String) evt.getNewValue();
        //System.err.println("Cambio: " + currtentValue +"  vs "+ horaActual);
        //if ()
        horaActual = currtentValue;
        if (currtentValue.equals("--SELECCIONE--")){
            horaActual = "0";
        }
        //HoraReposicion hora = new HoraReposicion();
        horaRep.setDescripcion(horaActual);
        gestorZonas.saveHoraReposicion(horaRep, 0);
    }

    public ZonaAfectada crearZonaAfectada(Zona zona) {

        ZonaAfectada zonaAfectada = new ZonaAfectada();
        zonaAfectada.setUsuario(session.getUsuario());
        zonaAfectada.setZona(zona);
        zonaAfectada.setEstado(1);
        zonaAfectada.setFechaInicio(new Date());
        zonaAfectada.setHoraInicio(new Date());

        return zonaAfectada;
    }

    @PostConstruct
    public void resetearVariables() {
        //  zonas.clear();
    }

    public List<Object> getObjZonasAfectadas() {
        return objZonasAfectadas;
    }

    public void setObjZonasAfectadas(List<Object> objZonasAfectadas) {
        this.objZonasAfectadas = objZonasAfectadas;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    //   
    public String getHoraActual() {
        return horaActual;
    }

    public void setHoraActual(String horaActual) {
        this.horaActual = horaActual;
    }

    public List<String> getListaHoras() {
        listaHoras.add("--SELECCIONE--");
        listaHoras.add("Una Hora");
        listaHoras.add("Dos Horas");
        listaHoras.add("Tres Horas");
        listaHoras.add("Cuatro Horas");
        listaHoras.add("Cinco Horas");
        listaHoras.add("Seis Horas");
        listaHoras.add("Siete Horas");
        listaHoras.add("Ocho Horas");
        listaHoras.add("Nueve Horas");
        listaHoras.add("Diez Horas");

        return listaHoras;
    }

    public void setListaHoras(List<String> listaHoras) {

        this.listaHoras = listaHoras;
    }

    public Set<Alimentador> getAlimentadores() {
        return alimentadores;
    }

    public void setAlimentadores(Set<Alimentador> alimentadores) {
        this.alimentadores = alimentadores;
    }

    public List<Alimentador> getListaAlimentadores() {
        return listaAlimentadores;
    }

    public void setListaAlimentadores(List<Alimentador> listaAlimentadores) {
        this.listaAlimentadores = listaAlimentadores;
    }

    public Subestacion getSubestacion() {
        return subestacion;
    }

    public void setSubestacion(Subestacion subestacion) {
        this.subestacion = subestacion;
    }

    public List<Object> getObjAlimentadores() {
        return objAlimentadores;
    }

    public void setObjAlimentadores(List<Object> objAlimentadores) {
        this.objAlimentadores = objAlimentadores;
    }

    public Alimentador getAlimentadorSeleccionado() {
        return alimentadorSeleccionado;
    }

    public void setAlimentadorSeleccionado(Alimentador alimentadorSeleccionado) {
        this.alimentadorSeleccionado = alimentadorSeleccionado;
    }

    public String[] getCodigosAlimentador() {
        return codigosAlimentador;
    }

    public void setCodigosAlimentador(String[] codigosAlimentador) {
        this.codigosAlimentador = codigosAlimentador;
    }

    public List<Object> getObjZonas() {
        return objZonas;
    }

    public void setObjZonas(List<Object> objZonas) {
        this.objZonas = objZonas;
    }

    public List<Zona> getListadoZonasAfectadas() {
        return listadoZonasAfectadas;
    }

    public void setListadoZonasAfectadas(List<Zona> listadoZonasAfectadas) {
        this.listadoZonasAfectadas = listadoZonasAfectadas;
    }

    public String getBuscar() {
        return buscar;
    }

    public void setBuscar(String buscar) {
        this.buscar = buscar;
    }

    public List<Zona> getZonas() {
        return zonas;
    }

    public void setZonas(List<Zona> zonas) {
        this.zonas = zonas;
    }

    public List<Zona> getZonasAfectadas() {
        return zonasAfectadas;
    }

    public void setZonasAfectadas(List<Zona> zonasAfectadas) {
        this.zonasAfectadas = zonasAfectadas;
    }

    public Zona getZonaDisponibleSeleccionada() {
        return zonaDisponibleSeleccionada;
    }

    public void setZonaDisponibleSeleccionada(Zona zonaDisponibleSeleccionada) {
        this.zonaDisponibleSeleccionada = zonaDisponibleSeleccionada;
    }

    public Zona getZonaAfectadaSeleccionada() {
        return zonaAfectadaSeleccionada;
    }

    public void setZonaAfectadaSeleccionada(Zona zonaAfectadaSeleccionada) {
        this.zonaAfectadaSeleccionada = zonaAfectadaSeleccionada;
    }

    public Alimentador getAlimentadorActual() {
        return alimentadorActual;
    }

    public void setAlimentadorActual(Alimentador alimentadorActual) {
        this.alimentadorActual = alimentadorActual;
    }

    public Boolean getControGuardar() {
        return controGuardar;
    }

    public void setControGuardar(Boolean controGuardar) {
        this.controGuardar = controGuardar;
    }

    @Override
    public String nuevo() {
        return null;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String editar() {
        return null;
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void imprimir() {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String refrescar() {
        return null;
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void auditoria() {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String regresar() {
        return null;
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cargarColumnasGrupo() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void buscarWsql() {
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
//*********************Codigo para migrar
//         String filePath = new File("").getAbsolutePath();
//        System.err.println(">>>>> " + filePath);
//         ZonaDAO zonaDAO =new ZonaDAO();
//         List <Alimentador> listaAlimentadores = new ArrayList<Alimentador>();
//         Zona zona = new Zona();
//         AlimentadorDAO controladorDAO = new AlimentadorDAO();
//         BufferedReader br = null;
//         
//		try {
//                        
//			String sCurrentLine;
//                        int  minimum = 100;
//                        int maximum = 999;
//  
//			br = new BufferedReader(new FileReader(filePath+"/File.csv"));
//
//			while ((sCurrentLine = br.readLine()) != null) {
//			       System.err.println("Procesando.... " + sCurrentLine);
//                               String [] linea = sCurrentLine.split(",");
//                               int rd= minimum + (int)(Math.random()* (maximum-minimum) ); 
//                               codigo = linea[1].trim().charAt(1)+"";
//                               codigo = codigo+ rd;
//                               descripcion =  linea[0].trim();
//                               for (int i =1 ; i< linea.length ; i++){
//                                  Alimentador alimentador = controladorDAO.getAlimentadorByCodigo(linea[i].trim());
//                                   System.err.println(">>>>>" + alimentador.getId());
//                                  listaAlimentadores.add(alimentador);
//                               }
//  
//                                alimentadores = new HashSet(listaAlimentadores);
//                                //List<Alimentador> lst = new ArrayList<Alimentador>(alimentadores);
//                                zona.setCodigo(codigo);
//                                zona.setDescripcion(descripcion);  
//                                zonaDAO.saveZona(zona, alimentadores, 1);
//                                listaAlimentadores.clear();
//                            
//			}
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		} 
//         public List<Zona> getZonasAfectadas() {
//  
//        zonasAfectadas = new ArrayList<Zona>();
//        List<Zona> comp = getListaZonasAfectadas();
//
//            for (Zona s : zonas) {
//                for (Zona sel : comp) {
//                    if (sel.getId() == s.getId()) {
//                        zonasAfectadas.add(s);
//                        break;
//                    }
//                }
//            }  
//        return zonasAfectadas;
//    }
//
//    public void setZonasAfectadas(List<Zona> zonasAfectadas) {
//
//        this.zonasAfectadas = zonasAfectadas;
//    }
//    
//    public List<Zona> getZonas() {
//        
//         System.err.println("get a zonas");
//        return zonas;
//    }
//
//    public void setZonas(List<Zona> zonas) {
// 
//     
//        this.zonas = zonas;
//    }