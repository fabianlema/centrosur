/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.SkillsDAO;
import ec.com.centrosurcall.datos.modelo.Skill;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrSkills extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Skill selSkill = new Skill();
    SkillsDAO SkillsDAO = new SkillsDAO();
    String opcionBoton = "";

    public CtrSkills() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarSkills();
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoSkillEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar este Skill?");
        confPop();
    }

    public void cargarSkills() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Skill> listaSkill = SkillsDAO.getSkills();
        if (listaSkill != null) {
            listaBaseObjetos.addAll(listaSkill);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargoSkillEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Skill")) {
                if (((Skill) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }

        if (existeObjeto) {
            opcionBoton = "Guardar";
            selSkill = (Skill) session.getObjeto();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Skill");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            selSkill.setEstado(1);
            exito = SkillsDAO.saveSkill(selSkill, 1);
        } else {
            exito = SkillsDAO.saveSkill(selSkill, 0);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Skill> respuesta = new ArrayList<Skill>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Skill cl = (Skill) obj;
                if (cl.getNombreSkill().toUpperCase().contains(whereSql.toUpperCase()) || cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
     //   listaColumnas.put("id", "Id");
        listaColumnas.put("nombreSkill", "Nombre");
        listaColumnas.put("descripcion", "Descripción");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionSkills";
    }

    @Override
    public void auditoria() {
        try {
            selSkill = (Skill) objetoSeleccionado;
        } catch (Exception e) {
            selSkill.setId(0);
        }

        if (selSkill.getId() > 0) {
            creado = selSkill.getNombreSkill();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarSkills();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Skill> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Skill) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmSkills";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmSkills";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Skill> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = SkillsDAO.eliminarSkills(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarSkills();
        }
        return exito;
    }

    public List<Skill> sacarSeleccionado() {
        Skill selectSkill = new Skill();
        List<Skill> listadoSeleccionado = new ArrayList<Skill>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectSkill = (Skill) o;
            if (selectSkill.getSeleccionado()) {
                listadoSeleccionado.add(selectSkill);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Skill getSelSkill() {
        return selSkill;
    }

    public void setSelSkill(Skill selSkill) {
        this.selSkill = selSkill;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String nombreSkill;
    private String descripcion;
    private String estado;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreSkill() {
        return nombreSkill;
    }

    public void setNombreSkill(String nombreSkill) {
        this.nombreSkill = nombreSkill;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
