/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

//import ec.com.centrosurcall.datos.DAO.PersonasDAO;
import ec.com.centrosurcall.utils.ClaseGeneral;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ProveedorMensajes;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.swing.tree.TreeNode;
import org.richfaces.component.UITree;
import org.richfaces.event.TreeSelectionChangeEvent;
import java.lang.reflect.Field;
import org.richfaces.component.SortOrder;
import ec.com.centrosurcall.datos.DAO.LlamadasDAO;
import ec.com.centrosurcall.datos.modelo.Llamada;
import ec.com.centrosurcall.datos.modelo.Razon;
import ec.com.centrosurcall.negocio.campania.Cola;
import javax.faces.model.SelectItem;
import java.math.BigDecimal;
import ec.com.centrosurcall.call.MakeCall1;
//import ec.com.centrosurcall.middle.Credito;
import javax.faces.context.ExternalContext;

/**
 *
 * @author persona
 */
@ManagedBean
@ViewScoped
public final class CtrPersonas extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    //PersonasDAO personasHelper = new PersonasDAO();
    String opcionBoton = "";
    CtrSession session;
    //private Credito selCredito = new Credito();
    private Llamada selLlamada = new Llamada();
    private Razon selRazon = new Razon();
    public boolean exitograbar = false;
    private boolean bandContesta = true;
    private boolean bandRazonNoConexion = true;
    private String direccion = "";
    private String idSocio = "";
    private String nombreCompleto = "";
    private String nombreCompletoConyuge = "";
    private String var1[] = {"10,20,30"};
    private String var2[] = {"30,20,10"};
    private SelectItem[] selectItemRazones;
    private List<Object> objRazon;
    SelectItem item = new SelectItem();
    private String codigoCredito = "";
    private String fechaVencimiento = "";
    private String lineaCredito = "";
    private Double montoOriginal;
    private Double montoVencido;
    private Double saldoActual;
    private Double saldoConfirmar;
    private Double saldoCuentaAhorros;
    private String frecuencia = "";
    private String analisisCrediticio = "";
    private String tipoGarantia = "";
    private String descripcionGarantia = "";
    private String estadoOperacion = "";
    private String oficialCobranza = "";
    private String agencia = "";
    private String profesionCliente = "";
    private String correo = "";
    private Integer numeroCuotasVencidas;
    private Integer numeroCuotasPendientes;
    private Integer numeroCuotasTotales;
    private Integer numeroCuotasPagadas;
    private String numeroCuenta = "";
    private BigDecimal montoAdeudaCuota;
    private BigDecimal montoTotalCuota;
    private Integer numeroCuota;
    private String identificacionGarante1;
    private String identificacionGarante2;
    private String nombreGarante1 = "";
    private String nombreGarante2 = "";
    private String direccionGarante1 = "";
    private String direccionGarante2 = "";
    private String nombreConyuge1 = "";
    private String nombreConyuge2 = "";
    private String relacionCliente = "";
    private List<Object> informacionGarantes;
    private Integer i;
    private List<Object> cuotas;
    String nombreUsuario;
    private MakeCall1 objLlamar;
    private LlamadasDAO llamadasDAO = new LlamadasDAO();

    public CtrPersonas() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
    }
    
    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public boolean grabar() {

        boolean exito = false;
        //Pongo los datos para la grabacion:


//        if (parametroSession.getEsNuevo()) {
//            //Pongo Parametros de Auditoria:
//            selPersona.setEstado(1);
//            exito = personasHelper.savePersona(selPersona, 1);
//
//        } else {
//            exito = personasHelper.savePersona(selPersona, 0);
//
//        }

        //mostrarExito();
        return exito;
    }

    //Configurar el pop Confirmacion
    public void confPop() {
        banderaUna = false;

        setMensajeConfirmacion("Seguro desea registrar esta persona");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    //Configurar Pop Eliminar
    public void confPopEliminar() {
        banderaUna = false;

        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public void cargarColumnasGrupo() {


        listaColumnas.put("nombresCompletosSocio", "Nombre");
        listaColumnas.put("direccionPrincipal", "Direccion");
        listaColumnas.put("mail", "E-mail");
        listaColumnas.put("estado", "Estado");




    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmPersona";
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        session.setTituloSeccion("Editar");

        try {
//            if (((Persona) parametroSession.getObjeto()).getId() > 0) {
//                parametroSession.setBoton("btnActualizar");
//                return "frmPersona";
//            } else {
            return "";
//            }
        } catch (Exception e) {
            return "";
        }
    }

//    @Override
//    public void imprimir() {
//        GeneraReporte r = new GeneraReporte();
//        Map parametros = new HashMap();
//        String logoUniversidad = FacesUtils.getExternalContext().getRealPath("/resources/Imagenes/EscudoUniversidad.jpg");
//        parametros.put("LOGO", logoUniversidad);
//        parametros.put("USUARIO", parametroSession.getPersonaLogueado().getNombres());
//        parametros.put("SISTEMA", msgs.getValue("sistema"));
//        parametros.put("NOMREPORTE", msgs.getValue("rptAmbitos"));
//        r.generaPdfListado(listaObjetos, "rptAmbito", this.getClass(), parametros, "ambitos");
//    }
    @Override
    public boolean eliminar() {
        boolean exito = false;
//        listaErrores = new ArrayList<ClaseGeneral>();
//        List<Persona> listaAeliminar = sacarSeleccionado();
//
//        if (listaAeliminar.size() > 0) {
//            listaErrores = personasHelper.eliminarPersonas(listaAeliminar, listaObjetos);
//        }
//
//        if (listaErrores.isEmpty()) {
//            exito = true;
//            cargarPersonas();
//        }

        return exito;

    }

    public void abrirPopup() {
    }
//
//    List<ClaseGeneral> transformarIntitucionesGeneral(List<Persona> listado) {
//        List<ClaseGeneral> listaGen = new ArrayList<ClaseGeneral>();
//        for (Persona objeto : listado) {
//            ClaseGeneral cg = new ClaseGeneral();
//            cg.setCampo1(objeto.getDescripcion());
//            listaGen.add(cg);
//        }
//        return listaGen;
//    }

    public void cerrarSesion() {
        session.closeSession();
        try {
            String url = "/CentroSurCM/faces/index.xhtml";
            FacesContext faces = FacesContext.getCurrentInstance();
            ExternalContext context = faces.getExternalContext();
            context.redirect(url);
            
            faces.responseComplete();
            Runtime r = Runtime.getRuntime();
            r.gc();
            session.setIsSessionOn(false);            
        } catch (IOException ex) {
            Logger.getLogger("No se pudo cerrar sesión");
        }
    }

    @Override
    public String refrescar() {
        whereSql = "";
        //cargarPersonas();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public String regresar() {
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmLstPersonas";
    }

    @Override
    public void auditoria() {
//        try {
//            selPersona = (Persona) objetoSeleccionado;
//        } catch (Exception e) {
//            selPersona.setId(0);
//        }
//
//
//        if (selPersona.getId() > 0) {
//            creado = selPersona.getNombresCompletosSocio();
//            setCreado(creado);
//            fechacreacion = new Date().toString();
//            setActualizado("");
//            setFechaactualizacion("");
//            mostrarAuditoriaVal = true;
//            mostrarAuditoria();
//
//        }
        //return "";
    }

//    public Persona getSelPersona() {
//        return selPersona;
//    }
//
//    public void setSelPersona(Persona selPersona) {
//        this.selPersona = selPersona;
//    }
    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void buscarWsql() {
//        List<Persona> respuesta = new ArrayList<Persona>();
//        parametroSession.setNumPaginas(1);
//        listaObjetos.clear();
//        //Realizo una busqueda en el vector localmente
//        if (!whereSql.equals("")) {
//            for (Object obj : listaBaseObjetos) {
//                Persona cl = (Persona) obj;
//                if (cl.getNombresCompletosSocio().toUpperCase().contains(whereSql.toUpperCase())) {
//
//                    respuesta.add(cl);
//                }
//            }
//
//            listaObjetos.addAll(respuesta);
//        } else {
//            listaObjetos.addAll(listaBaseObjetos);
//        }
    }
    //Busquedas
    private String nombresCompletosSocio;
    private String direccionPrincipal;
    private String mail;
    private String estado;

    public String getNombresCompletosSocio() {
        return nombresCompletosSocio;
    }

    public void setNombresCompletosSocio(String nombresCompletosSocio) {
        this.nombresCompletosSocio = nombresCompletosSocio;
    }

    public String getDireccionPrincipal() {
        return direccionPrincipal;
    }

    public void setDireccionPrincipal(String direccionPrincipal) {
        this.direccionPrincipal = direccionPrincipal;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public boolean isBandRazonNoConexion() {
        return bandRazonNoConexion;
    }

    public void setBandRazonNoConexion(boolean bandRazonNoConexion) {
        this.bandRazonNoConexion = bandRazonNoConexion;
    }

    public boolean isBandContesta() {
        return bandContesta;
    }

    public void setContesta(boolean bandContesta) {
        this.bandContesta = bandContesta;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(String idSocio) {
        this.idSocio = idSocio;
    }

    public String getNombreCompleto() {
        if (parametroSession.getUsuario() != null) {
            return parametroSession.getUsuario().getNombre();
        } else {
            return "Invitado";
        }
        
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getNombreCompletoConyuge() {
        return nombreCompletoConyuge;
    }

    public void setNombreCompletoConyuge(String nombreCompletoConyuge) {
        this.nombreCompletoConyuge = nombreCompletoConyuge;
    }

    public String[] getVar1() {
        return var1;
    }

    public void setVar1(String[] var1) {
        this.var1 = var1;
    }

    public String[] getVar2() {
        return var2;
    }

    public void setVar2(String[] var2) {
        this.var2 = var2;
    }

    public SelectItem[] getSelectItemRazones() {
        return selectItemRazones;
    }

    public void setSelectItemRazones(SelectItem[] selectItemRazones) {
        this.selectItemRazones = selectItemRazones;
    }

    public List<Object> getObjRazon() {
        return objRazon;
    }

    public void setObjRazon(List<Object> objRazon) {
        this.objRazon = objRazon;
    }

    public Llamada getSelLlamada() {
        return selLlamada;
    }

    public void setSelLlamada(Llamada selLlamada) {
        this.selLlamada = selLlamada;
    }

//    public Credito getSelCredito() {
//        return selCredito;
//    }
//
//    public void setSelCredito(Credito selCredito) {
//        this.selCredito = selCredito;
//    }

    /**
     * @return the codigoCredito
     */
    public String getCodigoCredito() {
        return codigoCredito;
    }

    /**
     * @param codigoCredito the codigoCredito to set
     */
    public void setCodigoCredito(String codigoCredito) {
        this.codigoCredito = codigoCredito;
    }

    /**
     * @return the fechaVencimiento
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * @param fechaVencimiento the fechaVencimiento to set
     */
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @return the lineaCredito
     */
    public String getLineaCredito() {
        return lineaCredito;
    }

    /**
     * @param lineaCredito the lineaCredito to set
     */
    public void setLineaCredito(String lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    /**
     * @return the montoOriginal
     */
    public Double getMontoOriginal() {
        return montoOriginal;
    }

    /**
     * @param montoOriginal the montoOriginal to set
     */
    public void setMontoOriginal(Double montoOriginal) {
        this.montoOriginal = montoOriginal;
    }

    /**
     * @return the montoVencido
     */
    public Double getMontoVencido() {
        return montoVencido;
    }

    /**
     * @param montoVencido the montoVencido to set
     */
    public void setMontoVencido(Double montoVencido) {
        this.montoVencido = montoVencido;
    }

    /**
     * @return the saldoActual
     */
    public Double getSaldoActual() {
        return saldoActual;
    }

    /**
     * @param saldoActual the saldoActual to set
     */
    public void setSaldoActual(Double saldoActual) {
        this.saldoActual = saldoActual;
    }

    /**
     * @return the saldoConfirmar
     */
    public Double getSaldoConfirmar() {
        return saldoConfirmar;
    }

    /**
     * @param saldoConfirmar the saldoConfirmar to set
     */
    public void setSaldoConfirmar(Double saldoConfirmar) {
        this.saldoConfirmar = saldoConfirmar;
    }

    /**
     * @return the saldoCuentaAhorros
     */
    public Double getSaldoCuentaAhorros() {
        return saldoCuentaAhorros;
    }

    /**
     * @param saldoCuentaAhorros the saldoCuentaAhorros to set
     */
    public void setSaldoCuentaAhorros(Double saldoCuentaAhorros) {
        this.saldoCuentaAhorros = saldoCuentaAhorros;
    }

    /**
     * @return the frecuencia
     */
    public String getFrecuencia() {
        return frecuencia;
    }

    /**
     * @param frecuencia the frecuencia to set
     */
    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    /**
     * @return the analisisCrediticio
     */
    public String getAnalisisCrediticio() {
        return analisisCrediticio;
    }

    /**
     * @param analisisCrediticio the analisisCrediticio to set
     */
    public void setAnalisisCrediticio(String analisisCrediticio) {
        this.analisisCrediticio = analisisCrediticio;
    }

    /**
     * @return the tipoGarantia
     */
    public String getTipoGarantia() {
        return tipoGarantia;
    }

    /**
     * @param tipoGarantia the tipoGarantia to set
     */
    public void setTipoGarantia(String tipoGarantia) {
        this.tipoGarantia = tipoGarantia;
    }

    /**
     * @return the descripcionGarantia
     */
    public String getDescripcionGarantia() {
        return descripcionGarantia;
    }

    /**
     * @param descripcionGarantia the descripcionGarantia to set
     */
    public void setDescripcionGarantia(String descripcionGarantia) {
        this.descripcionGarantia = descripcionGarantia;
    }

    /**
     * @return the estadoOperacion
     */
    public String getEstadoOperacion() {
        return estadoOperacion;
    }

    /**
     * @param estadoOperacion the estadoOperacion to set
     */
    public void setEstadoOperacion(String estadoOperacion) {
        this.estadoOperacion = estadoOperacion;
    }

    /**
     * @return the oficialCobranza
     */
    public String getOficialCobranza() {
        return oficialCobranza;
    }

    /**
     * @param oficialCobranza the oficialCobranza to set
     */
    public void setOficialCobranza(String oficialCobranza) {
        this.oficialCobranza = oficialCobranza;
    }

    /**
     * @return the agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * @param agencia the agencia to set
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * @return the profesionCliente
     */
    public String getProfesionCliente() {
        return profesionCliente;
    }

    /**
     * @param profesionCliente the profesionCliente to set
     */
    public void setProfesionCliente(String profesionCliente) {
        this.profesionCliente = profesionCliente;
    }

    /**
     * @return the correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * @param correo the correo to set
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * @return the numeroCuotasVencidas
     */
    public Integer getNumeroCuotasVencidas() {
        return numeroCuotasVencidas;
    }

    /**
     * @param numeroCuotasVencidas the numeroCuotasVencidas to set
     */
    public void setNumeroCuotasVencidas(Integer numeroCuotasVencidas) {
        this.numeroCuotasVencidas = numeroCuotasVencidas;
    }

    /**
     * @return the numeroCuotasPendientes
     */
    public Integer getNumeroCuotasPendientes() {
        return numeroCuotasPendientes;
    }

    /**
     * @param numeroCuotasPendientes the numeroCuotasPendientes to set
     */
    public void setNumeroCuotasPendientes(Integer numeroCuotasPendientes) {
        this.numeroCuotasPendientes = numeroCuotasPendientes;
    }

    /**
     * @return the numeroCuotasTotales
     */
    public Integer getNumeroCuotasTotales() {
        return numeroCuotasTotales;
    }

    /**
     * @param numeroCuotasTotales the numeroCuotasTotales to set
     */
    public void setNumeroCuotasTotales(Integer numeroCuotasTotales) {
        this.numeroCuotasTotales = numeroCuotasTotales;
    }

    /**
     * @return the numeroCuotasPagadas
     */
    public Integer getNumeroCuotasPagadas() {
        return numeroCuotasPagadas;
    }

    /**
     * @param numeroCuotasPagadas the numeroCuotasPagadas to set
     */
    public void setNumeroCuotasPagadas(Integer numeroCuotasPagadas) {
        this.numeroCuotasPagadas = numeroCuotasPagadas;
    }

    /**
     * @return the numeroCuenta
     */
    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * @param numeroCuenta the numeroCuenta to set
     */
    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    /**
     * @return the montoAdeudaCuota
     */
    public BigDecimal getMontoAdeudaCuota() {
        return montoAdeudaCuota;
    }

    /**
     * @param montoAdeudaCuota the montoAdeudaCuota to set
     */
    public void setMontoAdeudaCuota(BigDecimal montoAdeudaCuota) {
        this.montoAdeudaCuota = montoAdeudaCuota;
    }

    /**
     * @return the montoTotalCuota
     */
    public BigDecimal getMontoTotalCuota() {
        return montoTotalCuota;
    }

    /**
     * @param montoTotalCuota the montoTotalCuota to set
     */
    public void setMontoTotalCuota(BigDecimal montoTotalCuota) {
        this.montoTotalCuota = montoTotalCuota;
    }

    /**
     * @return the numeroCuota
     */
    public Integer getNumeroCuota() {
        return numeroCuota;
    }

    /**
     * @param numeroCuota the numeroCuota to set
     */
    public void setNumeroCuota(Integer numeroCuota) {
        this.numeroCuota = numeroCuota;
    }

    /**
     * @return the identificacionGarante1
     */
    public String getIdentificacionGarante1() {
        return identificacionGarante1;
    }

    /**
     * @param identificacionGarante1 the identificacionGarante1 to set
     */
    public void setIdentificacionGarante1(String identificacionGarante1) {
        this.identificacionGarante1 = identificacionGarante1;
    }

    /**
     * @return the identificacionGarante2
     */
    public String getIdentificacionGarante2() {
        return identificacionGarante2;
    }

    /**
     * @param identificacionGarante2 the identificacionGarante2 to set
     */
    public void setIdentificacionGarante2(String identificacionGarante2) {
        this.identificacionGarante2 = identificacionGarante2;
    }

    /**
     * @return the nombreGarante1
     */
    public String getNombreGarante1() {
        return nombreGarante1;
    }

    /**
     * @param nombreGarante1 the nombreGarante1 to set
     */
    public void setNombreGarante1(String nombreGarante1) {
        this.nombreGarante1 = nombreGarante1;
    }

    /**
     * @return the nombreGarante2
     */
    public String getNombreGarante2() {
        return nombreGarante2;
    }

    /**
     * @param nombreGarante2 the nombreGarante2 to set
     */
    public void setNombreGarante2(String nombreGarante2) {
        this.nombreGarante2 = nombreGarante2;
    }

    /**
     * @return the direccionGarante1
     */
    public String getDireccionGarante1() {
        return direccionGarante1;
    }

    /**
     * @param direccionGarante1 the direccionGarante1 to set
     */
    public void setDireccionGarante1(String direccionGarante1) {
        this.direccionGarante1 = direccionGarante1;
    }

    /**
     * @return the direccionGarante2
     */
    public String getDireccionGarante2() {
        return direccionGarante2;
    }

    /**
     * @param direccionGarante2 the direccionGarante2 to set
     */
    public void setDireccionGarante2(String direccionGarante2) {
        this.direccionGarante2 = direccionGarante2;
    }

    /**
     * @return the nombreConyuge1
     */
    public String getNombreConyuge1() {
        return nombreConyuge1;
    }

    /**
     * @param nombreConyuge1 the nombreConyuge1 to set
     */
    public void setNombreConyuge1(String nombreConyuge1) {
        this.nombreConyuge1 = nombreConyuge1;
    }

    /**
     * @return the nombreConyuge2
     */
    public String getNombreConyuge2() {
        return nombreConyuge2;
    }

    /**
     * @param nombreConyuge2 the nombreConyuge2 to set
     */
    public void setNombreConyuge2(String nombreConyuge2) {
        this.nombreConyuge2 = nombreConyuge2;
    }

    /**
     * @return the relacionCliente
     */
    public String getRelacionCliente() {
        return relacionCliente;
    }

    /**
     * @param relacionCliente the relacionCliente to set
     */
    public void setRelacionCliente(String relacionCliente) {
        this.relacionCliente = relacionCliente;
    }

    /**
     * @return the informacionGarantes
     */
    public List<Object> getInformacionGarantes() {
        return informacionGarantes;
    }

    /**
     * @param informacionGarantes the informacionGarantes to set
     */
    public void setInformacionGarantes(List<Object> informacionGarantes) {
        this.informacionGarantes = informacionGarantes;
    }

    /**
     * @return the cuotas
     */
    public List<Object> getCuotas() {
        return cuotas;
    }

    /**
     * @param cuotas the cuotas to set
     */
    public void setCuotas(List<Object> cuotas) {
        this.cuotas = cuotas;
    }

    public String getNombreUsuario() {
        if (parametroSession.getUsuario() != null) {
            return parametroSession.getUsuario().getNick();
        } else {
            return "Invitado";
        }
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
}
