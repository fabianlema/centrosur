/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.FacDAO;
import ec.com.centrosurcall.datos.modelo.Fac;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */

@ManagedBean
@ViewScoped
public final class CtrFac extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Fac selFac = new Fac();
    FacDAO facDAO = new FacDAO();
    String opcionBoton = "";
    private SelectItem[] selectItemTipos;
    private Boolean atriEditable = false, esGuion = false;

    public CtrFac() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarParametros();
        if (parametroSession.getEsNuevo()) {
            atriEditable = false;
        }
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            atriEditable = true;
            cargoParametrosEditar();
        }
        setMensajeConfirmacion("Seguro desea grabar este Codigo?");
        confPop();
    }

    public void cargarParametros() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Fac> listaParametros = facDAO.getFac();
        if (listaParametros != null) {
            listaBaseObjetos.addAll(listaParametros);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargoParametrosEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Fac")) {
                if (((Fac) session.getObjeto()).getId() > 0) {
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selFac = (Fac) session.getObjeto();            
            atriEditable=false;            
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Codigo de llamada entrante");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            //selFac.setEstado(Boolean.TRUE);
            exito = facDAO.saveFac(selFac, 1);
            parametroSession.setEsNuevo(false);
        } else {
            exito = facDAO.saveFac(selFac, 0);
            parametroSession.setEsEditar(false);
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Fac> respuesta = new ArrayList<Fac>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Fac cl = (Fac) obj;
                if (cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        listaColumnas.put("codigo", "Codigo");
        listaColumnas.put("descripcion", "Descripcion");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionCodigoIVR";
    }

    @Override
    public void auditoria() {
        try {
            selFac = (Fac) objetoSeleccionado;
        } catch (Exception e) {
            selFac.setId(0);
        }
        if (selFac.getId() > 0) {
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarParametros();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Fac> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Fac) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmFac";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmFac";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Fac> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = facDAO.eliminarFac(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarParametros();
        }
        return exito;
    }

    public List<Fac> sacarSeleccionado() {
        Fac selectParametros = new Fac();
        List<Fac> listadoSeleccionado = new ArrayList<Fac>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectParametros = (Fac) o;
            if (selectParametros.getSeleccionado()) {
                listadoSeleccionado.add(selectParametros);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Fac getSelFac() {
        return selFac;
    }

    public void setSelFac(Fac selFac) {
        this.selFac = selFac;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }

    public SelectItem[] getSelectItemTipos() {
        return selectItemTipos;
    }

    public void setSelectItemTipos(SelectItem[] selectItemTipos) {
        this.selectItemTipos = selectItemTipos;
    }
    private String id;
    private String codigo;
    private String descripcion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getAtriEditable() {
        return atriEditable;
    }

    public void setAtriEditable(Boolean atriEditable) {
        this.atriEditable = atriEditable;
    }

    public Boolean getEsGuion() {
        return esGuion;
    }

    public void setEsGuion(Boolean esGuion) {
        this.esGuion = esGuion;
    }
}
