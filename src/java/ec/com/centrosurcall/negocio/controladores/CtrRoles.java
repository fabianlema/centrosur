/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import ec.com.centrosurcall.datos.DAO.DatosCorreoDAO;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.RolesDAO;
import ec.com.centrosurcall.datos.DAO.ModulosDAO;
import ec.com.centrosurcall.datos.modelo.DatosCorreo;
import ec.com.centrosurcall.datos.modelo.DcorreoRol;
import ec.com.centrosurcall.datos.modelo.DcorreoRolId;
import ec.com.centrosurcall.datos.modelo.DetalleMod;
import ec.com.centrosurcall.datos.modelo.Modulo;
import ec.com.centrosurcall.datos.modelo.ModuloId;
import ec.com.centrosurcall.datos.modelo.Rol;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrRoles extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Rol selRol = new Rol();
    RolesDAO RolesDAO = new RolesDAO();
    ModulosDAO ModulosDAO = new ModulosDAO();
    DatosCorreoDAO datosCorreoDAO = new DatosCorreoDAO();
    String opcionBoton = "";
    private List<Object> objModulo;
    private List<Rol> objModulo2 = new ArrayList<Rol>();
    private List<DatosCorreo> objModulo3 = new ArrayList<DatosCorreo>();
    private SelectItem selectItemModulos[];
    private SelectItem selectItemCorreos[];

    public CtrRoles() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarRoles();        
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoRolEditar();
        }
        cargarCorreos();
        cargarModulos();
        setMensajeConfirmacion("Seguro desea grabar este Rol?");
        confPop();
    }

    public void cargarRoles() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Rol> listaRol = RolesDAO.getRoles();
        if (listaRol != null) {
            listaBaseObjetos.addAll(listaRol);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargarModulos() {
        objModulo = consulta.getListHql(null, "Modulo", "estado = 1 order by nombre", null);
        if (objModulo != null && objModulo.size() > 0) {
            int cont = 0;
            selectItemModulos = new SelectItem[objModulo.size()];
            for (Object obj : objModulo) {
                Modulo cl = (Modulo) obj;
                selectItemModulos[cont] = new SelectItem(cl.getId().getIdPadre() + "" + cl.getId().getIdHijo(), cl.getNombre(), String.valueOf(cl.getId().getIdPadre()));
                cont++;
            }
        }
    }
    
    public void cargarCorreos() {
        objModulo = consulta.getListHql(null, "DatosCorreo", "estado = 1 order by usermail", null);
        if (objModulo != null && objModulo.size() > 0) {
            int cont = 0;
            selectItemCorreos = new SelectItem[objModulo.size()];
            for (Object obj : objModulo) {
                DatosCorreo cl = (DatosCorreo) obj;
                selectItemCorreos[cont] = new SelectItem(cl.getId(), cl.getUsermail(), cl.getUsermail());
                cont++;
            }
        }
    }

    public void cargoRolEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Rol")) {
                if (((Rol) session.getObjeto()).getId() > 0) {
                    Integer idRol = ((Rol) session.getObjeto()).getId();
                    objModulo2 = consulta.getListSql("select concat(a.idPadre, a.idHijo) "
                                                    + "from dbo.modulo a, dbo.detalle_mod b, dbo.rol c"
                                                    + " where b.idRol=" + idRol + " and b.idRol=c.id "
                                                    + "and b.idPadre=a.idPadre and b.idHijo=a.idHijo and b.estado=1");
                    objModulo3 = consulta.getListSql("select a.id from datos_correo a, dcorreo_rol b where b.idRol=" + idRol + " and a.id=b.idDcorreo and a.estado=1");
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selRol = (Rol) session.getObjeto();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Rol");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        Integer idPadre=0;
        Integer idHijo=0;
        if (parametroSession.getEsNuevo()) {
            selRol.setEstado(true);
            exito = RolesDAO.saveRol(selRol, 1);
            if (exito) {
                List maxId;
                maxId = consulta.getListSql("select max(id) from dbo.rol");
                for (int i = 0; i <= objModulo2.size() - 1; i++) {
                    Rol rol = new Rol();
                    Modulo modulo = new Modulo();
                    ModuloId moduloId = new ModuloId();
                    rol.setId(Integer.valueOf(String.valueOf(maxId.get(0))));
                    DetalleMod detalle = new DetalleMod();
                    String prueba = String.valueOf(objModulo2.get(i));
                    if(prueba.length()==3){
                        idPadre = Integer.valueOf(prueba.substring(0, 1));
                        idHijo = Integer.valueOf(prueba.substring(1, 3));
                    }else if(prueba.length()==2){
                        idPadre = Integer.valueOf(prueba.substring(0, 1));
                        idHijo = Integer.valueOf(prueba.substring(1, 2));
                    }
                    moduloId.setIdPadre(idPadre);
                    moduloId.setIdHijo(idHijo);
                    detalle.setEstado(1);
                    detalle.setTipoPermiso("1");
                    detalle.setRol(rol);
                    modulo.setId(moduloId);
                    detalle.setModulo(modulo);
                    ModulosDAO.saveDetModulo(detalle, 1);
                }
                for (int i = 0; i <= objModulo3.size() - 1; i++) {
                    DcorreoRol algo = new DcorreoRol();
                    DcorreoRolId asd = new DcorreoRolId();
                    asd.setIdDcorreo(Integer.valueOf(String.valueOf(objModulo3.get(i))));
                    asd.setIdRol(Integer.valueOf(String.valueOf(maxId.get(0))));
                    algo.setId(asd);
                    datosCorreoDAO.saveDcorreoRol(algo, 1);
                }
            }

        } else {
            exito = RolesDAO.saveRol(selRol, 0);
            if (exito) {
                consulta.getListSql("delete from dbo.detalle_mod where idRol=" + selRol.getId());
                for (int i = 0; i <= objModulo2.size() - 1; i++) {
                    Rol rol = new Rol();
                    Modulo modulo = new Modulo();
                    ModuloId moduloId = new ModuloId();
                    rol.setId(selRol.getId());
                    DetalleMod detalle = new DetalleMod();
                    String prueba = String.valueOf(objModulo2.get(i));
                    if(prueba.length()==3){
                        idPadre = Integer.valueOf(prueba.substring(0, 1));
                        idHijo = Integer.valueOf(prueba.substring(1, 3));
                    }else if(prueba.length()==2){
                        idPadre = Integer.valueOf(prueba.substring(0, 1));
                        idHijo = Integer.valueOf(prueba.substring(1, 2));
                    }
                    moduloId.setIdPadre(idPadre);
                    moduloId.setIdHijo(idHijo);
                    detalle.setEstado(1);
                    detalle.setTipoPermiso("1");
                    detalle.setRol(rol);
                    modulo.setId(moduloId);
                    detalle.setModulo(modulo);
                    ModulosDAO.saveDetModulo(detalle, 1);
                }
                consulta.getListSql("delete from dbo.dcorreo_rol where idRol=" + selRol.getId());
                for (int i = 0; i <= objModulo3.size() - 1; i++) {
                    DcorreoRol algo = new DcorreoRol();
                    DcorreoRolId asd = new DcorreoRolId();
                    asd.setIdDcorreo(Integer.valueOf(String.valueOf(objModulo3.get(i))));
                    asd.setIdRol(selRol.getId());
                    algo.setId(asd);
                    datosCorreoDAO.saveDcorreoRol(algo, 1);
                }
            }
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Rol> respuesta = new ArrayList<Rol>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Rol cl = (Rol) obj;
                if (cl.getNombreRol().toUpperCase().contains(whereSql.toUpperCase()) || cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
      //  listaColumnas.put("id", "Id");
        listaColumnas.put("nombreRol", "Nombre");
        listaColumnas.put("descripcion", "Descripción");
    }

    @Override
    public String regresar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "frmAdministracionRoles";
    }

    @Override
    public void auditoria() {
        try {
            selRol = (Rol) objetoSeleccionado;
        } catch (Exception e) {
            selRol.setId(0);
        }
        if (selRol.getId() > 0) {
            creado = selRol.getNombreRol();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        whereSql = "";
        cargarRoles();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Rol> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Rol) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmRoles";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmRoles";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Rol> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = RolesDAO.eliminarRoles(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarRoles();
        }
        return exito;
    }

    public List<Rol> sacarSeleccionado() {
        Rol selectRol = new Rol();
        List<Rol> listadoSeleccionado = new ArrayList<Rol>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectRol = (Rol) o;
            if (selectRol.getSeleccionado()) {
                listadoSeleccionado.add(selectRol);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Rol getSelRol() {
        return selRol;
    }

    public void setSelRol(Rol selRol) {
        this.selRol = selRol;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String nombreRol;
    private String descripcion;
    private String estado;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Object> getObjModulo() {
        return objModulo;
    }

    public void setObjModulo(List<Object> objModulo) {
        this.objModulo = objModulo;
    }

    public SelectItem[] getSelectItemModulos() {
        return selectItemModulos;
    }

    public void setSelectItemModulos(SelectItem[] selectItemModulos) {
        this.selectItemModulos = selectItemModulos;
    }

    public List<Rol> getObjModulo2() {
        return objModulo2;
    }

    public void setObjModulo2(List<Rol> objModulo2) {
        this.objModulo2 = objModulo2;
    }

    public List<DatosCorreo> getObjModulo3() {
        return objModulo3;
    }

    public void setObjModulo3(List<DatosCorreo> objModulo3) {
        this.objModulo3 = objModulo3;
    }

    public SelectItem[] getSelectItemCorreos() {
        return selectItemCorreos;
    }

    public void setSelectItemCorreos(SelectItem[] selectItemCorreos) {
        this.selectItemCorreos = selectItemCorreos;
    }
}
