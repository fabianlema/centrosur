/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.negocio.controladores;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;
import ec.com.centrosurcall.datos.DAO.TeamsDAO;
import ec.com.centrosurcall.datos.modelo.Cola;
import ec.com.centrosurcall.datos.modelo.Team;
import ec.com.centrosurcall.datos.modelo.TeamCola;
import ec.com.centrosurcall.datos.modelo.TeamColaId;
import ec.com.centrosurcall.datos.modelo.TeamUsuario;
import ec.com.centrosurcall.datos.modelo.TeamUsuarioId;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ClaseGeneral;
import javax.faces.model.SelectItem;

/**
 *
 * @author Paul Cabrera
 */
@ManagedBean
@ViewScoped
public final class CtrTeams extends MantenimientoGenerico implements MantenimientoInterface, Serializable {

    CtrSession session;
    private Team selTeam = new Team();
    TeamsDAO teamsDAO = new TeamsDAO();
    String opcionBoton = "";
    private List<Object> objUsuario;
    private List<Object[]> objSupervisores;
    private List<Team> objUsuario2 = new ArrayList<Team>();
    private SelectItem[] selectItemUsuarios;
    private List<Object> objCola;
    private List<Team> objCola2 = new ArrayList<Team>();
    private SelectItem[] selectItemColas;
    private SelectItem[] selectItemSupervisores;
    private Integer supSeleccionado;
     SelectItem item = new SelectItem();

    public CtrTeams() {
        session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
        setOpcionBoton("Grabar");
        session.setTituloSeccion("Listado");
        cargarColumnasGrupo();
        cargarTeams();
        cargarUsuarios();
        cargarSupervisores();
        cargarColas();
        
        if (parametroSession.getEsEditar() && parametroSession.getObjeto() != null) {
            cargoTeamEditar();
            
        }
        setMensajeConfirmacion("Seguro desea grabar este Team?");
        confPop();
    }

    public void cargarUsuarios() {
        objUsuario = consulta.getListHql(null, "Usuario", "estado = 1 order by nick", null);
        if (objUsuario != null && objUsuario.size() > 0) {
            int cont = 0;
            selectItemUsuarios = new SelectItem[objUsuario.size()];
            for (Object obj : objUsuario) {
                Usuario cl = (Usuario) obj;
                selectItemUsuarios[cont] = new SelectItem(cl.getId(), cl.getNick());
                cont++;
            }
        }
    }
    
    public void cargarSupervisores(){
        item.setLabel("-SELECCIONE-");
        item.setValue(1);
        objSupervisores = consulta.getListSql("select a.idRol,b.id, b.nombre from rol_usuario a, usuario b where a.idUsuario=b.id and idRol=3 order by idUsuario");
        if (objSupervisores != null && objSupervisores.size() > 0) {
            int cont = 1;
            selectItemSupervisores = new SelectItem[objSupervisores.size() + 1];
            selectItemSupervisores[0] = item;
            //selectItemSupervisores = new SelectItem[objSupervisores.size()];
            for (Object[] obj : objSupervisores) {
                
                selectItemSupervisores[cont] = new SelectItem(String.valueOf(obj[1]),String.valueOf(obj[2]));
                cont++;
            }
        }
    }

    public void cargarColas() {
        objCola = consulta.getListHql(null, "Cola", "activo=1", null);
        if (objCola != null && objCola.size() > 0) {
            int cont = 0;
            selectItemColas = new SelectItem[objCola.size()];
            for (Object obj : objCola) {
                Cola cl = (Cola) obj;
                selectItemColas[cont] = new SelectItem(cl.getId(), cl.getNombre());
                cont++;
            }
        }
    }

    public void cargarTeams() {
        listaBaseObjetos.clear();
        listaObjetos.clear();
        List<Team> listaTeam;
        if(session.getRolUsuario().get(0).getId().getIdRol()==1){
             listaTeam = teamsDAO.getTeams();
        }else{
            listaTeam = teamsDAO.getTeamsByRol(session.getUsuario().getId());
        }
        if (listaTeam != null) {
            listaBaseObjetos.addAll(listaTeam);
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    public void cargoTeamEditar() {
        boolean existeObjeto = false;
        try {
            if (session.getObjeto().getClass().getName().contains("Team")) {
                if (((Team) session.getObjeto()).getId() > 0) {
                    Integer idTeam = ((Team) session.getObjeto()).getId();
                    objUsuario2 = consulta.getListSql("select a.id from dbo.usuario a, dbo.team_usuario b where b.idTeam=" + idTeam + " and b.idUsuario=a.id and estado=1");
                    objCola2 = consulta.getListSql("select a.id from dbo.cola a, dbo.team_cola b where b.idTeam=" + idTeam + " and b.idCola=a.id and activo=1");
                    existeObjeto = true;
                }
            } else {
                parametroSession.setEsEditar(false);
                parametroSession.setEsNuevo(false);
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
        }
        if (existeObjeto) {
            opcionBoton = "Guardar";
            selTeam = (Team) session.getObjeto();
            supSeleccionado=selTeam.getIdSupervisor();
        }
    }

    public void confPop() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro desea registrar el Team");
        setMensajeCorrecto("Registro grabado");
        setMensajeError("Error al Registrar");
    }

    @Override
    public boolean grabar() {
        boolean exito = false;
        if (parametroSession.getEsNuevo()) {
            selTeam.setEstado(1);
            selTeam.setIdSupervisor(supSeleccionado);
            exito = teamsDAO.saveTeam(selTeam, 1);
            if (exito) {
                List maxId;
                maxId = consulta.getListSql("select max(id) from dbo.team");
                for (int i = 0; i <= objUsuario2.size() - 1; i++) {
                    TeamUsuario te_us = new TeamUsuario();
                    TeamUsuarioId te_us_id = new TeamUsuarioId();
                    te_us_id.setIdTeam(Integer.valueOf(String.valueOf(maxId.get(0))));
                    te_us_id.setIdUsuario(Integer.valueOf(String.valueOf(objUsuario2.get(i))));
                    te_us.setId(te_us_id);
                    teamsDAO.saveUsuarioTeam(te_us, 1);
                }
                for (int i = 0; i <= objCola2.size() - 1; i++) {
                    TeamCola te_co = new TeamCola();
                    TeamColaId te_co_id = new TeamColaId();
                    te_co_id.setIdTeam(Integer.valueOf(String.valueOf(maxId.get(0))));
                    te_co_id.setIdCola(Integer.valueOf(String.valueOf(objCola2.get(i))));
                    te_co.setId(te_co_id);
                    teamsDAO.saveColaTeam(te_co, 1);
                }
            }
        } else {
            selTeam.setIdSupervisor(supSeleccionado);
            exito = teamsDAO.saveTeam(selTeam, 0);
            if (exito) {
                consulta.getListSql("delete from dbo.team_usuario where idTeam=" + selTeam.getId());
                for (int i = 0; i <= objUsuario2.size() - 1; i++) {
                    TeamUsuario te_us = new TeamUsuario();
                    TeamUsuarioId te_us_id = new TeamUsuarioId();
                    te_us_id.setIdTeam(selTeam.getId());
                    te_us_id.setIdUsuario(Integer.valueOf(String.valueOf(objUsuario2.get(i))));
                    te_us.setId(te_us_id);
                    teamsDAO.saveUsuarioTeam(te_us, 1);
                }
                consulta.getListSql("delete from dbo.team_cola where idTeam=" + selTeam.getId());
                for (int i = 0; i <= objCola2.size() - 1; i++) {
                    TeamCola te_co = new TeamCola();
                    TeamColaId te_co_id = new TeamColaId();
                    te_co_id.setIdTeam(selTeam.getId());
                    te_co_id.setIdCola(Integer.valueOf(String.valueOf(objCola2.get(i))));
                    te_co.setId(te_co_id);
                    teamsDAO.saveColaTeam(te_co, 1);
                }
            }
        }
        return exito;
    }

    @Override
    public void buscarWsql() {
        List<Team> respuesta = new ArrayList<Team>();
        parametroSession.setNumPaginas(1);
        listaObjetos.clear();
        if (!whereSql.equals("")) {
            for (Object obj : listaBaseObjetos) {
                Team cl = (Team) obj;
                if (cl.getNombre().toUpperCase().contains(whereSql.toUpperCase()) || cl.getDescripcion().toUpperCase().contains(whereSql.toUpperCase())) {
                    respuesta.add(cl);
                }
            }
            listaObjetos.addAll(respuesta);
        } else {
            listaObjetos.addAll(listaBaseObjetos);
        }
    }

    @Override
    public void cargarColumnasGrupo() {
        listaColumnas.put("id", "Id");
        listaColumnas.put("nombre", "Nombre");
        listaColumnas.put("descripcion", "Descripción");
    }

    @Override
    public String regresar() {
        parametroSession.setEsEditar(false);
        parametroSession.setEsNuevo(false);
        session.setObjeto(null);
        session.setTituloSeccion("Listado");
        return "frmAdministracionTeams";
    }

    @Override
    public void auditoria() {
        try {
            selTeam = (Team) objetoSeleccionado;
        } catch (Exception e) {
            selTeam.setId(0);
        }
        if (selTeam.getId() > 0) {
            creado = selTeam.getNombre();
            setCreado(creado);
            fechacreacion = new Date().toString();
            setActualizado("");
            setFechaactualizacion("");
            mostrarAuditoriaVal = true;
            mostrarAuditoria();
        }
    }

    @Override
    public String refrescar() {
        parametroSession.setEsEditar(false);
        parametroSession.setEsNuevo(false);
        whereSql = "";
        cargarTeams();
        session.setSeleccionado(false);
        objetoSeleccionado = new Object();
        return "";
    }

    @Override
    public boolean isExitograbar() {
        return super.isExitograbar();
    }

    @Override
    public void imprimir() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String editar() {
        parametroSession.setEsNuevo(false);
        parametroSession.setEsEditar(true);
        session.setTituloSeccion("Editar");
        List<Team> objetoEditar = sacarSeleccionado();
        if (objetoEditar.size() > 0) {
            parametroSession.setObjeto(objetoEditar.get(0));
            try {
                if (((Team) objetoEditar.get(0)).getId() > 0) {
                    parametroSession.setBoton("btnActualizar");
                    return "frmTeams";
                } else {
                    return "";
                }
            } catch (Exception e) {
                return "";
            }
        } else {
            return "";
        }
    }

    @Override
    public String nuevo() {
        parametroSession.setEsNuevo(true);
        parametroSession.setEsEditar(false);
        parametroSession.setBoton("btnGrabar");
        setOpcionBoton("nuevo");
        session.setTituloSeccion("Nuevo");
        return "frmTeams";
    }

    public void confPopEliminar() {
        banderaUna = false;
        setMensajeConfirmacion("Seguro quiere eliminar?");
        setMensajeCorrecto("Eliminacion exitosa!");
        setMensajeError("Existen errores en:");
    }

    @Override
    public boolean eliminar() {
        parametroSession.setEsEditar(false);
        parametroSession.setEsNuevo(false);
        boolean exito = false;
        listaErrores = new ArrayList<ClaseGeneral>();
        List<Team> listaAeliminar = sacarSeleccionado();
        if (listaAeliminar.size() > 0) {
            listaErrores = teamsDAO.eliminarTeams(listaAeliminar, listaObjetos);
        }
        if (listaErrores.isEmpty()) {
            exito = true;
            cargarTeams();
        }
        return exito;
    }

    public List<Team> sacarSeleccionado() {
        Team selectTeam = new Team();
        List<Team> listadoSeleccionado = new ArrayList<Team>();
        boolean exito = false;
        for (Object o : listaObjetos) {
            selectTeam = (Team) o;
            if (selectTeam.getSeleccionado()) {
                listadoSeleccionado.add(selectTeam);
                exito = true;
            }
        }
        if (exito) {
            return listadoSeleccionado;
        } else {
            return listadoSeleccionado;
        }
    }

    public Team getSelTeam() {
        return selTeam;
    }

    public void setSelTeam(Team selTeam) {
        this.selTeam = selTeam;
    }

    public String getOpcionBoton() {
        return opcionBoton;
    }

    public void setOpcionBoton(String opcionBoton) {
        this.opcionBoton = opcionBoton;
    }
    private String id;
    private String nombre;
    private String descripcion;
    private String estado;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Object> getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(List<Object> objUsuario) {
        this.objUsuario = objUsuario;
    }

    public List<Team> getObjUsuario2() {
        return objUsuario2;
    }

    public void setObjUsuario2(List<Team> objUsuario2) {
        this.objUsuario2 = objUsuario2;
    }

    public SelectItem[] getSelectItemUsuarios() {
        return selectItemUsuarios;
    }

    public void setSelectItemUsuarios(SelectItem[] selectItemUsuarios) {
        this.selectItemUsuarios = selectItemUsuarios;
    }

    public List<Object> getObjCola() {
        return objCola;
    }

    public void setObjCola(List<Object> objCola) {
        this.objCola = objCola;
    }

    public SelectItem[] getSelectItemColas() {
        return selectItemColas;
    }

    public void setSelectItemColas(SelectItem[] selectItemColas) {
        this.selectItemColas = selectItemColas;
    }

    public List<Team> getObjCola2() {
        return objCola2;
    }

    public void setObjCola2(List<Team> objCola2) {
        this.objCola2 = objCola2;
    }

    public Integer getSupSeleccionado() {
        return supSeleccionado;
    }

    public void setSupSeleccionado(Integer supSeleccionado) {
        this.supSeleccionado = supSeleccionado;
    }

    public SelectItem[] getSelectItemSupervisores() {
        return selectItemSupervisores;
    }

    public void setSelectItemSupervisores(SelectItem[] selectItemSupervisores) {
        this.selectItemSupervisores = selectItemSupervisores;
    }
}
