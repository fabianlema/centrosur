
package ec.com.centrosurcall.negocio.campania;

import ec.com.centrosurcall.datos.DAO.ColasDAO;

public class CreacionColaCredito {
    
    /*Variables Globales*/
    public ec.com.centrosurcall.datos.modelo.Cola cola;
    
    /* Variables Locales*/
    private ColasDAO colaDao;
    
    public CreacionColaCredito() {
        colaDao = new ColasDAO();      
    }
    
    // tipo 1 = Dias Vencido
    // tipo 2 = Monto Prestamo
    // tipo 3 = Saldos
    public void crearCola(int actualizacion) {
        if (cola == null) return;
        
        crearCola(cola.getTipo(), actualizacion);
    }
    
    private boolean crearCola(String tipo, int actualizacion) {
        try {
            if (tipo.contains("1") && !tipo.contains("2") && !tipo.contains("3"))              // Nro Dias Vencidos
                return colaDao.setSPCrearCola(cola.getId(), cola.getDesdeTipo2(), cola.getHastaTipo2(), cola.getDesdeTipo1(), cola.getHastaTipo1(), cola.getProducto(), 0, 1, 0, actualizacion, cola.getAutomatico());
            else if (!tipo.contains("1") && tipo.contains("2") && !tipo.contains("3"))       // Monto Prestamo               
                return colaDao.setSPCrearCola(cola.getId(), cola.getDesdeTipo2(), cola.getHastaTipo2(), cola.getDesdeTipo1(), cola.getHastaTipo1(), cola.getProducto(), 1, 0, 0, actualizacion, cola.getAutomatico());
            else if (!tipo.contains("1") && !tipo.contains("2") && tipo.contains("3"))       // Saldos
                return colaDao.setSPCrearCola(cola.getId(), cola.getDesdeTipo2(), cola.getHastaTipo2(), cola.getDesdeTipo1(), cola.getHastaTipo1(), cola.getProducto(), 0, 0, 1, actualizacion, cola.getAutomatico());
            else if (tipo.contains("1") && tipo.contains("2") && !tipo.contains("3"))        // Dias Vencidos con Monto Prestamo
                return colaDao.setSPCrearCola(cola.getId(), cola.getDesdeTipo2(), cola.getHastaTipo2(), cola.getDesdeTipo1(), cola.getHastaTipo1(), cola.getProducto(), 1, 1, 0, actualizacion, cola.getAutomatico());
            else if (tipo.contains("1") && !tipo.contains("2") && tipo.contains("3"))        // Saldos con Dias Vencidos
                return colaDao.setSPCrearCola(cola.getId(), cola.getDesdeTipo2(), cola.getHastaTipo2(), cola.getDesdeTipo1(), cola.getHastaTipo1(), cola.getProducto(), 0, 1, 1, actualizacion, cola.getAutomatico());
            else 
                return false;
        } catch( Exception ex) {
            return false;
        }
    }
}
