package ec.com.centrosurcall.reports;

import ec.com.centrosurcall.datos.conexion.HibSession;
import ec.com.centrosurcall.utils.FacesUtils;
import ec.com.centrosurcall.utils.ResultSetToExcel;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import org.hibernate.Session;

@ManagedBean
public class GenerarReporte
  implements Serializable
{
  public void generaPdf(String nombreReporte, Map parametrosReporte, String nombreReporteGenerar)
  {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
    Session con = null;
    try
    {
      con = HibSession.abrirSesion();
      JasperReport jasperReport = null;
      try {
        //String archivo = FacesUtils.getExternalContext().getRealPath("reporte/" + nombreReporte + ".jasper");
        InputStream archivo = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/reporte/" + nombreReporte + ".jasper");
        jasperReport = (JasperReport)JRLoader.loadObject(archivo);
      }
      catch (Exception e) {
        e.printStackTrace();
      }

      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosReporte, con.connection());

      JRPdfExporter pdfExporter = new JRPdfExporter();
      ByteArrayOutputStream os = new ByteArrayOutputStream();

      pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
      pdfExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, nombreReporteGenerar + ".pdf");

      pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);

      pdfExporter.exportReport();

      response.setContentType("application/pdf");
      response.setHeader("Content-Disposition", "attachment; filename=" + nombreReporteGenerar + ".pdf");

      response.getOutputStream().write(os.toByteArray());
      response.getOutputStream().flush();
      response.getOutputStream().close();
      response.flushBuffer();
    } catch (JRException ex) {
      Logger.getLogger(GeneraReporte.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException e1) {
      e1.printStackTrace();
    } catch (Exception eq) {
      eq.printStackTrace();
    } finally {
      HibSession.cerrarSesion();
    }

    context.responseComplete();
  }

  public void generaXlsx(String nombreReporte, Map parametrosReporte, String nombreReporteGenerar) {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
    Session con = null;
    try
    {
      con = HibSession.abrirSesion();
      JasperReport jasperReport = null;
      try {
        //String archivo = FacesUtils.getExternalContext().getRealPath("reporte/" + nombreReporte + ".jasper");
        InputStream archivo = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/reporte/" + nombreReporte + ".jasper");
        jasperReport = (JasperReport)JRLoader.loadObject(archivo);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosReporte, con.connection());

      JRXlsxExporter xlsxExporter = new JRXlsxExporter();
      ByteArrayOutputStream os = new ByteArrayOutputStream();

      xlsxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
      xlsxExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, nombreReporteGenerar + ".xlsx");

      xlsxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);

      xlsxExporter.exportReport();

      response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      response.setHeader("Content-Disposition", "attachment; filename=" + nombreReporteGenerar + ".xlsx");

      response.getOutputStream().write(os.toByteArray());
      response.getOutputStream().flush();
      response.getOutputStream().close();
      response.flushBuffer();
    } catch (JRException ex) {
      Logger.getLogger(GeneraReporte.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException e1) {
      e1.printStackTrace();
    } catch (Exception eq) {
      eq.printStackTrace();
    } finally {
      HibSession.cerrarSesion();
    }

    context.responseComplete();
  }

  public void generaPdfListado(List lstDatos, String nombreReporte, Map parametrosReporte, String nombreReporteGenerar) {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
    JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lstDatos);
    OutputStream out = null;
    try {
      JasperReport jasperReport = null;
      try {
        //String archivo = FacesUtils.getExternalContext().getRealPath("reporte/" + nombreReporte + ".jasper");
        InputStream archivo = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/reporte/" + nombreReporte + ".jasper");
        jasperReport = (JasperReport)JRLoader.loadObject(archivo);
      }
      catch (Exception e) {
        e.printStackTrace();
      }

      ByteArrayInputStream fichero = null;
      fichero = new ByteArrayInputStream(JasperRunManager.runReportToPdf(jasperReport, parametrosReporte, ds));
      response.setContentType("application/pdf");
      response.setHeader("Content-disposition", "inline; filename=" + nombreReporteGenerar + ".pdf");

      response.setHeader("Cache-Control", "max-age=30");
      response.setHeader("Pragma", "No-cache");
      response.setDateHeader("Expires", 0L);
      out = response.getOutputStream();

      byte[] buf = new byte[1024];
      int len;
      while ((len = fichero.read(buf)) > 0) {
        out.write(buf, 0, len);
      }
      fichero.close();
      out.flush();
      out.close();
    } catch (JRException ex) {
      Logger.getLogger(GeneraReporte.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException e1) {
      e1.printStackTrace();
    } catch (Exception eq) {
      eq.printStackTrace();
    }

    context.responseComplete();
  }

  public void generaXlsxListado(List lstDatos, String nombreReporte, Map parametrosReporte, String nombreReporteGenerar) {
    FacesContext context = FacesContext.getCurrentInstance();
    HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
    JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lstDatos);
    OutputStream out = null;
    try {
      JasperReport jasperReport = null;
      try {
        //String archivo = FacesUtils.getExternalContext().getRealPath("reporte/" + nombreReporte + ".jasper");
        InputStream archivo = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/reporte/" + nombreReporte + ".jasper");
        jasperReport = (JasperReport)JRLoader.loadObject(archivo);
      }
      catch (Exception e) {
        e.printStackTrace();
      }

      JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametrosReporte, ds);

      JRXlsxExporter xlsxExporter = new JRXlsxExporter();
      ByteArrayOutputStream os = new ByteArrayOutputStream();

      xlsxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
      xlsxExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, nombreReporteGenerar + ".xlsx");

      xlsxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);

      xlsxExporter.exportReport();

      response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      response.setHeader("Content-Disposition", "attachment; filename=" + nombreReporteGenerar + ".xlsx");

      response.getOutputStream().write(os.toByteArray());
      response.getOutputStream().flush();
      response.getOutputStream().close();
      response.flushBuffer();
    } catch (JRException ex) {
      Logger.getLogger(GeneraReporte.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException e1) {
      e1.printStackTrace();
    } catch (Exception eq) {
      eq.printStackTrace();
    }

    context.responseComplete();
  }
    
    public void generaXlsxListado(ResultSet tempReportes, String nombreReporte, String nombreReporteGenerar, String desde, String hasta) {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        try {
            ResultSetToExcel resultSetToExcel = new ResultSetToExcel(tempReportes, nombreReporteGenerar);
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename=" + nombreReporteGenerar + ".xls");

            //uncomment this codes if u are want to use servlet output stream
            resultSetToExcel.generate(response.getOutputStream(), desde, hasta);
            response.getOutputStream().flush();
            response.getOutputStream().close();
            response.flushBuffer();
        } catch (JRException ex) {
            Logger.getLogger(GeneraReporte.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (Exception eq) {
            eq.printStackTrace();
        }

        context.responseComplete();
    }
}