
package ec.com.centrosurcall.reports;
/*
import ec.com.centrosurcall.utils.JasperServerClient;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporter;

public class JReport extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String parametros = request.getParameter("parametros");
        String reporte = request.getParameter("reporte");
        int tipoReporte = Integer.valueOf(request.getParameter("tipo")); // 1 = PDF, 2 = XLS
        
        String valores[] = parametros.split("~");
        
        // PDF
        ServletOutputStream servletOutputStream = response.getOutputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // EXCEL
        int bit = 0; 
        File f = null; 
        InputStream in = null; 
        
        try {
            JasperServerClient client = new JasperServerClient();
            Map params = new HashMap();

            String report = "/reports/" + reporte;
            for (String parametro : valores) {
                String param[] = parametro.split("=");
                params.put(param[0], param[1]);
            }
            
            JasperPrint jasperPrint = client.runReport(report, params);

            if (tipoReporte == 1) {
                response.setContentType("application/pdf");
                response.setCharacterEncoding("ISO-8859-1");
                
                JasperExportManager.exportReportToPdfStream(jasperPrint, baos);
                response.setContentLength(baos.size());
                baos.writeTo(servletOutputStream);
            } else {
                String xlsFileName = "LlamadasRealizadasGeneral.xls"; 
                JRXlsExporter exporter = new JRXlsExporter (); 
                exporter.setParameter (JRExporterParameter.JASPER_PRINT, jasperPrint); 
                exporter.setParameter (JRExporterParameter.OUTPUT_FILE_NAME, xlsFileName); 
                exporter.exportReport (); 
                
                f = new File ( xlsFileName ); 
                response.setContentType ("application/vnd.ms-excel"); //Tipo de fichero. 
                response.setHeader ("Content-Disposition", "attachment;filename=\"" + xlsFileName + "\""); //Configurar cabecera http 
                in = new FileInputStream (f); 
                servletOutputStream = response.getOutputStream (); 
                bit = 256; 
                while ((bit)>= 0) 
                { 
                   bit = in.read (); 
                   servletOutputStream.write (bit); 
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            if (tipoReporte == 1) {
                servletOutputStream.flush();
                servletOutputStream.close();
                baos.close();
            } else {
                servletOutputStream.flush (); 
                servletOutputStream.close (); 
                in.close (); 
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Servlet que llama a los reportes de Jasper Server";
    }
}
*/