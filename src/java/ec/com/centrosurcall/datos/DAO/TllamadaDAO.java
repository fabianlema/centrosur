/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.TipoLlamada;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author persona
 */

public class TllamadaDAO {
    
    public TipoLlamada getTipoLlamadaByID(int id) {
        String objeto = "TipoLlamada";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (TipoLlamada) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
          
    public List<TipoLlamada> getTiposLlamada() {
        String objeto = "TipoLlamada";
        String where = "estado!=0 order by codigoIvr";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return  consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    public Integer getTiposLlIvr(int eventoId) {
        Consultas consulta = new Consultas();
        List<TipoLlamada> listaHistorial = new ArrayList<TipoLlamada>();
        try
        {
            String cadenaSQL = "select t.codigo_ivr from evento e inner join tipo_llamada t on e.idfkrazon=t.id where e.id= "+ eventoId;
            List<Object> listaObjetos = consulta.getListSql(cadenaSQL);
            return Integer.parseInt(listaObjetos.get(0).toString());
        }catch (Exception e) {
            System.out.println("Error: " + e);
            return null;
        }
    }
    
    public List<TipoLlamada> getTiposLlamada(int codigoIVR) {
        String objeto = "TipoLlamada";
        String where = "estado!=0 and codigoIvr=" + codigoIVR + " order by codigoIvr";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return  consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveTipoLlamada(TipoLlamada tipoLlamada, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(tipoLlamada, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarTipoLlamada(List<TipoLlamada> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (TipoLlamada elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);
                } else {
                    //Grabacion Correcta
                }
            } catch (Exception e) {
            }
        }
        return listaErrores;
    }    
}