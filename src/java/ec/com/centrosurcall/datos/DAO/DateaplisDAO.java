/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

/**
 *
 * @author fabia
 */

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Dateapli;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DateaplisDAO
{
  List<Object> listaObjetosTemp = new ArrayList();

  public List<Dateapli> getDateapli(int idTarea)
  {
    String objeto = "Dateapli";
    String where = "id = " + idTarea;
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return consulta.getListHql(null, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public List<Dateapli> getDateapliEquipo(int idEquipo)
  {
    String objeto = "Dateapli";
    String where = "idEquipo = " + idEquipo;
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return consulta.getListHql(null, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public List<Dateapli> getDateapliEquipoJoin(int idEquipo)
  {
    Consultas consulta = new Consultas();
    List lisDateapli = new ArrayList();
    try {
      List listaDate = consulta.getListSql("SELECT a.id, CONCAT(t.descripcion, '&', a.descripcion) as descripcion, a.id_equipo, a.anio, a.mes, a.diames, a.hora, a.minuto FROM dateapli as a Inner join tarea as t on (a.id_tarea= t.id) WHERE a.id_equipo = " + idEquipo);
      for (Iterator i$ = listaDate.iterator(); i$.hasNext(); ) { Object o = i$.next();
        Object[] lista = (Object[])(Object[])o;
        String[] descripciones = ((String)lista[1]).split("&");
        Dateapli temp = new Dateapli();
        temp.setId((Integer)lista[0]);
        temp.setDescripcionTarea(descripciones[0]);
        temp.setIdEquipo((Integer)lista[2]);
        temp.setAnio((Integer)lista[3]);
        temp.setMes((Integer)lista[4]);
        temp.setDiames((Integer)lista[5]);
        temp.setHora((Integer)lista[6]);
        temp.setMinuto((Integer)lista[7]);
        temp.setDescripcion(descripciones.length > 1 ? descripciones[1] : "");
        lisDateapli.add(temp);
      }
      return lisDateapli; } catch (Exception e) {
    }
    return null;
  }

  public boolean saveDateapli(Dateapli modulo, int opcion)
  {
    boolean exito = true;
    Consultas consulta = new Consultas();
    int resp = 0;
    resp = consulta.guardar(modulo, opcion);

    if (resp == 0) {
      exito = false;
    }
    return exito;
  }

  public List<ClaseGeneral> eliminarDateaplis(List<Dateapli> objetoEliminar, List<Object> listObject)
  {
    Consultas consulta = new Consultas();
    List listaErrores = new ArrayList();

    for (Dateapli elimina : objetoEliminar) {
      this.listaObjetosTemp.remove(elimina);
      try
      {
        int val = consulta.eliminar(elimina);
        if (val != 1) {
          ClaseGeneral cg = new ClaseGeneral();
          cg.setCampo1(elimina.getId());
          cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
          listaErrores.add(cg);
        }

      }
      catch (Exception e)
      {
      }

    }

    return listaErrores;
  }

  public boolean eliminarDateapli(Dateapli dateapli) {
    Consultas consulta = new Consultas();
    try {
      int val = consulta.eliminar(dateapli);

      return val == 1;
    }
    catch (Exception e)
    {
    }

    return false;
  }
}