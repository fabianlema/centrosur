/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.TipoLlamada;
import ec.com.centrosurcall.negocio.controladores.CtrSession;
import ec.com.centrosurcall.utils.ClaseGeneral;
import ec.com.centrosurcall.utils.FacesUtils;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author persona
 */

@ManagedBean
@ViewScoped
public class TipoLlamadaDAO {
    private List<TipoLlamada> listaTiposLlamada;
    private List<TipoLlamada> tiposLlamada;
        CtrSession session;
    
    public TipoLlamadaDAO(){
             session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
    }
    
    public TipoLlamada getTipoLlamadaByID(int id) {
        String objeto = "TipoLlamada";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (TipoLlamada) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public synchronized  List<TipoLlamada>getTiposLlamada(){
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Runtime r = Runtime.getRuntime();
         Consultas consulta = new Consultas();
        String codigoivr = request.getParameter("codigoivr");
        try {
            Integer.parseInt(codigoivr);
        } catch (NumberFormatException nfe) {
            codigoivr = "1";
        }
         session.setCodigoivr(codigoivr);
         if (tiposLlamada == null){
            tiposLlamada = consulta.getListHql(null, "TipoLlamada", "codigoIvr = " + session.getCodigoivr() + " and estado!=0", null);
         }
        return  tiposLlamada;
    }
    
    public  synchronized List<TipoLlamada> getListaTiposLlamada() {
        String objeto = "TipoLlamada";
        String where = "estado!=0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return  consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveTipoLlamada(TipoLlamada tipoLlamada, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(tipoLlamada, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarObservaciones(List<TipoLlamada> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (TipoLlamada elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            //elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);
                } else {
                    //Grabacion Correcta
                }
            } catch (Exception e) {
            }
        }
        return listaErrores;
    }    
}