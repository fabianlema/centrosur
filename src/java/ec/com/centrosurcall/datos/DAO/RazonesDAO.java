/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Razon;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author persona
 */
public class RazonesDAO {

    public Razon getRazonByID(int id) {
        String objeto = "Razon";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (Razon) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Razon> getRazonesY() {
        String objeto = "Razon";
        String where = "estado!=0 order by nombre" ;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<Razon> getRazones() {
        String objeto = "Razon";
        String where = "estado!=0 and padreId!=0 order by id" ;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveRazon(Razon razon, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(razon, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarRazones(List<Razon> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Razon elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
    
}
