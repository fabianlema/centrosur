/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.ColaChat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author persona
 */
public class ColaChatDAO {

    public ColaChat getBandejaEntradaByID(int id) {
        String objeto = "ColaChat";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (ColaChat) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
   
    public List<ColaChat> getColaChats() {
        String objeto = "ColaChat";
        String where = "estado=0 order by id" ;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    public ColaChat getFirstColaChat() {
        String objeto = "ColaChat";
        String where = "estado=0 and tipo='chat' order by id" ;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            List<ColaChat> temp = consulta.getListHql(null, objeto, where, lisPar);
            if (temp.size()>0) return temp.get(0);
            else return null;
        } catch (Exception e) {
            return null;
        }
    }
    
    public ColaChat getFirstColaTwitter(String tipo) {
        String objeto = "ColaChat";
        String where = "estado=0 and tipo='"+tipo+"' order by id" ;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            List<ColaChat> temp = consulta.getListHql(null, objeto, where, lisPar);
            if (temp.size()>0) return temp.get(0);
            else return null;
        } catch (Exception e) {
            return null;
        }
    }
    
    public ColaChat getFirstColaFacebook() {
        String objeto = "ColaChat";
        String where = "estado=0 and tipo='facebook' order by id" ;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            List<ColaChat> temp = consulta.getListHql(null, objeto, where, lisPar);
            if (temp.size()>0) return temp.get(0);
            else return null;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveColaChat(ColaChat razon, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(razon, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }    
}
