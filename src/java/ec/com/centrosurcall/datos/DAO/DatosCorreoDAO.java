/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.DatosCorreo;
import ec.com.centrosurcall.datos.modelo.DcorreoRol;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TaurusTech
 */
public class DatosCorreoDAO {

    public DatosCorreo getDatosCorreoByID(int id) {
        String objeto = "DatosCorreo";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (DatosCorreo) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<DatosCorreo> getDatosCorreos() {
        String objeto = "DatosCorreo";
        String where = "estado!=0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<DcorreoRol> getDcorreoRoles(String idRol) {
        String objeto = "DcorreoRol";
        String where = "id.idRol = " + idRol;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveDatosCorreo(DatosCorreo DatosCorreo, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(DatosCorreo, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
    public boolean saveDcorreoRol(DcorreoRol dcorreoRol, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(dcorreoRol, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarDatosCorreos(List<DatosCorreo> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (DatosCorreo elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            elimina.setEstado(Boolean.FALSE);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }
            } catch (Exception e) {
            }
        }
        return listaErrores;
    }   
}

