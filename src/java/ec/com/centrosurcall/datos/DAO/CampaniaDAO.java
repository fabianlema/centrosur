/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Campania;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author persona
 */
public class CampaniaDAO {

    public Campania getCampaniaByID(int id) {
        String objeto = "Campania";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (Campania) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

//    public List<Campania> getRazonesY() {
//        String objeto = "Razon";
//        String where = "estado!=0 order by nombre" ;
//        List<ParametrosConsulta> lisPar = new ArrayList();
//        Consultas consulta = new Consultas();
//        try {
//            return consulta.getListHql(null, objeto, where, lisPar);
//        } catch (Exception e) {
//            return null;
//        }
//    }
    
    public List<Campania> getCampanias() {
        String objeto = "Campania";
        String where = "estado!=0" ;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveCampania(Campania razon, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(razon, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarCampanias(List<Campania> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Campania elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            elimina.setEstado(false);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
    
}
