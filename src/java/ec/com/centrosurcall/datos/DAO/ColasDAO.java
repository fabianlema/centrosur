/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Cola;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author persona
 */
public class ColasDAO {

    public Cola getColaByID(int id) {
        String objeto = "Cola";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (Cola) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Cola> getColas() {
        String objeto = "Cola";
        String where = "activo!=0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveCola(Cola cola, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(cola, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarColas(List<Cola> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Cola elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            elimina.setActivo(0);
            elimina.setEstado(1);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
    
    public boolean setSPCrearCola(int idCola, String desdeMonto, String hastaMonto, String desdeDia, String hastaDia, String producto, int monto, int dias, int saldo, int actualizacion, int automatica) {
        Consultas consulta = new Consultas();
        try
        {
            return consulta.getSPCrearCola(idCola, desdeMonto, hastaMonto, desdeDia, hastaDia, producto, monto, dias, saldo, actualizacion, automatica);
        }catch (Exception e) {
            return false;
        }
    }    
}
