/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

/**
 *
 * @author fabia
 */

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Comando;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

public class ComandosDAO
{
  List<Object> listaObjetosTemp = new ArrayList();

  public List<Comando> getComandos(int idTarea)
  {
    String objeto = "Comando";
    String where = "idTarea = " + idTarea;
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return consulta.getListHql(null, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public boolean saveComando(Comando modulo, int opcion)
  {
    boolean exito = true;
    Consultas consulta = new Consultas();
    int resp = 0;
    resp = consulta.guardar(modulo, opcion);

    if (resp == 0) {
      exito = false;
    }
    return exito;
  }

  public List<ClaseGeneral> eliminarComando(List<Comando> objetoEliminar, List<Object> listObject)
  {
    Consultas consulta = new Consultas();
    List listaErrores = new ArrayList();

    for (Comando elimina : objetoEliminar) {
      this.listaObjetosTemp.remove(elimina);
      try
      {
        int val = consulta.eliminar(elimina);
        if (val != 1) {
          ClaseGeneral cg = new ClaseGeneral();
          cg.setCampo1(elimina.getId());
          cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
          listaErrores.add(cg);
        }

      }
      catch (Exception e)
      {
      }

    }

    return listaErrores;
  }
}