/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;
import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Evento;
import ec.com.centrosurcall.datos.modelo.TipoLlamada;
import ec.com.centrosurcall.negocio.controladores.CtrSession;
import ec.com.centrosurcall.utils.ClaseGeneral;
import ec.com.centrosurcall.utils.FacesUtils;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
/**
 *
 * @author carlosguaman65
 */

@ManagedBean
@ViewScoped
public class EventoDAO {
    
    private List<Evento> eventosList;

     CtrSession session;
    
    public EventoDAO(){
             session = (CtrSession) FacesUtils.getManagedBean("ctrSession");
    }
    
    
    public Evento getEventoByID(int id) {
        String objeto = "Evento";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (Evento) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public synchronized List<Evento> getEventosList() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Runtime r = Runtime.getRuntime();
         Consultas consulta = new Consultas();
        String codigoivr = request.getParameter("codigoivr");
        try {
            Integer.parseInt(codigoivr);
        } catch (NumberFormatException nfe) {
            codigoivr = "1";
        }
        session.setCodigoivr(codigoivr);
        String objeto = "Evento";
        List<ParametrosConsulta> lisPar = new ArrayList();
        try {
            if (eventosList == null){
            eventosList= new ArrayList<Evento>();  
            TipoLlamada tllamada= (TipoLlamada) consulta.getListHql(null, "TipoLlamada", "codigoIvr = " + session.getCodigoivr() + " and estado!=0", null).get(0);
            eventosList = consulta.getListHql(null, objeto, "idfkrazon = "+ tllamada.getId() + " and estado!=0", lisPar);
            }
            return  eventosList;
        } catch (Exception e) {
            return null;
        }
      
    }

    public boolean saveEvento(Evento evento, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(evento, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarEventos(List<Evento> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Evento elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
  
}
