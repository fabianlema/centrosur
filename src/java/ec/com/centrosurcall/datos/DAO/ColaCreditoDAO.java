/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Cola;
//import ec.com.centrosurcall.datos.modelo.ColaCliente;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author persona
 */
public class ColaCreditoDAO {

    public Cola getColaCreditoByIdCola(int idCola) {
        String objeto = "ColaCredito";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", idCola));
        String where = " idCola=:ID";
        try {
            return (Cola) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Cola> getColasCredito() {
        String objeto = "ColaCredito";
        String where = "estado!=0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

//    public boolean saveCola(ColaCliente colaCredito, int opcion) {
//        boolean exito = true;
//        Consultas consulta = new Consultas();
//        int resp = 0;
//        resp = consulta.guardar(colaCredito, opcion);
//
//        if (resp == 0) {
//            exito = false;
//        }
//        return exito;
//    }
}
