/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Zona;
import ec.com.centrosurcall.datos.modelo.ZonaAfectada;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author carlosguaman65
 */
@ManagedBean
@ViewScoped
public class ZonaDAO {
   
    private List<Zona> zonasList;
    
    private List<Zona> zonasAfectadasList;
     
    
 public Zona getZonaByID(int id) {
        String objeto = "Zona";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (Zona) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public synchronized List<Zona> getZonasList() {
        String objeto = "Zona";
        String where = "id = 1";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
       
        try {
             if (zonasList== null){
                 zonasList= new ArrayList<Zona>();
                 zonasList = consulta.getListHql(null, objeto, where, lisPar);
                }
              Set <Zona>nuevosElementos  = new HashSet<Zona>(zonasList);
              Set <Zona>elementosActuales  = new HashSet<Zona>(getZonasAfectadasList());
             
              for (Zona zona : nuevosElementos){
                for (Zona zonaActual: elementosActuales){
                    if (zona.getId() == zonaActual.getId()){
                        zonasList.remove(zona);
                    }
                   }
                }
             
             return  zonasList;
        } catch (Exception e) {
            return null;
        }
    }

     public synchronized List<Zona> getZonasAfectadasList() {
        String objeto = "Zona";
        String where = "estado = 1 ";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
             if (zonasAfectadasList== null){
                 zonasAfectadasList= new ArrayList<Zona>();
                 zonasAfectadasList = consulta.getZonasAfectadasList(null, objeto, 1 , lisPar);
                }
             if (zonasAfectadasList.isEmpty()){
                 zonasAfectadasList.add(new Zona(1, "No existen Zonas Afectadas", "0000"));
             }
             return  zonasAfectadasList;
        } catch (Exception e) {
            return null;
        }
    }
     public  List<Zona> getZonas() {
        String objeto = "Zona";
        String where = "id>0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
                        return  consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

     
     
     
     public List <Zona>  getZonasByAlimentadores(int id) {
        String objeto = "Alimentador";
          String where = "id = " + id;
         List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            List<Zona> zonas =  consulta.getListHqlZonas(null, objeto, id, lisPar);
            Set <Zona>nuevosElementos  = new HashSet<Zona>(zonas);
            Set <Zona>elementosActuales  = new HashSet<Zona>(getZonasAfectadasList());
             
              for (Zona zona : nuevosElementos){
                for (Zona zonaActual: elementosActuales){
                    if (zona.getId() == zonaActual.getId()){
                        zonas.remove(zona);
                    }
                   }
                }

            return zonas;
        } catch (Exception e) {
            return null;
        }
    }  
     
    public boolean saveZona(Zona zona, Set alimentadores ,int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardarZonas(zona, alimentadores, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarZona(List<Zona> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Zona elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            //elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }


    
    
}