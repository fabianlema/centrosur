/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Campania;
import ec.com.centrosurcall.datos.modelo.Cliente;
import ec.com.centrosurcall.datos.modelo.ClienteAux;
import ec.com.centrosurcall.datos.modelo.ClienteId;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author persona
 */
public class ClientesDAO {

//    public Cliente getClienteByID(String id) {
//        String objeto = "Cliente";
//        List<ParametrosConsulta> lisPar = new ArrayList();
//        Consultas consulta = new Consultas();
//        lisPar.add(new ParametrosConsulta("ID", id));
//        String where = " clicod=:ID";
//        try {
//            return (Cliente) consulta.getListHql(null, objeto, where, lisPar).get(0);
//        } catch (Exception e) {
//            return null;
//        }
//    }
     public Cliente getClienteByID(String clicod) {
        String objeto = "Cliente";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("clicod", clicod));
        String where = " clicod=:clicod";
        try {
            return (Cliente) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
     
     public Cliente getClienteByID(String clicod, int idCampania) {
        String objeto = "Cliente";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("clicod", clicod));
        lisPar.add(new ParametrosConsulta("idcampania", idCampania));
        String where = " id.clicod=:clicod and id.idCampania=:idcampania";
        try {
            return (Cliente) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<Cliente> getClientes() {
        String objeto = "Cliente";
        //String where = null;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, null, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    public Cliente getClientesByIdSocio(String idCliente) {
        String objeto = "Cliente";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("idCliente", idCliente));
        String where = " idCliente=:idCliente order by montoPrestamo";
        try {
            return (Cliente) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<Cliente> getListClientesByIdSocio(String idCliente) {
        String objeto = "Cliente";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("idCliente", idCliente));
        String where = " idCliente=:idCliente order by montoPrestamo";
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveCliente(Cliente credito, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(credito, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
    public boolean saveClienteAux(ClienteAux credito, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(credito, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    
    public List<ClaseGeneral> eliminarClientes(List<Cliente> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Cliente elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }

    public Cliente getSPCliente(String idUsuario, int idRol, int campania) {
        Consultas consulta = new Consultas();
        try
        {
            ResultSet result = consulta.getSPCliente(idUsuario, idRol);
            Cliente credito = null;
            if (result != null){
                while (result.next()) {
                    credito = new Cliente();
                    ClienteId ci = new ClienteId(result.getString("clicod"), campania);
                    credito.setId(ci);
                    credito.setApellidos(result.getString("apellidos"));
                    credito.setNombres(result.getString("nombres"));
                    credito.setTlfCasa(result.getString("tlfCasa"));
                    credito.setTlfCelular(result.getString("tlfCelular"));
                    credito.setAdicional1(result.getString("adicional1"));
                    credito.setAdicional2(result.getString("adicional2"));
                    credito.setAdicional3(result.getString("adicional3"));
                    credito.setIdUsuario(result.getInt("idUsuario")); 
                    credito.setEstado(result.getInt("estado")); 
                    credito.setIntentos(result.getInt("intentos")); 
                }                
            }
            return credito;
        }catch (Exception e) {
            System.out.println("Error: " + e);
            return null;
        }
    }
    
//    public Cliente getSPNuance() {
//        Consultas consulta = new Consultas();
//        try
//        {
//            ResultSet result = consulta.getSPNuance();
//            Cliente credito = null;
//            if (result != null){
//                while (result.next()) {                    
//                    credito = new Cliente();
//                    credito.setClicod(result.getString("clicod")); 
//                    credito.setApellidos(result.getString("apellidos"));
//                    credito.setNombres(result.getString("nombres"));
//                    credito.setTlfCasa(result.getString("tlfCasa"));
//                    credito.setTlfCelular(result.getString("tlfCelular"));                    
//                    credito.setIdUsuario(result.getInt("idUsuario")); 
//                    credito.setEstado(result.getInt("estado"));
//                    credito.setIntentos(result.getInt("intentos")); 
//                }                
//            }
//            return credito;
//        }catch (Exception e) {
//            System.out.println("Error: " + e);
//            return null;
//        }
//    }
    
    public Boolean getSPActualizaClientes() {
        Consultas consulta = new Consultas();
        try
        {
            ResultSet result = consulta.getSPActualizaClientes();
            Cliente credito = null;
            if (result != null){
                return true;
            }else{
                return false;
            }
        }catch (Exception e) {
            System.out.println("Error: " + e);
            return false;
        }
    }
}