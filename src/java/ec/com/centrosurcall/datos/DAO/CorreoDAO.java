/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.BandejaEntrada;
import ec.com.centrosurcall.datos.modelo.ColaChat;
import ec.com.centrosurcall.datos.modelo.CredencialesCorreo;
import ec.com.centrosurcall.datos.modelo.EstadoLlamada;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author wilso
 */
public class CorreoDAO {
      
    public CredencialesCorreo getCredencialesByIDs(int id) {
        String objeto = "CredencialesCorreo";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (CredencialesCorreo) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
        public BandejaEntrada getCorreoByID(int id) {
        String objeto = "BandejaEntrada";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (BandejaEntrada) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
       }
        
     public BandejaEntrada getCorreoDisponible() {
        String objeto = "BandejaEntrada";
        String where = " id > 0  and leido= 0 " ;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return (BandejaEntrada) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
       }
        
    
    public BandejaEntrada getCorreoByEstado() {
        String objeto = "BandejaEntrada";
        String where = "estado = 0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
             List<BandejaEntrada> correos= consulta.getListHql(null, objeto, where, lisPar);
             return correos.get(0);
        } catch (Exception e) {
            return null;
        }
    }


    public   List<BandejaEntrada> getCorreos() {
        String objeto = "BandejaEntrada";
        String where = "id>0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
         return  consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
   public   List<Integer> getNumeroCorreos() {
        String objeto = "BandejaEntrada";
        String where = "id>0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
          List <BandejaEntrada>  correos= consulta.getListHql(null, objeto, where, lisPar);
          List <Integer>  numeros = new ArrayList();
           for (BandejaEntrada correo: correos){
               numeros.add(correo.getNumeroMail());
           }
           return numeros;
        } catch (Exception e) {
            return null;
        }
    }


    public boolean guardarCorreoDescargado(BandejaEntrada correo, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(correo, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
    public boolean guardarEstadoCorreo (EstadoLlamada estado, int opcion){
     boolean exito= true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(estado, opcion);

        if (resp == 0) {
            exito = false;
        }
     return exito;   
    }
    
    public   EstadoLlamada getEstadoCorreo() {
        String objeto = "EstadoLlamada";
        String where = "id>0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
         List<EstadoLlamada> list = consulta.getListHql(null, objeto, where, lisPar);
         return  list.get(list.size() -1);
        } catch (Exception e) {
            return null;
        }
    }
    
    public Integer getEstadoBandeja() {
        String cadenaSQL = "select top 1 estado from bandeja_entrada";
        Consultas consulta = new Consultas();
        try {
            List temp = consulta.getListSql(cadenaSQL);
            return Integer.parseInt(temp.get(0).toString());
        } catch (Exception e) {
            return 0;
        }
    }
    
    public boolean saveColaChat(ColaChat colaChat, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(colaChat, opcion);
        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
}
