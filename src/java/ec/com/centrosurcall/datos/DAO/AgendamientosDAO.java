/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Agendamiento;
import ec.com.centrosurcall.datos.modelo.Agendamiento;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author persona
 */
public class AgendamientosDAO {


    public List<Agendamiento> getAgendamientosByUsuario(int usuarioID) {
        String objeto = "Agendamiento";
        String where = "estado!=0 and usuarioByIdUsuario2.id:=ID";
        List<ParametrosConsulta> lisPar = new ArrayList();
        lisPar.add(new ParametrosConsulta("ID", usuarioID));
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<Agendamiento> getAgendamientosByCredito(String numCredito) {
        String objeto = "Agendamiento";
        String where = "estado!=0 and numCredito='" + numCredito.trim() + "'";
//        List<ParametrosConsulta> lisPar = new ArrayList();
//        lisPar.add(new ParametrosConsulta("ID", numCredito));
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, null);
        } catch (Exception e) {
            return null;
        }
    }

    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarAgedamientos(List<Agendamiento> agendamientosEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Agendamiento elimina : agendamientosEliminar) {
            listaObjetosTemp.remove(elimina);
            elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
    
    public boolean saveAgendamiento(Agendamiento agendamiento, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(agendamiento, opcion);
        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
}
