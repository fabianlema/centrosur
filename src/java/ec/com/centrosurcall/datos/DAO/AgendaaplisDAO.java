/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

/**
 *
 * @author fabia
 */

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Agendaapli;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AgendaaplisDAO
{
  List<Object> listaObjetosTemp = new ArrayList();

  public List<Agendaapli> getAgendaapli(int idTarea)
  {
    String objeto = "Agendaapli";
    String where = "id = " + idTarea;
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return consulta.getListHql(null, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public List<Agendaapli> getAgendaapliEquipo(int idEquipo)
  {
    String objeto = "Agendaapli";
    String where = "idEquipo = " + idEquipo;
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return consulta.getListHql(null, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public List<Agendaapli> getAgendaapliEquipoJoin(int idEquipo)
  {
    Consultas consulta = new Consultas();
    List lisAgendaapli = new ArrayList();
    try {
      List listaDate = consulta.getListSql("SELECT a.id, CONCAT(t.descripcion, '&', a.descripcion) as descripcion, a.id_equipo, a.mes, a.diasemana, a.hora, a.minuto FROM agendaapli as a Inner join tarea as t on (a.id_tarea= t.id) WHERE a.id_equipo = " + idEquipo);
      for (Iterator i$ = listaDate.iterator(); i$.hasNext(); ) { Object o = i$.next();
        Object[] lista = (Object[])(Object[])o;
        String[] descripciones = ((String)lista[1]).split("&");
        Agendaapli temp = new Agendaapli();
        temp.setId((Integer)lista[0]);
        temp.setDescripcionTarea(descripciones[0]);
        temp.setIdEquipo((Integer)lista[2]);
        temp.setMes((Integer)lista[3]);
        temp.setDiasemana((Integer)lista[4]);
        temp.setHora((Integer)lista[5]);
        temp.setMinuto((Integer)lista[6]);
        temp.setDescripcion(descripciones.length > 1 ? descripciones[1] : "");
        lisAgendaapli.add(temp);
      }
      return lisAgendaapli; } catch (Exception e) {
    }
    return null;
  }

  public boolean saveAgendaapli(Agendaapli modulo, int opcion)
  {
    boolean exito = true;
    Consultas consulta = new Consultas();
    int resp = 0;
    resp = consulta.guardar(modulo, opcion);

    if (resp == 0) {
      exito = false;
    }
    return exito;
  }

  public List<ClaseGeneral> eliminarAgendaaplis(List<Agendaapli> objetoEliminar, List<Object> listObject)
  {
    Consultas consulta = new Consultas();
    List listaErrores = new ArrayList();

    for (Agendaapli elimina : objetoEliminar) {
      this.listaObjetosTemp.remove(elimina);
      try
      {
        int val = consulta.eliminar(elimina);
        if (val != 1) {
          ClaseGeneral cg = new ClaseGeneral();
          cg.setCampo1(elimina.getId());
          cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
          listaErrores.add(cg);
        }

      }
      catch (Exception e)
      {
      }

    }

    return listaErrores;
  }

  public boolean eliminarAgendaapli(Agendaapli agendaapli) {
    Consultas consulta = new Consultas();
    try {
      int val = consulta.eliminar(agendaapli);

      return val == 1;
    }
    catch (Exception e)
    {
    }

    return false;
  }
}