/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.DetalleMod;
import ec.com.centrosurcall.datos.modelo.Modulo;
//import ec.com.centrosurcall.datos.modelo.Persona;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author persona
 */
public class ModulosDAO {

    public List<DetalleMod> getModulosxUsuario(String cadenaIn) {
        String objeto = "DetalleMod";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        //lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id in (" + cadenaIn + ") order by modulo.id.idPadre";
        try {
            return  consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    public String getDetallePadreIdsString(int idUsuario){
        Consultas consulta = new Consultas();
        List<Object> usuarios;
        String cadena = "";
        try {
            usuarios = (List<Object>)consulta.getListSql("select min(id) from detalle_mod where idHijo=idPadre and idRol in (select ru.idRol from rol_usuario ru where ru.idUsuario="+idUsuario+") group by idHijo, idPadre");
            for (Object user : usuarios){
                if (cadena.equals("")){
                    cadena += user;
                }else{
                    cadena += "," + user;
                }                
            }
            return cadena;
        } catch (Exception e) {
            return null;
        }
    }
    
    public String getDetalleIdsString(int idUsuario){
        Consultas consulta = new Consultas();
        List<Object> usuarios;
        String cadena = "";
        try {
            usuarios = (List<Object>)consulta.getListSql("select min(id) from detalle_mod where idRol in (select ru.idRol from rol_usuario ru where ru.idUsuario=" + idUsuario + ") group by idHijo, idPadre");
            for (Object user : usuarios){
                if (cadena.equals("")){
                    cadena += user;
                }else{
                    cadena += "," + user;
                }                
            }
            return cadena;
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<DetalleMod> getModulosxRol(String cadenaIn) {
        String objeto = "DetalleMod";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        //lisPar.add(new ParametrosConsulta("ID", id));
        String where = "id in (" + cadenaIn + ") order by modulo.id.idPadre";
        try {
            return  consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Modulo> getModulosPadre(){
        String objeto = "Modulo";
        String where = "estado!=0 and id.idPadre=id.idHijo";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    public String getHijosIdsString(int idPadre, int idUsuario){
        Consultas consulta = new Consultas();
        List<Object> usuarios;
        String cadena = "";
        try {
            usuarios = (List<Object>)consulta.getListSql("select idHijo from detalle_mod where idPadre="+idPadre+" and idRol in (select ru.idRol from rol_usuario ru where ru.idUsuario="+idUsuario+") group by idHijo, idPadre");
            for (Object user : usuarios){
                if (cadena.equals("")){
                    cadena += user;
                }else{
                    cadena += "," + user;
                }                
            }
            return cadena;
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<Modulo> getModulosHijosxId(int idPadre, String idHijos) {
        //String from ="dm.modulo";
        String objeto = "Modulo";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
//        lisPar.add(new ParametrosConsulta("ID", idModulo));
//        lisPar.add(new ParametrosConsulta("IDU", idRol));
        String where = " idPadre="+idPadre+" and idHijo in ("+idHijos+")";
        try {
            return  consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    /*public List<Modulo> getModulosxId(int idModulo, int idRol) {
        String from ="dm.modulo";
        String objeto = "DetalleMod dm";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", idModulo));
        lisPar.add(new ParametrosConsulta("IDU", idRol));
        String where = " dm.modulo.id.idPadre=:ID  and dm.rol.id=:IDU";
        try {
            return  consulta.getListHql(from, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }*/
    
     public List<Modulo> getModulos() {
        String objeto = "Modulo";
        String where = "estado!=0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
     public boolean saveModulo(Modulo modulo, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardarMinusculas(modulo, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
     
     public boolean saveDetModulo(DetalleMod detalle, int opcion){
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(detalle, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
     }
     
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    
    public List<ClaseGeneral> eliminarModulo(List<Modulo> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Modulo elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
     
    
}
