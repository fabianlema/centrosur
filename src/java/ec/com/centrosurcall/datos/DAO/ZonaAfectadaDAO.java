/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Evento;
import ec.com.centrosurcall.datos.modelo.HoraReposicion;
import ec.com.centrosurcall.datos.modelo.ZonaAfectada;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author persona
 */

@ManagedBean
@RequestScoped
public class ZonaAfectadaDAO {
   
    
    public  synchronized ZonaAfectada getZonaAfectadaByID(int id) {
        String objeto = "ZonaAfectada";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (ZonaAfectada) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
        public  ZonaAfectada getZonaAfectadaByZona(int zonaId) {
        String objeto = "ZonaAfectada";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", zonaId));
        String where = " id_fk_zona=:ID order by id desc";
        try {
            return (ZonaAfectada) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
        public List<ZonaAfectada> getZonaAfectadaByEstados() {
       String objeto = "ZonaAfectada";
        String where = "estado = " + 1;
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
           return   consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    
    public ZonaAfectada getZonaAfectadaByEstado(int estado) {
        String objeto = "ZonaAfectada";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ESTADO", estado));
        String where = " estado=:ESTADO";
        try {
            return (ZonaAfectada) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public boolean saveHoraReposicion( HoraReposicion horaReposicion, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(horaReposicion, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
     
    }
    
      public  HoraReposicion getLastHoraReposicion() {
        String objeto = "HoraReposicion";
        String where = "id=1";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
                 List <HoraReposicion> horas= consulta.getListHql(null, objeto, where, lisPar);
                 return  horas.get(horas.size()-1);
        } catch (Exception e) {
            return null;
        }
    }
    
    

    public synchronized List<ZonaAfectada> getZonasAfectadas() {
        String objeto = "ZonaAfectada";
        String where = "id>0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
                        return  consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveZonaAfectada(ZonaAfectada zonaAfectada, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(zonaAfectada, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarZonasAfectadas(List<ZonaAfectada> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (ZonaAfectada elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            //elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
    
}
