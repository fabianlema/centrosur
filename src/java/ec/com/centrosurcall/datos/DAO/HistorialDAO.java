package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Historial;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.io.PrintStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HistorialDAO {

    List<Object> listaObjetosTemp = new ArrayList();

    public Historial getHistorialByID(int id) {
        String objeto = "Historial";
        List lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", Integer.valueOf(id)));
        String where = " id=:ID";
        try {
            return (Historial) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
        }
        return null;
    }

    public boolean saveHistorial(Historial historial, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(historial, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }

    public List<Historial> getHistorialSI() {
        Consultas consulta = new Consultas();
        List listaHistorial = new ArrayList();
        try {
            String cadenaSQL = "select distinct(grupo) from historial";
            List listaObjetos = consulta.getListSql(cadenaSQL);

            for (Iterator i$ = listaObjetos.iterator(); i$.hasNext();) {
                Object temp = i$.next();

                Historial hisTemp = new Historial();
                hisTemp.setGrupo(temp.toString());
                listaHistorial.add(hisTemp);
            }
            return listaHistorial;
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return null;
    }

    public List<Historial> getHistorialListaN() {
        Consultas consulta = new Consultas();
        List listaHistorial = new ArrayList();
        try {
            String cadenaSQL = "select distinct(numero) from  Historial WITH(NOLOCK) where grupo='FAILED'";
            List listaObjetos = consulta.getListSql(cadenaSQL);

            for (Iterator i$ = listaObjetos.iterator(); i$.hasNext();) {
                Object temp = i$.next();

                Historial hisTemp = new Historial();
                hisTemp.setNumero(temp.toString());
                listaHistorial.add(hisTemp);
            }
            return listaHistorial;
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return null;
    }

    public List<Historial> getHistorialLN(String desde, String hasta) {
        String objeto = "Historial";
        String where = "grupo='FAILED' and fecha >= '" + desde + "' and fecha <= '" + hasta + "'";
        List lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
        }
        return null;
    }

    public Historial getHistorialAByFecha(String numMensaje, String mensaje, String numero) {
        String objeto = "Historial";
        List lisPar = new ArrayList();
        Consultas consulta = new Consultas();

        String where = "grupo='" + numMensaje + "' and mensaje='" + mensaje + "' and numero='" + numero + "'";
        try {
            return (Historial) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
        }
        return null;
    }

    public Historial getHistorialNumFailed(String numero) {
        String objeto = "Historial";
        List lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        String where = "grupo='FAILED' and numero='" + numero + "'";
        try {
            return (Historial) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
        }
        return null;
    }

    public int getSPSetIncorrecct(String numero, String mensaje) {
        Consultas consulta = new Consultas();
        try {
            ResultSet result = consulta.getSPSetIncorrecct(numero, mensaje);

            if (result != null) {
                return 1;
            }
            return 0;
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return 0;
    }

    public int getSPBlackList() {
        Consultas consulta = new Consultas();
        try {
            ResultSet result = consulta.getSPBlackList();

            if (result != null) {
                return 1;
            }
            return 0;
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return 0;
    }

    public List<ClaseGeneral> eliminarHistorial(List<Historial> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List listaErrores = new ArrayList();

        for (Historial elimina : objetoEliminar) {
            this.listaObjetosTemp.remove(elimina);
            List listaObjetos;
            try {
                String cadenaSQL = "delete from historial where numero = '" + elimina.getNumero() + "'";
                listaObjetos = consulta.getListSql(cadenaSQL);
            } catch (Exception e) {
            }
        }
        return listaErrores;
    }

    public List<ClaseGeneral> eliminarHistorial(List<Historial> objetoEliminar) {
        Consultas consulta = new Consultas();
        List listaErrores = new ArrayList();

        for (Historial elimina : objetoEliminar) {
            this.listaObjetosTemp.remove(elimina);
            //elimina.setEstado(Integer.valueOf(0));
            try {
                int val = consulta.eliminar(elimina);
                if (val != 1) {
                    ClaseGeneral cg = new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);
                }

            } catch (Exception e) {
            }
        }
        return listaErrores;
    }
}