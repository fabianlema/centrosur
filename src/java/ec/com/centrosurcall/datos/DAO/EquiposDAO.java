/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

/**
 *
 * @author fabia
 */

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Equipo;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

public class EquiposDAO
{
  List<Object> listaObjetosTemp = new ArrayList();

  public List<Equipo> getEquipoPadre()
  {
    String objeto = "Equipo";
    String where = "estado!=0 and id.idPadre=id.idHijo";
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return consulta.getListHql(null, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public List<Equipo> getEquipoxIdj(int idEquipo, int idRol) {
    String from = "dm.modulo";
    String objeto = "DetalleMod dm";
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    lisPar.add(new ParametrosConsulta("ID", Integer.valueOf(idEquipo)));
    lisPar.add(new ParametrosConsulta("IDU", Integer.valueOf(idRol)));
    String where = " dm.modulo.id.idPadre=:ID  and dm.rol.id=:IDU";
    try {
      return consulta.getListHql(from, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public List<Equipo> getEquipo()
  {
    String objeto = "Equipo";
    String where = "id != 0 and jerarquia = 'Agencia'";
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return consulta.getListHql(null, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public boolean saveEquipo(Equipo modulo, int opcion)
  {
    boolean exito = true;
    Consultas consulta = new Consultas();
    int resp = 0;
    resp = consulta.guardarMinusculas(modulo, opcion);

    if (resp == 0) {
      exito = false;
    }
    return exito;
  }

  public List<ClaseGeneral> eliminarEquipo(List<Equipo> objetoEliminar, List<Object> listObject)
  {
    Consultas consulta = new Consultas();
    List listaErrores = new ArrayList();

    for (Equipo elimina : objetoEliminar) {
      this.listaObjetosTemp.remove(elimina);
      try
      {
        int val = consulta.eliminar(elimina);
        if (val != 1) {
          ClaseGeneral cg = new ClaseGeneral();
          cg.setCampo1(Integer.valueOf(elimina.getId()));
          cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
          listaErrores.add(cg);
        }

      }
      catch (Exception e)
      {
      }

    }

    return listaErrores;
  }
}