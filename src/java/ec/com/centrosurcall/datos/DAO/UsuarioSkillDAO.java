/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.UsuarioSkill;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Paul
 */
public class UsuarioSkillDAO {
    
    
    
    public List<UsuarioSkill> getUsuariosSkills() {
        String objeto = "UsuarioSkill";
        String where = "";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<UsuarioSkill> getUsuarioSkills(Integer usuario){
        String objeto = "UsuarioSkill";
        List<ParametrosConsulta> lisPar = new ArrayList();
        lisPar.add(new ParametrosConsulta("USUARIO", usuario));
        String where = " idUsuario=:USUARIO";
        Consultas consulta = new Consultas();
        try{
            return consulta.getListHql(null,objeto,where,lisPar);
        }catch(Exception e){
            return null;
        }
    }
    
    public boolean saveUsuarioSkill(UsuarioSkill usuarioSkill, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(usuarioSkill, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
}
