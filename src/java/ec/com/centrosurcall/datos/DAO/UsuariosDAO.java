/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.RolUsuario;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author persona
 */
public class UsuariosDAO {

    public Usuario getUsuario(String usuario) {
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("USUARIO", usuario));
        String where = " nick=:USUARIO and estado=1";
        try {
         //   System.err.println("Conaulta usuarios >>>"+ usuario);
            return (Usuario)consulta.getListHql(null, "Usuario", where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public Usuario getUsuarioById(String idUsuario) {
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("idUsuario", idUsuario));
        String where = " id=:idUsuario";
        try {
            return (Usuario)consulta.getListHql(null, "Usuario", where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public Object[] getUsuariosByAlias(String alias) {
        Consultas consulta = new Consultas();
        List<Object[]> usuarios;
        try {
            usuarios = (List<Object[]>)consulta.getListSql("select * from Usuario where alias='"+alias+"'");
            if(usuarios!=null){
                return usuarios.get(0);
            }else{
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    public Usuario getUsuarioByAliasObj(String alias) {
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("alias", alias));
        String where = " alias=:alias";
        try {
            return (Usuario)consulta.getListHql(null, "Usuario", where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public Usuario getUsuarioByNombre(String nombre) {
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("nick", nombre));
        String where = " nick=:nick and estado=1";
        try {
            return (Usuario)consulta.getListHql(null, "Usuario", where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<Usuario> getUsuarios() {
        String objeto = "Usuario";
        String where = "estado!=0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<Object[]> getUsuariosByTeam(Integer idTeam) {
        String cadena = "select u.id, u.nick, u.nivelU, u.estado, u.extension, u.seleccionado, u.nombre, u.alias, u.extension2 from dbo.usuario u join team_usuario t on t.idUsuario=u.id where t.idTeam="+idTeam;
        Consultas consulta = new Consultas();
        try {
            return (List<Object[]>)consulta.getListSql(cadena);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    
    public boolean saveUsuario(Usuario usuario, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(usuario, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
    public boolean saveRolUsuario(RolUsuario rolUsuario, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(rolUsuario, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }

    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarUsuario(List<Usuario> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Usuario elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
    
    
}
