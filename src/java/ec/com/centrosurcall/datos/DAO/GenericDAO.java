/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author persona
 */
public class GenericDAO {

    public Object getObjectByID(String objeto, long id) {
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveObject(Object objeto, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(objeto, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    List<Object> listaObjetosTemp = new ArrayList<Object>();

    public List<ClaseGeneral> eliminarObjetos(List<Object> objetosEliminar, List<Object> listObject) throws IllegalAccessException, InvocationTargetException {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Object elimina : objetosEliminar) {
            try {
                listaObjetosTemp.remove(elimina);
                Class oClase = elimina.getClass();
                Method setEstado;
                setEstado = oClase.getMethod("setEstado", new Class[]{String.class});
                setEstado.invoke(elimina, 0);
                try {
                    int val = consulta.guardar(elimina, 0);
                    if (val != 1) {
                        ClaseGeneral cg = new ClaseGeneral();
                        Method getId;
                        getId = oClase.getMethod("getId", new Class[]{String.class});
                        
                        cg.setCampo1(getId.invoke(elimina));
                        cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                        listaErrores.add(cg);

                    } else {
                        //Grabacion Correcta
                    }

                } catch (Exception e) {
                }

            } catch (NoSuchMethodException ex) {
                Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecurityException ex) {
                Logger.getLogger(GenericDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return listaErrores;
    }
}
