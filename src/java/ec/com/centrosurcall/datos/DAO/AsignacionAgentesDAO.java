/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.datos.modelo.Zona;
import ec.com.centrosurcall.datos.modelo.ZonaAfectada;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author carlosguaman65
 */
@ManagedBean
@ViewScoped
public class AsignacionAgentesDAO {
   
    private List<Usuario> agentesList;
    
    
 public Usuario getDescripcionByID(int id) {
        String objeto = "Usuario";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (Usuario) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public synchronized List<Usuario> getAgentesList() {
        String objeto = "Usuario";
        String where = "id>0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
             if (agentesList== null){
                 agentesList= new ArrayList<Usuario>();
                 agentesList = consulta.getListHql(null, objeto, where, lisPar);
                }
             return  agentesList;
        } catch (Exception e) {
            return null;
        }
    }


    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarAgente(List<Usuario> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Usuario elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            //elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
    
}