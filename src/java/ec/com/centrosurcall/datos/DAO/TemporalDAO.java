/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Temporal;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.io.PrintStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TemporalDAO
{
  List<Object> listaObjetosTemp = new ArrayList();

  public Temporal getTemporalByID(int id)
  {
    String objeto = "Historial";
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    lisPar.add(new ParametrosConsulta("ID", Integer.valueOf(id)));
    String where = " id=:ID";
    try {
      return (Temporal)consulta.getListHql(null, objeto, where, lisPar).get(0); } catch (Exception e) {
    }
    return null;
  }

  public List<Temporal> getTemporal()
  {
    String objeto = "Temporal";
    String where = "estadodato!=0";
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return consulta.getListHql(null, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public List<Temporal> getTemporalXUsuario(int idUsuario)
  {
    String objeto = "Temporal";
    String where = "estadodato!=0 and idUsuario=" + idUsuario;
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return consulta.getListHql(null, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public boolean saveTemporal(Temporal temporal, int opcion)
  {
    boolean exito = true;
    Consultas consulta = new Consultas();
    int resp = 0;
    resp = consulta.guardar(temporal, opcion);

    if (resp == 0) {
      exito = false;
    }
    return exito;
  }

  public boolean deleteTemporal(Temporal temporal) {
    boolean exito = true;
    Consultas consulta = new Consultas();
    int resp = 0;
    resp = consulta.eliminar(temporal);
    if (resp == 0) {
      exito = false;
    }
    return exito;
  }

  public List<ClaseGeneral> eliminarHistorial(List<Temporal> objetoEliminar, List<Object> listObject)
  {
    Consultas consulta = new Consultas();
    List listaErrores = new ArrayList();

    for (Temporal elimina : objetoEliminar) {
      this.listaObjetosTemp.remove(elimina);
      elimina.setEstadodato(Boolean.FALSE);
      try {
        int val = consulta.guardar(elimina, 0);
        if (val != 1) {
          ClaseGeneral cg = new ClaseGeneral();
          cg.setCampo1(Integer.valueOf(elimina.getId()));
          cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
          listaErrores.add(cg);
        }
      }
      catch (Exception e)
      {
      }
    }

    return listaErrores;
  }

  public boolean dropTemporalXUsuario(int idUsuario) {
    String cadenaSQL = "delete from Temporal where idUsuario=" + idUsuario;

    Consultas consulta = new Consultas();
    try {
      consulta.getListSql(cadenaSQL);
      return true; } catch (Exception e) {
    }
    return false;
  }

  public boolean eliminarList(List lstObjetoEliminar)
  {
    Consultas consulta = new Consultas();
    try
    {
      return consulta.guardarEditarEliminarList(null, null, lstObjetoEliminar) == 1;
    }
    catch (Exception e)
    {
    }
    return false;
  }

  public List<Temporal> getSPListaTemporal(int idUsuario)
  {
    Consultas consulta = new Consultas();
    try
    {
      ResultSet result = consulta.getSPListaTemporal(idUsuario);
      List listaTemporal = new ArrayList();
      if (result != null) {
        while (result.next()) {
          Temporal temporal = new Temporal();
          temporal.setId(result.getInt("id"));
          temporal.setNumero(result.getString("numero"));
          temporal.setMensaje(result.getString("mensaje"));
          temporal.setEstadodato(Boolean.valueOf(result.getBoolean("estadodato")));
          listaTemporal.add(temporal);
        }
      }
      return listaTemporal;
    } catch (Exception e) {
      System.out.println("Error: " + e);
    }return null;
  }

  public List<Temporal> getSPFirstTemporal(int idUsuario)
  {
    Consultas consulta = new Consultas();
    try
    {
      ResultSet result = consulta.getSPFirstTemporal(idUsuario);
      List listaTemporal = new ArrayList();
      if (result != null) {
        while (result.next()) {
          Temporal temporal = new Temporal();
          temporal.setId(result.getInt("id"));
          temporal.setNumero(result.getString("numero"));
          temporal.setMensaje(result.getString("mensaje"));
          temporal.setEstadodato(Boolean.valueOf(result.getBoolean("estadodato")));
          listaTemporal.add(temporal);
        }
      }
      return listaTemporal;
    } catch (Exception e) {
      System.out.println("Error: " + e);
    }return null;
  }

  public int getUsuariosTemporal()
  {
    String cadenaSQL = "select count(distinct(idUsuario)) from temporal";
    Consultas consulta = new Consultas();
    try {
      List temp = consulta.getListSql(cadenaSQL);
      return ((Integer)temp.get(0)).intValue(); } catch (Exception e) {
    }
    return 0;
  }

  public int getMsjXUsuarioTemporal(int idUsuario)
  {
    String cadenaSQL = "select count(*) from temporal where idUsuario =" + idUsuario;
    Consultas consulta = new Consultas();
    try {
      List temp = consulta.getListSql(cadenaSQL);
      return ((Integer)temp.get(0)).intValue(); } catch (Exception e) {
    }
    return 0;
  }
}