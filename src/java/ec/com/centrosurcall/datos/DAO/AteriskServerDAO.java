package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.AsteriskServer;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

public class AteriskServerDAO {

    List<Object> listaObjetosTemp = new ArrayList();

    public AsteriskServer getAsteriskServerByID(int id) {
        String objeto = "AsteriskServer";
        List lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", Integer.valueOf(id)));
        String where = " id=:ID";
        try {
            return (AsteriskServer) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
        }
        return null;
    }

    public List<AsteriskServer> getAsteriskServers() {
        String objeto = "AsteriskServer";
        String where = "id>0";
        List lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
        }
        return null;
    }
    
//    public AsteriskServer getAsteriskServer() {
//        String objeto = "AsteriskServer";
//        List lisPar = new ArrayList();
//        Consultas consulta = new Consultas();
//        lisPar.add(new ParametrosConsulta("ATRIBUTO", atributo));
//        String where = " atributo=:ATRIBUTO";
//        try {
//            return (AsteriskServer) consulta.getListHql(null, objeto, where, lisPar).get(0);
//        } catch (Exception e) {
//        }
//        return null;
//    }

    public boolean saveAsteriskServer(AsteriskServer parametro, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardarMinusculas(parametro, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }

    public List<ClaseGeneral> eliminarAsteriskServers(List<AsteriskServer> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List listaErrores = new ArrayList();

        for (AsteriskServer elimina : objetoEliminar) {
            this.listaObjetosTemp.remove(elimina);
            //elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg = new ClaseGeneral();
                    cg.setCampo1(Integer.valueOf(elimina.getId()));
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);
                }
            } catch (Exception e) {
            }
        }

        return listaErrores;
    }
}