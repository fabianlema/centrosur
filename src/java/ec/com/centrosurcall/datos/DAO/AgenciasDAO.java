/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

/**
 *
 * @author fabia
 */

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.modelo.Agencia;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

public class AgenciasDAO
{
  List<Object> listaObjetosTemp = new ArrayList();

  public List<Agencia> getAgencia()
  {
    String objeto = "Agencia";
    String where = "id != 0";
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return consulta.getListHql(null, objeto, where, lisPar); } catch (Exception e) {
    }
    return null;
  }

  public Agencia getAgenciaXLogin(String login)
  {
    String objeto = "Agencia";
    String where = "nombre='" + login + "'";
    List lisPar = new ArrayList();
    Consultas consulta = new Consultas();
    try {
      return (Agencia)consulta.getListHql(null, objeto, where, lisPar).get(0); } catch (Exception e) {
    }
    return null;
  }

  public boolean saveAgencia(Agencia modulo, int opcion)
  {
    boolean exito = true;
    Consultas consulta = new Consultas();
    int resp = 0;
    resp = consulta.guardarMinusculas(modulo, opcion);

    if (resp == 0) {
      exito = false;
    }
    return exito;
  }

  public List<ClaseGeneral> eliminarAgencia(List<Agencia> objetoEliminar, List<Object> listObject)
  {
    Consultas consulta = new Consultas();
    List listaErrores = new ArrayList();

    for (Agencia elimina : objetoEliminar) {
      this.listaObjetosTemp.remove(elimina);
      try
      {
        int val = consulta.eliminar(elimina);
        if (val != 1) {
          ClaseGeneral cg = new ClaseGeneral();
          cg.setCampo1(Integer.valueOf(elimina.getId()));
          cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
          listaErrores.add(cg);
        }
      }
      catch (Exception e)
      {
      }
    }
    return listaErrores;
  }
}