/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Alimentador;
import ec.com.centrosurcall.datos.modelo.Zona;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author carlosguaman65
 */

@ManagedBean
@ViewScoped
public class AlimentadorDAO {
    
   private List<Alimentador> alimentadoresList;
    
    
 public Alimentador getAlimentadorByID(int id) {
        String objeto = "Alimentador";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (Alimentador) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
  }

 
     public Alimentador getAlimentadorByCodigo(String codigo) {
       String objeto = "Alimentador";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("CODIGO", codigo));
        String where = " codigo=:CODIGO";
        try {
            return (Alimentador) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public synchronized List<Alimentador> getAlimentadoresList() {
        String objeto = "Alimentador";
        String where = "id>0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
             if (alimentadoresList== null){
                 alimentadoresList= new ArrayList<Alimentador>();
                 alimentadoresList = consulta.getListHql(null, objeto, where, lisPar);
                }
              
             return  alimentadoresList;
        } catch (Exception e) {
          //  System.err.println(">>>>>>>>>>> Eroorrr");
            return null;
        }
    }

    public boolean saveAlimentador(Alimentador alimentador, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(alimentador, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarAlimentador(List<Alimentador> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Alimentador elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            //elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
    
}