/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Evento;
import ec.com.centrosurcall.datos.modelo.EventoEnfocado;
import ec.com.centrosurcall.datos.modelo.EventoProcesado;
import ec.com.centrosurcall.negocio.controladores.CtrSession;
import ec.com.centrosurcall.utils.ClaseGeneral;
import ec.com.centrosurcall.utils.FacesUtils;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author carlosguaman65
 */
@ManagedBean
@ViewScoped
public class EventoEnfocadoDAO {
    
    
    public boolean getEventoEnfocadoByID(int id) {
        System.out.println(" *********** " + id);
        String objeto = "EventoEnfocado";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = "id_fk_evento=:ID";
        try {
              if(consulta.getListHql(null, objeto, where, lisPar).size()==1)
                   return true;
              else
                   return false;
        } catch (Exception e) {
            return false;
        }
    }

    public List<EventoEnfocado> getEventosEnfocadosList() {
        String objeto = "EventoEnfocado";
        String where = "id>0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }


  
}
