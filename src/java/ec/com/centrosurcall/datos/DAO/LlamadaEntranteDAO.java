/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.EventoProcesado;
import ec.com.centrosurcall.datos.modelo.LlamadaEntrante;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author persona
 */
public class LlamadaEntranteDAO {

    public LlamadaEntrante getObservacionByID(int id) {
        String objeto = "LlamadaEntrante";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (LlamadaEntrante) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<LlamadaEntrante> getLlamadaEntrante() {
        String objeto = "LlamadaEntrante";
        String where = "id>0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveLlamadaEntrante(LlamadaEntrante tipoLlamada, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(tipoLlamada, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
    
    public boolean saveLlamadaEntranteProcesadas(LlamadaEntrante tipoLlamada, Set eventosProcesados,int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;

        resp = consulta.guardarLlamadasProcesadas(tipoLlamada, eventosProcesados, opcion);
        
        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
    
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarObservaciones(List<LlamadaEntrante> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (LlamadaEntrante elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            //elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }

            } catch (Exception e) {
            }

        }
        return listaErrores;
    }
    
}
