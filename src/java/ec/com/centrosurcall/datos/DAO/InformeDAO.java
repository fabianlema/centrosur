package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Informe;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

public class InformeDAO {

    List<Object> listaObjetosTemp = new ArrayList();

    public Informe getInformeByID(int id) {
        String objeto = "Informe";
        List lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", Integer.valueOf(id)));
        String where = " id=:ID";
        try {
            return (Informe) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
        }
        return null;
    }

    public Informe getInformeByAtributo(String atributo) {
        String objeto = "Informe";
        List lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ATRIBUTO", atributo));
        String where = " atributo=:ATRIBUTO";
        try {
            return (Informe) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
        }
        return null;
    }

    public boolean saveInforme(Informe parametro, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardarMinusculas(parametro, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }

    public List<ClaseGeneral> eliminarInformes(List<Informe> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List listaErrores = new ArrayList();

        for (Informe elimina : objetoEliminar) {
            this.listaObjetosTemp.remove(elimina);
            //elimina.setEstado(0);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg = new ClaseGeneral();
                    cg.setCampo1(Integer.valueOf(elimina.getId()));
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);
                }
            } catch (Exception e) {
            }
        }

        return listaErrores;
    }
}
