/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.DAO;

import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.datos.modelo.Columna;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TaurusTech
 */
public class ColumnaDAO {

    public Columna getColumnaByID(int id) {
        String objeto = "Columna";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        lisPar.add(new ParametrosConsulta("ID", id));
        String where = " id=:ID";
        try {
            return (Columna) consulta.getListHql(null, objeto, where, lisPar).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Columna> getColumnas() {
        String objeto = "Columna";
        String where = "estado!=0";
        List<ParametrosConsulta> lisPar = new ArrayList();
        Consultas consulta = new Consultas();
        try {
            return consulta.getListHql(null, objeto, where, lisPar);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveColumna(Columna Columna, int opcion) {
        boolean exito = true;
        Consultas consulta = new Consultas();
        int resp = 0;
        resp = consulta.guardar(Columna, opcion);

        if (resp == 0) {
            exito = false;
        }
        return exito;
    }
    
    List<Object> listaObjetosTemp = new ArrayList<Object>();
    public List<ClaseGeneral> eliminarColumnas(List<Columna> objetoEliminar, List<Object> listObject) {
        Consultas consulta = new Consultas();
        List<ClaseGeneral> listaErrores = new ArrayList<ClaseGeneral>();

        //Compruebo que no tenga relaciones:
        for (Columna elimina : objetoEliminar) {
            listaObjetosTemp.remove(elimina);
            elimina.setEstado(Boolean.FALSE);
            try {
                int val = consulta.guardar(elimina, 0);
                if (val != 1) {
                    ClaseGeneral cg=new ClaseGeneral();
                    cg.setCampo1(elimina.getId());
                    cg.setCampo1("Consulte a su Administrador la eliminacion del registro");
                    listaErrores.add(cg);

                } else {
                    //Grabacion Correcta
                }
            } catch (Exception e) {
            }
        }
        return listaErrores;
    }   
}

