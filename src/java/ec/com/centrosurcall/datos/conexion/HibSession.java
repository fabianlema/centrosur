/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.conexion;

/**
 *
 * @author TaurusTech
 */
import org.apache.commons.logging.*;
import org.hibernate.*;
import org.hibernate.cfg.*;

public class HibSession {

    private static Log log = LogFactory.getLog(HibSession.class);
    private static final SessionFactory sessionFactory;

    static {
        try {
// Create the SessionFactory
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
// Make sure you log the exception, as it might be swallowed
            log.error("Initial SessionFactory creation failed.", ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    public static final ThreadLocal session = new ThreadLocal();

    public static Session abrirSesion() {
        Session s = (Session) session.get();
        if (s == null || !s.isOpen()) {
            s = sessionFactory.openSession();
            session.set(s);
        }
        return s;
    }

    public static void cerrarSesion() {
        Session s = (Session) session.get();
        if (s != null) {
            s.close();
        }
        session.set(null);
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}