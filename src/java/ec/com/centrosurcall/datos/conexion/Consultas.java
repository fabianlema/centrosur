/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.datos.conexion;

import ec.com.centrosurcall.datos.modelo.Alimentador;
import ec.com.centrosurcall.datos.modelo.EventoProcesado;
import ec.com.centrosurcall.datos.modelo.LlamadaEntrante;
import ec.com.centrosurcall.datos.modelo.Subestacion;
import ec.com.centrosurcall.datos.modelo.Zona;
import ec.com.centrosurcall.datos.modelo.ZonaAfectada;
import ec.com.centrosurcall.negocio.controladores.CtrSession;
import ec.com.centrosurcall.utils.ClaseGeneral;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author TaurusTech
 */
public class Consultas implements Serializable {
    final String formatoFecha = "dd/MM/yyyy";
    final String formatoTimestamp = "dd/MM/yyyy HH:mm:ss";
    final static int AGENTE1 = 1;
    final static int AGENTE2 = 2;
    public Consultas() {
    }
    
    public Object get(Class clase, Serializable id) {
        Session conn = null;
        try {
            conn = HibSession.abrirSesion();
            return conn.get(clase, id);
        } catch (Exception e) {
            return null;
        } finally {
            HibSession.cerrarSesion();
        }
    }

    public int guardar(Object objetoGuardar, int nuevo) {
        Session conn = null;
        try {
            conn = HibSession.abrirSesion();
            conn.getTransaction().begin();
            //cambio a Mayusculas:
            //objetoGuardar = transformarObjetoMayusculas(objetoGuardar);
            if (nuevo == 0) {
                conn.merge(objetoGuardar);
            } else {
                conn.save(objetoGuardar);
            }
            conn.getTransaction().commit();
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            HibSession.cerrarSesion();
        }
    }
    
        
     public int guardarZonas(Zona zona,  Set alimentadoresList,  int nuevo) {
           Session conn = null;
         try {
            conn = HibSession.abrirSesion();
            conn.getTransaction().begin();
                
                Set<Zona> zonas= new HashSet<Zona>(); 
                zonas.add(zona);
                Set<Alimentador> alimentadores = new HashSet<Alimentador>();
                
                    for (Object alimentador :alimentadoresList){
                        Alimentador alimentadorItem = (Alimentador) alimentador;
                        alimentadorItem.setZonas(zonas);
                        alimentadores.add(alimentadorItem);
                    }
                
                 zona.setAlimentadors(alimentadores);
                 conn.save(zona);        
                 conn.getTransaction().commit();
            return 1;    

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            HibSession.cerrarSesion();
        }
        
     }
    
    public int guardarLlamadasProcesadas(LlamadaEntrante llamadaEntrante,  
        Set eventoProcesadoList,  int nuevo) {
        Session conn = null;
        try {
            conn = HibSession.abrirSesion();
            conn.getTransaction().begin();

            conn.save(llamadaEntrante);  
            
            Set eventos = new HashSet(0);
            for ( Object obj: eventoProcesadoList){
                 EventoProcesado evt= ( EventoProcesado) obj;
                 System.err.println("******************* " + evt.getObservacion() );
                 evt.setLlamadaEntrante(llamadaEntrante);  
                 eventos.add(evt);
                  //System.err.println("%%%%%% " + evt.getLlamadaEntrante().getUsuario().getNombre());
            }
            
            llamadaEntrante.setEventoProcesados(eventos);
            Object  objetoGuardar =  (Object)llamadaEntrante;
                 
             for ( Object obj: eventos){
                 EventoProcesado evt= ( EventoProcesado) obj;
                 conn.save(evt);
             }
             
            conn.getTransaction().commit();
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            HibSession.cerrarSesion();
        }
        
    }
    
    public int guardarMinusculas(Object objetoGuardar, int nuevo) {
        Session conn = null;
        try {
            conn = HibSession.abrirSesion();
            conn.getTransaction().begin();
            //cambio a Mayusculas:
            //objetoGuardar = transformarObjetoMayusculas(objetoGuardar);
            if (nuevo == 0) {
                conn.merge(objetoGuardar);
            } else {
                conn.save(objetoGuardar);
            }
            conn.getTransaction().commit();
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            HibSession.cerrarSesion();
        }
    }
    
    public int eliminar(Object objetoGuardar) {
        Session conn = null;
        try {
            conn = HibSession.abrirSesion();
            conn.getTransaction().begin();

            conn.delete(objetoGuardar);

            conn.getTransaction().commit();
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            HibSession.cerrarSesion();
        }
    }

    public int guardarEditarEliminarList(List lstObjetoGuardar, List lstObjetoEditar, List lstObjetoEliminar) {
        Session conn = null;
        try {
            conn = HibSession.abrirSesion();
            conn.getTransaction().begin();
//            objetoGuardar = util.transformarObjetoMayusculas(objetoGuardar);
            if (lstObjetoEliminar !=null){
            for (Iterator<Object> ittt = lstObjetoEliminar.iterator(); ittt.hasNext();) {
                Object obj = ittt.next();
                conn.delete(obj);
            }}
            if (lstObjetoGuardar !=null){
            for (Iterator<Object> it = lstObjetoGuardar.iterator(); it.hasNext();) {
                Object obj = it.next();
                conn.save(obj);
            }
            }
            if (lstObjetoEditar !=null){
            for (Iterator<Object> itt = lstObjetoEditar.iterator(); itt.hasNext();) {
                Object obj = itt.next();
                conn.merge(obj);
            }
            }
            conn.getTransaction().commit();
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            HibSession.cerrarSesion();
        }
    }    

    private List getList(String sqlQuery, Session sesion) {
        Session conn = null;

        if (sesion == null) {
            conn = HibSession.abrirSesion();
        } else {
            conn = sesion;
        }
        try {
            return conn.createSQLQuery(sqlQuery).list();
        } catch (Exception e) {
           // System.err.println("fallo consultaaa");
           // e.printStackTrace();
            return null;
        }
    }

    /**
     * Creado: TaurusTech
     *
     * @param strSelect : nombre del objeto a retornar, si desea que retorne
     * todo se debe enviar NULL
     * @param strObj: Nombre de la clase de la cual se obtendrÃ¡n los datos
     * @param strWhere: predicado de la consulta hql
     * @param tipoQuery: predicado de la consulta hql
     * @return strSql : sql listo para ser ejecutado
     */
    private String getSqlArmado(String strSelect, String strObj, String strWhere, String tipoQuery) {
        String strSql = "";
        String where = "where ";
        String from = "from ";
        if (tipoQuery.equals("H")) {//H:HQL - S:SQL
            /*
             * Cuando se debe recuperar un objeto particular
             */
            if (strSelect != null) {
                strSql = String.format("select %s ", strSelect);
            }
        } else {//CUANDO ES SQL
            /*
             * Cuando se debe recuperar ciertos campos de la base de datos
             */
            if (strSelect != null) {
                strSql = String.format("SELECT %s ", strSelect);
            } else {
                strSql = String.format("SELECT * ");
            }
            where = where.toUpperCase();
            from = from.toUpperCase();
        }
        /*
         * Cuando la consulta tiene un predicado
         */
        if (strWhere != null) {
            strSql = String.format(strSql + from + " %s %s", strObj, where + strWhere);
        } else {
            strSql = String.format(strSql + from + " %s ", strObj);
        }
        return strSql;
    }
        /**
     * Creado: TaurusTech
     *
     * @param q : query a la que se agregará cada uno de los valores de las
     * variables ligadas
     * @param parametros : variable con los valores de las variables ligadas
     * @return q : query listo para ejecutarse
     */
    private Query getQueryParametros(Query q, List<ParametrosConsulta> parametros) {
        try {
            /*
             * Agrego cada uno de los valores a las variables ligadas de la
             * consulta hql
             */
            for (int i = 0; i < parametros.size(); i++) {
                ParametrosConsulta parametro = parametros.get(i);
                if (parametro.getValor() instanceof java.lang.String) {
                    q.setString(parametro.getNombreParametro(), (String) parametro.getValor());
                } else if (parametro.getValor() instanceof java.lang.Boolean) {
                    q.setBoolean(parametro.getNombreParametro(), (Boolean) parametro.getValor());
                } else if (parametro.getValor() instanceof java.lang.Character) {
                    q.setCharacter(parametro.getNombreParametro(), (Character) parametro.getValor());
                } else if (parametro.getValor() instanceof java.math.BigDecimal) {
                    q.setBigDecimal(parametro.getNombreParametro(), (BigDecimal) parametro.getValor());
                } else if (parametro.getValor() instanceof java.lang.Double) {
                    q.setDouble(parametro.getNombreParametro(), (Double) parametro.getValor());
                } else if (parametro.getValor() instanceof java.lang.Integer) {
                    q.setInteger(parametro.getNombreParametro(), (Integer) parametro.getValor());
                } else if (parametro.getValor() instanceof java.lang.Byte) {
                    q.setByte(parametro.getNombreParametro(), (Byte) parametro.getValor());
                } else if (parametro.getValor() instanceof java.lang.Long) {
                    q.setLong(parametro.getNombreParametro(), (Long) parametro.getValor());
                } else if (parametro.getValor() instanceof java.util.Date) {
                    DateFormat myDateFormat = new SimpleDateFormat(formatoFecha);
                    SimpleDateFormat formato = new SimpleDateFormat(formatoFecha);
                    String fechaS = formato.format(parametro.getValor());
                    Date fecha = myDateFormat.parse(fechaS);
                    q.setDate(parametro.getNombreParametro(), fecha);
                } else if (parametro.getValor() instanceof Timestamp) {
                    DateFormat myDateFormat = new SimpleDateFormat(formatoTimestamp);
                    SimpleDateFormat formato = new SimpleDateFormat(formatoTimestamp);
                    String fechaS = formato.format(parametro.getValor());
                    Date fecha = myDateFormat.parse(fechaS);
                    q.setTimestamp(parametro.getNombreParametro(), new java.sql.Timestamp(fecha.getTime()));
                }
            }
            return q;
        } catch (Exception e) {
            return null;
        }

    }


    /**
     * Creado: TaurusTech
     *
     * @param strSelect : nombre del objeto a retornar, si desea que retorne
     * todo se debe enviar null
     * @param strObj: Nombre de la clase de la cual se obtendrÃ¡n los datos
     * @param strWhere: predicado de la consulta hql
     * @param numRegistros: nÃºmero de registros a recuperar
     * @param numPagina: posiciÃ³n de la paginaciÃ³n
     * @param parametros: parÃ¡metros para en enlace de las variables que se
     * usarÃ¡n con el strWhere tiene la forma tipoDato|nombre|valor
     * @param paginado : utilizado para setear los valores de paginación si es
     * que fue invocado desde el evento getListHqlPaginado
     * @param tipoQuery : tipo de consulta que se va a generar (H:HQL / S:SQL)
     * @return : lista de datos
     */
    private List getList(String strSelect,
            String strObj,
            String strWhere,
            List<ParametrosConsulta> parametros,
            String tipoQuery,
            Session sesion) {
        boolean bandera = false;
        Session conn = null;
        if (sesion == null) {
            conn = HibSession.abrirSesion();
        } else {
            bandera = true;
            conn = sesion;
        }
        try {
            String strSql = null;
            /*
             * Si se trata de una consulta que devuelve todos los objetos
             * encontrados, se ejecutarás la consulta desde el from, caso
             * contrario ejecuta también el comando select con su respectivo
             * objeto a retornar
             */
            strSql = getSqlArmado(strSelect, strObj, strWhere, tipoQuery);
            Query q = null;
            /*
             * Dependiendo del tipo de Consulta creo el query
             */
            //System.out.println("CADENA--" + strSql);
            if (tipoQuery.equals("H")) {//H:HQL / S:SQL
                q = conn.createQuery(strSql);
            } else {
                q = conn.createSQLQuery(strSql);
            }

            /*
             * Agrego cada uno de los valores a las variables ligadas de la
             * consulta hql
             */
            if (parametros != null) {
                q = this.getQueryParametros(q, parametros);
            }

           return q.list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            /*
             * sólo si no se envió la sesion se la debe cerrar, caso contrario
             * se la debe mantener abierta
             */
            if (!bandera) {
                HibSession.cerrarSesion();
            }
        }
    }

    public List getListHql(String strSelect,
            String strObj,
            String strWhere,
            List<ParametrosConsulta> parametros) {
        try {
            return this.getList(strSelect, strObj, strWhere,parametros,"H", null);
        } catch (Exception e) {
            return null;
        }
    }
    
      public List<Zona> getListHqlZonas(String strSelect,
            String strObj,
            int id,
            List<ParametrosConsulta> parametros) {
             Session conn = null;
              List<Zona> zonasLista = new ArrayList();
         try {
            conn = HibSession.abrirSesion();
            conn.getTransaction().begin();
            //String  strSql = getSqlArmado(strSelect, strObj, strWhere, "H");
            //Query q =   conn.createQuery(strSql);
               String hql = "FROM Alimentador A WHERE A.id = " + id ;
               Alimentador alimentador  = (Alimentador) conn.createQuery( hql).list().get(0);
              // System.err.println(">>>>>>>>  "+  alimentador.getSubestacion().getDescripcion());
                Set zonas  = alimentador.getZonas();
                for (Iterator iterator = 
                   zonas.iterator(); iterator.hasNext();){
                  Zona zona = (Zona) iterator.next(); 
               //   System.out.println("Zona: " + zona.getDescripcion());
                  zonasLista.add(zona);
                 }

            conn.getTransaction().commit();
            return  zonasLista;
         } catch (Exception e) {
            e.printStackTrace();
            return  null;
            
        } finally {
            HibSession.cerrarSesion();
        }
       
    }
    
      
   public List<Zona> getZonasAfectadasList(String strSelect,
            String strObj,
            int id,
            List<ParametrosConsulta> parametros) {
             Session conn = null;
              List<Zona> zonasLista = new ArrayList();
         try {
            conn = HibSession.abrirSesion();
            conn.getTransaction().begin();
            //String  strSql = getSqlArmado(strSelect, strObj, strWhere, "H");
            //Query q =   conn.createQuery(strSql);
               String hql = "FROM ZonaAfectada A WHERE A.estado = 1";
               List <ZonaAfectada> zonasAfectadas  =  conn.createQuery( hql).list();
              // System.err.println(">>>>>>>>  "+  alimentador.getSubestacion().getDescripcion());
               for (ZonaAfectada zonaAfectada : zonasAfectadas){
                  String var=   zonaAfectada.getZona().getDescripcion();
                   zonasLista.add(zonaAfectada.getZona());
                 }

            conn.getTransaction().commit();
            return  zonasLista;
         } catch (Exception e) {
            e.printStackTrace();
            return  null;
            
        } finally {
            HibSession.cerrarSesion();
        }
       
    }
       
      

    /**
     * Creado: kjimbo
     *
     * @param strCampos : nombre de los campos a retornar, si desea que retorne
     * todo se debe enviar NULL y se coloca (*) implicitamente
     * @param strTablas: Nombre de las tablas de la cual se obtendran los datos
     * @param strWhere: predicado de la consulta hql
     * @param parametros: parametros para en enlace de las variables que se
     * usaran con el strWhere tiene la forma tipoDato|nombre|valor
     * @return
     */
    public List getListSql(String strCampos,
            String strTablas,
            String strWhere,
            List<ParametrosConsulta> parametros) {
        try {
            return this.getList(strCampos, strTablas, strWhere, parametros, "S", null);
        } catch (Exception e) {
            return null;
        }
    }

    public List getListSql(String sqlQuery) {
        try {
            return this.getList(sqlQuery, null);
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<ClaseGeneral> getListSqlCG(String sql) {
        try {
            List lista = getList(sql,null);
            List<ClaseGeneral> lstGeneral = new ArrayList();
            for (Iterator it = lista.iterator(); it.hasNext();) {
                Object[] row = (Object[]) it.next();
                ClaseGeneral objeto = new ClaseGeneral();
                Method metodo = null;
                Class[] valorParametro;
                for (int i = 0; i < row.length; i++) {
                    int idAtributo = i + 1;
                    valorParametro = new Class[1];
                    valorParametro[0] = Class.forName("java.lang.Object");
                    metodo = objeto.getClass().getMethod("setCampo" + idAtributo, valorParametro);
                    metodo.invoke(objeto, row[i]);
                }
                lstGeneral.add(objeto);
            }
            return lstGeneral;
        } catch (Exception e) {
            return null;
        }
    }
    

    public Object transformarObjetoMayusculas(Object objeto) {

        try {
            Class<?> c = objeto.getClass();
            Field[] todoscampos = c.getDeclaredFields();


            for (int n = 0; n < todoscampos.length; n++) {

                Field fld = c.getDeclaredField(todoscampos[n].getName());
                String tipoDato = fld.getType().toString();

                if (tipoDato.equals("class java.lang.String")) {
                    try {
                        fld.setAccessible(true);
                        String valorant = fld.get(objeto).toString();

                        //Valido una Excepcion para los campos de email:
                        if (!(valorant.contains("@"))) {
                            valorant = valorant.toUpperCase();
                        }

                        //Valido si es un valor que no quiero que haga mayuscula
                        //Le presedo al valor de un nomayusc_
                        if (!(valorant.contains("nomay_"))) {
                            valorant = valorant.replaceAll("nomay_", "");
                        }

                        fld.set(objeto, valorant);


                    } catch (Exception e) {
                        //      System.out.println("Error Transformacion Mayusculas: "+e);
                    }
                }

            }


        } catch (Exception e) {
        }

        return objeto;
    }
    public Object getOneSqlSession(String strCampos, String strTablas, String strWhere, List<ParametrosConsulta> parametros, Session sesion) {
        Session conn = null;
        if (sesion == null) {
            conn = HibSession.abrirSesion();
        } else {
            conn = sesion;
        }
        try {
            String strSql = null;
            /*
             * Si se trata de una consulta que devuelve todos los objetos
             * encontrados, se ejecutarás la consulta desde el from, caso
             * contrario ejecuta también el comando select con su respectivo
             * objeto a retornar
             */
            strSql = getSqlArmado(strCampos, strTablas, strWhere, "S");
            Query q = conn.createSQLQuery(strSql);
            /*
             * Agrego cada uno de los valores a las variables ligadas de la
             * consulta hql
             */
            if (parametros != null) {
                q = this.getQueryParametros(q, parametros);
            }
            return q.uniqueResult();
        } catch (Exception e) {
            return null;
        }
    }
    

    //Saca el proximo valor de la secuencia
    public Long nextSecuencia(String secuenciaNombre) {
        //NombreExquema+"."+NombreSequencia".nextcval
        //String sqlSelect = "data_usr.comun_s_codigo.nextval";
        Session conn = null;
        try {
            conn = HibSession.abrirSesion();
            Consultas consulta = new Consultas();
            Object obj = consulta.getOneSqlSession(secuenciaNombre, "dual", null, null, conn);
            return Long.parseLong(obj.toString());
        } catch (Exception e) {
            return new Long(0);
        } finally {
            HibSession.cerrarSesion();
        }

    }
    
    public ResultSet ejecutarQuery(String query){
        //System.out.println("AA:" + query);
        Session conn = null;
        ResultSet rs = null;
        try{
            conn = HibSession.abrirSesion();
            rs = conn.connection().createStatement().executeQuery(query);
            conn.beginTransaction().commit();
            HibSession.cerrarSesion();
            return rs;
        } catch (Exception e) {
            System.out.println("TTTTT--" + e);
            return null;
        }
    }

        public void setAllClientesMontoPrestamo(Session sesion) {
        Session conn = null;
        /*if (sesion == null) {
            conn = HibSession.abrirSesion();
        } else {
            conn = sesion;
        }*/
        
        try {
            //String strSql = null;
            
            conn = HibSession.abrirSesion();
            
            System.out.println("123");
            
            
            CallableStatement proc = conn.connection().prepareCall("{call SetAllClientesMontoPrestamo(?,?,?)}");
            proc.setString(1, "0");
            proc.setString(2, "11000");
            proc.setInt(3, 1);
            
            //proc.executeQuery();
            ResultSet rs = proc.executeQuery();
            conn.connection().commit();
            System.out.println("456");
            
            //ResultSet rs = proc.executeQuery();
            System.out.println("654");
            while (rs.next()) {
                String userid = rs.getString("idCola");
                String username = rs.getString("numCliente");
                System.out.println("SSSS--" + userid + "--" + username);
            }
            
            System.out.println("321");
            
            /*
            List result = q.list();
            for(int i=0; i<result.size(); i++){
                System.out.println("OBJETOOO-" + result.get(i).toString());
                //Stock stock = (Stock)result.get(i);
                //System.out.println(stock.getStockCode());
            }*/
        } catch (Exception e) {
            System.out.println("TTTTT--" + e);
            //return null;
        }
    }
        
    public ResultSet getSPNew(String idCampania, String desde, String hasta) {
        Session conn = null;        
        ResultSet rs = null;
        try {
                conn = HibSession.abrirSesion();
                conn.beginTransaction();
                CallableStatement proc = null;
                //conn.connection().prepareCall("{call sp_reporte_resumen(?,?,?)}");
                String SPsql = "EXEC sp_reporte_resumen ?,?,?";   // for stored proc taking 2 parameters
                PreparedStatement ps = conn.connection().prepareStatement(SPsql);
                ps.setEscapeProcessing(true);
                //ps.setQueryTimeout(<timeout value>);
                ps.setString(1, idCampania);
                ps.setString(2, desde);
                ps.setString(3, hasta);
                rs = ps.executeQuery();
                conn.beginTransaction().commit();
                HibSession.cerrarSesion();
                return rs;
            
        } catch (Exception e) {
            System.out.println("No se pudo recuperar el crédito - " + e);
            HibSession.cerrarSesion();
            return null;
        }
    }
        
    public ResultSet getSPTests(String idCampania, String desde, String hasta) {
        Session conn = null;        
        ResultSet rs = null;
        try {
                conn = HibSession.abrirSesion();
                conn.beginTransaction();
                CallableStatement proc = null;
                conn.connection().prepareCall("{call sp_reporte_resumen(?,?,?)}");
                proc.clearParameters();
                proc.setString(1, idCampania);
                proc.setString(2, desde);
                proc.setString(3, hasta);
                rs = proc.executeQuery();
                conn.beginTransaction().commit();
                HibSession.cerrarSesion();
                return rs;
            
        } catch (Exception e) {
            System.out.println("No se pudo recuperar el crédito - " + e);
            HibSession.cerrarSesion();
            return null;
        }
    }
    
    public ResultSet getSPCliente(String idUsuario, int idRol) {
        Session conn = null;        
        ResultSet rs = null;
        try {
            if (idRol == AGENTE1 || idRol == AGENTE2){
                conn = HibSession.abrirSesion();
                conn.beginTransaction();
                CallableStatement proc = null;
                if (idRol == AGENTE1) proc = conn.connection().prepareCall("{call sp_seleccionar_cliente(?)}");
                else if (idRol == AGENTE2) proc = conn.connection().prepareCall("{call sp_seleccionar_cliente(?)}");
                proc.clearParameters();
                proc.setString(1, idUsuario);
                rs = proc.executeQuery();
                conn.beginTransaction().commit();
                HibSession.cerrarSesion();
                return rs;                
            }else return null;
        } catch (Exception e) {
            System.out.println("No se pudo recuperar el crédito - " + e);
            HibSession.cerrarSesion();
            return null;
        }
    }
    
    public ResultSet getSPNuance() {
        Session conn = null;        
        ResultSet rs = null;
        try {
             conn = HibSession.abrirSesion();
            conn.beginTransaction();
            CallableStatement proc = null;
            proc = conn.connection().prepareCall("{call sp_seleccionar_credito_cola_nuance()}");
            proc.clearParameters();
            rs = proc.executeQuery();
            conn.beginTransaction().commit();
            HibSession.cerrarSesion();
            return rs;
            
        } catch (Exception e) {
            System.out.println("No se pudo recuperar el crédito - " + e);
            HibSession.cerrarSesion();
            return null;
        }
    }
    
    public boolean getSPCrearCola(int idCola, String desdeMonto, String hastaMonto, String desdeDia, String hastaDia, String producto, int monto, int dias, int saldo, int actualizacion, int automatica) {
        Session conn = null;        
        ResultSet rs = null;
        try {
            conn = HibSession.abrirSesion();            
            conn.beginTransaction();
            String sql;
            
            if (automatica == 0)
                sql = "{call sp_creacion_cola(?,?,?,?,?,?,?,?,?,?)}";
            else
                sql = "{call sp_creacion_cola_nuance(?,?,?,?,?,?,?,?,?,?)}";
            
            CallableStatement proc = conn.connection().prepareCall(sql);
            proc.clearParameters();      
            proc.setString(1, desdeMonto);     
            proc.setString(2, hastaMonto);     
            proc.setString(3, desdeDia);     
            proc.setString(4, hastaDia);     
            proc.setInt(5, idCola);      
            proc.setString(6, producto);    
            proc.setInt(7, monto);    
            proc.setInt(8, dias);    
            proc.setInt(9, saldo);    
            proc.setInt(10, actualizacion);    
            proc.execute();
            conn.beginTransaction().commit();   
            HibSession.cerrarSesion();
            return true;
        } catch (Exception e) {
            HibSession.cerrarSesion();
            return false;
        }
    }
    
    ///////////////////////////////////////////////
    
    public ResultSet getSPListaTemporal(int idUsuario) {
        Session conn = null;        
        ResultSet rs = null;
        try {
            conn = HibSession.abrirSesion();
            conn.beginTransaction();
            CallableStatement proc = null;
            proc = conn.connection().prepareCall("{call sp_get_list_phone(?)}");
            proc.clearParameters();
            proc.setInt(1, idUsuario);
            rs = proc.executeQuery();
            conn.beginTransaction().commit();
            HibSession.cerrarSesion();
            return rs;
            
        } catch (Exception e) {
            System.out.println("No se pudo recuperar números" + e);
            HibSession.cerrarSesion();
            return null;
        }
    }
    
    public ResultSet getSPFirstTemporal(int idUsuario) {
        Session conn = null;        
        ResultSet rs = null;
        try {
            conn = HibSession.abrirSesion();
            conn.beginTransaction();
            CallableStatement proc = null;
            proc = conn.connection().prepareCall("{call sp_get_first_phone(?)}");
            proc.clearParameters();
            proc.setInt(1, idUsuario);
            rs = proc.executeQuery();
            conn.beginTransaction().commit();
            HibSession.cerrarSesion();
            return rs;
            
        } catch (Exception e) {
            System.out.println("No se pudo recuperar números" + e);
            HibSession.cerrarSesion();
            return null;
        }
    }
    
    public ResultSet getSPSetIncorrecct(String numero, String mensaje) {
        Session conn = null;        
        ResultSet rs = null;
        try {
            conn = HibSession.abrirSesion();
            conn.beginTransaction();
            CallableStatement proc = null;
            proc = conn.connection().prepareCall("{call sp_set_incorrect(?,?)}");
            proc.clearParameters();
            proc.setString(1, numero);
            proc.setString(2, mensaje);
            rs = proc.executeQuery();
            conn.beginTransaction().commit();
            HibSession.cerrarSesion();
            return rs;
            
        } catch (Exception e) {
            System.out.println("No se pudo actualizar números" + e);
            HibSession.cerrarSesion();
            return null;
        }
    }
    
        public ResultSet getSPBlackList() {
        Session conn = null;        
        ResultSet rs = null;
        try {
            conn = HibSession.abrirSesion();
            conn.beginTransaction();
            CallableStatement proc = null;
            proc = conn.connection().prepareCall("{call sp_black_list()}");
            proc.clearParameters();
            rs = proc.executeQuery();
            conn.beginTransaction().commit();
            HibSession.cerrarSesion();
            return rs;
            
        } catch (Exception e) {
            System.out.println("No se pudo actualizar la lista negra" + e);
            HibSession.cerrarSesion();
            return null;
        }
    }
        
        public ResultSet getSPActualizaClientes() {
        Session conn = null;        
        ResultSet rs = null;
        try {
            conn = HibSession.abrirSesion();
            conn.beginTransaction();
            CallableStatement proc = null;
            proc = conn.connection().prepareCall("{call sp_actualizacion_clientes()}");
            proc.clearParameters();
            rs = proc.executeQuery();
            conn.beginTransaction().commit();
            HibSession.cerrarSesion();
            return rs;
            
        } catch (Exception e) {
            System.out.println("No se pudo actualizar ejecutar la actualizacion" + e);
            HibSession.cerrarSesion();
            return null;
        }
    }
}
