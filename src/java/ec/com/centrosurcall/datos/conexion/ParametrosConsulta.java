/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ec.com.centrosurcall.datos.conexion;

import java.io.Serializable;

/**
 *
 * @author TaurusTech
 */
public class ParametrosConsulta implements Serializable{
    String nombreParametro;
    Object valor;

    public ParametrosConsulta() {
    }

    public ParametrosConsulta(String nombreParametro, Object valor) {
        this.nombreParametro = nombreParametro;
        this.valor = valor;
    }

    public String getNombreParametro() {
        return nombreParametro;
    }

    public void setNombreParametro(String nombreParametro) {
        this.nombreParametro = nombreParametro;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }


}
