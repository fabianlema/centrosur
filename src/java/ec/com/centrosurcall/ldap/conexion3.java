/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.ldap;

import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.modelo.Parametro;
import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;

/**
 *
 * @author TaurusTech
 */

public class conexion3{
    ParametrosDAO paramDAO = new ParametrosDAO();
    Parametro svr = paramDAO.getParametroByAtributo("SERVERLDAP");
    Parametro puerto = paramDAO.getParametroByAtributo("PUERTO");
    Parametro usuarioldap = paramDAO.getParametroByAtributo("USUARIOLDAP");
    Parametro pwdusuarioldap = paramDAO.getParametroByAtributo("PWDUSUARIOLDAP");
    Parametro baseDN1 = paramDAO.getParametroByAtributo("BASEDNOU");
    String baseDNuax;

    public LdapContext getConnection() throws NamingException, IOException{
        // host, port, username and password
        if("".equals(pwdusuarioldap)){
               return null;
           }
            
            String ldapURL = "ldap://"+svr.getValor()+":"+puerto.getValor();
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, ldapURL);
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, usuarioldap.getValor()); //we have 2 \\ because it's a escape char
            env.put(Context.SECURITY_CREDENTIALS, pwdusuarioldap.getValor());
            
            LdapContext ctx = new InitialLdapContext(env, null);

            ctx.setRequestControls(new Control[]{ 
                new PagedResultsControl(10000, Control.CRITICAL) });
            
            return ctx;
    }

    public NamingEnumeration getResults(LdapContext connection, String baseDN, String filter) throws NamingException{
        //SearchResult searchResult;
        if (connection!=null) {
            return connection.search(baseDN, filter, getSimpleSearchControls());
        }
        return null;
    }
    
    public static SearchControls getSimpleSearchControls() {
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setTimeLimit(30000);
        String[] attrIDs =
        { "SAMAccountName", "sn", "givenname", "employeeID", 
            "employeeNumber" };

        searchControls.setReturningAttributes(attrIDs);
        return searchControls;
    }

    public NamingEnumeration getallbyOU(String ou) throws NamingException, IOException {
        String baseDN = ou;
        String filter = "(&(objectClass=user)(sn=*))";
        LdapContext connection = getConnection();
        try {
            NamingEnumeration results = getResults(connection, baseDN, filter);
            if(results!=null){
                return results;
            }else{
                return null;
            }
        } catch (Exception e) {
            System.out.println(e.getCause().getMessage());
            return null; 
        }
    }
}