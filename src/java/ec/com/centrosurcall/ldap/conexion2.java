/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.ldap;

import java.util.ArrayList;
import javax.naming.*;
import javax.naming.directory.*;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.io.IOException;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
/**
 * Demonstrates how to create an initial context to an LDAP server
 * using simple authentication.
 *
 * usage: java Simple
 */
class conexion2 {
    private Hashtable<?, ?> env;
    
    
    public boolean conexion(String user, String password,String OU, String DC1, String DC2, String DC3) {
    try
        {

            String ldapURL = "ldap://192.168.2.50:389/OU="+OU+",DC="+DC1+", DC="+DC2+", DC="+DC3+"";
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, ldapURL);
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, "COOPJEP\\"+user+""); //we have 2 \\ because it's a escape char
            env.put(Context.SECURITY_CREDENTIALS, password);

            // Create the initial context

            DirContext ctx = new InitialDirContext(env);
            boolean result = ctx != null;
            if(ctx != null)
                ctx.close();

            return result;
        }
        catch (Exception e)
        {           
            return false;
        }
   }
}