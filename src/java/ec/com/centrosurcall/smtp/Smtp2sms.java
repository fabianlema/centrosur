package ec.com.centrosurcall.smtp;

import ec.com.centrosurcall.negocio.controladores.CtrSession;
import ec.com.centrosurcall.utils.FacesUtils;
import java.io.PrintStream;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Smtp2sms {

    private Session session;
    CtrSession ctrsession;

    public Smtp2sms() {
        try {
            this.ctrsession = ((CtrSession) FacesUtils.getManagedBean("ctrSession"));
            Authenticator authenticator = new Authenticator() {
                private PasswordAuthentication authentication;
                {
                    authentication = new PasswordAuthentication(ctrsession.getUsuario2n()+"@"+ctrsession.getDominio2n(), ctrsession.getContrasenia2n());
                }
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return this.authentication;
                }
            };

            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.host", this.ctrsession.getIp2n());
            props.put("mail.smtp.port", "25");
            props.put("mail.smtp.auth", "true");
            this.session = Session.getInstance(props, authenticator);
        } catch (Exception e) {
            System.out.println("Error Autenticación 2N" + e);
        }
    }

    public String send2N(InternetAddress[] direciones, String mensaje)
            throws MessagingException {
        try {
            Message message = new MimeMessage(this.session);
            message.setFrom(new InternetAddress("from@" + this.ctrsession.getUsuario2n()));

            message.addRecipients(Message.RecipientType.TO, direciones);
            message.setSubject("SMS2N");
            message.setContent(mensaje, "text/plain; charset=UTF-8");
            Transport.send(message);
            return "0";
        } catch (Exception e) {
            System.out.println(e);
            return "" + e;
        }
    }
}