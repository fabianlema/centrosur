/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.servlet;

import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fabian
 */
public class ExisteCorreo extends HttpServlet {
    ParametrosDAO parametroDao = new ParametrosDAO();
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            if (responderMail())
                out.write(formarXML());
            else 
                out.write(formarXMLError());
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String formarXML() {
        String xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml  += "<Estados>\n";
        xml += "<Estado>" +  true + "</Estado>\n";
        xml += "</Estados>";
        return xml;
    }
    
    private String formarXMLError() {
        String xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml  += "<Estados>\n";
        xml += "<Estado>" +  false + "</Estado>\n";
        xml += "</Estados>";
        return xml;
    }
    
    public Boolean responderMail() {
        Boolean temBool;        
        String hostImap = parametroDao.getParametroByAtributo("HOSTIMAP").getValor();
        String usernameImap = parametroDao.getParametroByAtributo("USERIMAP").getValor();
        String portImap = parametroDao.getParametroByAtributo("PORTIMAP").getValor();
        String passwordImap = parametroDao.getParametroByAtributo("PASSIMAP").getValor();
        try {
            Session sesionMail = autenticarEntidad(usernameImap, passwordImap);
            Store store = sesionMail.getStore("imap");
            store.connect(hostImap, usernameImap, passwordImap);
            Folder emailFolder = store.getFolder("INBOX");
            emailFolder.open(Folder.READ_ONLY);
            //emailFolder.open(Folder.READ_WRITE);

            Flags seen = new Flags(Flags.Flag.SEEN);
            FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
            Message messages[] = emailFolder.search(unseenFlagTerm);

            if (messages.length == 0) {
                temBool= false;
            } else {
                temBool= true;
            }
            emailFolder.close(true);
            store.close();
        } catch (Exception ex) {
            System.err.println("Error al obtener el contenido Web Service");
            ex.printStackTrace();
            temBool = false;
        }
        return temBool;
        }
        
        public Session autenticarEntidad(final String usernameImap, final String passwordImap) {
        return Session.getInstance(getReadProperties(), new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usernameImap, passwordImap);
            }
        });
    }
        
        public Properties getReadProperties() {
        Properties properties = new Properties();
        properties.setProperty("mail.imap.starttls.enable", "true");
        properties.setProperty("ssl.SocketFactory.provider", "ec.com.centrosurcall.utils.ZimbraSSLSocketFactory");
        properties.setProperty("mail.imap.socketFactory.class", "ec.com.centrosurcall.utils.ZimbraSSLSocketFactory");
        return properties;
    }
}