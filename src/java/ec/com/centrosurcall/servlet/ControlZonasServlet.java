/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.servlet;

import ec.com.centrosurcall.datos.DAO.ZonaAfectadaDAO;
import ec.com.centrosurcall.datos.DAO.ZonaDAO;
import ec.com.centrosurcall.datos.modelo.HoraReposicion;
import ec.com.centrosurcall.datos.modelo.Zona;
import ec.com.centrosurcall.datos.modelo.ZonaAfectada;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author VClient
 */
@WebServlet(name = "ControlZonasServlet", urlPatterns = {"/ControlZonasServlet"})
public class ControlZonasServlet extends HttpServlet {
    private String xml;
    private List<ZonaAfectada> zonaAfectada;
    private String hora;
  //  private List<String> zonas =  new ArrayList<String>();
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if (obtenerDatosPersona() && zonaAfectada.size()>0) 
                out.write(formarXML());                
            else
                out.write(formarXMLError());
        } catch(Exception ex) {
                out.write(formarXMLError());
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private boolean obtenerDatosPersona() throws Exception {
        ZonaAfectadaDAO zonaAfectadaDAO = new ZonaAfectadaDAO();
         //ZonaDAO zonaDAO = new ZonaDAO();
        zonaAfectada = zonaAfectadaDAO.getZonaAfectadaByEstados();
//        for (ZonaAfectada zonaT : zonaAfectada){
//            zonas.add(zonaDAO.getZonaByID(zonaT.getZona().getId()).getDescripcion());
//        }
        if (zonaAfectada != null) {
            HoraReposicion horaRep = zonaAfectadaDAO.getLastHoraReposicion();
            hora = horaRep.getDescripcion();
            if (hora == null) hora="0";
            switch (hora){
                case "Una Hora": hora ="1" ;break;
                case "Dos Horas": hora = "2" ;break;
                case "Tres Horas": hora = "3";break;
                case "Cuatro Horas": hora ="4" ;break;
                case "Cinco Horas": hora ="5";break;
                case "Seis Horas": hora="6";break;
                case "Siete Horas":hora ="7";break;
                case "Ocho Horas": hora ="8" ;break;
                case "Nueve Horas": hora ="9" ;break;
                case "Diez Horas": hora ="10" ;break;
                default : hora="0"; break;
                }
            return true;
        }else return false;
    }
    
    private String formarXML() {
        xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml  += "<ZonasAfectadas>\n";
        xml += "<Error>" +  true + "</Error>\n";
        xml += "<Cantidad>" +  zonaAfectada.size() + "</Cantidad>\n";
        xml += "<Zonas>|";
        for (ZonaAfectada  zona : zonaAfectada) {
            xml +=  zona.getZona().getId() + "|";
        }
        xml += "</Zonas>\n";
        xml += "<Hora>" +  hora + "</Hora>\n";
        xml += "</ZonasAfectadas>";
        return xml;
    }
    
    private String formarXMLError() {
        xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml  += "<ZonasAfectadas>\n";
        xml += "<Error>" +  false + "</Error>\n";
        xml += "<Cantidad>0</Cantidad>\n";
        xml += "<Zonas>0</Zonas>\n";
        xml += "<Hora>0</Hora>\n";
        xml += "</ZonasAfectadas>";
        return xml;
    }
}