/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.servlet;

import ec.com.centrosurcall.datos.DAO.CorreoDAO;
import ec.com.centrosurcall.datos.modelo.EstadoLlamada;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wilso
 */
@WebServlet(name = "MailAlertServlet", urlPatterns = {"/MailAlertServlet"})
public class MailAlertServlet extends HttpServlet {
    private String xml;
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        CorreoDAO correo = new CorreoDAO();
        EstadoLlamada estadoLlamada = correo.getEstadoCorreo();
        boolean estado ;
         if  (estadoLlamada.getEstado()==1)estado=true;
         else estado = false;
         //estado=true;
        try {
           if (estado)
             out.write(formarXML());
           else 
             out.write(formarXMLError());
           
        } finally {            
            out.close();
        }
    }
    
    private String formarXML() {
        xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml  += "<Estados>\n";
        xml += "<Estado>" +  true + "</Estado>\n";
        xml += "</Estados>";
        return xml;
    }
    
    private String formarXMLError() {
        xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml  += "<Estados>\n";
        xml += "<Estado>" +  false + "</Estado>\n";
        xml += "</Estados>";
        return xml;
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
