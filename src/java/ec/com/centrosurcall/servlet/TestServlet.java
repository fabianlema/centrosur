/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.servlet;

import ec.com.centrosurcall.datos.DAO.TipoLlamadaDAO;
import ec.com.centrosurcall.datos.modelo.TipoLlamada;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author VClient
 */
@WebServlet(name = "TestServlet", urlPatterns = {"/TestServlet"})
public class TestServlet extends HttpServlet {
    private String xml;
    private TipoLlamada personaLlamar = new TipoLlamada();

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       // System.err.println(">>>>>>>>>>>> tEST SERVLET");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if (obtenerDatosPersona()) 
                out.write(formarXML());                
            else
                out.write(formarXMLError());
        } catch(Exception ex) {
                out.write(formarXMLError());
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private boolean obtenerDatosPersona() throws Exception {
        TipoLlamadaDAO personaLlamarDAO = new TipoLlamadaDAO();
        personaLlamar = personaLlamarDAO.getListaTiposLlamada().get(0);
        if (personaLlamar != null) return true;
        else return false;
    }
    
    private String formarXML() {
        xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml  += "<Llamada>\n";
        xml += "<Error>" +  true + "</Error>\n";
        xml += "<Cedula>" +  personaLlamar.getDescripcion()+ "</Cedula>\n";
        xml += "<Nombres>" +  personaLlamar.getCodigoIvr() + "</Nombres>\n";
        xml += "</Llamada>";
        return xml;
    }
    
    private String formarXMLError() {
        xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml  += "<Llamada>\n";
        xml += "<Error>" +  false + "</Error>\n";
        xml += "<Cedula></Cedula>\n";
        xml += "<Nombres></Nombres>\n";
        xml += "</Llamada>";
        return xml;
    }

}
