/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.servlet;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLDecoder;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FilenameUtils;

public class ImageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
        
         private String filePath;
        public void init() throws ServletException {
            String basePath =  new File("").getAbsolutePath();
            this.filePath = basePath+"/resources";

        }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

            String requestedFile = request.getPathInfo();
                if (requestedFile == null) {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
                    return;
                }

                File file = new File(filePath, URLDecoder.decode(requestedFile, "UTF-8"));
                if (!file.exists()) {
                    response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
                    return;
                }


		  response.setContentType("image/png");
                    BufferedImage bi = ImageIO.read(file);
                    OutputStream out = response.getOutputStream();
                    ImageIO.write(bi, "png", out);
                    out.close();
	

	}

}