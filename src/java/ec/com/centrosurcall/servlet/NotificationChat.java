/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.servlet;

import ec.com.centrosurcall.datos.DAO.CorreoDAO;
import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.modelo.ColaChat;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
//import org.openide.util.Exceptions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Fabian
 */
public class NotificationChat extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NotificationChat</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet NotificationChat at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        BufferedReader reader = request.getReader();
        StringBuilder builder = new StringBuilder();
        String aux = "";
        while ((aux = reader.readLine()) != null) {
            builder.append(aux);
        }
        String text = builder.toString();
        //System.out.println("AA:"+text);
        byte[] bytes = text.getBytes(StandardCharsets.UTF_8);
        InputStream inputStream = new ByteArrayInputStream(bytes);

        //String text = builder.toString();
        //System.out.println("AA:"+text);
        readXml(inputStream);
        reader.close();
        inputStream.close();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void readXml(InputStream is) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        int contador = 0;
        ParametrosDAO parametro = new ParametrosDAO();
        String cuentaTwiter = parametro.getParametroByAtributo("CUENTATWITTER").getValor();
        String cuentaFace = parametro.getParametroByAtributo("CUENTAFACEBOOK").getValor();
        CorreoDAO objCorreo = new CorreoDAO();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            System.out.println("ROOT" + doc.getDocumentElement().getNodeName());
            NodeList nodes = doc.getElementsByTagName("SocialContact");
            System.out.println("==========================");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    contador++;
                    String autorTemp = getValue("author", element);
                    //System.out.println("=====:"+autorTemp);
                    if (!autorTemp.equals(cuentaTwiter) && (!autorTemp.equals(cuentaFace))) {
                        ColaChat colach = new ColaChat();
                        colach.setAutor(autorTemp);
                        colach.setFecha(new Date());
                        colach.setEstado(Boolean.FALSE);
                        colach.setAtendido(Boolean.FALSE);
                        colach.setTexto(getValue("description", element));
                        //System.out.println("=====:"+getValue("description", element));
                        colach.setIdSocialContact(getValue("id", element));
                        //System.out.println("=====:"+getValue("id", element));
                        colach.setUrlContacto(getValue("link", element));
                        //System.out.println("=====:"+getValue("link", element));
                        colach.setTipo(getValue("sourceType", element));
                        //System.out.println("=====:"+getValue("sourceType", element));
                        if (getValue("sourceType", element).equals("chat")) {
                            colach.setTitulo(getValue("integrationAuthTokenGUID", element));
                            //System.out.println("=====:"+getValue("integrationAuthTokenGUID", element));
                        } else {
                            colach.setTitulo(getValue("title", element));
                            //System.out.println("=====:"+getValue("title", element));
                            if (getValue("sourceType", element).equals("facebook")) {
                                String[] arrayTemp = getValue("link", element).split("_");
                                colach.setUrlContacto("https://www.facebook.com/" + arrayTemp[arrayTemp.length - 2] + "?comment_id=" + arrayTemp[arrayTemp.length - 1]);
                            }
                        }
                        colach.setUrl(getValue("refURL", element));
                        //System.out.println("=====:"+getValue("refURL", element));
                        objCorreo.saveColaChat(colach, 1);
                        switch (getValue("sourceType", element)) {
                            case "chat": {
                                sendGet(80);
                                break;
                            }
                            case "twitter_account": {
                                sendGet(78);
                                break;
                            }
                            case "facebook": {
                                sendGet(79);
                                break;
                            }
                        }
                    }
                    System.out.println("==========================");
                }
            }
            //System.out.println("CONTADOR: " + contador);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(NotificationChat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(NotificationChat.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NotificationChat.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String getValue(String tag, Element element) {
        NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodes.item(0);
        if (node!=null){
            return node.getNodeValue();
        }else{
            return "";
        }        
    }

    private void sendGet(int caso) {
        try {
            String url = "";
            //172.19.129.16
            //10.1.1.13
            if (caso == 77) {
                url = "http://172.19.129.16:9080/CSADICOEMAIL";
            } else if (caso == 78) {
                url = "http://172.19.129.16:9080/CSADICOTWITTER";
            } else if (caso == 79) {
                url = "http://172.19.129.16:9080/CSADICOFACEBOOK";
            } else if (caso == 80) {
                url = "http://172.19.129.16:9080/CSADICOCHAT";
            }
            //System.out.println("AAA:" + url);

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
        } catch (Exception ex) {
            System.out.println("Error SendURL: " + ex);
            //Exceptions.printStackTrace(ex);
        }
    }
}
