/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.social;

import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.utils.Base64Converter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Fabian
 */
public class RedesSociales {

    String servidor;
    String usuario;
    String password;
    String cuentaTwiter;
    String respuestaError = "";

    public RedesSociales() {
        ParametrosDAO parametro = new ParametrosDAO();
        servidor = parametro.getParametroByAtributo("HOSTSOCIAL").getValor();
        usuario = parametro.getParametroByAtributo("USERSOCIAL").getValor();
        password = parametro.getParametroByAtributo("PASSSOCIAL").getValor();
        cuentaTwiter = parametro.getParametroByAtributo("CUENTATWITTER").getValor();
    }

    public String getMensaje(String contactoSocial) {
        HostnameVerifier hv = new HostnameVerifier() {
            @Override
            public boolean verify(String urlHostName, SSLSession session) {
                System.out.println("Warning: URL Host: " + urlHostName
                        + " vs. " + session.getPeerHost());
                return true;
            }
        };

        try {
            trustAllHttpsCertificates();
        } catch (Exception e) {
            System.out.println("Trustall" + e.getStackTrace());
        }
        HttpsURLConnection.setDefaultHostnameVerifier(hv);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            URL url = new URL("https://" + servidor + "/ccp-webapp/ccp/conversation/" + contactoSocial);
            String userPassword = usuario + ":" + password;
            String encoding = Base64Converter.encode(userPassword.getBytes("UTF-8"));
            HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
            uc.setRequestProperty("Authorization", String.format("Basic %s", encoding));

            int responseCode = uc.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String html = "";
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(uc.getInputStream());
                doc.getDocumentElement().normalize();

                NodeList nodes = doc.getElementsByTagName("SocialContact");
                System.out.println("==========================");
                Date fecha;
                SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

                for (int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        fecha = new Date(Long.parseLong(getValue("publishedDate", element)));
                        html = html + "<h3>" + getValue("author", element) + " <small>" + formatoDelTexto.format(fecha) + "</small></h3>" + "<h4>" + getValue("description", element) + "  </h4>";
                    }
                }
                return html;
            } else {
                return "";
            }
        } catch (Exception ex) {
            System.err.println("Error Mensajes");
            ex.printStackTrace();
            return "";
        }
    }

    public String responderTweetDirect(String texto, String para) {
        HostnameVerifier hv = new HostnameVerifier() {
            @Override
            public boolean verify(String urlHostName, SSLSession session) {
                System.out.println("Warning: URL Host: " + urlHostName
                        + " vs. " + session.getPeerHost());
                return true;
            }
        };

        try {
            trustAllHttpsCertificates();
        } catch (Exception e) {
            System.out.println("Trustall" + e.getStackTrace());
        }
        HttpsURLConnection.setDefaultHostnameVerifier(hv);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            URL url = new URL("https://" + servidor + "/ccp-webapp/ccp/reply/twitter/direct_messages/new");
            String userPassword = usuario + ":" + password;
            String encoding = Base64Converter.encode(userPassword.getBytes("UTF-8"));
            HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
            uc.setRequestProperty("Authorization", String.format("Basic %s", encoding));

            uc.setRequestMethod("POST");
            uc.setDoOutput(true);
            //uc.setDoInput(true);

            //System.out.println("AA:"+texto);
            String data = "<?xml version='1.0' encoding='utf-8'?>\n"
                    + "<DirectMessage>\n"
                    + "<fromUsername>" + cuentaTwiter + "</fromUsername>\n"
                    + "<toUsername>" + para + "</toUsername>\n"
                    + "<message>" + texto + "</message>\n"
                    + "</DirectMessage>";

            //System.out.println("ASSA:"+data);

            uc.setRequestProperty("Content-Type", "application/xml");
            uc.setRequestProperty("User-Agent", "Mozilla/5.0");
            uc.setRequestProperty("Accept-Charset", "UTF-8");

            PrintWriter out = new PrintWriter(new OutputStreamWriter(uc.getOutputStream(), StandardCharsets.UTF_8), true);
            out.print(data);
            out.close();
            int responseCode = uc.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_ACCEPTED) {
                String urlRespuesta = uc.getHeaderField("Location");
                System.out.println("-----Val : " + urlRespuesta);
                Thread.sleep(2500);
                if (getMensajeRespuesta(urlRespuesta)) {
                    return "0";
                } else {
                    return respuestaError;
                }
            } else {
                return "Error de coneccion";
            }
        } catch (Exception ex) {
            System.err.println("Error Mensajes");
            ex.printStackTrace();
            return "Error al enviar el mensaje";
        }
    }

    public Integer responderTweetStatus(String texto, String para, String urldirecto) {
        HostnameVerifier hv = new HostnameVerifier() {
            @Override
            public boolean verify(String urlHostName, SSLSession session) {
                System.out.println("Warning: URL Host: " + urlHostName
                        + " vs. " + session.getPeerHost());
                return true;
            }
        };

        try {
            trustAllHttpsCertificates();
        } catch (Exception e) {
            System.out.println("Trustall" + e.getStackTrace());
        }
        HttpsURLConnection.setDefaultHostnameVerifier(hv);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            URL url = new URL("https://" + servidor + "/ccp-webapp/ccp/reply/twitter/statuses/update");
            String userPassword = usuario + ":" + password;
            String encoding = Base64Converter.encode(userPassword.getBytes("UTF-8"));
            HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
            uc.setRequestProperty("Authorization", String.format("Basic %s", encoding));

            uc.setRequestMethod("POST");
            uc.setDoOutput(true);
            String[] statusId = urldirecto.split("/");
            String data = "<?xml version='1.0' encoding='utf-8'?>\n"
                    + "<Status>\n"
                    + "<username>" + cuentaTwiter + "</username>\n"
                    + "<message>" + texto + "</message>\n"
                    + "<inReplyToStatusId>" + statusId[statusId.length - 1] + "</inReplyToStatusId>\n"
                    + "</Status>";

            uc.setRequestProperty("Content-Type", "application/xml");
            uc.setRequestProperty("User-Agent", "Mozilla/5.0");
            uc.setRequestProperty("Accept-Language", "UTF-8");

            PrintWriter out = new PrintWriter(new OutputStreamWriter(uc.getOutputStream(), StandardCharsets.UTF_8), true);
            out.print(data);
            out.close();

            int responseCode = uc.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_ACCEPTED) {
                return 0;
            } else {
                return 1;
            }
        } catch (Exception ex) {
            System.err.println("Error Mensajes");
            ex.printStackTrace();
            return 1;
        }
    }

    public Integer responderFaceStatus(String texto, String idsocial) {
        HostnameVerifier hv = new HostnameVerifier() {
            @Override
            public boolean verify(String urlHostName, SSLSession session) {
                System.out.println("Warning: URL Host: " + urlHostName
                        + " vs. " + session.getPeerHost());
                return true;
            }
        };

        try {
            trustAllHttpsCertificates();
        } catch (Exception e) {
            System.out.println("Trustall" + e.getStackTrace());
        }
        HttpsURLConnection.setDefaultHostnameVerifier(hv);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            URL url = new URL("https://" + servidor + "/ccp-webapp/ccp/reply/facebook/comment");
            String userPassword = usuario + ":" + password;
            String encoding = Base64Converter.encode(userPassword.getBytes("UTF-8"));
            HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
            uc.setRequestProperty("Authorization", String.format("Basic %s", encoding));

            uc.setRequestMethod("POST");
            uc.setDoOutput(true);
            String data = "<?xml version='1.0' encoding='utf-8'?>\n"
                    + "<Comment>\n"
                    + "<socialContact>http://" + servidor + "/ccp-webapp/ccp/socialcontact/" + idsocial + "</socialContact>\n"
                    + "<message>" + texto + "</message>\n"
                    + "</Comment>";

            uc.setRequestProperty("Content-Type", "application/xml");
            uc.setRequestProperty("User-Agent", "Mozilla/5.0");
            uc.setRequestProperty("Accept-Language", "UTF-8");

            PrintWriter out = new PrintWriter(new OutputStreamWriter(uc.getOutputStream(), StandardCharsets.UTF_8), true);
            out.print(data);
            out.close();

            int responseCode = uc.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_ACCEPTED) {
                return 0;
            } else {
                return 1;
            }
        } catch (Exception ex) {
            System.err.println("Error Mensajes");
            ex.printStackTrace();
            return 1;
        }
    }

    public Integer responderFaceLike(String idsocial) {
        HostnameVerifier hv = new HostnameVerifier() {
            @Override
            public boolean verify(String urlHostName, SSLSession session) {
                System.out.println("Warning: URL Host: " + urlHostName
                        + " vs. " + session.getPeerHost());
                return true;
            }
        };

        try {
            trustAllHttpsCertificates();
        } catch (Exception e) {
            System.out.println("Trustall" + e.getStackTrace());
        }
        HttpsURLConnection.setDefaultHostnameVerifier(hv);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            URL url = new URL("https://" + servidor + "/ccp-webapp/ccp/reply/facebook/like");
            String userPassword = usuario + ":" + password;
            String encoding = Base64Converter.encode(userPassword.getBytes("UTF-8"));
            HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
            uc.setRequestProperty("Authorization", String.format("Basic %s", encoding));

            uc.setRequestMethod("POST");
            uc.setDoOutput(true);
            String data = "<?xml version='1.0' encoding='utf-8'?>\n"
                    + "<Like>\n"
                    + "<socialContact>http://" + servidor + "/ccp-webapp/ccp/socialcontact/" + idsocial + "</socialContact>\n"
                    + "<likes>1</likes>\n"
                    + "</Like>";

            uc.setRequestProperty("Content-Type", "application/xml");
            uc.setRequestProperty("User-Agent", "Mozilla/5.0");
            uc.setRequestProperty("Accept-Language", "UTF-8");

            PrintWriter out = new PrintWriter(new OutputStreamWriter(uc.getOutputStream(), StandardCharsets.UTF_8), true);
            out.print(data);
            out.close();

            int responseCode = uc.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_ACCEPTED) {
                return 0;
            } else {
                return 1;
            }
        } catch (Exception ex) {
            System.err.println("Error Mensajes");
            ex.printStackTrace();
            return 1;
        }
    }

    public Integer responderFollow(String para) {
        HostnameVerifier hv = new HostnameVerifier() {
            @Override
            public boolean verify(String urlHostName, SSLSession session) {
                System.out.println("Warning: URL Host: " + urlHostName
                        + " vs. " + session.getPeerHost());
                return true;
            }
        };

        try {
            trustAllHttpsCertificates();
        } catch (Exception e) {
            System.out.println("Trustall" + e.getStackTrace());
        }
        HttpsURLConnection.setDefaultHostnameVerifier(hv);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            URL url = new URL("https://" + servidor + "/ccp-webapp/ccp/reply/twitter/follow?account_user=" + cuentaTwiter + "&user_to_follow=" + para);
            String userPassword = usuario + ":" + password;
            String encoding = Base64Converter.encode(userPassword.getBytes("UTF-8"));
            HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
            uc.setRequestProperty("Authorization", String.format("Basic %s", encoding));

            uc.setRequestMethod("POST");
            uc.setDoOutput(true);

            uc.setRequestProperty("Content-Type", "application/xml");
            uc.setRequestProperty("User-Agent", "Mozilla/5.0");
            uc.setRequestProperty("Accept-Language", "UTF-8");

            PrintWriter out = new PrintWriter(new OutputStreamWriter(uc.getOutputStream(), StandardCharsets.UTF_8), true);
            out.close();

            int responseCode = uc.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_ACCEPTED) {
                return 0;
            } else {
                return 1;
            }
        } catch (Exception ex) {
            System.err.println("Error Mensajes");
            ex.printStackTrace();
            return 1;
        }
    }

    public boolean getMensajeRespuesta(String urlRespuesta) {
        HostnameVerifier hv = new HostnameVerifier() {
            @Override
            public boolean verify(String urlHostName, SSLSession session) {
                System.out.println("Warning: URL Host: " + urlHostName
                        + " vs. " + session.getPeerHost());
                return true;
            }
        };

        try {
            trustAllHttpsCertificates();
        } catch (Exception e) {
            System.out.println("Trustall" + e.getStackTrace());
        }
        HttpsURLConnection.setDefaultHostnameVerifier(hv);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        try {
            URL url = new URL(urlRespuesta);
            String userPassword = usuario + ":" + password;
            String encoding = Base64Converter.encode(userPassword.getBytes("UTF-8"));
            HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();
            uc.setRequestProperty("Authorization", String.format("Basic %s", encoding));

            int responseCode = uc.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String html = "";
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(uc.getInputStream());
                doc.getDocumentElement().normalize();

                NodeList nodes = doc.getElementsByTagName("TwitterProgress");
                System.out.println("==========================");
                String respuesta;
                //SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                boolean banderaCorrecto = false;

                for (int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        respuesta = getValue("httpResponseCode", element);
                        if (respuesta.equals("200")) {
                            banderaCorrecto = true;
                        } else {
                            respuestaError = getValue("errorData", element);
                        }
                        //html = html + "<h3>" + getValue("author", element) + " <small>" + formatoDelTexto.format(fecha) + "</small></h3>" + "<h4>" + getValue("description", element) + "  </h4>";
                    }
                }
                return banderaCorrecto;
            } else {
                return false;
            }
        } catch (Exception ex) {
            System.err.println("Error Mensajes");
            ex.printStackTrace();
            return false;
        }
    }

    public static class TempTrustedManager implements
            javax.net.ssl.TrustManager, javax.net.ssl.X509TrustManager {

        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        public boolean isServerTrusted(
                java.security.cert.X509Certificate[] certs) {
            return true;
        }

        public boolean isClientTrusted(
                java.security.cert.X509Certificate[] certs) {
            return true;
        }

        public void checkServerTrusted(
                java.security.cert.X509Certificate[] certs, String authType)
                throws java.security.cert.CertificateException {
            return;
        }

        public void checkClientTrusted(
                java.security.cert.X509Certificate[] certs, String authType)
                throws java.security.cert.CertificateException {
            return;
        }
    }

    private static void trustAllHttpsCertificates() throws Exception {
        javax.net.ssl.TrustManager[] trustAllCerts =
                new javax.net.ssl.TrustManager[1];
        javax.net.ssl.TrustManager tm = new TempTrustedManager();
        trustAllCerts[0] = tm;
        javax.net.ssl.SSLContext sc =
                javax.net.ssl.SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, null);
        javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
                sc.getSocketFactory());
    }

    private static String getValue(String tag, Element element) {
        NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodes.item(0);
        return node.getNodeValue();
    }

    public String getServidor() {
        return servidor;
    }

    public void setServidor(String servidor) {
        this.servidor = servidor;
    }
}
