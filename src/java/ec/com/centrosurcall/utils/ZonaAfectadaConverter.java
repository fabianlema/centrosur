/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import ec.com.centrosurcall.datos.DAO.EventoDAO;
import ec.com.centrosurcall.datos.DAO.ZonaAfectadaDAO;
import ec.com.centrosurcall.datos.DAO.ZonaDAO;
import ec.com.centrosurcall.datos.modelo.Alimentador;
import ec.com.centrosurcall.datos.modelo.Evento;
import ec.com.centrosurcall.datos.modelo.Zona;
import ec.com.centrosurcall.datos.modelo.ZonaAfectada;
import javax.el.ELContext;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author carlosguaman65
 */


@FacesConverter("zonasAfectadasConverter")
public class ZonaAfectadaConverter implements  Converter  {

 
    private ZonaDAO zonaAfectadaParser;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
          for (Zona zonaAfectada : getZonasAgectadasParser(context).getZonasList()) {
            if (zonaAfectada.getDescripcion().equals(value)) {
              //  System.err.println("Elemento:"  +zonaAfectada.getDescripcion());
                return zonaAfectada;
            }
        }
        return null; 
     
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) return null;
   //     System.out.println(" objeto -  String"  + ((Zona) value).getDescripcion());
        return ((Zona) value).getDescripcion();
    }
    private ZonaDAO getZonasAgectadasParser(FacesContext facesContext) {
        if (zonaAfectadaParser == null) {
            ELContext elContext = facesContext.getELContext();
            zonaAfectadaParser = (ZonaDAO) elContext.getELResolver().getValue(elContext, null, "zonaDAO");
        }
        return zonaAfectadaParser;
    }
   
}
