/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import javax.faces.model.SelectItem;

/**
 *
 * @author andresmolina
 */
public class RelacionTmp implements java.io.Serializable {

    private String identificacion;
    private String nombreLegal;
    private String relacion;
    private String direccionPrincipal;
    private String mail;
    private String nombresCompletosConyugue;
    private String identificacionConyugue;
    private String profesion;
    private SelectItem[] selectItemTelefonos;

    public RelacionTmp(String identificacion, String nombreLegal, String relacion, String direccionPrincipal, String mail, String nombresCompletosConyugue, String identificacionConyugue, String profesion, SelectItem[] selectItemTelefonos) {
        this.identificacion = identificacion;
        this.nombreLegal = nombreLegal;
        this.relacion = relacion;
        this.direccionPrincipal = direccionPrincipal;
        this.mail = mail;
        this.nombresCompletosConyugue = nombresCompletosConyugue;
        this.identificacionConyugue = identificacionConyugue;
        this.profesion = profesion;
        this.selectItemTelefonos = selectItemTelefonos;
    }

    /**
     * @return the identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the identificacion to set
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * @return the nombreLegal
     */
    public String getNombreLegal() {
        return nombreLegal;
    }

    /**
     * @param nombreLegal the nombreLegal to set
     */
    public void setNombreLegal(String nombreLegal) {
        this.nombreLegal = nombreLegal;
    }

    /**
     * @return the relacion
     */
    public String getRelacion() {
        return relacion;
    }

    /**
     * @param relacion the relacion to set
     */
    public void setRelacion(String relacion) {
        this.relacion = relacion;
    }

    public String getDireccionPrincipal() {
        return direccionPrincipal;
    }

    public void setDireccionPrincipal(String direccionPrincipal) {
        this.direccionPrincipal = direccionPrincipal;
    }

    public String getIdentificacionConyugue() {
        return identificacionConyugue;
    }

    public void setIdentificacionConyugue(String identificacionConyugue) {
        this.identificacionConyugue = identificacionConyugue;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNombresCompletosConyugue() {
        return nombresCompletosConyugue;
    }

    public void setNombresCompletosConyugue(String nombresCompletosConyugue) {
        this.nombresCompletosConyugue = nombresCompletosConyugue;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public SelectItem[] getSelectItemTelefonos() {
        return selectItemTelefonos;
    }

    public void setSelectItemTelefonos(SelectItem[] selectItemTelefonos) {
        this.selectItemTelefonos = selectItemTelefonos;
    }
}
