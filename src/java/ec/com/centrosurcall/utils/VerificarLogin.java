/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import ec.com.centrosurcall.datos.modelo.DetalleMod;
import ec.com.centrosurcall.negocio.controladores.CtrModulos;
import java.io.IOException;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author TaurusTech
 */
public class VerificarLogin implements javax.servlet.Filter {

    FilterConfig fc;
    CtrModulos totalmodulos;
    List<DetalleMod> algo;

    public VerificarLogin() {
    }
    boolean pagLogueo = false;
    FacesContext context = FacesContext.getCurrentInstance();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        fc = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession(true);
        String pageRequested = req.getRequestURL().toString();
        if (pageRequested.contains(".xhtml")) {
            pagLogueo = false;
        }

        if (session.getAttribute("ctrModulos") != null) {
            totalmodulos = (CtrModulos) session.getAttribute("ctrModulos");
            algo = totalmodulos.lstModulos;
            boolean bandera = false;
            for (int i = 0; i <= algo.size() - 1; i++) {
                if(algo.get(i).getModulo()!=null){
                    if(pageRequested.contains(algo.get(i).getModulo().getUrl())){
                        bandera = true;
                        break;
                    }
                }
                if (pageRequested.contains("index.xhtml") || pageRequested.contains("Principal.xhtml") || 
                        pageRequested.contains("frmModulos.xhtml") || pageRequested.contains("frmCampanias.xhtml") || 
                        pageRequested.contains("frmParametros.xhtml") || pageRequested.contains("frmRazones.xhtml") || 
                        pageRequested.contains("frmRoles.xhtml") || pageRequested.contains("frmSkills.xhtml") || 
                        pageRequested.contains("frmTeams.xhtml") || pageRequested.contains("frmUsuarios.xhtml") || 
                        pageRequested.contains("frmTllamada.xhtml") || pageRequested.contains("frmEventos.xhtml") || 
                        pageRequested.contains("frmPreguntas.xhtml") || pageRequested.contains("frmDatosCorreo.xhtml") || 
                        pageRequested.contains("frmRegistroLlamada.xhtml") || pageRequested.contains("redirectTwitter.xhtml") || 
                        pageRequested.contains("frmCodigoIvr.xhtml") || pageRequested.contains("frmAsteriskServer.xhtml") || 
                        pageRequested.contains("frmInterfazTkr.xhtml") || pageRequested.contains("frmCiudad.xhtml") || 
                        pageRequested.contains("frmCentroCosto.xhtml") || pageRequested.contains("frmDepartamento.xhtml") || 
                        pageRequested.contains("frmUsuarioLlamada.xhtml") || pageRequested.contains("frmAgencia.xhtml") || 
                        pageRequested.contains("frmEquipo.xhtml") || pageRequested.contains("frmTareas.xhtml") || 
                        pageRequested.contains("frmRegistroSeccion.xhtml")) {
                    bandera = true;
                    break;
                }
            }
            if (bandera == false) {
                this.pagLogueo = true;
                resp.sendRedirect("/CentroSurCM/faces/index.xhtml");
            }
        }

        if (pageRequested.contains("Login.aspx") || pageRequested.contains("cerrarSesion.aspx") || pageRequested.contains("frmInicio.xhtml") || pageRequested.contains("principal.xhtml") || pageRequested.contains("index.xhtml") || pageRequested.contains("redirectTwitter.xhtml")) {
            this.pagLogueo = true;
        }

        String[] filtros = {".xhtml", "resources/Estilos", "javax.faces.resource", "Imagenes/"};
        if (!this.pagLogueo) {
            boolean encuentra = false;
            for (int n = 0; n < filtros.length; n++) {
                if (!pageRequested.contains(filtros[n])) {
                } else {
                    boolean usuarioLogueado = false;
                    String usuarioId = (String) session.getAttribute("usuarioId");
                    if (usuarioId != null && !(usuarioId.equals("")) && !pageRequested.contains("Ingreso/Login.aspx")) {
                        usuarioLogueado = true;
                    }
                    if (usuarioLogueado) {
                        chain.doFilter(request, response);
                        encuentra = true;
                        break;
                    }
                }
            }
            try {
                if (encuentra == false) {
                    resp.sendRedirect("/CentroSurCM/faces/index.xhtml");
                    session.setAttribute("usuarioId", null);
                    req.getSession(false).invalidate();
                }
            } catch (Exception e) {
                System.out.println(e.getCause().getMessage());
            }
        } else {
            chain.doFilter(request, response);
        }
    }
    @Override
    public void destroy() {
        fc = null;
    }
}
