/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

//import com.soft.middleware.core.interfaces.IJep;
import ec.com.centrosurcall.datos.DAO.ClientesDAO;
import ec.com.centrosurcall.datos.DAO.LlamadasDAO;
import ec.com.centrosurcall.datos.DAO.RazonesDAO;
import ec.com.centrosurcall.datos.DAO.UsuariosDAO;
import ec.com.centrosurcall.datos.modelo.Cliente;
import ec.com.centrosurcall.datos.modelo.Llamada;
import ec.com.centrosurcall.datos.modelo.Razon;
import ec.com.centrosurcall.datos.modelo.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author TaurusTech
 */
public class RegistroLlamada extends HttpServlet {

    private String clicod, numTelefono, fecha, hora, duracion, xml;
    private int contestada;
    private Llamada llamada;
    private Cliente cliente;
    private Usuario usuario;
    private Razon razon;
    private ClientesDAO clienteDAO;
    private UsuariosDAO usuarioDAO;
    private LlamadasDAO llamadasDAO;
    private RazonesDAO razonDAO;
    //private IJep jep;
    private Date fechaA, horaA, duracionA;
    
    public RegistroLlamada() {
        clienteDAO = new ClientesDAO();
        llamadasDAO = new LlamadasDAO();
        usuarioDAO = new UsuariosDAO();
        razonDAO = new RazonesDAO();
        llamada = new Llamada();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
             clicod = request.getParameter("cliente");
             numTelefono = request.getParameter("telefono");
             fecha = request.getParameter("fecha");
             
             SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyyMMdd");
             fechaA = formatoFecha.parse(fecha);
             
             hora = request.getParameter("hora");
             String hora2;
            
             String[] campos = hora.split ("N");
             hora2=campos [0].toString()+":"+campos [1].toString()+":"+campos [2].toString();
             
             SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");
             horaA = formatoHora.parse(hora2);
                          
             duracion = request.getParameter("duracion");
             
             String duracion2;
            
             String[] camposd = duracion.split ("N");
             duracion2=camposd [0].toString()+":"+camposd [1].toString()+":"+camposd [2];
             
             SimpleDateFormat formatoDuracion = new SimpleDateFormat("HH:mm:ss");
             duracionA = formatoDuracion.parse(duracion2);
             
             contestada = Integer.valueOf(request.getParameter("contestada"));
             
             grabarEstadoLlamada();
             out.write(xml);
        } catch (Exception ex) {
            generarXMLError();
            out.write(xml);
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private void grabarEstadoLlamada() throws Exception{
        if (clicod.equals("") || numTelefono.equals("") || fecha.equals("") || hora.equals("") || duracion.equals("")) {
            generarXMLError();
            return;
        }
        
        cliente = clienteDAO.getClienteByID(clicod);
        if (cliente == null) {
            generarXMLError();
            return;
        }        
        
        usuario = usuarioDAO.getUsuarioByNombre("nuance");
        if (usuario == null) {
            generarXMLError();
            return;
        }
        
        llamada.setCliente(cliente);
        llamada.setEstado(1);
        llamada.setEstadoLlamada(contestada);
        llamada.setFechaLlamada(fechaA);
        llamada.setHora(horaA);
        llamada.setNumeroTelefono(numTelefono);
        llamada.setObservacion("");
        
        if (contestada == 1) 
            razon = razonDAO.getRazonByID(1);
        else
            razon = razonDAO.getRazonByID(9);
        
        llamada.setRazon(razon);
        llamada.setTiempoDuracion(duracionA);
        llamada.setTipoLlamada(0);
        llamada.setTipoTelefono(numTelefono);
        llamada.setUsuario(usuario);
        
        if (llamadasDAO.saveLlamada(llamada, 0)) {
            generarXML();
        } else {
            generarXMLError();
        }
    }
    
    private void generarXMLError() {
        xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml  += "<RegistroLlamada>\n";
        xml += "<Error>" +  true + "</Error>\n";
        xml += "<Mensaje>Registro Incorrecto</Mensaje>\n";
        xml += "</RegistroLlamada>"; 
    }
    
    private void generarXML() {
        xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        xml  += "<RegistroLlamada>\n";
        xml += "<Error>" +  false + "</Error>\n";
        xml += "<Mensaje>Registro Correcto</Mensaje>\n";
        xml += "</RegistroLlamada>"; 
    }
}