/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import ec.com.centrosurcall.datos.DAO.AlimentadorDAO;
import ec.com.centrosurcall.datos.DAO.EventoDAO;
import ec.com.centrosurcall.datos.modelo.Alimentador;
import ec.com.centrosurcall.datos.modelo.Evento;
import javax.el.ELContext;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author carlosguaman65
 */
@FacesConverter("alimentadorConverter")
public class AlimentadorConverter implements Converter {
   
    private AlimentadorDAO alimentadorParser;

    public Object getAsObject(FacesContext facesContext, UIComponent component, String s) {
        for (Alimentador alimentador : getAlimentadorParser(facesContext).getAlimentadoresList()) {
            if (alimentador.getCodigo().equals(s)) {
               
                return alimentador;
            }
        }
        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object o) {
        if (o == null) return null;
      //  System.out.println(" objeto -  String"  + ((Evento) o).getDescripcion());
        return ((Alimentador) o).getCodigo();
    }

    private AlimentadorDAO getAlimentadorParser(FacesContext facesContext) {
        if (alimentadorParser == null) {
            ELContext elContext = facesContext.getELContext();
            alimentadorParser = (AlimentadorDAO) elContext.getELResolver().getValue(elContext, null, "alimentadorDAO");
        }
        return alimentadorParser;
    }
}
