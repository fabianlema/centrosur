/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

/**
 *
 * @author Fabian
 */
public class ColumnaTemporal {
    private int orden;
    private String nombre;

    public int getOrden() {
        return orden;
    }

    public ColumnaTemporal(int orden, String nombre) {
        this.orden = orden;
        this.nombre = nombre;
    }
    
    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
