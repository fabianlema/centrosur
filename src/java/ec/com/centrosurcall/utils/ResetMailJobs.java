/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import ec.com.centrosurcall.datos.DAO.CorreoDAO;
import ec.com.centrosurcall.datos.modelo.BandejaEntrada;
import java.util.List;
//import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author wilso
 */
public class ResetMailJobs  implements Job {

    //private Logger log = Logger.getLogger(ResetMailJobs.class);

   public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {
      CorreoDAO  correo = new CorreoDAO();
      List<BandejaEntrada> bandeja= correo.getCorreos();
      for (BandejaEntrada  mail : bandeja ) {
          if (mail.getLeido() && mail.getRespondido()){}
          else {
             mail.setLeido(true);
             correo.guardarCorreoDescargado(mail, 0);
          }
      }
      
       System.err.println(">>>> Mails Actualizados"); 
    //  log.debug("Mails actulizados  exitosamente");
   }
}
