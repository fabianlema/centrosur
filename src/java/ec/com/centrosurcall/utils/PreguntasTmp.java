/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import javax.faces.model.SelectItem;

/**
 *
 * @author Fabian
 */
public class PreguntasTmp implements java.io.Serializable{
    private Integer numeroPregunta;
    private String titulo;
    private Integer tipoPregunta;  
    private String tituloPregunta;
    private String respuesta;
    private SelectItem[] selectItems;

    public PreguntasTmp(Integer numeroPregunta, String titulo, Integer tipoPregunta, String tituloPregunta) {
        this.numeroPregunta = numeroPregunta;
        this.titulo = titulo;
        this.tipoPregunta = tipoPregunta;
        this.tituloPregunta = tituloPregunta;
        this.selectItems = selectItems;
    }

    public Integer getNumeroPregunta() {
        return numeroPregunta;
    }

    public void setNumeroPregunta(Integer numeroPregunta) {
        this.numeroPregunta = numeroPregunta;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getTipoPregunta() {
        return tipoPregunta;
    }

    public void setTipoPregunta(Integer tipoPregunta) {
        this.tipoPregunta = tipoPregunta;
    }

    public String getTituloPregunta() {
        return tituloPregunta;
    }

    public void setTituloPregunta(String tituloPregunta) {
        this.tituloPregunta = tituloPregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public SelectItem[] getSelectItems() {
        return selectItems;
    }

    public void setSelectItems(SelectItem[] selectItems) {
        this.selectItems = selectItems;
    }
}
