/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import ec.com.centrosurcall.datos.DAO.AsignacionAgentesDAO;
import ec.com.centrosurcall.datos.DAO.ZonaDAO;
import ec.com.centrosurcall.datos.modelo.Usuario;
import ec.com.centrosurcall.datos.modelo.Zona;
import javax.el.ELContext;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;


/**
 *
 * @author carlosguaman65
 */


@FacesConverter("asignacionAgentesConverter")
public class AsignacionAgentesConverter implements  Converter  {

 
    private AsignacionAgentesDAO asignacionAgentesParser;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
          for (Usuario usuario : getAsignacionAgentesParser(context).getAgentesList()) {
            if (usuario.getNombre().equals(value)) {
                System.err.println("Elemento:"  + usuario.getNombre());
                return usuario;
            }
        }
        return null; 
   
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) return null;
      //  System.out.println(" objeto -  String"  + ((Evento) o).getDescripcion());
        return ((Usuario) value).getNombre();
    }
    private AsignacionAgentesDAO getAsignacionAgentesParser(FacesContext facesContext) {
        if (asignacionAgentesParser == null) {
            ELContext elContext = facesContext.getELContext();
            asignacionAgentesParser = (AsignacionAgentesDAO) elContext.getELResolver().getValue(elContext, null, "asignacionAgentesDAO");
        }
        return asignacionAgentesParser;
    }
   
}
