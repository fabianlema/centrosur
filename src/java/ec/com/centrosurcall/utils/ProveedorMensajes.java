/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;

/**
 *
 * @author TaurusTech
 */
public class ProveedorMensajes {
    
    private ResourceBundle bundle;
    
    public ResourceBundle getBundle() {
	        if (bundle == null) {
	            FacesContext context = FacesContext.getCurrentInstance();
	            bundle = context.getApplication().getResourceBundle(context, "msgs");
	        }
	        return bundle;
	    }
	 
	    public String getValue(String key) {
	 
	        String result = null;
	        try {
	            result = getBundle().getString(key);
	        } catch (MissingResourceException e) {
	            result = "???" + key + "??? not found";
	        }
	        return result;
	    }
    
}
