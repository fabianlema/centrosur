/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;
import ec.com.centrosurcall.datos.DAO.TipoLlamadaDAO;
import ec.com.centrosurcall.datos.modelo.TipoLlamada;
import javax.el.ELContext;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author carlosguaman65
 */


@FacesConverter("tiposConverter")
public class TiposConverter implements Converter {
    
    private TipoLlamadaDAO tipoParser;

    public Object getAsObject(FacesContext facesContext, UIComponent component, String s) {
        for (TipoLlamada tipo : getTypesParser(facesContext).getTiposLlamada()) {
            if (tipo.getDescripcion().equals(s)) {
                //System.err.println("S-O"+  tipo.getDescripcion());
                return tipo;
            }
        }
        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object o) {
        if (o == null) return null;
        // System.err.println("O-S"+ ((TipoLlamada) o).getDescripcion());
        return ((TipoLlamada) o).getDescripcion();
    }

    private TipoLlamadaDAO getTypesParser(FacesContext facesContext) {
        if (tipoParser == null) {
            ELContext elContext = facesContext.getELContext();
            tipoParser = (TipoLlamadaDAO) elContext.getELResolver().getValue(elContext, null, "tipoLlamadaDAO");
        }
        return tipoParser;
    }
}
