///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package ec.com.centrosurcall.utils;
//
//import ec.com.centrosurcall.datos.DAO.ClientesDAO;
//import ec.com.centrosurcall.datos.DAO.TelefonosDAO;
//import ec.com.centrosurcall.middle.Credito;
//import ec.com.centrosurcall.middle.IJep;
//import ec.com.centrosurcall.middle.ReferenciaPersonal;
//import ec.com.centrosurcall.middle.Socio;
//import ec.com.centrosurcall.middle.Telefono;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.math.BigDecimal;
//import java.util.List;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// *
// * @author TaurusTech
// */
//public class PrestamoUsuario extends HttpServlet {
//
//    /*
//     * Variables Locales
//     */
//    private List<Credito> creditos;
//    private Socio socio;
//    private List<ReferenciaPersonal> referencias;
//    private List<Telefono> telefonos;
//    private TelefonosDAO telefonoDao;
//    private ClientesDAO creditoDao;
//    private ec.com.centrosurcall.datos.modelo.Credito credito = null; 
//    private List<ec.com.centrosurcall.datos.modelo.Telefono> listaTelefonos;
//    private String idUsuario, xml;
//    private IJep jep;
//    private Credito selCreditoSWeb = new Credito();
//    /** 
//     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.setContentType("text/xml;charset=UTF-8");
//        PrintWriter out = response.getWriter();
//        try {
//            idUsuario = request.getParameter("idUsuario");
//            obtenerDatosUsuario();
//            out.write(xml);            
//        } catch(Exception ex) {
//            out.write(xml);
//        } finally {            
//            out.close();
//        }
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /** 
//     * Handles the HTTP <code>GET</code> method.
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /** 
//     * Handles the HTTP <code>POST</code> method.
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /** 
//     * Returns a short description of the servlet.
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>
//    
//    private void obtenerDatosUsuario() {
//        jep = new IJep();
//        telefonoDao = new TelefonosDAO();
//        creditoDao = new ClientesDAO();
//        
//        credito = creditoDao.getCreditosByIdSocio(idUsuario);
//
//        if (credito == null) {
//            formarXMLError();
//            return;
//        }
//
//        /*
//         * Obtiene del Servicio web los datos de:
//         *  - Credito
//         *  - Socio
//         *  - Referencias Personales
//         *  - Telefonos asociados al socio
//         */
//
//        socio = jep.obtenerDatosSocioGarante(idUsuario);
//        referencias = jep.obtenerReferenciasPersonales(idUsuario);
//        telefonos = jep.obtenerTelefonosDelSocio(idUsuario);                
//        listaTelefonos = telefonoDao.getTelefonoByIdCliente(idUsuario);
//        creditos = jep.obtenerCreditosPorSocio(credito.getNumCredito());
//        
//        if (creditos.size() > 0)
//            setSelCreditoSWeb(creditos.get(0));
//            
//        formarXML();
//    }
//    
//    private void formarXML() {        
//        xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
//        xml  += "<Prestamo>\n";
//        xml += "<Error>" +  false + "</Error>\n";
//        xml += "<IdCliente>" +  (socio.getIdentificacion().equals("null") ? "" : socio.getIdentificacion()) + "</IdCliente>\n";
//        xml += "<NombreLegal>" + (socio.getNombrelegal().equals("null") ? "" : socio.getNombrelegal())+ "</NombreLegal>\n";
//        xml += "<Profesion>" + (socio.getProfesion() == null ? "" : socio.getProfesion())+ "</Profesion>\n";
//        xml += "<NumCredito>" + (credito.getNumCredito().equals("null") ? "" : credito.getNumCredito()) + "</NumCredito>\n";
//        xml += "<NCuotasPendientes>" + (credito.getNcuotasPendientes() == null ? "" : credito.getNcuotasPendientes()) + "</NCuotasPendientes>\n";
//        xml += "<NCuotasVencidas>" + (credito.getNcuotasVencidas() == null ? "" : credito.getNcuotasVencidas()) + "</NCuotasVencidas>\n";
//        xml += "<NDiasVencimiento>" + (credito.getNdiasVencimiento() == null ? "" : credito.getNdiasVencimiento()) + "</NDiasVencimiento>\n";
//        xml += "<MontoPrestamo>" + (credito.getMontoPrestamo() == null ? "" : credito.getMontoPrestamo()) + "</MontoPrestamo>\n";
//        xml += "<MontoPagado>" + (credito.getMontoPagado() == null ? "" : credito.getMontoPagado()) + "</MontoPagado>\n";
//        xml += "<MontoVencido>" + (selCreditoSWeb.getMontoVencido() == null ? "" : selCreditoSWeb.getMontoVencido()) + "</MontoVencido>\n";
//        xml += "<SaldoActual>" + (selCreditoSWeb.getSaldoActual() == null ? "" : selCreditoSWeb.getSaldoActual()) + "</SaldoActual>\n";
//        xml += "<CuotaVencida>" + (credito.getCuotaVencida() == null ? "" : credito.getCuotaVencida()) + "</CuotaVencida>\n";
//        xml += "<FechaProximoPago>" + (credito.getFproximoPago() == null ? "" : credito.getFproximoPago()) + "</FechaProximoPago>\n";
//        
//        // Se obtiene la lista de telefonos ingresados en el sistema asociados al socio
//        xml += "<Telefonos>\n";
//        int cont = 1;
//        if (listaTelefonos != null && listaTelefonos.size()>0) {
//            for (ec.com.centrosurcall.datos.modelo.Telefono telefono : listaTelefonos) {
//                if (cont == 6)  break;
//                xml += "<Telefono" + cont + ">" + telefono.getTelefono() + "</Telefono" + cont + ">\n";
//                cont++;
//            }            
//        }
//        
//        // Se obtiene la lista de telefonos del FIT tanto del socio, conyugue y referencias personales.        
//        if (cont <= 5) {
//            if (telefonos != null && telefonos.size() > 0) {
//                for (Telefono telefono : telefonos) {
//                    if (cont == 6)  break;
//                    xml += "<Telefono" + cont + ">" + telefono.getNumeroTelefono() + "</Telefono" + cont + ">\n";
//                    cont++;
//                }
//            }
//        }
//        
//        if (cont <= 5) {
//            if (referencias != null && referencias.size() >0) {
//                for (ReferenciaPersonal referencia : referencias) {
//                    if (cont == 6) break;
//                    xml += "<Telefono" + cont + ">" + referencia.getTelefono() + "</Telefono" + cont + ">\n";
//                    cont++;
//                }
//            }
//        }
//        
//        if (cont <= 5) {
//            for (int i = cont; i <= 5; i++) {
//                xml += "<Telefono" + i + ">0</Telefono" + i + ">\n";
//            }
//        }
//        
//        xml += "</Telefonos>\n";
//        xml += "</Prestamo>";
//    }
//    
//    private void formarXMLError() {
//        xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
//        xml  += "<Prestamo>\n";
//        xml += "<Error>" +  true + "</Error>\n";
//        xml += "<IdCliente></IdCliente>\n";
//        xml += "<NombreLegal></NombreLegal>\n";
//        xml += "<Profesion></Profesion>\n";
//        xml += "<NumCredito></NumCredito>\n";
//        xml += "<NCuotasPendientes></NCuotasPendientes>\n";
//        xml += "<NCuotasVencidas></NCuotasVencidas>\n";
//        xml += "<NDiasVencimiento></NDiasVencimiento>\n";
//        xml += "<MontoPrestamo></MontoPrestamo>\n";
//        xml += "<MontoPagado></MontoPagado>\n";
//        xml += "<MontoVencido></MontoVencido>\n";
//        xml += "<SaldoActual></SaldoActual>\n";
//        xml += "<CuotaVencida></CuotaVencida>\n";
//        xml += "<FechaProximoPago></FechaProximoPago>\n";
//        
//        // Se obtiene la lista de telefonos ingresados en el sistema asociados al socio
//        xml += "<Telefonos>\n";
//        for (int i = 1; i <= 5; i++) {
//            xml += "<Telefono" + i + ">0</Telefono" + i + ">\n";
//        }
//        xml += "</Telefonos>\n";
//        xml += "</Prestamo>"; 
//    }
//    
//    /**
//     * @return the selCreditoSWeb
//     */
//    public Credito getSelCreditoSWeb() {
//        return selCreditoSWeb;
//    }
//
//    /**
//     * @param selCreditoSWeb the selCreditoSWeb to set
//     */
//    public void setSelCreditoSWeb(Credito selCreditoSWeb) {
//        this.selCreditoSWeb = selCreditoSWeb;
//    }
//}