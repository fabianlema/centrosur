/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author wilso
 */
public class MailScheduler {
    
    public MailScheduler(){
        try {
         // specify the job' s details..
         JobDetail job = JobBuilder.newJob(ResetMailJobs.class)
                                   .withIdentity("resetMailJobs")
                                   .build();

         // specify the running period of the job
         Trigger trigger = TriggerBuilder.newTrigger()
                                         .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                                                                            .withIntervalInMinutes(2)
                                                                            .repeatForever())
                                          .build();
         //schedule the job
         Scheduler scheduler = new StdSchedulerFactory().getScheduler();
         scheduler.start();
         scheduler.scheduleJob(job, trigger);

      } catch (SchedulerException e) {
         e.printStackTrace();
      }
    }
            
            
    
}
