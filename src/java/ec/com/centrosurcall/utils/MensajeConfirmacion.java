/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Scope;

/**
 *
 * @author TaurusTech
 */

public class MensajeConfirmacion {
        
    private String mensajeConfirmacion;
    private String mensajeCorrecto;
    protected String mensajeError;    
    private List<ClaseGeneral> lstDetalleErrores ;
     private List<ClaseGeneral> listaErrores ;
    
       
    public String getMensajeConfirmacion() {
        return mensajeConfirmacion;
    }

    public void setMensajeConfirmacion(String mensajeConfirmacion) {
        this.mensajeConfirmacion = mensajeConfirmacion;
    }

    public String getMensajeCorrecto() {
        return mensajeCorrecto;
    }

    public void setMensajeCorrecto(String mensajeCorrecto) {
        this.mensajeCorrecto = mensajeCorrecto;
    }

    public String getMensajeError() {
        return mensajeError;
    }

    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    public List<ClaseGeneral> getLstDetalleErrores() {
        return lstDetalleErrores;
    }

    public void setLstDetalleErrores(List<ClaseGeneral> lstDetalleErrores) {
        this.lstDetalleErrores = lstDetalleErrores;
    }

    public List<ClaseGeneral> getListaErrores() {
        return listaErrores;
    }

    public void setListaErrores(List<ClaseGeneral> listaErrores) {
        this.listaErrores = listaErrores;
    }
    
    
}
