/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

//import java.util.AbstractList;
import ec.com.centrosurcall.datos.conexion.Consultas;
import ec.com.centrosurcall.datos.conexion.ParametrosConsulta;
import ec.com.centrosurcall.negocio.controladores.CtrSession;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author TaurusTech
 */
public class FuncionesComunes {

    private Consultas consulta = new Consultas();
    private ProveedorMensajes msgs = new ProveedorMensajes();
    protected static CtrSession parametroSession;

    public FuncionesComunes() {
        FuncionesComunes.parametroSession = (CtrSession) FacesContext.getCurrentInstance().getELContext().getELResolver().getValue(FacesContext.getCurrentInstance().getELContext(),
                null, "ctrSession");
    }

    public FuncionesComunes(String param1) {
    }

    public SelectItem[] cargarListadoDias() {
        SelectItem[] selectDias = new SelectItem[7];
        try {
            selectDias[0] = new SelectItem("LUN", msgs.getValue("dLunes"));
            selectDias[1] = new SelectItem("MAR", msgs.getValue("dMartes"));
            selectDias[2] = new SelectItem("MIE", msgs.getValue("dMiercoles"));
            selectDias[3] = new SelectItem("JUE", msgs.getValue("dJueves"));
            selectDias[4] = new SelectItem("VIE", msgs.getValue("dViernes"));
            selectDias[5] = new SelectItem("SAB", msgs.getValue("dSabado"));
            selectDias[6] = new SelectItem("DOM", msgs.getValue("dDomingo"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return selectDias;
    }

    /**
     *
     * @param select
     * @param from
     * @param where
     * @param parametros
     * @param tipo si es Sql o Hql
     * @param camposBusqueda son los campos contra los que se haran los likes,
     * separados con una
     * @
     * @param cadenaBusqueda la cadena que se buscara
     * @return
     */
    public List accionBuscar(String select, String from, String where, List<ParametrosConsulta> parametros, String tipo, String camposBusqueda, String cadenaBusqueda, String ordenar) {
        List listaResultante = new ArrayList();
        try {
            //            cadenaBusqueda = cadenaBusqueda.toUpperCase();
            if (!camposBusqueda.equals("") && !cadenaBusqueda.equals("")) {
                String queryBusqueda = this.funArmarQuery(camposBusqueda, cadenaBusqueda);
                listaResultante = actualizaListaDatos(select, from, where, parametros, queryBusqueda, ordenar, tipo);
//                ordenar();
            } else {
                listaResultante = actualizaListaDatos(select, from, where, parametros, null, ordenar, tipo);
            }
            System.out.println("-----------fin --buscar where sql >>>>>>");
            //actualizaListaDatos();
        } catch (Exception e) {

            System.out.println("ERROR: CtrlMantenimiento metodo: buscar() -->>" + e.getMessage());
        }
        return listaResultante;
    }

    private String funArmarQuery(String camposBusqueda, String busqueda) {

        String queryBusqueda = "";
        try {

            String[] datosCampos = funRegresaTokens(camposBusqueda, "@");

//            String busqueda = this.getCadenaBusqueda();
            busqueda = busqueda.trim().toUpperCase();

            //Remplaso los espacios en blanco por | para que luego me sriva de token
            busqueda = busqueda.replace(" ", "|");
            //En tokens las palabras que busca
            String[] datosBusqueda = funRegresaTokens(busqueda, "|");


            if (datosBusqueda == null || datosCampos == null) {
                return "";
            }

            for (int i = 0; i < datosCampos.length; i++) {
                for (int j = 0; j < datosBusqueda.length; j++) {

                    String campo = datosCampos[i];
                    System.out.println("campo " + campo);
                    String busca = datosBusqueda[j];
                    System.out.println("busca " + busca);

                    if (datosCampos[i] != null || !datosCampos[i].equals("")) {
                        if (j == 0 && i == 0) {
                            queryBusqueda = queryBusqueda + "   " + datosCampos[i] + " like '%" + datosBusqueda[j] + "%'";
                            queryBusqueda = queryBusqueda + " OR  " + datosCampos[i] + " like '" + datosBusqueda[j] + "%'";
                        } else {
                            queryBusqueda = queryBusqueda + " OR " + datosCampos[i] + " like '%" + datosBusqueda[j] + "%'";
                            queryBusqueda = queryBusqueda + " OR " + datosCampos[i] + " like '" + datosBusqueda[j] + "%'  ";
                        }
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            queryBusqueda = "";
        }
        if (!queryBusqueda.trim().equals("")) {
            queryBusqueda = " (" + queryBusqueda + ") ";

        }
        return queryBusqueda;

    }

    private List actualizaListaDatos(String select, String from, String where, List<ParametrosConsulta> parametros, String queryBusqueda, String ordenar, String tipo) {
        //utils.presentarError("");
        try {
            List listaDeDatos = new ArrayList();
            String whereConcatenado = "";

//            if (getNombreObjetoModelado() != null && getNombreObjetoModelado().length() > 0) {
            //listaDeDatos = new accesoDatos.acceso.Consultaas().getListHql(null, "   " + getNombreObjetoModelado() + " where " + query, null, null);
            if (queryBusqueda == null || queryBusqueda.trim().equals("")) {
                whereConcatenado = where;
            } else {
                if (where == null || where.trim().equals("")) {
                    whereConcatenado = queryBusqueda;
                } else {
                    whereConcatenado = where + " AND " + queryBusqueda;
                }
            }
            whereConcatenado += " order by " + ordenar;
            if (tipo.equals("Hql")) {
                listaDeDatos = consulta.getListHql(select, from, whereConcatenado, parametros);
            } else {
                listaDeDatos = consulta.getListSql(select, from, whereConcatenado, parametros);
            }

//            }
            return listaDeDatos;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * JRPT 19/09/2011 Regresa el una matriz de strings ya separada por el token
     * que ingrese
     *
     * @param cadena donde se implementran los token
     * @param token el separador de la cadena
     * @return lista de palabras que ha encontrado
     */
    public static String[] funRegresaTokens(String cadena, String token) {
        String datos[];
        try {
            StringTokenizer tokens = new StringTokenizer(cadena, token);
            int nDatos = tokens.countTokens();
            datos = new String[nDatos];
            int i = 0;
            while (tokens.hasMoreTokens()) {
                String str = tokens.nextToken();
                datos[i] = (str);
                i++;

            }

        } catch (Exception e) {
            datos = null;
        }
        return datos;
    }

    // <editor-fold defaultstate="collapsed" desc="Función para checkear todas la pagina">
    public List seleccionarTodos(Boolean seleccion, int pagina, int numFilas, List listaDeDatos) {
        if (seleccion) {
            seleccion = false;
        } else {
            seleccion = true;
        }
        if (pagina != 0) {
            pagina = pagina - 1;
        }
        int start = pagina * numFilas;
        int stop = (pagina * numFilas) + numFilas;
        if (stop > listaDeDatos.size()) {
            stop = listaDeDatos.size();
        }
        String msnError = " PSRA--> seleccionarTodos ";
        for (int i = start; i < stop; i++) {
            try {
                Object objeto = listaDeDatos.get(i);
                Class oClase = objeto.getClass();
                Method metodo = oClase.getMethod("setSeleccionado", boolean.class);
                metodo.invoke(objeto, seleccion);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(FuncionesComunes.class.getName()).log(Level.SEVERE, msnError, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(FuncionesComunes.class.getName()).log(Level.SEVERE, msnError, ex);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(FuncionesComunes.class.getName()).log(Level.SEVERE, msnError, ex);
            }
        }
        return listaDeDatos;
    }
    // </editor-fold>

    /**
     * Función que elimina acentos y caracteres especiales de una cadena de
     * texto.
     *
     * @param input
     * @return cadena de texto limpia de acentos y caracteres especiales.
     */
    public static String quitarCaracteresEspeciales(String input) {
        // Cadena de caracteres original a sustituir.
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        // Cadena de caracteres ASCII que reemplazarán los originales.
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        String output = input;
        for (int i = 0; i < original.length(); i++) {
            // Reemplazamos los caracteres especiales.
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }//for i
        return output;
    }//remove1




    public static String redondear(String valor) {
        try {
            //convierto a double el valor que llega
            String signo = "";
            Double valorDoble = Double.parseDouble(valor);
            if (valorDoble < 0) {
                signo = "-";
                valorDoble = valorDoble * -1;
            }
            //tomo la parte entera
            int entero = valorDoble.intValue();
            //tomo la parte decimal
            Double auxRed = valorDoble - entero;
            //la parte decimal la convierto en entero y redondeo para luego concatenar
            Long aux = java.lang.Math.round(auxRed * 100);
            Integer decimal = aux.intValue();
            //retorno el valor concatenado
            return signo + Integer.toString(entero) + tomaDecimal(decimal);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("empty-statement")
    static String tomaDecimal(int decimal) {
        String ceros = "";
        if (decimal < 10) {
            ceros = ".0" + Integer.toString(decimal);
        } else if (decimal < 100) {
            ceros = "." + Integer.toString(decimal);
        }
        return ceros;
    }

    public static String redondearU(String valor) {
        try {
            //convierto a double el valor que llega
            double valorDoble = Double.parseDouble(valor);
            //tomo la parte entera
            int entero = (int) valorDoble;
            //tomo la parte decimal
            double auxRed = valorDoble - entero;
            //la parte decimal la convierto en entero y redondeo para luego concatenar
            double decimal = (double) java.lang.Math.round(auxRed * 100);
            //retorno el valor concatenado
            return Integer.toString(entero) + tomaDecimal(decimal);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            // return "0.0";
        }
        return "0.0";
    }

    @SuppressWarnings("empty-statement")
    static String tomaDecimal(double decimal) {
        String ceros = "";
        if (decimal < 10) {
            ceros = ".0" + String.valueOf(decimal);
        } else if (decimal < 100) {
            ceros = "." + String.valueOf(decimal);
        }
        return ceros;
    }

    public static Object funLlamarEventoItem(String nombreClase, String nombreEvento, Object[] mainArgs, Class claseArgumento) {
        Object objRetorna = null;

        try {
            Object object = FacesUtils.getManagedBean(nombreClase);
            Class clase = object.getClass();
            Class[] argTypes = new Class[mainArgs.length];
            for (int i = 0; i < argTypes.length; i++) {
                argTypes[i] = claseArgumento;
            }
            Method metodo = clase.getMethod(nombreEvento, argTypes);
            objRetorna = metodo.invoke(object, mainArgs);

        } catch (Exception e) {
            System.out.println("Error: FuncionesGenericas metodo: proLlamarEvento(String nombreEvento , String nombreClase) -- " + e.getMessage());
            e.printStackTrace();
            return null;
        }

        return objRetorna;
    }

    // <editor-fold defaultstate="collapsed" desc="Funciones para buscar todas las personas">
    public LinkedHashMap<String, String> cargarColumnasPersona() {
        LinkedHashMap<String, String> listaColumnasPersonas = new LinkedHashMap();
        try {
            listaColumnasPersonas.put("id", "id");
            listaColumnasPersonas.put("nombre", "nombre");
            return listaColumnasPersonas;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
//    public List<SraPersonas> cargarGrillaPersonas(String valorBuscar, String tipo) {
//        List<SraPersonas> lstPersonas = new ArrayList<SraPersonas>();
//        try {
//            BigDecimal totalregistros = pDao.numMaxPersonasPaginada(valorBuscar);
//            int total = 0;
//            if (!totalregistros.equals(new BigDecimal("0"))) {
//                BigDecimal division = totalregistros.divide(new BigDecimal(parametroSession.getMaximoFila()));
//                String totalString = division.toString();
//                int esDecimal = totalString.indexOf(".");
//                total = division.intValue();
//                if (esDecimal != -1) {
//                    total++;
//                }
//            }
//            parametroSession.setNumTotalPaginas(total);
//            parametroSession.setNumPaginas(1);
//            if (tipo.equals("todas")) {
//                lstPersonas = pDao.lstPersonasPaginada(valorBuscar, parametroSession.getMaximoFila(), parametroSession.getNumPaginas());
//            } else if (tipo.equals("accesos")) {
//                lstPersonas = pDao.lstPersonasPaginadaAcceso(valorBuscar, parametroSession.getMaximoFila(), parametroSession.getNumPaginas());
//            }
//            return lstPersonas;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//
//    }
// </editor-fold>
}
