/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;
import ec.com.centrosurcall.datos.DAO.EventoDAO;
import ec.com.centrosurcall.datos.modelo.Evento;
import javax.el.ELContext;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author carlosguaman65
 */


@FacesConverter("eventosConverter")
public class EventosConverter implements Converter {
    
    private EventoDAO eventoParser;

    public Object getAsObject(FacesContext facesContext, UIComponent component, String s) {
        for (Evento evento : getEventosParser(facesContext).getEventosList()) {
            if (evento.getDescripcion().equals(s)) {
             //   System.out.println("String - objeto "  +evento.getDescripcion());
                return evento;
            }
        }
        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object o) {
        if (o == null) return null;
      //  System.out.println(" objeto -  String"  + ((Evento) o).getDescripcion());
        return ((Evento) o).getDescripcion();
    }

    private EventoDAO getEventosParser(FacesContext facesContext) {
        if (eventoParser == null) {
            ELContext elContext = facesContext.getELContext();
            eventoParser = (EventoDAO) elContext.getELResolver().getValue(elContext, null, "eventoDAO");
        }
        return eventoParser;
    }
}
