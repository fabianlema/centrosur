/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import ec.com.centrosurcall.datos.DAO.ParametrosDAO;
import ec.com.centrosurcall.datos.modelo.Parametro;
import javax.naming.directory.Attributes.*;
import javax.naming.directory.*;
import javax.naming.*;
import java.util.*;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;


/**
 *
 * @author Taurus Tech
 */
public class Login {

        ParametrosDAO paramDAO = new ParametrosDAO();
        
        Parametro svr = paramDAO.getParametroByAtributo("SERVERLDAP");
        Parametro puerto = paramDAO.getParametroByAtributo("PUERTO");
        Parametro dc1 = paramDAO.getParametroByAtributo("DC1");
        Parametro dc2 = paramDAO.getParametroByAtributo("DC2");
        Parametro dc3 = paramDAO.getParametroByAtributo("DC3");
        
        
    public boolean login(String user, String password) {
    try
        {
           if("".equals(password)){
               return false;
           }
            
            String ldapURL = "ldap://"+svr.getValor()+"/DC="+dc1.getValor()+", DC="+dc2.getValor()+", DC="+dc3.getValor()+"";
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, ldapURL);
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, ""+dc1.getValor()+"\\"+user+""); //we have 2 \\ because it's a escape char
            
//             env.put(Context.SECURITY_PRINCIPAL, ""+dc1.getValor()+"@"+user+"");
            env.put(Context.SECURITY_CREDENTIALS, password);

            // Create the initial context

            DirContext ctx = new InitialDirContext(env);
            boolean result = ctx != null;
            if(ctx != null)
                ctx.close();
          
            return result;
        }
        catch (Exception e)
        {   

            return false;
             
            
        }
  
   }
}
