/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import ec.com.centrosurcall.datos.DAO.EventoDAO;
import ec.com.centrosurcall.datos.DAO.SubestacionDAO;
import ec.com.centrosurcall.datos.modelo.Evento;
import ec.com.centrosurcall.datos.modelo.Subestacion;
import javax.el.ELContext;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author carlosguaman65
 */
@FacesConverter("subestacionConverter")
public class SubEstacionConverter implements Converter{
    
   
    private SubestacionDAO subestacionParser;

    public Object getAsObject(FacesContext facesContext, UIComponent component, String s) {
        for (Subestacion subestacion : getSubestacionParser(facesContext).getSubestacionesList()) {
            if (subestacion .getDescripcion().equals(s)) {
               //System.out.println("String - objeto "  +subestacion .getDescripcion());
                return subestacion ;
            }
        }
        return null;
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object o) {
        if (o == null) return null;
      //  System.out.println(" objeto -  String"  + ((Evento) o).getDescripcion());
        return ((Subestacion) o).getDescripcion();
    }

    private SubestacionDAO getSubestacionParser(FacesContext facesContext) {
        if (subestacionParser == null) {
            ELContext elContext = facesContext.getELContext();
            subestacionParser = (SubestacionDAO) elContext.getELResolver().getValue(elContext, null, "subestacionDAO");
        }
        return subestacionParser;
    }
}
