/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.com.centrosurcall.utils;

import ec.com.centrosurcall.datos.DAO.ZonaDAO;
import ec.com.centrosurcall.datos.modelo.Zona;
import javax.el.ELContext;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author wilso
 */

@FacesConverter("zonasConverter")
public class ZonaConverter implements  Converter  {
    
     private ZonaDAO zonaAfectadaParser;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
       for (Zona zonaAfectada : getZonasAgectadasParser(context).getZonasAfectadasList()) {
            if (zonaAfectada.getDescripcion().equals(value)) {
             //   System.err.println("Elemento:  "  +zonaAfectada.getDescripcion());
                return zonaAfectada;
            }
        }
        return null; 
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
      if (value == null) return null;
       // System.out.println(" objeto -  String  on zona conveter"  + ((Zona) value).getDescripcion());
        return ((Zona) value).getDescripcion(); 
    }

    
     private ZonaDAO getZonasAgectadasParser(FacesContext facesContext) {
        if (zonaAfectadaParser == null) {
            ELContext elContext = facesContext.getELContext();
            zonaAfectadaParser = (ZonaDAO) elContext.getELResolver().getValue(elContext, null, "zonaDAO");
        }
        return zonaAfectadaParser;
    }
    
}
